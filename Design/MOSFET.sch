<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Dimensions" color="10" fill="1" visible="yes" active="yes"/>
<layer number="115" name="Divisions" color="7" fill="0" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="IMP" color="12" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="Logo" color="10" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="141" name="topSilk" color="14" fill="1" visible="yes" active="yes"/>
<layer number="142" name="Bottom_Silk" color="13" fill="1" visible="yes" active="yes"/>
<layer number="143" name="QR-code" color="6" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="234" name="Logo_b" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="253" name="LEGEND" color="10" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="adafruit">
<packages>
<package name="SOT23-W">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Wave soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.3984" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.3984" width="0.1524" layer="21"/>
<wire x1="0.2954" y1="-0.6604" x2="-0.3094" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.032" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TO220V">
<description>&lt;b&gt;TO 200 vertical&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.127" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.175" y="-3.175" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="-3.175" size="1.27" layer="51" ratio="10">2</text>
<text x="1.905" y="-3.175" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MOSFET-N">
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.048" y2="0.254" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="5.08" y="0.635" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-1.27" size="1.27" layer="96">&gt;VALUE</text>
<text x="3.175" y="3.175" size="0.8128" layer="93">D</text>
<text x="3.175" y="-3.81" size="0.8128" layer="93">S</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="93">G</text>
<pin name="G" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.762"/>
<vertex x="2.032" y="-0.762"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOSFET-N" prefix="Q" uservalue="yes">
<description>&lt;b&gt;N-Channel Mosfet&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;2N7002E - 60V 260mA SOT23 [Digikey: 2N7002ET1GOSTR-ND] - &lt;b&gt;REEL&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;BSH103 - 30V 850mA SOT23 [Digikey: 568-5013-1-ND]&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="-2.54" y="0"/>
</gates>
<devices>
<device name="WAVE" package="SOT23-W">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="REFLOW" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="GDS_TO220V" package="TO220V">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago-500">
<description>&lt;b&gt;Wago Screw Clamps&lt;/b&gt;&lt;p&gt;
Grid 5.00 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="W237-102">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="-3.491" y1="-2.286" x2="-1.484" y2="-0.279" width="0.254" layer="51"/>
<wire x1="1.488" y1="-2.261" x2="3.469" y2="-0.254" width="0.254" layer="51"/>
<wire x1="-4.989" y1="-5.461" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-5.461" x2="-4.989" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-3.389" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-3.389" y1="-3.073" x2="-1.611" y2="-3.073" width="0.1524" layer="51"/>
<wire x1="-1.611" y1="-3.073" x2="1.615" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="3.393" y1="-3.073" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-4.989" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="-3.073" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.531" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-3.073" x2="3.393" y2="-3.073" width="0.1524" layer="51"/>
<circle x="-2.5" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-2.5" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.5038" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.5038" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<text x="-5.04" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.8462" y="-5.0038" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="KL">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="W237-102" prefix="X" uservalue="yes">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="KL+V" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-102">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-102" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="70K9898" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="heatsink">
<description>&lt;b&gt;Heatsinks&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SK129">
<description>&lt;b&gt;HEATSINK&lt;/b&gt;&lt;p&gt; manufacturer Fischer/distributor Buerklin</description>
<wire x1="13.716" y1="-0.127" x2="13.716" y2="-1.651" width="0.1524" layer="51" curve="286.260205"/>
<wire x1="13.716" y1="-0.127" x2="14.859" y2="0.508" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-1.651" x2="14.859" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="8.509" y1="11.049" x2="9.779" y2="11.049" width="0.1524" layer="21" curve="-180"/>
<wire x1="19.939" y1="11.43" x2="20.955" y2="10.668" width="0.1524" layer="21" curve="-180"/>
<wire x1="14.224" y1="11.2522" x2="15.5334" y2="10.8858" width="0.1524" layer="21" curve="-167.655909"/>
<wire x1="20.193" y1="4.953" x2="20.7107" y2="3.7887" width="0.1524" layer="21" curve="-179.045338"/>
<wire x1="8.509" y1="11.049" x2="8.509" y2="0" width="0.1524" layer="21"/>
<wire x1="15.5448" y1="10.922" x2="14.5542" y2="7.8994" width="0.1524" layer="21"/>
<wire x1="10.4902" y1="5.6896" x2="9.779" y2="11.0998" width="0.1524" layer="21"/>
<wire x1="16.0782" y1="2.8702" x2="20.193" y2="4.953" width="0.1524" layer="21"/>
<wire x1="11.7856" y1="5.334" x2="14.224" y2="11.2522" width="0.1524" layer="21"/>
<wire x1="15.5448" y1="7.1628" x2="19.9898" y2="11.4808" width="0.1524" layer="21"/>
<wire x1="15.24" y1="3.7592" x2="20.955" y2="10.6934" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="-1.778" x2="8.509" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="14.859" y1="0.508" x2="20.7264" y2="3.81" width="0.1524" layer="21"/>
<wire x1="10.5014" y1="5.7512" x2="11.811" y2="5.3848" width="0.1524" layer="21" curve="167.65778"/>
<wire x1="14.605" y1="8.001" x2="15.621" y2="7.239" width="0.1524" layer="21" curve="180"/>
<wire x1="15.24" y1="3.7846" x2="16.1346" y2="2.8992" width="0.1524" layer="21" curve="157.962063"/>
<wire x1="-13.716" y1="-1.651" x2="-13.716" y2="-0.127" width="0.1524" layer="51" curve="286.260205"/>
<wire x1="-13.716" y1="-1.651" x2="-14.859" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-9.779" y1="-12.827" x2="-8.509" y2="-12.827" width="0.1524" layer="21" curve="180"/>
<wire x1="-20.955" y1="-12.446" x2="-19.939" y2="-13.208" width="0.1524" layer="21" curve="180"/>
<wire x1="-15.5336" y1="-12.6638" x2="-14.2238" y2="-13.0303" width="0.1524" layer="21" curve="167.651002"/>
<wire x1="-20.7106" y1="-5.5669" x2="-20.1931" y2="-6.7308" width="0.1524" layer="21" curve="179.04501"/>
<wire x1="-8.509" y1="-12.827" x2="-8.509" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-15.5448" y1="-12.7" x2="-14.5542" y2="-9.6774" width="0.1524" layer="21"/>
<wire x1="-10.4902" y1="-7.4676" x2="-9.779" y2="-12.8778" width="0.1524" layer="21"/>
<wire x1="-16.0782" y1="-4.6482" x2="-20.193" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-11.7856" y1="-7.112" x2="-14.224" y2="-13.0302" width="0.1524" layer="21"/>
<wire x1="-15.5448" y1="-8.9408" x2="-19.9898" y2="-13.2588" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-5.5372" x2="-20.955" y2="-12.4714" width="0.1524" layer="21"/>
<wire x1="-14.859" y1="-2.286" x2="-20.7264" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="-7.1628" x2="-10.5014" y2="-7.5292" width="0.1524" layer="21" curve="-167.65778"/>
<wire x1="-15.621" y1="-9.017" x2="-14.605" y2="-9.779" width="0.1524" layer="21" curve="-180"/>
<wire x1="-16.1345" y1="-4.6773" x2="-15.2401" y2="-5.5625" width="0.1524" layer="21" curve="-157.957256"/>
<wire x1="8.509" y1="-12.827" x2="9.779" y2="-12.827" width="0.1524" layer="21" curve="180"/>
<wire x1="19.939" y1="-13.208" x2="20.955" y2="-12.446" width="0.1524" layer="21" curve="180"/>
<wire x1="14.224" y1="-13.0302" x2="15.5334" y2="-12.6638" width="0.1524" layer="21" curve="167.655909"/>
<wire x1="20.193" y1="-6.731" x2="20.7107" y2="-5.5667" width="0.1524" layer="21" curve="179.045338"/>
<wire x1="8.509" y1="-12.827" x2="8.509" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="15.5448" y1="-12.7" x2="14.5542" y2="-9.6774" width="0.1524" layer="21"/>
<wire x1="10.4902" y1="-7.4676" x2="9.779" y2="-12.8778" width="0.1524" layer="21"/>
<wire x1="16.0782" y1="-4.6482" x2="20.193" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="11.7856" y1="-7.112" x2="14.224" y2="-13.0302" width="0.1524" layer="21"/>
<wire x1="15.5448" y1="-8.9408" x2="19.9898" y2="-13.2588" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.5372" x2="20.955" y2="-12.4714" width="0.1524" layer="21"/>
<wire x1="14.859" y1="-2.286" x2="20.7264" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="10.5014" y1="-7.5292" x2="11.811" y2="-7.1628" width="0.1524" layer="21" curve="-167.65778"/>
<wire x1="14.605" y1="-9.779" x2="15.621" y2="-9.017" width="0.1524" layer="21" curve="-180"/>
<wire x1="15.24" y1="-5.5626" x2="16.1344" y2="-4.6774" width="0.1524" layer="21" curve="-157.981945"/>
<wire x1="-13.716" y1="-0.127" x2="-14.859" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-9.779" y1="11.049" x2="-8.509" y2="11.049" width="0.1524" layer="21" curve="-180"/>
<wire x1="-20.955" y1="10.668" x2="-19.939" y2="11.43" width="0.1524" layer="21" curve="-180"/>
<wire x1="-15.5336" y1="10.8858" x2="-14.2238" y2="11.2523" width="0.1524" layer="21" curve="-167.651002"/>
<wire x1="-20.7106" y1="3.7889" x2="-20.1931" y2="4.9528" width="0.1524" layer="21" curve="-179.04501"/>
<wire x1="-8.509" y1="11.049" x2="-8.509" y2="0" width="0.1524" layer="21"/>
<wire x1="-15.5448" y1="10.922" x2="-14.5542" y2="7.8994" width="0.1524" layer="21"/>
<wire x1="-10.4902" y1="5.6896" x2="-9.779" y2="11.0998" width="0.1524" layer="21"/>
<wire x1="-16.0782" y1="2.8702" x2="-20.193" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-11.7856" y1="5.334" x2="-14.224" y2="11.2522" width="0.1524" layer="21"/>
<wire x1="-15.5448" y1="7.1628" x2="-19.9898" y2="11.4808" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="3.7592" x2="-20.955" y2="10.6934" width="0.1524" layer="21"/>
<wire x1="-14.859" y1="0.508" x2="-20.7264" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="5.3848" x2="-10.5014" y2="5.7512" width="0.1524" layer="21" curve="167.65778"/>
<wire x1="-15.621" y1="7.239" x2="-14.605" y2="8.001" width="0.1524" layer="21" curve="180"/>
<wire x1="-16.1345" y1="2.8993" x2="-15.2399" y2="3.7847" width="0.1524" layer="21" curve="157.986747"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="8.509" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.239" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="0.635" x2="-7.366" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="0.635" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.366" y1="0.635" x2="-7.62" y2="0.127" width="0.1524" layer="21"/>
<wire x1="-7.366" y1="0.635" x2="-7.874" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.127" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.127" x2="-7.874" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="0.635" x2="-8.001" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="0.635" x2="-7.62" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.508" x2="-7.62" y2="0.127" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.508" x2="-7.366" y2="0.635" width="0.1524" layer="21"/>
<pad name="B2,5" x="12.7" y="-0.889" drill="2.54" diameter="5.08"/>
<pad name="A2,5" x="-12.7" y="-0.889" drill="2.54" diameter="5.08"/>
<text x="-5.08" y="-5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="11.43" y="-3.81" size="1.27" layer="48">2,5</text>
<text x="-13.97" y="-3.81" size="1.27" layer="48">2,5 </text>
<rectangle x1="-21.59" y1="-13.97" x2="-8.255" y2="12.065" layer="41"/>
<rectangle x1="8.255" y1="-13.97" x2="21.59" y2="12.065" layer="41"/>
<rectangle x1="-8.255" y1="-1.905" x2="8.255" y2="0" layer="41"/>
<rectangle x1="-21.59" y1="-13.97" x2="-8.255" y2="12.065" layer="43"/>
<rectangle x1="-8.255" y1="-1.905" x2="8.255" y2="0" layer="43"/>
<rectangle x1="8.255" y1="-13.97" x2="21.59" y2="12.065" layer="43"/>
</package>
<package name="D03PA-PAD">
<description>&lt;b&gt;HEATSINK&lt;/b&gt;&lt;p&gt;
distributor Schukat</description>
<wire x1="6.858" y1="5.08" x2="5.588" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.858" y1="5.08" x2="-5.588" y2="5.08" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0.254" x2="6.858" y2="5.08" width="0.1524" layer="21"/>
<wire x1="5.588" y1="0.254" x2="5.588" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-5.588" y1="0.254" x2="-5.588" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.858" y1="0.254" x2="-6.858" y2="5.08" width="0.1524" layer="21"/>
<wire x1="5.334" y1="0" x2="5.588" y2="0.254" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.588" y1="0.254" x2="-5.334" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.858" y1="0.254" x2="-5.334" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="5.334" y1="-1.27" x2="6.858" y2="0.254" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.27" x2="-1.397" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="0" x2="-1.016" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="0" x2="1.016" y2="0" width="0.1524" layer="51"/>
<wire x1="1.016" y1="0" x2="5.334" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.794" x2="2.794" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="-3.302" x2="-0.381" y2="-3.302" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-2.54" x2="-0.381" y2="-3.302" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-2.54" x2="0.381" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-3.302" x2="0.381" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.286" y1="-3.302" x2="0.381" y2="-3.302" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="-2.794" x2="-2.794" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-1.27" x2="5.334" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-1.27" x2="-5.334" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-2.794" x2="-2.286" y2="-3.302" width="0.1524" layer="51" curve="90"/>
<wire x1="2.286" y1="-3.302" x2="2.795" y2="-2.8049" width="0.1524" layer="51" curve="101.307265"/>
<pad name="B" x="0" y="-1.016" drill="1.8034" diameter="2.54"/>
<text x="-7.62" y="0" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="8.89" y="0" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3.683" y1="-1.778" x2="-1.397" y2="-1.27" layer="21"/>
<rectangle x1="1.397" y1="-1.778" x2="3.683" y2="-1.27" layer="21"/>
<rectangle x1="-1.397" y1="-1.778" x2="1.397" y2="-1.27" layer="51"/>
</package>
<package name="FK222">
<description>&lt;b&gt;HEATSINK&lt;/b&gt;&lt;p&gt; manufacturer Fischer/distributor Buerklin</description>
<wire x1="-1.016" y1="13.208" x2="3.048" y2="13.208" width="0.1524" layer="21"/>
<wire x1="11.684" y1="13.208" x2="11.684" y2="12.192" width="0.1524" layer="21"/>
<wire x1="11.684" y1="12.192" x2="8.509" y2="12.192" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="13.208" x2="-1.016" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-13.208" x2="3.048" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="11.684" y1="-13.208" x2="11.684" y2="-12.192" width="0.1524" layer="21"/>
<wire x1="11.684" y1="-12.192" x2="8.509" y2="-12.192" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-13.208" x2="3.048" y2="-12.192" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-13.208" x2="8.509" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="3.048" y1="-12.192" x2="0" y2="-12.192" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-13.208" x2="8.509" y2="-12.192" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-13.208" x2="11.684" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-12.192" x2="3.048" y2="-12.192" width="0.1524" layer="51"/>
<wire x1="0" y1="12.192" x2="0" y2="-12.192" width="0.1524" layer="21"/>
<wire x1="3.048" y1="12.192" x2="3.048" y2="13.208" width="0.1524" layer="21"/>
<wire x1="3.048" y1="12.192" x2="0" y2="12.192" width="0.1524" layer="21"/>
<wire x1="3.048" y1="13.208" x2="8.509" y2="13.208" width="0.1524" layer="51"/>
<wire x1="8.509" y1="12.192" x2="8.509" y2="13.208" width="0.1524" layer="21"/>
<wire x1="8.509" y1="12.192" x2="3.048" y2="12.192" width="0.1524" layer="51"/>
<wire x1="8.509" y1="13.208" x2="11.684" y2="13.208" width="0.1524" layer="21"/>
<pad name="A2,8" x="5.7912" y="-12.7" drill="2.794" diameter="5.08"/>
<pad name="B2,8" x="5.7912" y="12.7" drill="2.794" diameter="5.08"/>
<text x="-1.397" y="-10.668" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-1.397" y="5.588" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-12.192" x2="0" y2="12.192" layer="21"/>
<rectangle x1="3.048" y1="-13.208" x2="8.509" y2="-12.192" layer="51"/>
<rectangle x1="-1.016" y1="-13.208" x2="3.048" y2="-12.192" layer="21"/>
<rectangle x1="8.509" y1="-13.208" x2="11.684" y2="-12.192" layer="21"/>
<rectangle x1="8.509" y1="12.192" x2="11.684" y2="13.208" layer="21"/>
<rectangle x1="3.048" y1="12.192" x2="8.509" y2="13.208" layer="51"/>
<rectangle x1="-1.016" y1="12.192" x2="3.048" y2="13.208" layer="21"/>
<rectangle x1="-1.905" y1="11.43" x2="12.7" y2="13.97" layer="43"/>
<rectangle x1="-1.905" y1="-13.97" x2="0.635" y2="13.97" layer="43"/>
<rectangle x1="-1.905" y1="-13.97" x2="12.7" y2="-11.43" layer="43"/>
<rectangle x1="-1.905" y1="11.43" x2="12.7" y2="13.97" layer="41"/>
<rectangle x1="-1.905" y1="-13.97" x2="0.635" y2="13.97" layer="41"/>
<rectangle x1="-1.905" y1="-13.97" x2="12.7" y2="-11.43" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="KK-2">
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="4.445" y2="5.08" width="0.254" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="4.445" y1="1.27" x2="3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.175" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-3.175" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="5.08" width="0.254" layer="94"/>
<wire x1="-4.445" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<text x="5.715" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="5.715" y="0.635" size="1.778" layer="96">&gt;VALUE</text>
<pin name="K@1" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K@2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="KK-1">
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="4.445" y2="5.08" width="0.254" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="4.445" y1="1.27" x2="3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.175" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-3.175" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="5.08" width="0.254" layer="94"/>
<wire x1="-4.445" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<text x="5.715" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="5.715" y="0.635" size="1.778" layer="96">&gt;VALUE</text>
<pin name="K@1" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SK129-PAD" prefix="KK">
<description>&lt;b&gt;HEATSINK&lt;/b&gt;&lt;p&gt; manufacturer Fischer/distributor Buerklin</description>
<gates>
<gate name="G$1" symbol="KK-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SK129">
<connects>
<connect gate="G$1" pin="K@1" pad="A2,5"/>
<connect gate="G$1" pin="K@2" pad="B2,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D03PA-PAD" prefix="KK">
<description>&lt;b&gt;HEATSINK&lt;/b&gt;&lt;p&gt;
distributor Schukat</description>
<gates>
<gate name="G$1" symbol="KK-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="D03PA-PAD">
<connects>
<connect gate="G$1" pin="K@1" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FK222" prefix="KK">
<description>&lt;b&gt;HEATSINK&lt;/b&gt;&lt;p&gt; manufacturer Fischer/distributor Buerklin</description>
<gates>
<gate name="G$1" symbol="KK-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FK222">
<connects>
<connect gate="G$1" pin="K@1" pad="A2,8"/>
<connect gate="G$1" pin="K@2" pad="B2,8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Jumpers">
<description>&lt;h3&gt;SparkFun Jumpers&lt;/h3&gt;
In this library you'll find jumpers, or other semipermanent means of changing current paths. The least permanent form is the solder jumper. These can be changed by adding, removing, or moving solder. In cases that are less likely to be changed we have jumpers that are connected with traces. These can be cut with a razor, or reconnected with solder. Reference designator JP.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SMT-JUMPER_3_NO_NO-SILK">
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
</package>
<package name="SMT-JUMPER_3_NO_SILK">
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.27" y1="-1.016" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.524" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="0.762" x2="-1.27" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="-0.762" x2="-1.27" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<smd name="1" x="-0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="0.8128" y="0" dx="0.635" dy="1.27" layer="1" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="SMT-JUMPER_3_NO">
<wire x1="-0.635" y1="-1.397" x2="0.635" y2="-1.397" width="1.27" layer="94" curve="180" cap="flat"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<text x="2.54" y="0.381" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="2.54" y="-0.381" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.635" x2="1.27" y2="0.635" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JUMPER-SMT_3_NO" prefix="JP">
<description>&lt;h3&gt;Normally open jumper&lt;/h3&gt;
&lt;p&gt;This jumper has three pads in close proximity to each other. Apply solder to close the connection(s).&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SMT-JUMPER_3_NO" x="0" y="0"/>
</gates>
<devices>
<device name="_NO-SILK" package="SMT-JUMPER_3_NO_NO-SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SILK" package="SMT-JUMPER_3_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ER">
<packages>
<package name="LOGO-9.5MMX4.5MM">
<rectangle x1="0.07111875" y1="-0.00431875" x2="0.17271875" y2="0.00431875" layer="37"/>
<rectangle x1="0.98551875" y1="-0.00431875" x2="1.09728125" y2="0.00431875" layer="37"/>
<rectangle x1="1.50368125" y1="-0.00431875" x2="2.1971" y2="0.00431875" layer="37"/>
<rectangle x1="2.61111875" y1="-0.00431875" x2="3.47471875" y2="0.00431875" layer="37"/>
<rectangle x1="4.0513" y1="-0.00431875" x2="4.75488125" y2="0.00431875" layer="37"/>
<rectangle x1="6.37031875" y1="-0.00431875" x2="6.9469" y2="0.00431875" layer="37"/>
<rectangle x1="7.3279" y1="-0.00431875" x2="8.02131875" y2="0.00431875" layer="37"/>
<rectangle x1="8.56488125" y1="-0.00431875" x2="9.2583" y2="0.00431875" layer="37"/>
<rectangle x1="0.04571875" y1="0.00431875" x2="0.19811875" y2="0.0127" layer="37"/>
<rectangle x1="0.97028125" y1="0.00431875" x2="1.11251875" y2="0.0127" layer="37"/>
<rectangle x1="1.4605" y1="0.00431875" x2="2.24028125" y2="0.0127" layer="37"/>
<rectangle x1="2.59588125" y1="0.00431875" x2="3.5179" y2="0.0127" layer="37"/>
<rectangle x1="4.01828125" y1="0.00431875" x2="4.7879" y2="0.0127" layer="37"/>
<rectangle x1="6.35508125" y1="0.00431875" x2="6.96468125" y2="0.0127" layer="37"/>
<rectangle x1="7.29488125" y1="0.00431875" x2="8.0645" y2="0.0127" layer="37"/>
<rectangle x1="8.5217" y1="0.00431875" x2="9.29131875" y2="0.0127" layer="37"/>
<rectangle x1="0.02031875" y1="0.0127" x2="0.22351875" y2="0.02108125" layer="37"/>
<rectangle x1="0.9525" y1="0.0127" x2="1.1303" y2="0.02108125" layer="37"/>
<rectangle x1="1.4351" y1="0.0127" x2="2.26568125" y2="0.02108125" layer="37"/>
<rectangle x1="2.5781" y1="0.0127" x2="3.5433" y2="0.02108125" layer="37"/>
<rectangle x1="3.98271875" y1="0.0127" x2="4.82091875" y2="0.02108125" layer="37"/>
<rectangle x1="6.3373" y1="0.0127" x2="6.97991875" y2="0.02108125" layer="37"/>
<rectangle x1="7.25931875" y1="0.0127" x2="8.09751875" y2="0.02108125" layer="37"/>
<rectangle x1="8.48868125" y1="0.0127" x2="9.32688125" y2="0.02108125" layer="37"/>
<rectangle x1="0.0127" y1="0.02108125" x2="0.23368125" y2="0.02971875" layer="37"/>
<rectangle x1="0.93471875" y1="0.02108125" x2="1.13791875" y2="0.02971875" layer="37"/>
<rectangle x1="1.4097" y1="0.02108125" x2="2.29108125" y2="0.02971875" layer="37"/>
<rectangle x1="2.56031875" y1="0.02108125" x2="3.5687" y2="0.02971875" layer="37"/>
<rectangle x1="3.96748125" y1="0.02108125" x2="4.84631875" y2="0.02971875" layer="37"/>
<rectangle x1="6.32968125" y1="0.02108125" x2="6.99008125" y2="0.02971875" layer="37"/>
<rectangle x1="7.23391875" y1="0.02108125" x2="8.1153" y2="0.02971875" layer="37"/>
<rectangle x1="8.46328125" y1="0.02108125" x2="9.35228125" y2="0.02971875" layer="37"/>
<rectangle x1="0.0127" y1="0.02971875" x2="0.23368125" y2="0.0381" layer="37"/>
<rectangle x1="0.9271" y1="0.02971875" x2="1.14808125" y2="0.0381" layer="37"/>
<rectangle x1="1.39191875" y1="0.02971875" x2="2.30631875" y2="0.0381" layer="37"/>
<rectangle x1="2.56031875" y1="0.02971875" x2="3.58648125" y2="0.0381" layer="37"/>
<rectangle x1="3.94208125" y1="0.02971875" x2="4.8641" y2="0.0381" layer="37"/>
<rectangle x1="6.31951875" y1="0.02971875" x2="6.9977" y2="0.0381" layer="37"/>
<rectangle x1="7.21868125" y1="0.02971875" x2="8.1407" y2="0.0381" layer="37"/>
<rectangle x1="8.4455" y1="0.02971875" x2="9.36751875" y2="0.0381" layer="37"/>
<rectangle x1="0.0127" y1="0.0381" x2="0.23368125" y2="0.04648125" layer="37"/>
<rectangle x1="0.91948125" y1="0.0381" x2="1.14808125" y2="0.04648125" layer="37"/>
<rectangle x1="1.36651875" y1="0.0381" x2="2.33171875" y2="0.04648125" layer="37"/>
<rectangle x1="2.56031875" y1="0.0381" x2="3.60171875" y2="0.04648125" layer="37"/>
<rectangle x1="3.9243" y1="0.0381" x2="4.88188125" y2="0.04648125" layer="37"/>
<rectangle x1="5.63371875" y1="0.0381" x2="5.7277" y2="0.04648125" layer="37"/>
<rectangle x1="6.31951875" y1="0.0381" x2="6.9977" y2="0.04648125" layer="37"/>
<rectangle x1="7.2009" y1="0.0381" x2="8.15848125" y2="0.04648125" layer="37"/>
<rectangle x1="8.42771875" y1="0.0381" x2="9.3853" y2="0.04648125" layer="37"/>
<rectangle x1="0.0127" y1="0.04648125" x2="0.23368125" y2="0.05511875" layer="37"/>
<rectangle x1="0.90931875" y1="0.04648125" x2="1.14808125" y2="0.05511875" layer="37"/>
<rectangle x1="1.35128125" y1="0.04648125" x2="2.3495" y2="0.05511875" layer="37"/>
<rectangle x1="2.56031875" y1="0.04648125" x2="3.62711875" y2="0.05511875" layer="37"/>
<rectangle x1="3.90651875" y1="0.04648125" x2="4.89711875" y2="0.05511875" layer="37"/>
<rectangle x1="5.6007" y1="0.04648125" x2="5.76071875" y2="0.05511875" layer="37"/>
<rectangle x1="6.31951875" y1="0.04648125" x2="6.9977" y2="0.05511875" layer="37"/>
<rectangle x1="7.1755" y1="0.04648125" x2="8.17371875" y2="0.05511875" layer="37"/>
<rectangle x1="8.41248125" y1="0.04648125" x2="9.40308125" y2="0.05511875" layer="37"/>
<rectangle x1="0.0127" y1="0.05511875" x2="0.23368125" y2="0.0635" layer="37"/>
<rectangle x1="0.9017" y1="0.05511875" x2="1.13791875" y2="0.0635" layer="37"/>
<rectangle x1="1.3335" y1="0.05511875" x2="2.36728125" y2="0.0635" layer="37"/>
<rectangle x1="2.56031875" y1="0.05511875" x2="3.63728125" y2="0.0635" layer="37"/>
<rectangle x1="3.89128125" y1="0.05511875" x2="4.9149" y2="0.0635" layer="37"/>
<rectangle x1="5.58291875" y1="0.05511875" x2="5.7785" y2="0.0635" layer="37"/>
<rectangle x1="6.31951875" y1="0.05511875" x2="6.99008125" y2="0.0635" layer="37"/>
<rectangle x1="7.16788125" y1="0.05511875" x2="8.18388125" y2="0.0635" layer="37"/>
<rectangle x1="8.3947" y1="0.05511875" x2="9.41831875" y2="0.0635" layer="37"/>
<rectangle x1="0.0127" y1="0.0635" x2="0.23368125" y2="0.07188125" layer="37"/>
<rectangle x1="0.89408125" y1="0.0635" x2="1.1303" y2="0.07188125" layer="37"/>
<rectangle x1="1.32588125" y1="0.0635" x2="2.3749" y2="0.07188125" layer="37"/>
<rectangle x1="2.56031875" y1="0.0635" x2="3.65251875" y2="0.07188125" layer="37"/>
<rectangle x1="3.8735" y1="0.0635" x2="4.93268125" y2="0.07188125" layer="37"/>
<rectangle x1="5.5753" y1="0.0635" x2="5.78611875" y2="0.07188125" layer="37"/>
<rectangle x1="6.32968125" y1="0.0635" x2="6.97991875" y2="0.07188125" layer="37"/>
<rectangle x1="7.1501" y1="0.0635" x2="8.19911875" y2="0.07188125" layer="37"/>
<rectangle x1="8.38708125" y1="0.0635" x2="9.4361" y2="0.07188125" layer="37"/>
<rectangle x1="0.0127" y1="0.07188125" x2="0.23368125" y2="0.08051875" layer="37"/>
<rectangle x1="0.88391875" y1="0.07188125" x2="1.12268125" y2="0.08051875" layer="37"/>
<rectangle x1="1.3081" y1="0.07188125" x2="2.39268125" y2="0.08051875" layer="37"/>
<rectangle x1="2.56031875" y1="0.07188125" x2="3.66268125" y2="0.08051875" layer="37"/>
<rectangle x1="3.86588125" y1="0.07188125" x2="4.9403" y2="0.08051875" layer="37"/>
<rectangle x1="5.56768125" y1="0.07188125" x2="5.78611875" y2="0.08051875" layer="37"/>
<rectangle x1="6.34491875" y1="0.07188125" x2="6.9723" y2="0.08051875" layer="37"/>
<rectangle x1="7.14248125" y1="0.07188125" x2="8.20928125" y2="0.08051875" layer="37"/>
<rectangle x1="8.3693" y1="0.07188125" x2="9.44371875" y2="0.08051875" layer="37"/>
<rectangle x1="0.0127" y1="0.08051875" x2="0.23368125" y2="0.0889" layer="37"/>
<rectangle x1="0.86868125" y1="0.08051875" x2="1.11251875" y2="0.0889" layer="37"/>
<rectangle x1="1.30048125" y1="0.08051875" x2="2.4003" y2="0.0889" layer="37"/>
<rectangle x1="2.56031875" y1="0.08051875" x2="3.67791875" y2="0.0889" layer="37"/>
<rectangle x1="3.85571875" y1="0.08051875" x2="4.95808125" y2="0.0889" layer="37"/>
<rectangle x1="5.56768125" y1="0.08051875" x2="5.78611875" y2="0.0889" layer="37"/>
<rectangle x1="6.3627" y1="0.08051875" x2="6.95451875" y2="0.0889" layer="37"/>
<rectangle x1="7.1247" y1="0.08051875" x2="8.2169" y2="0.0889" layer="37"/>
<rectangle x1="8.36168125" y1="0.08051875" x2="9.45388125" y2="0.0889" layer="37"/>
<rectangle x1="0.0127" y1="0.0889" x2="0.23368125" y2="0.09728125" layer="37"/>
<rectangle x1="0.85851875" y1="0.0889" x2="1.1049" y2="0.09728125" layer="37"/>
<rectangle x1="1.29031875" y1="0.0889" x2="2.40791875" y2="0.09728125" layer="37"/>
<rectangle x1="2.56031875" y1="0.0889" x2="3.68808125" y2="0.09728125" layer="37"/>
<rectangle x1="3.8481" y1="0.0889" x2="4.95808125" y2="0.09728125" layer="37"/>
<rectangle x1="5.56768125" y1="0.0889" x2="5.78611875" y2="0.09728125" layer="37"/>
<rectangle x1="6.3881" y1="0.0889" x2="6.9215" y2="0.09728125" layer="37"/>
<rectangle x1="7.11708125" y1="0.0889" x2="8.22451875" y2="0.09728125" layer="37"/>
<rectangle x1="8.35151875" y1="0.0889" x2="9.4615" y2="0.09728125" layer="37"/>
<rectangle x1="0.0127" y1="0.09728125" x2="0.23368125" y2="0.10591875" layer="37"/>
<rectangle x1="0.8509" y1="0.09728125" x2="1.09728125" y2="0.10591875" layer="37"/>
<rectangle x1="1.2827" y1="0.09728125" x2="1.55448125" y2="0.10591875" layer="37"/>
<rectangle x1="2.1463" y1="0.09728125" x2="2.41808125" y2="0.10591875" layer="37"/>
<rectangle x1="2.56031875" y1="0.09728125" x2="2.7813" y2="0.10591875" layer="37"/>
<rectangle x1="3.42391875" y1="0.09728125" x2="3.68808125" y2="0.10591875" layer="37"/>
<rectangle x1="3.84048125" y1="0.09728125" x2="4.1021" y2="0.10591875" layer="37"/>
<rectangle x1="4.70408125" y1="0.09728125" x2="4.9657" y2="0.10591875" layer="37"/>
<rectangle x1="5.56768125" y1="0.09728125" x2="5.78611875" y2="0.10591875" layer="37"/>
<rectangle x1="6.54811875" y1="0.09728125" x2="6.7691" y2="0.10591875" layer="37"/>
<rectangle x1="7.11708125" y1="0.09728125" x2="7.3787" y2="0.10591875" layer="37"/>
<rectangle x1="7.98068125" y1="0.09728125" x2="8.23468125" y2="0.10591875" layer="37"/>
<rectangle x1="8.3439" y1="0.09728125" x2="8.60551875" y2="0.10591875" layer="37"/>
<rectangle x1="9.2075" y1="0.09728125" x2="9.46911875" y2="0.10591875" layer="37"/>
<rectangle x1="0.0127" y1="0.10591875" x2="0.23368125" y2="0.1143" layer="37"/>
<rectangle x1="0.84328125" y1="0.10591875" x2="1.08711875" y2="0.1143" layer="37"/>
<rectangle x1="1.2827" y1="0.10591875" x2="1.52908125" y2="0.1143" layer="37"/>
<rectangle x1="2.16408125" y1="0.10591875" x2="2.41808125" y2="0.1143" layer="37"/>
<rectangle x1="2.56031875" y1="0.10591875" x2="2.7813" y2="0.1143" layer="37"/>
<rectangle x1="3.44931875" y1="0.10591875" x2="3.6957" y2="0.1143" layer="37"/>
<rectangle x1="3.84048125" y1="0.10591875" x2="4.08431875" y2="0.1143" layer="37"/>
<rectangle x1="4.71931875" y1="0.10591875" x2="4.9657" y2="0.1143" layer="37"/>
<rectangle x1="5.56768125" y1="0.10591875" x2="5.78611875" y2="0.1143" layer="37"/>
<rectangle x1="6.54811875" y1="0.10591875" x2="6.7691" y2="0.1143" layer="37"/>
<rectangle x1="7.10691875" y1="0.10591875" x2="7.36091875" y2="0.1143" layer="37"/>
<rectangle x1="7.99591875" y1="0.10591875" x2="8.2423" y2="0.1143" layer="37"/>
<rectangle x1="8.3439" y1="0.10591875" x2="8.59028125" y2="0.1143" layer="37"/>
<rectangle x1="9.22528125" y1="0.10591875" x2="9.46911875" y2="0.1143" layer="37"/>
<rectangle x1="0.0127" y1="0.1143" x2="0.23368125" y2="0.12268125" layer="37"/>
<rectangle x1="0.83311875" y1="0.1143" x2="1.07188125" y2="0.12268125" layer="37"/>
<rectangle x1="1.2827" y1="0.1143" x2="1.5113" y2="0.12268125" layer="37"/>
<rectangle x1="2.18948125" y1="0.1143" x2="2.41808125" y2="0.12268125" layer="37"/>
<rectangle x1="2.56031875" y1="0.1143" x2="2.7813" y2="0.12268125" layer="37"/>
<rectangle x1="3.4671" y1="0.1143" x2="3.6957" y2="0.12268125" layer="37"/>
<rectangle x1="3.84048125" y1="0.1143" x2="4.06908125" y2="0.12268125" layer="37"/>
<rectangle x1="4.7371" y1="0.1143" x2="4.97331875" y2="0.12268125" layer="37"/>
<rectangle x1="5.56768125" y1="0.1143" x2="5.78611875" y2="0.12268125" layer="37"/>
<rectangle x1="6.54811875" y1="0.1143" x2="6.7691" y2="0.12268125" layer="37"/>
<rectangle x1="7.10691875" y1="0.1143" x2="7.33551875" y2="0.12268125" layer="37"/>
<rectangle x1="8.00608125" y1="0.1143" x2="8.2423" y2="0.12268125" layer="37"/>
<rectangle x1="8.33628125" y1="0.1143" x2="8.5725" y2="0.12268125" layer="37"/>
<rectangle x1="9.24051875" y1="0.1143" x2="9.47928125" y2="0.12268125" layer="37"/>
<rectangle x1="0.0127" y1="0.12268125" x2="0.23368125" y2="0.13131875" layer="37"/>
<rectangle x1="0.8255" y1="0.12268125" x2="1.06171875" y2="0.13131875" layer="37"/>
<rectangle x1="1.2827" y1="0.12268125" x2="1.50368125" y2="0.13131875" layer="37"/>
<rectangle x1="2.1971" y1="0.12268125" x2="2.41808125" y2="0.13131875" layer="37"/>
<rectangle x1="2.56031875" y1="0.12268125" x2="2.7813" y2="0.13131875" layer="37"/>
<rectangle x1="3.47471875" y1="0.12268125" x2="3.6957" y2="0.13131875" layer="37"/>
<rectangle x1="3.84048125" y1="0.12268125" x2="4.05891875" y2="0.13131875" layer="37"/>
<rectangle x1="4.75488125" y1="0.12268125" x2="4.97331875" y2="0.13131875" layer="37"/>
<rectangle x1="5.56768125" y1="0.12268125" x2="5.78611875" y2="0.13131875" layer="37"/>
<rectangle x1="6.54811875" y1="0.12268125" x2="6.7691" y2="0.13131875" layer="37"/>
<rectangle x1="7.10691875" y1="0.12268125" x2="7.3279" y2="0.13131875" layer="37"/>
<rectangle x1="8.0137" y1="0.12268125" x2="8.2423" y2="0.13131875" layer="37"/>
<rectangle x1="8.33628125" y1="0.12268125" x2="8.56488125" y2="0.13131875" layer="37"/>
<rectangle x1="9.25068125" y1="0.12268125" x2="9.47928125" y2="0.13131875" layer="37"/>
<rectangle x1="0.0127" y1="0.13131875" x2="0.23368125" y2="0.1397" layer="37"/>
<rectangle x1="0.81788125" y1="0.13131875" x2="1.0541" y2="0.1397" layer="37"/>
<rectangle x1="1.2827" y1="0.13131875" x2="1.50368125" y2="0.1397" layer="37"/>
<rectangle x1="2.1971" y1="0.13131875" x2="2.41808125" y2="0.1397" layer="37"/>
<rectangle x1="2.56031875" y1="0.13131875" x2="2.7813" y2="0.1397" layer="37"/>
<rectangle x1="3.47471875" y1="0.13131875" x2="3.6957" y2="0.1397" layer="37"/>
<rectangle x1="3.84048125" y1="0.13131875" x2="4.05891875" y2="0.1397" layer="37"/>
<rectangle x1="4.75488125" y1="0.13131875" x2="4.97331875" y2="0.1397" layer="37"/>
<rectangle x1="5.56768125" y1="0.13131875" x2="5.78611875" y2="0.1397" layer="37"/>
<rectangle x1="6.54811875" y1="0.13131875" x2="6.7691" y2="0.1397" layer="37"/>
<rectangle x1="7.10691875" y1="0.13131875" x2="7.3279" y2="0.1397" layer="37"/>
<rectangle x1="8.02131875" y1="0.13131875" x2="8.2423" y2="0.1397" layer="37"/>
<rectangle x1="8.33628125" y1="0.13131875" x2="8.56488125" y2="0.1397" layer="37"/>
<rectangle x1="9.25068125" y1="0.13131875" x2="9.47928125" y2="0.1397" layer="37"/>
<rectangle x1="0.0127" y1="0.1397" x2="0.23368125" y2="0.14808125" layer="37"/>
<rectangle x1="0.8001" y1="0.1397" x2="1.04648125" y2="0.14808125" layer="37"/>
<rectangle x1="1.2827" y1="0.1397" x2="1.50368125" y2="0.14808125" layer="37"/>
<rectangle x1="2.1971" y1="0.1397" x2="2.41808125" y2="0.14808125" layer="37"/>
<rectangle x1="2.56031875" y1="0.1397" x2="2.7813" y2="0.14808125" layer="37"/>
<rectangle x1="3.47471875" y1="0.1397" x2="3.6957" y2="0.14808125" layer="37"/>
<rectangle x1="3.84048125" y1="0.1397" x2="4.05891875" y2="0.14808125" layer="37"/>
<rectangle x1="4.75488125" y1="0.1397" x2="4.97331875" y2="0.14808125" layer="37"/>
<rectangle x1="5.56768125" y1="0.1397" x2="5.78611875" y2="0.14808125" layer="37"/>
<rectangle x1="6.54811875" y1="0.1397" x2="6.7691" y2="0.14808125" layer="37"/>
<rectangle x1="7.10691875" y1="0.1397" x2="7.3279" y2="0.14808125" layer="37"/>
<rectangle x1="8.02131875" y1="0.1397" x2="8.2423" y2="0.14808125" layer="37"/>
<rectangle x1="8.33628125" y1="0.1397" x2="8.55471875" y2="0.14808125" layer="37"/>
<rectangle x1="9.25068125" y1="0.1397" x2="9.47928125" y2="0.14808125" layer="37"/>
<rectangle x1="0.0127" y1="0.14808125" x2="0.23368125" y2="0.15671875" layer="37"/>
<rectangle x1="0.79248125" y1="0.14808125" x2="1.03631875" y2="0.15671875" layer="37"/>
<rectangle x1="1.2827" y1="0.14808125" x2="1.50368125" y2="0.15671875" layer="37"/>
<rectangle x1="2.1971" y1="0.14808125" x2="2.41808125" y2="0.15671875" layer="37"/>
<rectangle x1="2.56031875" y1="0.14808125" x2="2.7813" y2="0.15671875" layer="37"/>
<rectangle x1="3.47471875" y1="0.14808125" x2="3.6957" y2="0.15671875" layer="37"/>
<rectangle x1="3.84048125" y1="0.14808125" x2="4.05891875" y2="0.15671875" layer="37"/>
<rectangle x1="4.75488125" y1="0.14808125" x2="4.97331875" y2="0.15671875" layer="37"/>
<rectangle x1="5.56768125" y1="0.14808125" x2="5.78611875" y2="0.15671875" layer="37"/>
<rectangle x1="6.54811875" y1="0.14808125" x2="6.7691" y2="0.15671875" layer="37"/>
<rectangle x1="7.10691875" y1="0.14808125" x2="7.3279" y2="0.15671875" layer="37"/>
<rectangle x1="8.03148125" y1="0.14808125" x2="8.2423" y2="0.15671875" layer="37"/>
<rectangle x1="8.33628125" y1="0.14808125" x2="8.55471875" y2="0.15671875" layer="37"/>
<rectangle x1="9.25068125" y1="0.14808125" x2="9.47928125" y2="0.15671875" layer="37"/>
<rectangle x1="0.0127" y1="0.15671875" x2="0.23368125" y2="0.1651" layer="37"/>
<rectangle x1="0.78231875" y1="0.15671875" x2="1.0287" y2="0.1651" layer="37"/>
<rectangle x1="1.2827" y1="0.15671875" x2="1.50368125" y2="0.1651" layer="37"/>
<rectangle x1="2.1971" y1="0.15671875" x2="2.41808125" y2="0.1651" layer="37"/>
<rectangle x1="2.56031875" y1="0.15671875" x2="2.7813" y2="0.1651" layer="37"/>
<rectangle x1="3.47471875" y1="0.15671875" x2="3.6957" y2="0.1651" layer="37"/>
<rectangle x1="3.84048125" y1="0.15671875" x2="4.05891875" y2="0.1651" layer="37"/>
<rectangle x1="4.75488125" y1="0.15671875" x2="4.97331875" y2="0.1651" layer="37"/>
<rectangle x1="5.56768125" y1="0.15671875" x2="5.78611875" y2="0.1651" layer="37"/>
<rectangle x1="6.54811875" y1="0.15671875" x2="6.7691" y2="0.1651" layer="37"/>
<rectangle x1="7.10691875" y1="0.15671875" x2="7.3279" y2="0.1651" layer="37"/>
<rectangle x1="8.03148125" y1="0.15671875" x2="8.2423" y2="0.1651" layer="37"/>
<rectangle x1="8.3439" y1="0.15671875" x2="8.5471" y2="0.1651" layer="37"/>
<rectangle x1="9.25068125" y1="0.15671875" x2="9.47928125" y2="0.1651" layer="37"/>
<rectangle x1="0.0127" y1="0.1651" x2="0.23368125" y2="0.17348125" layer="37"/>
<rectangle x1="0.7747" y1="0.1651" x2="1.02108125" y2="0.17348125" layer="37"/>
<rectangle x1="1.2827" y1="0.1651" x2="1.50368125" y2="0.17348125" layer="37"/>
<rectangle x1="2.1971" y1="0.1651" x2="2.41808125" y2="0.17348125" layer="37"/>
<rectangle x1="2.56031875" y1="0.1651" x2="2.7813" y2="0.17348125" layer="37"/>
<rectangle x1="3.47471875" y1="0.1651" x2="3.6957" y2="0.17348125" layer="37"/>
<rectangle x1="3.84048125" y1="0.1651" x2="4.05891875" y2="0.17348125" layer="37"/>
<rectangle x1="4.75488125" y1="0.1651" x2="4.97331875" y2="0.17348125" layer="37"/>
<rectangle x1="5.56768125" y1="0.1651" x2="5.78611875" y2="0.17348125" layer="37"/>
<rectangle x1="6.54811875" y1="0.1651" x2="6.7691" y2="0.17348125" layer="37"/>
<rectangle x1="7.10691875" y1="0.1651" x2="7.3279" y2="0.17348125" layer="37"/>
<rectangle x1="8.0391" y1="0.1651" x2="8.23468125" y2="0.17348125" layer="37"/>
<rectangle x1="8.3439" y1="0.1651" x2="8.5471" y2="0.17348125" layer="37"/>
<rectangle x1="9.25068125" y1="0.1651" x2="9.47928125" y2="0.17348125" layer="37"/>
<rectangle x1="0.0127" y1="0.17348125" x2="0.23368125" y2="0.18211875" layer="37"/>
<rectangle x1="0.76708125" y1="0.17348125" x2="1.01091875" y2="0.18211875" layer="37"/>
<rectangle x1="1.2827" y1="0.17348125" x2="1.50368125" y2="0.18211875" layer="37"/>
<rectangle x1="2.1971" y1="0.17348125" x2="2.41808125" y2="0.18211875" layer="37"/>
<rectangle x1="2.56031875" y1="0.17348125" x2="2.7813" y2="0.18211875" layer="37"/>
<rectangle x1="3.47471875" y1="0.17348125" x2="3.6957" y2="0.18211875" layer="37"/>
<rectangle x1="3.84048125" y1="0.17348125" x2="4.05891875" y2="0.18211875" layer="37"/>
<rectangle x1="4.75488125" y1="0.17348125" x2="4.97331875" y2="0.18211875" layer="37"/>
<rectangle x1="5.56768125" y1="0.17348125" x2="5.78611875" y2="0.18211875" layer="37"/>
<rectangle x1="6.54811875" y1="0.17348125" x2="6.7691" y2="0.18211875" layer="37"/>
<rectangle x1="7.10691875" y1="0.17348125" x2="7.3279" y2="0.18211875" layer="37"/>
<rectangle x1="8.0391" y1="0.17348125" x2="8.2169" y2="0.18211875" layer="37"/>
<rectangle x1="8.35151875" y1="0.17348125" x2="8.53948125" y2="0.18211875" layer="37"/>
<rectangle x1="9.25068125" y1="0.17348125" x2="9.47928125" y2="0.18211875" layer="37"/>
<rectangle x1="0.0127" y1="0.18211875" x2="0.23368125" y2="0.1905" layer="37"/>
<rectangle x1="0.75691875" y1="0.18211875" x2="1.0033" y2="0.1905" layer="37"/>
<rectangle x1="1.2827" y1="0.18211875" x2="1.50368125" y2="0.1905" layer="37"/>
<rectangle x1="2.1971" y1="0.18211875" x2="2.41808125" y2="0.1905" layer="37"/>
<rectangle x1="2.56031875" y1="0.18211875" x2="2.7813" y2="0.1905" layer="37"/>
<rectangle x1="3.47471875" y1="0.18211875" x2="3.6957" y2="0.1905" layer="37"/>
<rectangle x1="3.84048125" y1="0.18211875" x2="4.05891875" y2="0.1905" layer="37"/>
<rectangle x1="4.75488125" y1="0.18211875" x2="4.97331875" y2="0.1905" layer="37"/>
<rectangle x1="5.56768125" y1="0.18211875" x2="5.78611875" y2="0.1905" layer="37"/>
<rectangle x1="6.54811875" y1="0.18211875" x2="6.7691" y2="0.1905" layer="37"/>
<rectangle x1="7.10691875" y1="0.18211875" x2="7.3279" y2="0.1905" layer="37"/>
<rectangle x1="8.0645" y1="0.18211875" x2="8.19911875" y2="0.1905" layer="37"/>
<rectangle x1="8.37691875" y1="0.18211875" x2="8.5217" y2="0.1905" layer="37"/>
<rectangle x1="9.25068125" y1="0.18211875" x2="9.47928125" y2="0.1905" layer="37"/>
<rectangle x1="0.0127" y1="0.1905" x2="0.23368125" y2="0.19888125" layer="37"/>
<rectangle x1="0.7493" y1="0.1905" x2="0.99568125" y2="0.19888125" layer="37"/>
<rectangle x1="1.2827" y1="0.1905" x2="1.50368125" y2="0.19888125" layer="37"/>
<rectangle x1="2.1971" y1="0.1905" x2="2.41808125" y2="0.19888125" layer="37"/>
<rectangle x1="2.56031875" y1="0.1905" x2="2.7813" y2="0.19888125" layer="37"/>
<rectangle x1="3.47471875" y1="0.1905" x2="3.6957" y2="0.19888125" layer="37"/>
<rectangle x1="3.84048125" y1="0.1905" x2="4.05891875" y2="0.19888125" layer="37"/>
<rectangle x1="4.75488125" y1="0.1905" x2="4.97331875" y2="0.19888125" layer="37"/>
<rectangle x1="5.56768125" y1="0.1905" x2="5.78611875" y2="0.19888125" layer="37"/>
<rectangle x1="6.54811875" y1="0.1905" x2="6.7691" y2="0.19888125" layer="37"/>
<rectangle x1="7.10691875" y1="0.1905" x2="7.3279" y2="0.19888125" layer="37"/>
<rectangle x1="8.09751875" y1="0.1905" x2="8.1661" y2="0.19888125" layer="37"/>
<rectangle x1="8.41248125" y1="0.1905" x2="8.48868125" y2="0.19888125" layer="37"/>
<rectangle x1="9.25068125" y1="0.1905" x2="9.47928125" y2="0.19888125" layer="37"/>
<rectangle x1="0.0127" y1="0.19888125" x2="0.23368125" y2="0.20751875" layer="37"/>
<rectangle x1="0.74168125" y1="0.19888125" x2="0.98551875" y2="0.20751875" layer="37"/>
<rectangle x1="1.2827" y1="0.19888125" x2="1.50368125" y2="0.20751875" layer="37"/>
<rectangle x1="2.1971" y1="0.19888125" x2="2.41808125" y2="0.20751875" layer="37"/>
<rectangle x1="2.56031875" y1="0.19888125" x2="2.7813" y2="0.20751875" layer="37"/>
<rectangle x1="3.47471875" y1="0.19888125" x2="3.6957" y2="0.20751875" layer="37"/>
<rectangle x1="3.84048125" y1="0.19888125" x2="4.05891875" y2="0.20751875" layer="37"/>
<rectangle x1="4.75488125" y1="0.19888125" x2="4.97331875" y2="0.20751875" layer="37"/>
<rectangle x1="5.56768125" y1="0.19888125" x2="5.78611875" y2="0.20751875" layer="37"/>
<rectangle x1="6.54811875" y1="0.19888125" x2="6.7691" y2="0.20751875" layer="37"/>
<rectangle x1="7.10691875" y1="0.19888125" x2="7.3279" y2="0.20751875" layer="37"/>
<rectangle x1="9.25068125" y1="0.19888125" x2="9.47928125" y2="0.20751875" layer="37"/>
<rectangle x1="0.0127" y1="0.20751875" x2="0.23368125" y2="0.2159" layer="37"/>
<rectangle x1="0.7239" y1="0.20751875" x2="0.97028125" y2="0.2159" layer="37"/>
<rectangle x1="1.2827" y1="0.20751875" x2="1.50368125" y2="0.2159" layer="37"/>
<rectangle x1="2.1971" y1="0.20751875" x2="2.41808125" y2="0.2159" layer="37"/>
<rectangle x1="2.56031875" y1="0.20751875" x2="2.7813" y2="0.2159" layer="37"/>
<rectangle x1="3.47471875" y1="0.20751875" x2="3.6957" y2="0.2159" layer="37"/>
<rectangle x1="3.84048125" y1="0.20751875" x2="4.05891875" y2="0.2159" layer="37"/>
<rectangle x1="4.75488125" y1="0.20751875" x2="4.97331875" y2="0.2159" layer="37"/>
<rectangle x1="5.56768125" y1="0.20751875" x2="5.78611875" y2="0.2159" layer="37"/>
<rectangle x1="6.54811875" y1="0.20751875" x2="6.7691" y2="0.2159" layer="37"/>
<rectangle x1="7.10691875" y1="0.20751875" x2="7.3279" y2="0.2159" layer="37"/>
<rectangle x1="9.25068125" y1="0.20751875" x2="9.47928125" y2="0.2159" layer="37"/>
<rectangle x1="0.0127" y1="0.2159" x2="0.23368125" y2="0.22428125" layer="37"/>
<rectangle x1="0.71628125" y1="0.2159" x2="0.96011875" y2="0.22428125" layer="37"/>
<rectangle x1="1.2827" y1="0.2159" x2="1.50368125" y2="0.22428125" layer="37"/>
<rectangle x1="2.1971" y1="0.2159" x2="2.41808125" y2="0.22428125" layer="37"/>
<rectangle x1="2.56031875" y1="0.2159" x2="2.7813" y2="0.22428125" layer="37"/>
<rectangle x1="3.47471875" y1="0.2159" x2="3.6957" y2="0.22428125" layer="37"/>
<rectangle x1="3.84048125" y1="0.2159" x2="4.05891875" y2="0.22428125" layer="37"/>
<rectangle x1="4.75488125" y1="0.2159" x2="4.97331875" y2="0.22428125" layer="37"/>
<rectangle x1="5.56768125" y1="0.2159" x2="5.78611875" y2="0.22428125" layer="37"/>
<rectangle x1="6.54811875" y1="0.2159" x2="6.7691" y2="0.22428125" layer="37"/>
<rectangle x1="7.10691875" y1="0.2159" x2="7.3279" y2="0.22428125" layer="37"/>
<rectangle x1="9.25068125" y1="0.2159" x2="9.47928125" y2="0.22428125" layer="37"/>
<rectangle x1="0.0127" y1="0.22428125" x2="0.23368125" y2="0.23291875" layer="37"/>
<rectangle x1="0.70611875" y1="0.22428125" x2="0.9525" y2="0.23291875" layer="37"/>
<rectangle x1="1.2827" y1="0.22428125" x2="1.50368125" y2="0.23291875" layer="37"/>
<rectangle x1="2.1971" y1="0.22428125" x2="2.41808125" y2="0.23291875" layer="37"/>
<rectangle x1="2.56031875" y1="0.22428125" x2="2.7813" y2="0.23291875" layer="37"/>
<rectangle x1="3.47471875" y1="0.22428125" x2="3.6957" y2="0.23291875" layer="37"/>
<rectangle x1="3.84048125" y1="0.22428125" x2="4.05891875" y2="0.23291875" layer="37"/>
<rectangle x1="4.75488125" y1="0.22428125" x2="4.97331875" y2="0.23291875" layer="37"/>
<rectangle x1="5.56768125" y1="0.22428125" x2="5.78611875" y2="0.23291875" layer="37"/>
<rectangle x1="6.54811875" y1="0.22428125" x2="6.7691" y2="0.23291875" layer="37"/>
<rectangle x1="7.10691875" y1="0.22428125" x2="7.3279" y2="0.23291875" layer="37"/>
<rectangle x1="9.25068125" y1="0.22428125" x2="9.47928125" y2="0.23291875" layer="37"/>
<rectangle x1="0.0127" y1="0.23291875" x2="0.23368125" y2="0.2413" layer="37"/>
<rectangle x1="0.6985" y1="0.23291875" x2="0.94488125" y2="0.2413" layer="37"/>
<rectangle x1="1.2827" y1="0.23291875" x2="1.50368125" y2="0.2413" layer="37"/>
<rectangle x1="2.1971" y1="0.23291875" x2="2.41808125" y2="0.2413" layer="37"/>
<rectangle x1="2.56031875" y1="0.23291875" x2="2.7813" y2="0.2413" layer="37"/>
<rectangle x1="3.47471875" y1="0.23291875" x2="3.6957" y2="0.2413" layer="37"/>
<rectangle x1="3.84048125" y1="0.23291875" x2="4.05891875" y2="0.2413" layer="37"/>
<rectangle x1="4.75488125" y1="0.23291875" x2="4.97331875" y2="0.2413" layer="37"/>
<rectangle x1="5.56768125" y1="0.23291875" x2="5.78611875" y2="0.2413" layer="37"/>
<rectangle x1="6.54811875" y1="0.23291875" x2="6.7691" y2="0.2413" layer="37"/>
<rectangle x1="7.10691875" y1="0.23291875" x2="7.3279" y2="0.2413" layer="37"/>
<rectangle x1="9.25068125" y1="0.23291875" x2="9.47928125" y2="0.2413" layer="37"/>
<rectangle x1="0.0127" y1="0.2413" x2="0.23368125" y2="0.24968125" layer="37"/>
<rectangle x1="0.69088125" y1="0.2413" x2="0.93471875" y2="0.24968125" layer="37"/>
<rectangle x1="1.2827" y1="0.2413" x2="1.50368125" y2="0.24968125" layer="37"/>
<rectangle x1="2.1971" y1="0.2413" x2="2.41808125" y2="0.24968125" layer="37"/>
<rectangle x1="2.56031875" y1="0.2413" x2="2.7813" y2="0.24968125" layer="37"/>
<rectangle x1="3.47471875" y1="0.2413" x2="3.6957" y2="0.24968125" layer="37"/>
<rectangle x1="3.84048125" y1="0.2413" x2="4.05891875" y2="0.24968125" layer="37"/>
<rectangle x1="4.75488125" y1="0.2413" x2="4.97331875" y2="0.24968125" layer="37"/>
<rectangle x1="5.56768125" y1="0.2413" x2="5.78611875" y2="0.24968125" layer="37"/>
<rectangle x1="6.54811875" y1="0.2413" x2="6.7691" y2="0.24968125" layer="37"/>
<rectangle x1="7.10691875" y1="0.2413" x2="7.3279" y2="0.24968125" layer="37"/>
<rectangle x1="9.25068125" y1="0.2413" x2="9.47928125" y2="0.24968125" layer="37"/>
<rectangle x1="0.0127" y1="0.24968125" x2="0.23368125" y2="0.25831875" layer="37"/>
<rectangle x1="0.68071875" y1="0.24968125" x2="0.9271" y2="0.25831875" layer="37"/>
<rectangle x1="1.2827" y1="0.24968125" x2="1.50368125" y2="0.25831875" layer="37"/>
<rectangle x1="2.1971" y1="0.24968125" x2="2.41808125" y2="0.25831875" layer="37"/>
<rectangle x1="2.56031875" y1="0.24968125" x2="2.7813" y2="0.25831875" layer="37"/>
<rectangle x1="3.47471875" y1="0.24968125" x2="3.6957" y2="0.25831875" layer="37"/>
<rectangle x1="3.84048125" y1="0.24968125" x2="4.05891875" y2="0.25831875" layer="37"/>
<rectangle x1="4.75488125" y1="0.24968125" x2="4.97331875" y2="0.25831875" layer="37"/>
<rectangle x1="5.56768125" y1="0.24968125" x2="5.78611875" y2="0.25831875" layer="37"/>
<rectangle x1="6.54811875" y1="0.24968125" x2="6.7691" y2="0.25831875" layer="37"/>
<rectangle x1="7.10691875" y1="0.24968125" x2="7.3279" y2="0.25831875" layer="37"/>
<rectangle x1="9.25068125" y1="0.24968125" x2="9.47928125" y2="0.25831875" layer="37"/>
<rectangle x1="0.0127" y1="0.25831875" x2="0.23368125" y2="0.2667" layer="37"/>
<rectangle x1="0.6731" y1="0.25831875" x2="0.91948125" y2="0.2667" layer="37"/>
<rectangle x1="1.2827" y1="0.25831875" x2="1.50368125" y2="0.2667" layer="37"/>
<rectangle x1="2.1971" y1="0.25831875" x2="2.41808125" y2="0.2667" layer="37"/>
<rectangle x1="2.56031875" y1="0.25831875" x2="2.7813" y2="0.2667" layer="37"/>
<rectangle x1="3.47471875" y1="0.25831875" x2="3.6957" y2="0.2667" layer="37"/>
<rectangle x1="3.84048125" y1="0.25831875" x2="4.05891875" y2="0.2667" layer="37"/>
<rectangle x1="4.75488125" y1="0.25831875" x2="4.97331875" y2="0.2667" layer="37"/>
<rectangle x1="5.56768125" y1="0.25831875" x2="5.78611875" y2="0.2667" layer="37"/>
<rectangle x1="6.54811875" y1="0.25831875" x2="6.7691" y2="0.2667" layer="37"/>
<rectangle x1="7.10691875" y1="0.25831875" x2="7.3279" y2="0.2667" layer="37"/>
<rectangle x1="9.25068125" y1="0.25831875" x2="9.47928125" y2="0.2667" layer="37"/>
<rectangle x1="0.0127" y1="0.2667" x2="0.23368125" y2="0.27508125" layer="37"/>
<rectangle x1="0.66548125" y1="0.2667" x2="0.90931875" y2="0.27508125" layer="37"/>
<rectangle x1="1.2827" y1="0.2667" x2="1.50368125" y2="0.27508125" layer="37"/>
<rectangle x1="2.1971" y1="0.2667" x2="2.41808125" y2="0.27508125" layer="37"/>
<rectangle x1="2.56031875" y1="0.2667" x2="2.7813" y2="0.27508125" layer="37"/>
<rectangle x1="3.47471875" y1="0.2667" x2="3.6957" y2="0.27508125" layer="37"/>
<rectangle x1="3.84048125" y1="0.2667" x2="4.05891875" y2="0.27508125" layer="37"/>
<rectangle x1="4.75488125" y1="0.2667" x2="4.97331875" y2="0.27508125" layer="37"/>
<rectangle x1="5.56768125" y1="0.2667" x2="5.78611875" y2="0.27508125" layer="37"/>
<rectangle x1="6.54811875" y1="0.2667" x2="6.7691" y2="0.27508125" layer="37"/>
<rectangle x1="7.10691875" y1="0.2667" x2="7.3279" y2="0.27508125" layer="37"/>
<rectangle x1="9.25068125" y1="0.2667" x2="9.47928125" y2="0.27508125" layer="37"/>
<rectangle x1="0.0127" y1="0.27508125" x2="0.23368125" y2="0.28371875" layer="37"/>
<rectangle x1="0.6477" y1="0.27508125" x2="0.89408125" y2="0.28371875" layer="37"/>
<rectangle x1="1.2827" y1="0.27508125" x2="1.50368125" y2="0.28371875" layer="37"/>
<rectangle x1="2.1971" y1="0.27508125" x2="2.41808125" y2="0.28371875" layer="37"/>
<rectangle x1="2.56031875" y1="0.27508125" x2="2.7813" y2="0.28371875" layer="37"/>
<rectangle x1="3.45948125" y1="0.27508125" x2="3.6957" y2="0.28371875" layer="37"/>
<rectangle x1="3.84048125" y1="0.27508125" x2="4.05891875" y2="0.28371875" layer="37"/>
<rectangle x1="4.75488125" y1="0.27508125" x2="4.97331875" y2="0.28371875" layer="37"/>
<rectangle x1="5.56768125" y1="0.27508125" x2="5.78611875" y2="0.28371875" layer="37"/>
<rectangle x1="6.54811875" y1="0.27508125" x2="6.7691" y2="0.28371875" layer="37"/>
<rectangle x1="7.10691875" y1="0.27508125" x2="7.3279" y2="0.28371875" layer="37"/>
<rectangle x1="9.24051875" y1="0.27508125" x2="9.47928125" y2="0.28371875" layer="37"/>
<rectangle x1="0.0127" y1="0.28371875" x2="0.23368125" y2="0.2921" layer="37"/>
<rectangle x1="0.64008125" y1="0.28371875" x2="0.88391875" y2="0.2921" layer="37"/>
<rectangle x1="1.2827" y1="0.28371875" x2="1.50368125" y2="0.2921" layer="37"/>
<rectangle x1="2.1971" y1="0.28371875" x2="2.41808125" y2="0.2921" layer="37"/>
<rectangle x1="2.56031875" y1="0.28371875" x2="2.7813" y2="0.2921" layer="37"/>
<rectangle x1="3.4417" y1="0.28371875" x2="3.68808125" y2="0.2921" layer="37"/>
<rectangle x1="3.84048125" y1="0.28371875" x2="4.05891875" y2="0.2921" layer="37"/>
<rectangle x1="4.75488125" y1="0.28371875" x2="4.97331875" y2="0.2921" layer="37"/>
<rectangle x1="5.56768125" y1="0.28371875" x2="5.78611875" y2="0.2921" layer="37"/>
<rectangle x1="6.54811875" y1="0.28371875" x2="6.7691" y2="0.2921" layer="37"/>
<rectangle x1="7.10691875" y1="0.28371875" x2="7.3279" y2="0.2921" layer="37"/>
<rectangle x1="9.21511875" y1="0.28371875" x2="9.46911875" y2="0.2921" layer="37"/>
<rectangle x1="0.0127" y1="0.2921" x2="0.23368125" y2="0.30048125" layer="37"/>
<rectangle x1="0.62991875" y1="0.2921" x2="0.8763" y2="0.30048125" layer="37"/>
<rectangle x1="1.2827" y1="0.2921" x2="1.50368125" y2="0.30048125" layer="37"/>
<rectangle x1="2.1971" y1="0.2921" x2="2.41808125" y2="0.30048125" layer="37"/>
<rectangle x1="2.56031875" y1="0.2921" x2="2.7813" y2="0.30048125" layer="37"/>
<rectangle x1="3.4163" y1="0.2921" x2="3.68808125" y2="0.30048125" layer="37"/>
<rectangle x1="3.84048125" y1="0.2921" x2="4.05891875" y2="0.30048125" layer="37"/>
<rectangle x1="4.75488125" y1="0.2921" x2="4.97331875" y2="0.30048125" layer="37"/>
<rectangle x1="5.56768125" y1="0.2921" x2="5.78611875" y2="0.30048125" layer="37"/>
<rectangle x1="6.54811875" y1="0.2921" x2="6.7691" y2="0.30048125" layer="37"/>
<rectangle x1="7.10691875" y1="0.2921" x2="7.3279" y2="0.30048125" layer="37"/>
<rectangle x1="9.19988125" y1="0.2921" x2="9.46911875" y2="0.30048125" layer="37"/>
<rectangle x1="0.0127" y1="0.30048125" x2="0.88391875" y2="0.30911875" layer="37"/>
<rectangle x1="1.2827" y1="0.30048125" x2="1.50368125" y2="0.30911875" layer="37"/>
<rectangle x1="2.1971" y1="0.30048125" x2="2.41808125" y2="0.30911875" layer="37"/>
<rectangle x1="2.56031875" y1="0.30048125" x2="3.67791875" y2="0.30911875" layer="37"/>
<rectangle x1="3.84048125" y1="0.30048125" x2="4.05891875" y2="0.30911875" layer="37"/>
<rectangle x1="4.75488125" y1="0.30048125" x2="4.97331875" y2="0.30911875" layer="37"/>
<rectangle x1="5.56768125" y1="0.30048125" x2="5.78611875" y2="0.30911875" layer="37"/>
<rectangle x1="6.54811875" y1="0.30048125" x2="6.7691" y2="0.30911875" layer="37"/>
<rectangle x1="7.10691875" y1="0.30048125" x2="7.3279" y2="0.30911875" layer="37"/>
<rectangle x1="8.5725" y1="0.30048125" x2="9.4615" y2="0.30911875" layer="37"/>
<rectangle x1="0.0127" y1="0.30911875" x2="0.94488125" y2="0.3175" layer="37"/>
<rectangle x1="1.2827" y1="0.30911875" x2="1.50368125" y2="0.3175" layer="37"/>
<rectangle x1="2.1971" y1="0.30911875" x2="2.41808125" y2="0.3175" layer="37"/>
<rectangle x1="2.56031875" y1="0.30911875" x2="3.66268125" y2="0.3175" layer="37"/>
<rectangle x1="3.84048125" y1="0.30911875" x2="4.05891875" y2="0.3175" layer="37"/>
<rectangle x1="4.75488125" y1="0.30911875" x2="4.97331875" y2="0.3175" layer="37"/>
<rectangle x1="5.56768125" y1="0.30911875" x2="5.78611875" y2="0.3175" layer="37"/>
<rectangle x1="6.54811875" y1="0.30911875" x2="6.7691" y2="0.3175" layer="37"/>
<rectangle x1="7.10691875" y1="0.30911875" x2="7.3279" y2="0.3175" layer="37"/>
<rectangle x1="8.52931875" y1="0.30911875" x2="9.45388125" y2="0.3175" layer="37"/>
<rectangle x1="0.0127" y1="0.3175" x2="0.9779" y2="0.32588125" layer="37"/>
<rectangle x1="1.2827" y1="0.3175" x2="1.50368125" y2="0.32588125" layer="37"/>
<rectangle x1="2.1971" y1="0.3175" x2="2.41808125" y2="0.32588125" layer="37"/>
<rectangle x1="2.56031875" y1="0.3175" x2="3.65251875" y2="0.32588125" layer="37"/>
<rectangle x1="3.84048125" y1="0.3175" x2="4.05891875" y2="0.32588125" layer="37"/>
<rectangle x1="4.75488125" y1="0.3175" x2="4.97331875" y2="0.32588125" layer="37"/>
<rectangle x1="5.56768125" y1="0.3175" x2="5.78611875" y2="0.32588125" layer="37"/>
<rectangle x1="6.54811875" y1="0.3175" x2="6.7691" y2="0.32588125" layer="37"/>
<rectangle x1="7.10691875" y1="0.3175" x2="7.3279" y2="0.32588125" layer="37"/>
<rectangle x1="8.4963" y1="0.3175" x2="9.44371875" y2="0.32588125" layer="37"/>
<rectangle x1="0.0127" y1="0.32588125" x2="1.01091875" y2="0.33451875" layer="37"/>
<rectangle x1="1.2827" y1="0.32588125" x2="1.50368125" y2="0.33451875" layer="37"/>
<rectangle x1="2.1971" y1="0.32588125" x2="2.41808125" y2="0.33451875" layer="37"/>
<rectangle x1="2.56031875" y1="0.32588125" x2="3.6449" y2="0.33451875" layer="37"/>
<rectangle x1="3.84048125" y1="0.32588125" x2="4.05891875" y2="0.33451875" layer="37"/>
<rectangle x1="4.75488125" y1="0.32588125" x2="4.97331875" y2="0.33451875" layer="37"/>
<rectangle x1="5.56768125" y1="0.32588125" x2="5.78611875" y2="0.33451875" layer="37"/>
<rectangle x1="6.54811875" y1="0.32588125" x2="6.7691" y2="0.33451875" layer="37"/>
<rectangle x1="7.10691875" y1="0.32588125" x2="7.3279" y2="0.33451875" layer="37"/>
<rectangle x1="8.4709" y1="0.32588125" x2="9.42848125" y2="0.33451875" layer="37"/>
<rectangle x1="0.0127" y1="0.33451875" x2="1.04648125" y2="0.3429" layer="37"/>
<rectangle x1="1.2827" y1="0.33451875" x2="1.50368125" y2="0.3429" layer="37"/>
<rectangle x1="2.1971" y1="0.33451875" x2="2.41808125" y2="0.3429" layer="37"/>
<rectangle x1="2.56031875" y1="0.33451875" x2="3.6195" y2="0.3429" layer="37"/>
<rectangle x1="3.84048125" y1="0.33451875" x2="4.05891875" y2="0.3429" layer="37"/>
<rectangle x1="4.75488125" y1="0.33451875" x2="4.97331875" y2="0.3429" layer="37"/>
<rectangle x1="5.56768125" y1="0.33451875" x2="5.78611875" y2="0.3429" layer="37"/>
<rectangle x1="6.54811875" y1="0.33451875" x2="6.7691" y2="0.3429" layer="37"/>
<rectangle x1="7.10691875" y1="0.33451875" x2="7.3279" y2="0.3429" layer="37"/>
<rectangle x1="8.45311875" y1="0.33451875" x2="9.41831875" y2="0.3429" layer="37"/>
<rectangle x1="0.0127" y1="0.3429" x2="1.06171875" y2="0.35128125" layer="37"/>
<rectangle x1="1.2827" y1="0.3429" x2="1.50368125" y2="0.35128125" layer="37"/>
<rectangle x1="2.1971" y1="0.3429" x2="2.41808125" y2="0.35128125" layer="37"/>
<rectangle x1="2.56031875" y1="0.3429" x2="3.58648125" y2="0.35128125" layer="37"/>
<rectangle x1="3.84048125" y1="0.3429" x2="4.05891875" y2="0.35128125" layer="37"/>
<rectangle x1="4.75488125" y1="0.3429" x2="4.97331875" y2="0.35128125" layer="37"/>
<rectangle x1="5.56768125" y1="0.3429" x2="5.78611875" y2="0.35128125" layer="37"/>
<rectangle x1="6.54811875" y1="0.3429" x2="6.7691" y2="0.35128125" layer="37"/>
<rectangle x1="7.10691875" y1="0.3429" x2="7.3279" y2="0.35128125" layer="37"/>
<rectangle x1="8.43788125" y1="0.3429" x2="9.40308125" y2="0.35128125" layer="37"/>
<rectangle x1="0.0127" y1="0.35128125" x2="1.07188125" y2="0.35991875" layer="37"/>
<rectangle x1="1.2827" y1="0.35128125" x2="1.50368125" y2="0.35991875" layer="37"/>
<rectangle x1="2.1971" y1="0.35128125" x2="2.41808125" y2="0.35991875" layer="37"/>
<rectangle x1="2.56031875" y1="0.35128125" x2="3.60171875" y2="0.35991875" layer="37"/>
<rectangle x1="3.84048125" y1="0.35128125" x2="4.05891875" y2="0.35991875" layer="37"/>
<rectangle x1="4.75488125" y1="0.35128125" x2="4.97331875" y2="0.35991875" layer="37"/>
<rectangle x1="5.56768125" y1="0.35128125" x2="5.78611875" y2="0.35991875" layer="37"/>
<rectangle x1="6.54811875" y1="0.35128125" x2="6.7691" y2="0.35991875" layer="37"/>
<rectangle x1="7.10691875" y1="0.35128125" x2="7.3279" y2="0.35991875" layer="37"/>
<rectangle x1="8.41248125" y1="0.35128125" x2="9.3853" y2="0.35991875" layer="37"/>
<rectangle x1="0.0127" y1="0.35991875" x2="1.08711875" y2="0.3683" layer="37"/>
<rectangle x1="1.2827" y1="0.35991875" x2="1.50368125" y2="0.3683" layer="37"/>
<rectangle x1="2.1971" y1="0.35991875" x2="2.41808125" y2="0.3683" layer="37"/>
<rectangle x1="2.56031875" y1="0.35991875" x2="3.62711875" y2="0.3683" layer="37"/>
<rectangle x1="3.84048125" y1="0.35991875" x2="4.05891875" y2="0.3683" layer="37"/>
<rectangle x1="4.75488125" y1="0.35991875" x2="4.97331875" y2="0.3683" layer="37"/>
<rectangle x1="5.56768125" y1="0.35991875" x2="5.78611875" y2="0.3683" layer="37"/>
<rectangle x1="6.54811875" y1="0.35991875" x2="6.7691" y2="0.3683" layer="37"/>
<rectangle x1="7.10691875" y1="0.35991875" x2="7.3279" y2="0.3683" layer="37"/>
<rectangle x1="8.40231875" y1="0.35991875" x2="9.3599" y2="0.3683" layer="37"/>
<rectangle x1="0.0127" y1="0.3683" x2="1.09728125" y2="0.37668125" layer="37"/>
<rectangle x1="1.2827" y1="0.3683" x2="1.50368125" y2="0.37668125" layer="37"/>
<rectangle x1="2.1971" y1="0.3683" x2="2.41808125" y2="0.37668125" layer="37"/>
<rectangle x1="2.56031875" y1="0.3683" x2="3.6449" y2="0.37668125" layer="37"/>
<rectangle x1="3.84048125" y1="0.3683" x2="4.05891875" y2="0.37668125" layer="37"/>
<rectangle x1="4.75488125" y1="0.3683" x2="4.97331875" y2="0.37668125" layer="37"/>
<rectangle x1="5.56768125" y1="0.3683" x2="5.78611875" y2="0.37668125" layer="37"/>
<rectangle x1="6.54811875" y1="0.3683" x2="6.7691" y2="0.37668125" layer="37"/>
<rectangle x1="7.10691875" y1="0.3683" x2="7.3279" y2="0.37668125" layer="37"/>
<rectangle x1="8.38708125" y1="0.3683" x2="9.34211875" y2="0.37668125" layer="37"/>
<rectangle x1="0.0127" y1="0.37668125" x2="1.11251875" y2="0.38531875" layer="37"/>
<rectangle x1="1.2827" y1="0.37668125" x2="1.50368125" y2="0.38531875" layer="37"/>
<rectangle x1="2.1971" y1="0.37668125" x2="2.41808125" y2="0.38531875" layer="37"/>
<rectangle x1="2.56031875" y1="0.37668125" x2="3.65251875" y2="0.38531875" layer="37"/>
<rectangle x1="3.84048125" y1="0.37668125" x2="4.05891875" y2="0.38531875" layer="37"/>
<rectangle x1="4.75488125" y1="0.37668125" x2="4.97331875" y2="0.38531875" layer="37"/>
<rectangle x1="5.56768125" y1="0.37668125" x2="5.78611875" y2="0.38531875" layer="37"/>
<rectangle x1="6.54811875" y1="0.37668125" x2="6.7691" y2="0.38531875" layer="37"/>
<rectangle x1="7.10691875" y1="0.37668125" x2="7.3279" y2="0.38531875" layer="37"/>
<rectangle x1="8.3693" y1="0.37668125" x2="9.31671875" y2="0.38531875" layer="37"/>
<rectangle x1="0.0127" y1="0.38531875" x2="1.12268125" y2="0.3937" layer="37"/>
<rectangle x1="1.2827" y1="0.38531875" x2="1.50368125" y2="0.3937" layer="37"/>
<rectangle x1="2.1971" y1="0.38531875" x2="2.41808125" y2="0.3937" layer="37"/>
<rectangle x1="2.56031875" y1="0.38531875" x2="3.66268125" y2="0.3937" layer="37"/>
<rectangle x1="3.84048125" y1="0.38531875" x2="4.05891875" y2="0.3937" layer="37"/>
<rectangle x1="4.75488125" y1="0.38531875" x2="4.97331875" y2="0.3937" layer="37"/>
<rectangle x1="5.56768125" y1="0.38531875" x2="5.78611875" y2="0.3937" layer="37"/>
<rectangle x1="6.54811875" y1="0.38531875" x2="6.7691" y2="0.3937" layer="37"/>
<rectangle x1="7.10691875" y1="0.38531875" x2="7.3279" y2="0.3937" layer="37"/>
<rectangle x1="8.36168125" y1="0.38531875" x2="9.2837" y2="0.3937" layer="37"/>
<rectangle x1="0.0127" y1="0.3937" x2="1.1303" y2="0.40208125" layer="37"/>
<rectangle x1="1.2827" y1="0.3937" x2="1.50368125" y2="0.40208125" layer="37"/>
<rectangle x1="2.1971" y1="0.3937" x2="2.41808125" y2="0.40208125" layer="37"/>
<rectangle x1="2.56031875" y1="0.3937" x2="3.67791875" y2="0.40208125" layer="37"/>
<rectangle x1="3.84048125" y1="0.3937" x2="4.05891875" y2="0.40208125" layer="37"/>
<rectangle x1="4.75488125" y1="0.3937" x2="4.97331875" y2="0.40208125" layer="37"/>
<rectangle x1="5.56768125" y1="0.3937" x2="5.78611875" y2="0.40208125" layer="37"/>
<rectangle x1="6.54811875" y1="0.3937" x2="6.7691" y2="0.40208125" layer="37"/>
<rectangle x1="7.10691875" y1="0.3937" x2="7.3279" y2="0.40208125" layer="37"/>
<rectangle x1="8.35151875" y1="0.3937" x2="9.24051875" y2="0.40208125" layer="37"/>
<rectangle x1="0.0127" y1="0.40208125" x2="0.23368125" y2="0.41071875" layer="37"/>
<rectangle x1="0.86868125" y1="0.40208125" x2="1.13791875" y2="0.41071875" layer="37"/>
<rectangle x1="1.2827" y1="0.40208125" x2="1.50368125" y2="0.41071875" layer="37"/>
<rectangle x1="2.1971" y1="0.40208125" x2="2.41808125" y2="0.41071875" layer="37"/>
<rectangle x1="2.56031875" y1="0.40208125" x2="2.7813" y2="0.41071875" layer="37"/>
<rectangle x1="3.4163" y1="0.40208125" x2="3.68808125" y2="0.41071875" layer="37"/>
<rectangle x1="3.84048125" y1="0.40208125" x2="4.05891875" y2="0.41071875" layer="37"/>
<rectangle x1="4.75488125" y1="0.40208125" x2="4.97331875" y2="0.41071875" layer="37"/>
<rectangle x1="5.56768125" y1="0.40208125" x2="5.78611875" y2="0.41071875" layer="37"/>
<rectangle x1="6.54811875" y1="0.40208125" x2="6.7691" y2="0.41071875" layer="37"/>
<rectangle x1="7.10691875" y1="0.40208125" x2="7.3279" y2="0.41071875" layer="37"/>
<rectangle x1="8.10768125" y1="0.40208125" x2="8.1661" y2="0.41071875" layer="37"/>
<rectangle x1="8.3439" y1="0.40208125" x2="8.61568125" y2="0.41071875" layer="37"/>
<rectangle x1="0.0127" y1="0.41071875" x2="0.23368125" y2="0.4191" layer="37"/>
<rectangle x1="0.9017" y1="0.41071875" x2="1.14808125" y2="0.4191" layer="37"/>
<rectangle x1="1.2827" y1="0.41071875" x2="1.50368125" y2="0.4191" layer="37"/>
<rectangle x1="2.1971" y1="0.41071875" x2="2.41808125" y2="0.4191" layer="37"/>
<rectangle x1="2.56031875" y1="0.41071875" x2="2.7813" y2="0.4191" layer="37"/>
<rectangle x1="3.44931875" y1="0.41071875" x2="3.68808125" y2="0.4191" layer="37"/>
<rectangle x1="3.84048125" y1="0.41071875" x2="4.05891875" y2="0.4191" layer="37"/>
<rectangle x1="4.75488125" y1="0.41071875" x2="4.97331875" y2="0.4191" layer="37"/>
<rectangle x1="5.56768125" y1="0.41071875" x2="5.78611875" y2="0.4191" layer="37"/>
<rectangle x1="6.54811875" y1="0.41071875" x2="6.7691" y2="0.4191" layer="37"/>
<rectangle x1="7.10691875" y1="0.41071875" x2="7.3279" y2="0.4191" layer="37"/>
<rectangle x1="8.0645" y1="0.41071875" x2="8.20928125" y2="0.4191" layer="37"/>
<rectangle x1="8.3439" y1="0.41071875" x2="8.59028125" y2="0.4191" layer="37"/>
<rectangle x1="0.0127" y1="0.4191" x2="0.23368125" y2="0.42748125" layer="37"/>
<rectangle x1="0.91948125" y1="0.4191" x2="1.14808125" y2="0.42748125" layer="37"/>
<rectangle x1="1.2827" y1="0.4191" x2="1.50368125" y2="0.42748125" layer="37"/>
<rectangle x1="2.1971" y1="0.4191" x2="2.41808125" y2="0.42748125" layer="37"/>
<rectangle x1="2.56031875" y1="0.4191" x2="2.7813" y2="0.42748125" layer="37"/>
<rectangle x1="3.4671" y1="0.4191" x2="3.6957" y2="0.42748125" layer="37"/>
<rectangle x1="3.84048125" y1="0.4191" x2="4.05891875" y2="0.42748125" layer="37"/>
<rectangle x1="4.75488125" y1="0.4191" x2="4.97331875" y2="0.42748125" layer="37"/>
<rectangle x1="5.56768125" y1="0.4191" x2="5.78611875" y2="0.42748125" layer="37"/>
<rectangle x1="6.54811875" y1="0.4191" x2="6.7691" y2="0.42748125" layer="37"/>
<rectangle x1="7.10691875" y1="0.4191" x2="7.3279" y2="0.42748125" layer="37"/>
<rectangle x1="8.04671875" y1="0.4191" x2="8.22451875" y2="0.42748125" layer="37"/>
<rectangle x1="8.3439" y1="0.4191" x2="8.5725" y2="0.42748125" layer="37"/>
<rectangle x1="0.0127" y1="0.42748125" x2="0.23368125" y2="0.43611875" layer="37"/>
<rectangle x1="0.9271" y1="0.42748125" x2="1.14808125" y2="0.43611875" layer="37"/>
<rectangle x1="1.2827" y1="0.42748125" x2="1.50368125" y2="0.43611875" layer="37"/>
<rectangle x1="2.1971" y1="0.42748125" x2="2.41808125" y2="0.43611875" layer="37"/>
<rectangle x1="2.56031875" y1="0.42748125" x2="2.7813" y2="0.43611875" layer="37"/>
<rectangle x1="3.47471875" y1="0.42748125" x2="3.6957" y2="0.43611875" layer="37"/>
<rectangle x1="3.84048125" y1="0.42748125" x2="4.05891875" y2="0.43611875" layer="37"/>
<rectangle x1="4.75488125" y1="0.42748125" x2="4.97331875" y2="0.43611875" layer="37"/>
<rectangle x1="5.56768125" y1="0.42748125" x2="5.78611875" y2="0.43611875" layer="37"/>
<rectangle x1="6.54811875" y1="0.42748125" x2="6.7691" y2="0.43611875" layer="37"/>
<rectangle x1="7.10691875" y1="0.42748125" x2="7.3279" y2="0.43611875" layer="37"/>
<rectangle x1="8.03148125" y1="0.42748125" x2="8.2423" y2="0.43611875" layer="37"/>
<rectangle x1="8.3439" y1="0.42748125" x2="8.56488125" y2="0.43611875" layer="37"/>
<rectangle x1="0.0127" y1="0.43611875" x2="0.23368125" y2="0.4445" layer="37"/>
<rectangle x1="0.9271" y1="0.43611875" x2="1.14808125" y2="0.4445" layer="37"/>
<rectangle x1="1.2827" y1="0.43611875" x2="1.50368125" y2="0.4445" layer="37"/>
<rectangle x1="2.1971" y1="0.43611875" x2="2.41808125" y2="0.4445" layer="37"/>
<rectangle x1="2.56031875" y1="0.43611875" x2="2.7813" y2="0.4445" layer="37"/>
<rectangle x1="3.47471875" y1="0.43611875" x2="3.6957" y2="0.4445" layer="37"/>
<rectangle x1="3.84048125" y1="0.43611875" x2="4.05891875" y2="0.4445" layer="37"/>
<rectangle x1="4.75488125" y1="0.43611875" x2="4.97331875" y2="0.4445" layer="37"/>
<rectangle x1="5.56768125" y1="0.43611875" x2="5.78611875" y2="0.4445" layer="37"/>
<rectangle x1="6.54811875" y1="0.43611875" x2="6.7691" y2="0.4445" layer="37"/>
<rectangle x1="7.10691875" y1="0.43611875" x2="7.3279" y2="0.4445" layer="37"/>
<rectangle x1="8.02131875" y1="0.43611875" x2="8.2423" y2="0.4445" layer="37"/>
<rectangle x1="8.3439" y1="0.43611875" x2="8.56488125" y2="0.4445" layer="37"/>
<rectangle x1="0.0127" y1="0.4445" x2="0.23368125" y2="0.45288125" layer="37"/>
<rectangle x1="0.9271" y1="0.4445" x2="1.14808125" y2="0.45288125" layer="37"/>
<rectangle x1="1.2827" y1="0.4445" x2="1.50368125" y2="0.45288125" layer="37"/>
<rectangle x1="2.1971" y1="0.4445" x2="2.41808125" y2="0.45288125" layer="37"/>
<rectangle x1="2.56031875" y1="0.4445" x2="2.7813" y2="0.45288125" layer="37"/>
<rectangle x1="3.47471875" y1="0.4445" x2="3.6957" y2="0.45288125" layer="37"/>
<rectangle x1="3.84048125" y1="0.4445" x2="4.05891875" y2="0.45288125" layer="37"/>
<rectangle x1="4.75488125" y1="0.4445" x2="4.97331875" y2="0.45288125" layer="37"/>
<rectangle x1="5.56768125" y1="0.4445" x2="5.78611875" y2="0.45288125" layer="37"/>
<rectangle x1="6.54811875" y1="0.4445" x2="6.7691" y2="0.45288125" layer="37"/>
<rectangle x1="7.10691875" y1="0.4445" x2="7.3279" y2="0.45288125" layer="37"/>
<rectangle x1="8.02131875" y1="0.4445" x2="8.2423" y2="0.45288125" layer="37"/>
<rectangle x1="8.3439" y1="0.4445" x2="8.56488125" y2="0.45288125" layer="37"/>
<rectangle x1="0.0127" y1="0.45288125" x2="0.23368125" y2="0.46151875" layer="37"/>
<rectangle x1="0.9271" y1="0.45288125" x2="1.14808125" y2="0.46151875" layer="37"/>
<rectangle x1="1.2827" y1="0.45288125" x2="1.50368125" y2="0.46151875" layer="37"/>
<rectangle x1="2.1971" y1="0.45288125" x2="2.41808125" y2="0.46151875" layer="37"/>
<rectangle x1="2.56031875" y1="0.45288125" x2="2.7813" y2="0.46151875" layer="37"/>
<rectangle x1="3.47471875" y1="0.45288125" x2="3.6957" y2="0.46151875" layer="37"/>
<rectangle x1="3.84048125" y1="0.45288125" x2="4.05891875" y2="0.46151875" layer="37"/>
<rectangle x1="4.75488125" y1="0.45288125" x2="4.97331875" y2="0.46151875" layer="37"/>
<rectangle x1="5.56768125" y1="0.45288125" x2="5.78611875" y2="0.46151875" layer="37"/>
<rectangle x1="6.54811875" y1="0.45288125" x2="6.7691" y2="0.46151875" layer="37"/>
<rectangle x1="7.10691875" y1="0.45288125" x2="7.3279" y2="0.46151875" layer="37"/>
<rectangle x1="8.02131875" y1="0.45288125" x2="8.2423" y2="0.46151875" layer="37"/>
<rectangle x1="8.3439" y1="0.45288125" x2="8.56488125" y2="0.46151875" layer="37"/>
<rectangle x1="0.0127" y1="0.46151875" x2="0.23368125" y2="0.4699" layer="37"/>
<rectangle x1="0.9271" y1="0.46151875" x2="1.14808125" y2="0.4699" layer="37"/>
<rectangle x1="1.2827" y1="0.46151875" x2="1.50368125" y2="0.4699" layer="37"/>
<rectangle x1="2.1971" y1="0.46151875" x2="2.41808125" y2="0.4699" layer="37"/>
<rectangle x1="2.56031875" y1="0.46151875" x2="2.7813" y2="0.4699" layer="37"/>
<rectangle x1="3.47471875" y1="0.46151875" x2="3.6957" y2="0.4699" layer="37"/>
<rectangle x1="3.84048125" y1="0.46151875" x2="4.05891875" y2="0.4699" layer="37"/>
<rectangle x1="4.75488125" y1="0.46151875" x2="4.97331875" y2="0.4699" layer="37"/>
<rectangle x1="5.56768125" y1="0.46151875" x2="5.78611875" y2="0.4699" layer="37"/>
<rectangle x1="6.54811875" y1="0.46151875" x2="6.7691" y2="0.4699" layer="37"/>
<rectangle x1="7.10691875" y1="0.46151875" x2="7.3279" y2="0.4699" layer="37"/>
<rectangle x1="8.02131875" y1="0.46151875" x2="8.2423" y2="0.4699" layer="37"/>
<rectangle x1="8.3439" y1="0.46151875" x2="8.56488125" y2="0.4699" layer="37"/>
<rectangle x1="0.0127" y1="0.4699" x2="0.23368125" y2="0.47828125" layer="37"/>
<rectangle x1="0.9271" y1="0.4699" x2="1.14808125" y2="0.47828125" layer="37"/>
<rectangle x1="1.2827" y1="0.4699" x2="1.50368125" y2="0.47828125" layer="37"/>
<rectangle x1="2.1971" y1="0.4699" x2="2.41808125" y2="0.47828125" layer="37"/>
<rectangle x1="2.56031875" y1="0.4699" x2="2.7813" y2="0.47828125" layer="37"/>
<rectangle x1="3.47471875" y1="0.4699" x2="3.6957" y2="0.47828125" layer="37"/>
<rectangle x1="3.84048125" y1="0.4699" x2="4.05891875" y2="0.47828125" layer="37"/>
<rectangle x1="4.75488125" y1="0.4699" x2="4.97331875" y2="0.47828125" layer="37"/>
<rectangle x1="5.56768125" y1="0.4699" x2="5.78611875" y2="0.47828125" layer="37"/>
<rectangle x1="6.54811875" y1="0.4699" x2="6.7691" y2="0.47828125" layer="37"/>
<rectangle x1="7.10691875" y1="0.4699" x2="7.3279" y2="0.47828125" layer="37"/>
<rectangle x1="8.02131875" y1="0.4699" x2="8.2423" y2="0.47828125" layer="37"/>
<rectangle x1="8.3439" y1="0.4699" x2="8.56488125" y2="0.47828125" layer="37"/>
<rectangle x1="0.0127" y1="0.47828125" x2="0.23368125" y2="0.48691875" layer="37"/>
<rectangle x1="0.9271" y1="0.47828125" x2="1.14808125" y2="0.48691875" layer="37"/>
<rectangle x1="1.2827" y1="0.47828125" x2="1.50368125" y2="0.48691875" layer="37"/>
<rectangle x1="2.1971" y1="0.47828125" x2="2.41808125" y2="0.48691875" layer="37"/>
<rectangle x1="2.56031875" y1="0.47828125" x2="2.7813" y2="0.48691875" layer="37"/>
<rectangle x1="3.47471875" y1="0.47828125" x2="3.6957" y2="0.48691875" layer="37"/>
<rectangle x1="3.84048125" y1="0.47828125" x2="4.05891875" y2="0.48691875" layer="37"/>
<rectangle x1="4.75488125" y1="0.47828125" x2="4.97331875" y2="0.48691875" layer="37"/>
<rectangle x1="5.56768125" y1="0.47828125" x2="5.78611875" y2="0.48691875" layer="37"/>
<rectangle x1="6.54811875" y1="0.47828125" x2="6.7691" y2="0.48691875" layer="37"/>
<rectangle x1="7.10691875" y1="0.47828125" x2="7.3279" y2="0.48691875" layer="37"/>
<rectangle x1="8.02131875" y1="0.47828125" x2="8.2423" y2="0.48691875" layer="37"/>
<rectangle x1="8.3439" y1="0.47828125" x2="8.56488125" y2="0.48691875" layer="37"/>
<rectangle x1="0.0127" y1="0.48691875" x2="0.23368125" y2="0.4953" layer="37"/>
<rectangle x1="0.9271" y1="0.48691875" x2="1.14808125" y2="0.4953" layer="37"/>
<rectangle x1="1.2827" y1="0.48691875" x2="1.50368125" y2="0.4953" layer="37"/>
<rectangle x1="2.1971" y1="0.48691875" x2="2.41808125" y2="0.4953" layer="37"/>
<rectangle x1="2.56031875" y1="0.48691875" x2="2.7813" y2="0.4953" layer="37"/>
<rectangle x1="3.47471875" y1="0.48691875" x2="3.6957" y2="0.4953" layer="37"/>
<rectangle x1="3.84048125" y1="0.48691875" x2="4.05891875" y2="0.4953" layer="37"/>
<rectangle x1="4.75488125" y1="0.48691875" x2="4.97331875" y2="0.4953" layer="37"/>
<rectangle x1="5.56768125" y1="0.48691875" x2="5.78611875" y2="0.4953" layer="37"/>
<rectangle x1="6.54811875" y1="0.48691875" x2="6.7691" y2="0.4953" layer="37"/>
<rectangle x1="7.10691875" y1="0.48691875" x2="7.3279" y2="0.4953" layer="37"/>
<rectangle x1="8.02131875" y1="0.48691875" x2="8.2423" y2="0.4953" layer="37"/>
<rectangle x1="8.3439" y1="0.48691875" x2="8.56488125" y2="0.4953" layer="37"/>
<rectangle x1="0.0127" y1="0.4953" x2="0.23368125" y2="0.50368125" layer="37"/>
<rectangle x1="0.9271" y1="0.4953" x2="1.14808125" y2="0.50368125" layer="37"/>
<rectangle x1="1.2827" y1="0.4953" x2="1.50368125" y2="0.50368125" layer="37"/>
<rectangle x1="2.1971" y1="0.4953" x2="2.41808125" y2="0.50368125" layer="37"/>
<rectangle x1="2.56031875" y1="0.4953" x2="2.7813" y2="0.50368125" layer="37"/>
<rectangle x1="3.47471875" y1="0.4953" x2="3.6957" y2="0.50368125" layer="37"/>
<rectangle x1="3.84048125" y1="0.4953" x2="4.05891875" y2="0.50368125" layer="37"/>
<rectangle x1="4.75488125" y1="0.4953" x2="4.97331875" y2="0.50368125" layer="37"/>
<rectangle x1="5.56768125" y1="0.4953" x2="5.78611875" y2="0.50368125" layer="37"/>
<rectangle x1="6.54811875" y1="0.4953" x2="6.7691" y2="0.50368125" layer="37"/>
<rectangle x1="7.10691875" y1="0.4953" x2="7.3279" y2="0.50368125" layer="37"/>
<rectangle x1="8.02131875" y1="0.4953" x2="8.2423" y2="0.50368125" layer="37"/>
<rectangle x1="8.3439" y1="0.4953" x2="8.56488125" y2="0.50368125" layer="37"/>
<rectangle x1="0.0127" y1="0.50368125" x2="0.23368125" y2="0.51231875" layer="37"/>
<rectangle x1="0.9271" y1="0.50368125" x2="1.14808125" y2="0.51231875" layer="37"/>
<rectangle x1="1.2827" y1="0.50368125" x2="1.50368125" y2="0.51231875" layer="37"/>
<rectangle x1="2.1971" y1="0.50368125" x2="2.41808125" y2="0.51231875" layer="37"/>
<rectangle x1="2.56031875" y1="0.50368125" x2="2.7813" y2="0.51231875" layer="37"/>
<rectangle x1="3.47471875" y1="0.50368125" x2="3.6957" y2="0.51231875" layer="37"/>
<rectangle x1="3.84048125" y1="0.50368125" x2="4.05891875" y2="0.51231875" layer="37"/>
<rectangle x1="4.75488125" y1="0.50368125" x2="4.97331875" y2="0.51231875" layer="37"/>
<rectangle x1="5.56768125" y1="0.50368125" x2="5.78611875" y2="0.51231875" layer="37"/>
<rectangle x1="6.54811875" y1="0.50368125" x2="6.7691" y2="0.51231875" layer="37"/>
<rectangle x1="7.10691875" y1="0.50368125" x2="7.3279" y2="0.51231875" layer="37"/>
<rectangle x1="8.02131875" y1="0.50368125" x2="8.2423" y2="0.51231875" layer="37"/>
<rectangle x1="8.3439" y1="0.50368125" x2="8.56488125" y2="0.51231875" layer="37"/>
<rectangle x1="9.31671875" y1="0.50368125" x2="9.4107" y2="0.51231875" layer="37"/>
<rectangle x1="0.0127" y1="0.51231875" x2="0.23368125" y2="0.5207" layer="37"/>
<rectangle x1="0.9271" y1="0.51231875" x2="1.14808125" y2="0.5207" layer="37"/>
<rectangle x1="1.2827" y1="0.51231875" x2="1.50368125" y2="0.5207" layer="37"/>
<rectangle x1="2.1971" y1="0.51231875" x2="2.41808125" y2="0.5207" layer="37"/>
<rectangle x1="2.56031875" y1="0.51231875" x2="2.7813" y2="0.5207" layer="37"/>
<rectangle x1="3.47471875" y1="0.51231875" x2="3.6957" y2="0.5207" layer="37"/>
<rectangle x1="3.84048125" y1="0.51231875" x2="4.05891875" y2="0.5207" layer="37"/>
<rectangle x1="4.75488125" y1="0.51231875" x2="4.97331875" y2="0.5207" layer="37"/>
<rectangle x1="5.56768125" y1="0.51231875" x2="5.78611875" y2="0.5207" layer="37"/>
<rectangle x1="6.54811875" y1="0.51231875" x2="6.7691" y2="0.5207" layer="37"/>
<rectangle x1="7.10691875" y1="0.51231875" x2="7.3279" y2="0.5207" layer="37"/>
<rectangle x1="8.02131875" y1="0.51231875" x2="8.2423" y2="0.5207" layer="37"/>
<rectangle x1="8.3439" y1="0.51231875" x2="8.56488125" y2="0.5207" layer="37"/>
<rectangle x1="9.2837" y1="0.51231875" x2="9.44371875" y2="0.5207" layer="37"/>
<rectangle x1="0.0127" y1="0.5207" x2="0.23368125" y2="0.52908125" layer="37"/>
<rectangle x1="0.9271" y1="0.5207" x2="1.14808125" y2="0.52908125" layer="37"/>
<rectangle x1="1.2827" y1="0.5207" x2="1.50368125" y2="0.52908125" layer="37"/>
<rectangle x1="2.1971" y1="0.5207" x2="2.41808125" y2="0.52908125" layer="37"/>
<rectangle x1="2.56031875" y1="0.5207" x2="2.7813" y2="0.52908125" layer="37"/>
<rectangle x1="3.47471875" y1="0.5207" x2="3.6957" y2="0.52908125" layer="37"/>
<rectangle x1="3.84048125" y1="0.5207" x2="4.05891875" y2="0.52908125" layer="37"/>
<rectangle x1="4.75488125" y1="0.5207" x2="4.97331875" y2="0.52908125" layer="37"/>
<rectangle x1="5.56768125" y1="0.5207" x2="5.78611875" y2="0.52908125" layer="37"/>
<rectangle x1="6.54811875" y1="0.5207" x2="6.7691" y2="0.52908125" layer="37"/>
<rectangle x1="7.10691875" y1="0.5207" x2="7.3279" y2="0.52908125" layer="37"/>
<rectangle x1="8.02131875" y1="0.5207" x2="8.2423" y2="0.52908125" layer="37"/>
<rectangle x1="8.3439" y1="0.5207" x2="8.56488125" y2="0.52908125" layer="37"/>
<rectangle x1="9.27608125" y1="0.5207" x2="9.4615" y2="0.52908125" layer="37"/>
<rectangle x1="0.0127" y1="0.52908125" x2="0.23368125" y2="0.53771875" layer="37"/>
<rectangle x1="0.9271" y1="0.52908125" x2="1.14808125" y2="0.53771875" layer="37"/>
<rectangle x1="1.2827" y1="0.52908125" x2="1.50368125" y2="0.53771875" layer="37"/>
<rectangle x1="2.1971" y1="0.52908125" x2="2.41808125" y2="0.53771875" layer="37"/>
<rectangle x1="2.56031875" y1="0.52908125" x2="2.7813" y2="0.53771875" layer="37"/>
<rectangle x1="3.47471875" y1="0.52908125" x2="3.6957" y2="0.53771875" layer="37"/>
<rectangle x1="3.84048125" y1="0.52908125" x2="4.05891875" y2="0.53771875" layer="37"/>
<rectangle x1="4.75488125" y1="0.52908125" x2="4.97331875" y2="0.53771875" layer="37"/>
<rectangle x1="5.56768125" y1="0.52908125" x2="5.78611875" y2="0.53771875" layer="37"/>
<rectangle x1="6.54811875" y1="0.52908125" x2="6.7691" y2="0.53771875" layer="37"/>
<rectangle x1="7.10691875" y1="0.52908125" x2="7.3279" y2="0.53771875" layer="37"/>
<rectangle x1="8.02131875" y1="0.52908125" x2="8.2423" y2="0.53771875" layer="37"/>
<rectangle x1="8.3439" y1="0.52908125" x2="8.56488125" y2="0.53771875" layer="37"/>
<rectangle x1="9.26591875" y1="0.52908125" x2="9.46911875" y2="0.53771875" layer="37"/>
<rectangle x1="0.0127" y1="0.53771875" x2="0.23368125" y2="0.5461" layer="37"/>
<rectangle x1="0.9271" y1="0.53771875" x2="1.14808125" y2="0.5461" layer="37"/>
<rectangle x1="1.2827" y1="0.53771875" x2="1.50368125" y2="0.5461" layer="37"/>
<rectangle x1="2.1971" y1="0.53771875" x2="2.41808125" y2="0.5461" layer="37"/>
<rectangle x1="2.56031875" y1="0.53771875" x2="2.7813" y2="0.5461" layer="37"/>
<rectangle x1="3.47471875" y1="0.53771875" x2="3.6957" y2="0.5461" layer="37"/>
<rectangle x1="3.84048125" y1="0.53771875" x2="4.05891875" y2="0.5461" layer="37"/>
<rectangle x1="4.75488125" y1="0.53771875" x2="4.97331875" y2="0.5461" layer="37"/>
<rectangle x1="5.56768125" y1="0.53771875" x2="5.78611875" y2="0.5461" layer="37"/>
<rectangle x1="6.54811875" y1="0.53771875" x2="6.7691" y2="0.5461" layer="37"/>
<rectangle x1="7.10691875" y1="0.53771875" x2="7.3279" y2="0.5461" layer="37"/>
<rectangle x1="8.02131875" y1="0.53771875" x2="8.2423" y2="0.5461" layer="37"/>
<rectangle x1="8.3439" y1="0.53771875" x2="8.56488125" y2="0.5461" layer="37"/>
<rectangle x1="9.2583" y1="0.53771875" x2="9.47928125" y2="0.5461" layer="37"/>
<rectangle x1="0.0127" y1="0.5461" x2="0.23368125" y2="0.55448125" layer="37"/>
<rectangle x1="0.9271" y1="0.5461" x2="1.14808125" y2="0.55448125" layer="37"/>
<rectangle x1="1.2827" y1="0.5461" x2="1.50368125" y2="0.55448125" layer="37"/>
<rectangle x1="2.1971" y1="0.5461" x2="2.41808125" y2="0.55448125" layer="37"/>
<rectangle x1="2.56031875" y1="0.5461" x2="2.7813" y2="0.55448125" layer="37"/>
<rectangle x1="3.47471875" y1="0.5461" x2="3.6957" y2="0.55448125" layer="37"/>
<rectangle x1="3.84048125" y1="0.5461" x2="4.05891875" y2="0.55448125" layer="37"/>
<rectangle x1="4.75488125" y1="0.5461" x2="4.97331875" y2="0.55448125" layer="37"/>
<rectangle x1="5.56768125" y1="0.5461" x2="5.78611875" y2="0.55448125" layer="37"/>
<rectangle x1="6.54811875" y1="0.5461" x2="6.7691" y2="0.55448125" layer="37"/>
<rectangle x1="7.10691875" y1="0.5461" x2="7.3279" y2="0.55448125" layer="37"/>
<rectangle x1="8.02131875" y1="0.5461" x2="8.2423" y2="0.55448125" layer="37"/>
<rectangle x1="8.3439" y1="0.5461" x2="8.56488125" y2="0.55448125" layer="37"/>
<rectangle x1="9.2583" y1="0.5461" x2="9.47928125" y2="0.55448125" layer="37"/>
<rectangle x1="0.0127" y1="0.55448125" x2="0.23368125" y2="0.56311875" layer="37"/>
<rectangle x1="0.9271" y1="0.55448125" x2="1.14808125" y2="0.56311875" layer="37"/>
<rectangle x1="1.2827" y1="0.55448125" x2="1.50368125" y2="0.56311875" layer="37"/>
<rectangle x1="2.1971" y1="0.55448125" x2="2.41808125" y2="0.56311875" layer="37"/>
<rectangle x1="2.56031875" y1="0.55448125" x2="2.7813" y2="0.56311875" layer="37"/>
<rectangle x1="3.47471875" y1="0.55448125" x2="3.6957" y2="0.56311875" layer="37"/>
<rectangle x1="3.84048125" y1="0.55448125" x2="4.05891875" y2="0.56311875" layer="37"/>
<rectangle x1="4.75488125" y1="0.55448125" x2="4.97331875" y2="0.56311875" layer="37"/>
<rectangle x1="5.56768125" y1="0.55448125" x2="5.78611875" y2="0.56311875" layer="37"/>
<rectangle x1="6.54811875" y1="0.55448125" x2="6.7691" y2="0.56311875" layer="37"/>
<rectangle x1="7.10691875" y1="0.55448125" x2="7.3279" y2="0.56311875" layer="37"/>
<rectangle x1="8.02131875" y1="0.55448125" x2="8.2423" y2="0.56311875" layer="37"/>
<rectangle x1="8.3439" y1="0.55448125" x2="8.56488125" y2="0.56311875" layer="37"/>
<rectangle x1="9.2583" y1="0.55448125" x2="9.47928125" y2="0.56311875" layer="37"/>
<rectangle x1="0.0127" y1="0.56311875" x2="0.23368125" y2="0.5715" layer="37"/>
<rectangle x1="0.9271" y1="0.56311875" x2="1.14808125" y2="0.5715" layer="37"/>
<rectangle x1="1.2827" y1="0.56311875" x2="1.50368125" y2="0.5715" layer="37"/>
<rectangle x1="2.1971" y1="0.56311875" x2="2.41808125" y2="0.5715" layer="37"/>
<rectangle x1="2.56031875" y1="0.56311875" x2="2.7813" y2="0.5715" layer="37"/>
<rectangle x1="3.47471875" y1="0.56311875" x2="3.6957" y2="0.5715" layer="37"/>
<rectangle x1="3.84048125" y1="0.56311875" x2="4.05891875" y2="0.5715" layer="37"/>
<rectangle x1="4.75488125" y1="0.56311875" x2="4.97331875" y2="0.5715" layer="37"/>
<rectangle x1="5.56768125" y1="0.56311875" x2="5.78611875" y2="0.5715" layer="37"/>
<rectangle x1="6.54811875" y1="0.56311875" x2="6.7691" y2="0.5715" layer="37"/>
<rectangle x1="7.10691875" y1="0.56311875" x2="7.3279" y2="0.5715" layer="37"/>
<rectangle x1="8.02131875" y1="0.56311875" x2="8.2423" y2="0.5715" layer="37"/>
<rectangle x1="8.3439" y1="0.56311875" x2="8.56488125" y2="0.5715" layer="37"/>
<rectangle x1="9.25068125" y1="0.56311875" x2="9.47928125" y2="0.5715" layer="37"/>
<rectangle x1="0.0127" y1="0.5715" x2="0.23368125" y2="0.57988125" layer="37"/>
<rectangle x1="0.9271" y1="0.5715" x2="1.14808125" y2="0.57988125" layer="37"/>
<rectangle x1="1.2827" y1="0.5715" x2="1.50368125" y2="0.57988125" layer="37"/>
<rectangle x1="2.1971" y1="0.5715" x2="2.41808125" y2="0.57988125" layer="37"/>
<rectangle x1="2.56031875" y1="0.5715" x2="2.7813" y2="0.57988125" layer="37"/>
<rectangle x1="3.47471875" y1="0.5715" x2="3.6957" y2="0.57988125" layer="37"/>
<rectangle x1="3.84048125" y1="0.5715" x2="4.05891875" y2="0.57988125" layer="37"/>
<rectangle x1="4.74471875" y1="0.5715" x2="4.97331875" y2="0.57988125" layer="37"/>
<rectangle x1="5.56768125" y1="0.5715" x2="5.78611875" y2="0.57988125" layer="37"/>
<rectangle x1="6.54811875" y1="0.5715" x2="6.7691" y2="0.57988125" layer="37"/>
<rectangle x1="7.10691875" y1="0.5715" x2="7.3279" y2="0.57988125" layer="37"/>
<rectangle x1="8.02131875" y1="0.5715" x2="8.2423" y2="0.57988125" layer="37"/>
<rectangle x1="8.3439" y1="0.5715" x2="8.56488125" y2="0.57988125" layer="37"/>
<rectangle x1="9.25068125" y1="0.5715" x2="9.47928125" y2="0.57988125" layer="37"/>
<rectangle x1="0.0127" y1="0.57988125" x2="0.23368125" y2="0.58851875" layer="37"/>
<rectangle x1="0.91948125" y1="0.57988125" x2="1.14808125" y2="0.58851875" layer="37"/>
<rectangle x1="1.2827" y1="0.57988125" x2="1.5113" y2="0.58851875" layer="37"/>
<rectangle x1="2.18948125" y1="0.57988125" x2="2.41808125" y2="0.58851875" layer="37"/>
<rectangle x1="2.56031875" y1="0.57988125" x2="2.7813" y2="0.58851875" layer="37"/>
<rectangle x1="3.45948125" y1="0.57988125" x2="3.6957" y2="0.58851875" layer="37"/>
<rectangle x1="3.84048125" y1="0.57988125" x2="4.06908125" y2="0.58851875" layer="37"/>
<rectangle x1="4.7371" y1="0.57988125" x2="4.97331875" y2="0.58851875" layer="37"/>
<rectangle x1="5.56768125" y1="0.57988125" x2="5.78611875" y2="0.58851875" layer="37"/>
<rectangle x1="6.54811875" y1="0.57988125" x2="6.7691" y2="0.58851875" layer="37"/>
<rectangle x1="7.10691875" y1="0.57988125" x2="7.34568125" y2="0.58851875" layer="37"/>
<rectangle x1="8.0137" y1="0.57988125" x2="8.2423" y2="0.58851875" layer="37"/>
<rectangle x1="8.3439" y1="0.57988125" x2="8.5725" y2="0.58851875" layer="37"/>
<rectangle x1="9.24051875" y1="0.57988125" x2="9.47928125" y2="0.58851875" layer="37"/>
<rectangle x1="0.0127" y1="0.58851875" x2="0.23368125" y2="0.5969" layer="37"/>
<rectangle x1="0.89408125" y1="0.58851875" x2="1.14808125" y2="0.5969" layer="37"/>
<rectangle x1="1.2827" y1="0.58851875" x2="1.52908125" y2="0.5969" layer="37"/>
<rectangle x1="2.16408125" y1="0.58851875" x2="2.41808125" y2="0.5969" layer="37"/>
<rectangle x1="2.56031875" y1="0.58851875" x2="2.7813" y2="0.5969" layer="37"/>
<rectangle x1="3.4417" y1="0.58851875" x2="3.6957" y2="0.5969" layer="37"/>
<rectangle x1="3.84048125" y1="0.58851875" x2="4.08431875" y2="0.5969" layer="37"/>
<rectangle x1="4.71931875" y1="0.58851875" x2="4.9657" y2="0.5969" layer="37"/>
<rectangle x1="5.56768125" y1="0.58851875" x2="5.78611875" y2="0.5969" layer="37"/>
<rectangle x1="6.54811875" y1="0.58851875" x2="6.7691" y2="0.5969" layer="37"/>
<rectangle x1="7.10691875" y1="0.58851875" x2="7.36091875" y2="0.5969" layer="37"/>
<rectangle x1="7.99591875" y1="0.58851875" x2="8.2423" y2="0.5969" layer="37"/>
<rectangle x1="8.3439" y1="0.58851875" x2="8.59028125" y2="0.5969" layer="37"/>
<rectangle x1="9.22528125" y1="0.58851875" x2="9.47928125" y2="0.5969" layer="37"/>
<rectangle x1="0.0127" y1="0.5969" x2="0.23368125" y2="0.60528125" layer="37"/>
<rectangle x1="0.86868125" y1="0.5969" x2="1.13791875" y2="0.60528125" layer="37"/>
<rectangle x1="1.2827" y1="0.5969" x2="1.55448125" y2="0.60528125" layer="37"/>
<rectangle x1="2.13868125" y1="0.5969" x2="2.40791875" y2="0.60528125" layer="37"/>
<rectangle x1="2.56031875" y1="0.5969" x2="2.7813" y2="0.60528125" layer="37"/>
<rectangle x1="3.4163" y1="0.5969" x2="3.68808125" y2="0.60528125" layer="37"/>
<rectangle x1="3.84048125" y1="0.5969" x2="4.10971875" y2="0.60528125" layer="37"/>
<rectangle x1="4.69391875" y1="0.5969" x2="4.9657" y2="0.60528125" layer="37"/>
<rectangle x1="5.56768125" y1="0.5969" x2="5.78611875" y2="0.60528125" layer="37"/>
<rectangle x1="6.54811875" y1="0.5969" x2="6.7691" y2="0.60528125" layer="37"/>
<rectangle x1="7.11708125" y1="0.5969" x2="7.3787" y2="0.60528125" layer="37"/>
<rectangle x1="7.97051875" y1="0.5969" x2="8.2423" y2="0.60528125" layer="37"/>
<rectangle x1="8.3439" y1="0.5969" x2="8.61568125" y2="0.60528125" layer="37"/>
<rectangle x1="9.19988125" y1="0.5969" x2="9.46911875" y2="0.60528125" layer="37"/>
<rectangle x1="0.0127" y1="0.60528125" x2="1.13791875" y2="0.61391875" layer="37"/>
<rectangle x1="1.29031875" y1="0.60528125" x2="2.40791875" y2="0.61391875" layer="37"/>
<rectangle x1="2.56031875" y1="0.60528125" x2="3.68808125" y2="0.61391875" layer="37"/>
<rectangle x1="3.8481" y1="0.60528125" x2="4.95808125" y2="0.61391875" layer="37"/>
<rectangle x1="5.17651875" y1="0.60528125" x2="6.1849" y2="0.61391875" layer="37"/>
<rectangle x1="6.38048125" y1="0.60528125" x2="6.92911875" y2="0.61391875" layer="37"/>
<rectangle x1="7.1247" y1="0.60528125" x2="8.23468125" y2="0.61391875" layer="37"/>
<rectangle x1="8.35151875" y1="0.60528125" x2="9.4615" y2="0.61391875" layer="37"/>
<rectangle x1="0.0127" y1="0.61391875" x2="1.1303" y2="0.6223" layer="37"/>
<rectangle x1="1.30048125" y1="0.61391875" x2="2.4003" y2="0.6223" layer="37"/>
<rectangle x1="2.56031875" y1="0.61391875" x2="3.67791875" y2="0.6223" layer="37"/>
<rectangle x1="3.85571875" y1="0.61391875" x2="4.94791875" y2="0.6223" layer="37"/>
<rectangle x1="5.15111875" y1="0.61391875" x2="6.2103" y2="0.6223" layer="37"/>
<rectangle x1="6.35508125" y1="0.61391875" x2="6.95451875" y2="0.6223" layer="37"/>
<rectangle x1="7.13231875" y1="0.61391875" x2="8.22451875" y2="0.6223" layer="37"/>
<rectangle x1="8.36168125" y1="0.61391875" x2="9.45388125" y2="0.6223" layer="37"/>
<rectangle x1="0.0127" y1="0.6223" x2="1.12268125" y2="0.63068125" layer="37"/>
<rectangle x1="1.31571875" y1="0.6223" x2="2.38251875" y2="0.63068125" layer="37"/>
<rectangle x1="2.56031875" y1="0.6223" x2="3.66268125" y2="0.63068125" layer="37"/>
<rectangle x1="3.86588125" y1="0.6223" x2="4.9403" y2="0.63068125" layer="37"/>
<rectangle x1="5.13588125" y1="0.6223" x2="6.22808125" y2="0.63068125" layer="37"/>
<rectangle x1="6.34491875" y1="0.6223" x2="6.9723" y2="0.63068125" layer="37"/>
<rectangle x1="7.14248125" y1="0.6223" x2="8.2169" y2="0.63068125" layer="37"/>
<rectangle x1="8.3693" y1="0.6223" x2="9.44371875" y2="0.63068125" layer="37"/>
<rectangle x1="0.0127" y1="0.63068125" x2="1.1049" y2="0.63931875" layer="37"/>
<rectangle x1="1.32588125" y1="0.63068125" x2="2.3749" y2="0.63931875" layer="37"/>
<rectangle x1="2.56031875" y1="0.63068125" x2="3.65251875" y2="0.63931875" layer="37"/>
<rectangle x1="3.88111875" y1="0.63068125" x2="4.93268125" y2="0.63931875" layer="37"/>
<rectangle x1="5.12571875" y1="0.63068125" x2="6.2357" y2="0.63931875" layer="37"/>
<rectangle x1="6.32968125" y1="0.63068125" x2="6.97991875" y2="0.63931875" layer="37"/>
<rectangle x1="7.1501" y1="0.63068125" x2="8.20928125" y2="0.63931875" layer="37"/>
<rectangle x1="8.37691875" y1="0.63068125" x2="9.4361" y2="0.63931875" layer="37"/>
<rectangle x1="0.0127" y1="0.63931875" x2="1.08711875" y2="0.6477" layer="37"/>
<rectangle x1="1.34111875" y1="0.63931875" x2="2.35711875" y2="0.6477" layer="37"/>
<rectangle x1="2.56031875" y1="0.63931875" x2="3.63728125" y2="0.6477" layer="37"/>
<rectangle x1="3.89128125" y1="0.63931875" x2="4.9149" y2="0.6477" layer="37"/>
<rectangle x1="5.1181" y1="0.63931875" x2="6.24331875" y2="0.6477" layer="37"/>
<rectangle x1="6.31951875" y1="0.63931875" x2="6.99008125" y2="0.6477" layer="37"/>
<rectangle x1="7.16788125" y1="0.63931875" x2="8.1915" y2="0.6477" layer="37"/>
<rectangle x1="8.3947" y1="0.63931875" x2="9.41831875" y2="0.6477" layer="37"/>
<rectangle x1="0.0127" y1="0.6477" x2="1.07188125" y2="0.65608125" layer="37"/>
<rectangle x1="1.3589" y1="0.6477" x2="2.34188125" y2="0.65608125" layer="37"/>
<rectangle x1="2.56031875" y1="0.6477" x2="3.6195" y2="0.65608125" layer="37"/>
<rectangle x1="3.90651875" y1="0.6477" x2="4.89711875" y2="0.65608125" layer="37"/>
<rectangle x1="5.1181" y1="0.6477" x2="6.24331875" y2="0.65608125" layer="37"/>
<rectangle x1="6.31951875" y1="0.6477" x2="6.9977" y2="0.65608125" layer="37"/>
<rectangle x1="7.18311875" y1="0.6477" x2="8.17371875" y2="0.65608125" layer="37"/>
<rectangle x1="8.41248125" y1="0.6477" x2="9.40308125" y2="0.65608125" layer="37"/>
<rectangle x1="0.0127" y1="0.65608125" x2="1.0541" y2="0.66471875" layer="37"/>
<rectangle x1="1.37668125" y1="0.65608125" x2="2.3241" y2="0.66471875" layer="37"/>
<rectangle x1="2.56031875" y1="0.65608125" x2="3.60171875" y2="0.66471875" layer="37"/>
<rectangle x1="3.9243" y1="0.65608125" x2="4.88188125" y2="0.66471875" layer="37"/>
<rectangle x1="5.1181" y1="0.65608125" x2="6.24331875" y2="0.66471875" layer="37"/>
<rectangle x1="6.31951875" y1="0.65608125" x2="6.9977" y2="0.66471875" layer="37"/>
<rectangle x1="7.2009" y1="0.65608125" x2="8.15848125" y2="0.66471875" layer="37"/>
<rectangle x1="8.42771875" y1="0.65608125" x2="9.3853" y2="0.66471875" layer="37"/>
<rectangle x1="0.0127" y1="0.66471875" x2="1.03631875" y2="0.6731" layer="37"/>
<rectangle x1="1.39191875" y1="0.66471875" x2="2.30631875" y2="0.6731" layer="37"/>
<rectangle x1="2.56031875" y1="0.66471875" x2="3.58648125" y2="0.6731" layer="37"/>
<rectangle x1="3.9497" y1="0.66471875" x2="4.8641" y2="0.6731" layer="37"/>
<rectangle x1="5.1181" y1="0.66471875" x2="6.24331875" y2="0.6731" layer="37"/>
<rectangle x1="6.31951875" y1="0.66471875" x2="6.9977" y2="0.6731" layer="37"/>
<rectangle x1="7.21868125" y1="0.66471875" x2="8.13308125" y2="0.6731" layer="37"/>
<rectangle x1="8.45311875" y1="0.66471875" x2="9.36751875" y2="0.6731" layer="37"/>
<rectangle x1="0.02031875" y1="0.6731" x2="1.01091875" y2="0.68148125" layer="37"/>
<rectangle x1="1.41731875" y1="0.6731" x2="2.28091875" y2="0.68148125" layer="37"/>
<rectangle x1="2.56031875" y1="0.6731" x2="3.56108125" y2="0.68148125" layer="37"/>
<rectangle x1="3.9751" y1="0.6731" x2="4.8387" y2="0.68148125" layer="37"/>
<rectangle x1="5.12571875" y1="0.6731" x2="6.2357" y2="0.68148125" layer="37"/>
<rectangle x1="6.32968125" y1="0.6731" x2="6.99008125" y2="0.68148125" layer="37"/>
<rectangle x1="7.24408125" y1="0.6731" x2="8.10768125" y2="0.68148125" layer="37"/>
<rectangle x1="8.47851875" y1="0.6731" x2="9.34211875" y2="0.68148125" layer="37"/>
<rectangle x1="0.03048125" y1="0.68148125" x2="0.98551875" y2="0.69011875" layer="37"/>
<rectangle x1="1.44271875" y1="0.68148125" x2="2.25551875" y2="0.69011875" layer="37"/>
<rectangle x1="2.5781" y1="0.68148125" x2="3.53568125" y2="0.69011875" layer="37"/>
<rectangle x1="4.0005" y1="0.68148125" x2="4.8133" y2="0.69011875" layer="37"/>
<rectangle x1="5.13588125" y1="0.68148125" x2="6.22808125" y2="0.69011875" layer="37"/>
<rectangle x1="6.3373" y1="0.68148125" x2="6.9723" y2="0.69011875" layer="37"/>
<rectangle x1="7.26948125" y1="0.68148125" x2="8.08228125" y2="0.69011875" layer="37"/>
<rectangle x1="8.50391875" y1="0.68148125" x2="9.31671875" y2="0.69011875" layer="37"/>
<rectangle x1="0.04571875" y1="0.69011875" x2="0.9525" y2="0.6985" layer="37"/>
<rectangle x1="1.47828125" y1="0.69011875" x2="2.2225" y2="0.6985" layer="37"/>
<rectangle x1="2.59588125" y1="0.69011875" x2="3.50011875" y2="0.6985" layer="37"/>
<rectangle x1="4.0259" y1="0.69011875" x2="4.78028125" y2="0.6985" layer="37"/>
<rectangle x1="5.1435" y1="0.69011875" x2="6.21791875" y2="0.6985" layer="37"/>
<rectangle x1="6.34491875" y1="0.69011875" x2="6.95451875" y2="0.6985" layer="37"/>
<rectangle x1="7.3025" y1="0.69011875" x2="8.05688125" y2="0.6985" layer="37"/>
<rectangle x1="8.52931875" y1="0.69011875" x2="9.29131875" y2="0.6985" layer="37"/>
<rectangle x1="0.08128125" y1="0.6985" x2="0.91948125" y2="0.70688125" layer="37"/>
<rectangle x1="1.5113" y1="0.6985" x2="2.18948125" y2="0.70688125" layer="37"/>
<rectangle x1="2.62128125" y1="0.6985" x2="3.45948125" y2="0.70688125" layer="37"/>
<rectangle x1="4.06908125" y1="0.6985" x2="4.7371" y2="0.70688125" layer="37"/>
<rectangle x1="5.1689" y1="0.6985" x2="6.19251875" y2="0.70688125" layer="37"/>
<rectangle x1="6.37031875" y1="0.6985" x2="6.92911875" y2="0.70688125" layer="37"/>
<rectangle x1="7.34568125" y1="0.6985" x2="8.0137" y2="0.70688125" layer="37"/>
<rectangle x1="8.5725" y1="0.6985" x2="9.25068125" y2="0.70688125" layer="37"/>
<rectangle x1="0.10668125" y1="0.89331875" x2="1.7907" y2="0.9017" layer="37"/>
<rectangle x1="2.07771875" y1="0.89331875" x2="3.14451875" y2="0.9017" layer="37"/>
<rectangle x1="4.2291" y1="0.89331875" x2="4.37388125" y2="0.9017" layer="37"/>
<rectangle x1="5.7531" y1="0.89331875" x2="5.88771875" y2="0.9017" layer="37"/>
<rectangle x1="6.3627" y1="0.89331875" x2="7.29488125" y2="0.9017" layer="37"/>
<rectangle x1="7.7089" y1="0.89331875" x2="7.82828125" y2="0.9017" layer="37"/>
<rectangle x1="9.2583" y1="0.89331875" x2="9.37768125" y2="0.9017" layer="37"/>
<rectangle x1="0.08128125" y1="0.9017" x2="1.8161" y2="0.91008125" layer="37"/>
<rectangle x1="2.05231875" y1="0.9017" x2="3.19531875" y2="0.91008125" layer="37"/>
<rectangle x1="4.2037" y1="0.9017" x2="4.38911875" y2="0.91008125" layer="37"/>
<rectangle x1="5.7277" y1="0.9017" x2="5.92328125" y2="0.91008125" layer="37"/>
<rectangle x1="6.3373" y1="0.9017" x2="7.32028125" y2="0.91008125" layer="37"/>
<rectangle x1="7.66571875" y1="0.9017" x2="7.86891875" y2="0.91008125" layer="37"/>
<rectangle x1="9.21511875" y1="0.9017" x2="9.4107" y2="0.91008125" layer="37"/>
<rectangle x1="0.0635" y1="0.91008125" x2="1.83388125" y2="0.91871875" layer="37"/>
<rectangle x1="2.03708125" y1="0.91008125" x2="3.23088125" y2="0.91871875" layer="37"/>
<rectangle x1="4.1783" y1="0.91008125" x2="4.39928125" y2="0.91871875" layer="37"/>
<rectangle x1="5.72008125" y1="0.91008125" x2="5.93851875" y2="0.91871875" layer="37"/>
<rectangle x1="6.32968125" y1="0.91008125" x2="7.33551875" y2="0.91871875" layer="37"/>
<rectangle x1="7.65048125" y1="0.91008125" x2="7.89431875" y2="0.91871875" layer="37"/>
<rectangle x1="9.19988125" y1="0.91008125" x2="9.4361" y2="0.91871875" layer="37"/>
<rectangle x1="0.05588125" y1="0.91871875" x2="1.84911875" y2="0.9271" layer="37"/>
<rectangle x1="2.02691875" y1="0.91871875" x2="3.2639" y2="0.9271" layer="37"/>
<rectangle x1="4.16051875" y1="0.91871875" x2="4.41451875" y2="0.9271" layer="37"/>
<rectangle x1="5.7023" y1="0.91871875" x2="5.9563" y2="0.9271" layer="37"/>
<rectangle x1="6.3119" y1="0.91871875" x2="7.34568125" y2="0.9271" layer="37"/>
<rectangle x1="7.6327" y1="0.91871875" x2="7.9121" y2="0.9271" layer="37"/>
<rectangle x1="9.1821" y1="0.91871875" x2="9.44371875" y2="0.9271" layer="37"/>
<rectangle x1="0.0381" y1="0.9271" x2="1.8669" y2="0.93548125" layer="37"/>
<rectangle x1="2.01168125" y1="0.9271" x2="3.2893" y2="0.93548125" layer="37"/>
<rectangle x1="4.14528125" y1="0.9271" x2="4.42468125" y2="0.93548125" layer="37"/>
<rectangle x1="5.68451875" y1="0.9271" x2="5.97408125" y2="0.93548125" layer="37"/>
<rectangle x1="6.30428125" y1="0.9271" x2="7.3533" y2="0.93548125" layer="37"/>
<rectangle x1="7.62508125" y1="0.9271" x2="7.92988125" y2="0.93548125" layer="37"/>
<rectangle x1="9.16431875" y1="0.9271" x2="9.4615" y2="0.93548125" layer="37"/>
<rectangle x1="0.03048125" y1="0.93548125" x2="1.87451875" y2="0.94411875" layer="37"/>
<rectangle x1="2.00151875" y1="0.93548125" x2="3.3147" y2="0.94411875" layer="37"/>
<rectangle x1="4.1275" y1="0.93548125" x2="4.43991875" y2="0.94411875" layer="37"/>
<rectangle x1="5.66928125" y1="0.93548125" x2="5.98931875" y2="0.94411875" layer="37"/>
<rectangle x1="6.29411875" y1="0.93548125" x2="7.37108125" y2="0.94411875" layer="37"/>
<rectangle x1="7.61491875" y1="0.93548125" x2="7.9375" y2="0.94411875" layer="37"/>
<rectangle x1="9.1567" y1="0.93548125" x2="9.46911875" y2="0.94411875" layer="37"/>
<rectangle x1="0.0127" y1="0.94411875" x2="1.88468125" y2="0.9525" layer="37"/>
<rectangle x1="1.9939" y1="0.94411875" x2="3.3401" y2="0.9525" layer="37"/>
<rectangle x1="4.11988125" y1="0.94411875" x2="4.4577" y2="0.9525" layer="37"/>
<rectangle x1="5.65911875" y1="0.94411875" x2="5.99948125" y2="0.9525" layer="37"/>
<rectangle x1="6.2865" y1="0.94411875" x2="7.3787" y2="0.9525" layer="37"/>
<rectangle x1="7.6073" y1="0.94411875" x2="7.94511875" y2="0.9525" layer="37"/>
<rectangle x1="9.14908125" y1="0.94411875" x2="9.47928125" y2="0.9525" layer="37"/>
<rectangle x1="0.00508125" y1="0.9525" x2="1.88468125" y2="0.96088125" layer="37"/>
<rectangle x1="1.97611875" y1="0.9525" x2="3.3655" y2="0.96088125" layer="37"/>
<rectangle x1="4.10971875" y1="0.9525" x2="4.46531875" y2="0.96088125" layer="37"/>
<rectangle x1="5.64388125" y1="0.9525" x2="6.0071" y2="0.96088125" layer="37"/>
<rectangle x1="6.27888125" y1="0.9525" x2="7.3787" y2="0.96088125" layer="37"/>
<rectangle x1="7.59968125" y1="0.9525" x2="7.95528125" y2="0.96088125" layer="37"/>
<rectangle x1="9.13891875" y1="0.9525" x2="9.4869" y2="0.96088125" layer="37"/>
<rectangle x1="0.00508125" y1="0.96088125" x2="1.8923" y2="0.96951875" layer="37"/>
<rectangle x1="1.97611875" y1="0.96088125" x2="3.38328125" y2="0.96951875" layer="37"/>
<rectangle x1="4.10971875" y1="0.96088125" x2="4.4831" y2="0.96951875" layer="37"/>
<rectangle x1="5.6261" y1="0.96088125" x2="6.0071" y2="0.96951875" layer="37"/>
<rectangle x1="6.27888125" y1="0.96088125" x2="7.38631875" y2="0.96951875" layer="37"/>
<rectangle x1="7.59968125" y1="0.96088125" x2="7.95528125" y2="0.96951875" layer="37"/>
<rectangle x1="9.1313" y1="0.96088125" x2="9.4869" y2="0.96951875" layer="37"/>
<rectangle x1="-0.00508125" y1="0.96951875" x2="1.8923" y2="0.9779" layer="37"/>
<rectangle x1="1.9685" y1="0.96951875" x2="3.39851875" y2="0.9779" layer="37"/>
<rectangle x1="4.10971875" y1="0.96951875" x2="4.50088125" y2="0.9779" layer="37"/>
<rectangle x1="5.60831875" y1="0.96951875" x2="6.01471875" y2="0.9779" layer="37"/>
<rectangle x1="6.26871875" y1="0.96951875" x2="7.39648125" y2="0.9779" layer="37"/>
<rectangle x1="7.58951875" y1="0.96951875" x2="7.9629" y2="0.9779" layer="37"/>
<rectangle x1="9.12368125" y1="0.96951875" x2="9.49451875" y2="0.9779" layer="37"/>
<rectangle x1="-0.00508125" y1="0.9779" x2="1.89991875" y2="0.98628125" layer="37"/>
<rectangle x1="1.9685" y1="0.9779" x2="3.42391875" y2="0.98628125" layer="37"/>
<rectangle x1="4.10971875" y1="0.9779" x2="4.51611875" y2="0.98628125" layer="37"/>
<rectangle x1="5.6007" y1="0.9779" x2="6.01471875" y2="0.98628125" layer="37"/>
<rectangle x1="6.2611" y1="0.9779" x2="7.39648125" y2="0.98628125" layer="37"/>
<rectangle x1="7.58951875" y1="0.9779" x2="7.9629" y2="0.98628125" layer="37"/>
<rectangle x1="9.11351875" y1="0.9779" x2="9.49451875" y2="0.98628125" layer="37"/>
<rectangle x1="-0.00508125" y1="0.98628125" x2="1.89991875" y2="0.99491875" layer="37"/>
<rectangle x1="1.9685" y1="0.98628125" x2="3.4417" y2="0.99491875" layer="37"/>
<rectangle x1="4.10971875" y1="0.98628125" x2="4.52628125" y2="0.99491875" layer="37"/>
<rectangle x1="5.58291875" y1="0.98628125" x2="6.01471875" y2="0.99491875" layer="37"/>
<rectangle x1="6.2611" y1="0.98628125" x2="7.4041" y2="0.99491875" layer="37"/>
<rectangle x1="7.58951875" y1="0.98628125" x2="7.9629" y2="0.99491875" layer="37"/>
<rectangle x1="9.1059" y1="0.98628125" x2="9.49451875" y2="0.99491875" layer="37"/>
<rectangle x1="-0.00508125" y1="0.99491875" x2="1.89991875" y2="1.0033" layer="37"/>
<rectangle x1="1.9685" y1="0.99491875" x2="3.45948125" y2="1.0033" layer="37"/>
<rectangle x1="4.10971875" y1="0.99491875" x2="4.54151875" y2="1.0033" layer="37"/>
<rectangle x1="5.5753" y1="0.99491875" x2="6.01471875" y2="1.0033" layer="37"/>
<rectangle x1="6.2611" y1="0.99491875" x2="7.4041" y2="1.0033" layer="37"/>
<rectangle x1="7.58951875" y1="0.99491875" x2="7.9629" y2="1.0033" layer="37"/>
<rectangle x1="9.08811875" y1="0.99491875" x2="9.49451875" y2="1.0033" layer="37"/>
<rectangle x1="-0.00508125" y1="1.0033" x2="1.89991875" y2="1.01168125" layer="37"/>
<rectangle x1="1.9685" y1="1.0033" x2="3.47471875" y2="1.01168125" layer="37"/>
<rectangle x1="4.10971875" y1="1.0033" x2="4.5593" y2="1.01168125" layer="37"/>
<rectangle x1="5.55751875" y1="1.0033" x2="6.01471875" y2="1.01168125" layer="37"/>
<rectangle x1="6.2611" y1="1.0033" x2="7.4041" y2="1.01168125" layer="37"/>
<rectangle x1="7.58951875" y1="1.0033" x2="7.9629" y2="1.01168125" layer="37"/>
<rectangle x1="9.0805" y1="1.0033" x2="9.49451875" y2="1.01168125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.01168125" x2="1.89991875" y2="1.02031875" layer="37"/>
<rectangle x1="1.9685" y1="1.01168125" x2="3.4925" y2="1.02031875" layer="37"/>
<rectangle x1="4.10971875" y1="1.01168125" x2="4.56691875" y2="1.02031875" layer="37"/>
<rectangle x1="5.54228125" y1="1.01168125" x2="6.01471875" y2="1.02031875" layer="37"/>
<rectangle x1="6.2611" y1="1.01168125" x2="7.39648125" y2="1.02031875" layer="37"/>
<rectangle x1="7.58951875" y1="1.01168125" x2="7.9629" y2="1.02031875" layer="37"/>
<rectangle x1="9.07288125" y1="1.01168125" x2="9.49451875" y2="1.02031875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.02031875" x2="1.89991875" y2="1.0287" layer="37"/>
<rectangle x1="1.9685" y1="1.02031875" x2="3.50011875" y2="1.0287" layer="37"/>
<rectangle x1="4.10971875" y1="1.02031875" x2="4.5847" y2="1.0287" layer="37"/>
<rectangle x1="5.53211875" y1="1.02031875" x2="6.01471875" y2="1.0287" layer="37"/>
<rectangle x1="6.2611" y1="1.02031875" x2="7.39648125" y2="1.0287" layer="37"/>
<rectangle x1="7.58951875" y1="1.02031875" x2="7.9629" y2="1.0287" layer="37"/>
<rectangle x1="9.06271875" y1="1.02031875" x2="9.49451875" y2="1.0287" layer="37"/>
<rectangle x1="-0.00508125" y1="1.0287" x2="1.8923" y2="1.03708125" layer="37"/>
<rectangle x1="1.9685" y1="1.0287" x2="3.5179" y2="1.03708125" layer="37"/>
<rectangle x1="4.10971875" y1="1.0287" x2="4.60248125" y2="1.03708125" layer="37"/>
<rectangle x1="5.51688125" y1="1.0287" x2="6.01471875" y2="1.03708125" layer="37"/>
<rectangle x1="6.26871875" y1="1.0287" x2="7.39648125" y2="1.03708125" layer="37"/>
<rectangle x1="7.58951875" y1="1.0287" x2="7.9629" y2="1.03708125" layer="37"/>
<rectangle x1="9.0551" y1="1.0287" x2="9.49451875" y2="1.03708125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.03708125" x2="1.8923" y2="1.04571875" layer="37"/>
<rectangle x1="1.9685" y1="1.03708125" x2="3.53568125" y2="1.04571875" layer="37"/>
<rectangle x1="4.10971875" y1="1.03708125" x2="4.6101" y2="1.04571875" layer="37"/>
<rectangle x1="5.4991" y1="1.03708125" x2="6.01471875" y2="1.04571875" layer="37"/>
<rectangle x1="6.27888125" y1="1.03708125" x2="7.38631875" y2="1.04571875" layer="37"/>
<rectangle x1="7.58951875" y1="1.03708125" x2="7.9629" y2="1.04571875" layer="37"/>
<rectangle x1="9.04748125" y1="1.03708125" x2="9.49451875" y2="1.04571875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.04571875" x2="1.88468125" y2="1.0541" layer="37"/>
<rectangle x1="1.9685" y1="1.04571875" x2="3.55091875" y2="1.0541" layer="37"/>
<rectangle x1="4.10971875" y1="1.04571875" x2="4.62788125" y2="1.0541" layer="37"/>
<rectangle x1="5.49148125" y1="1.04571875" x2="6.01471875" y2="1.0541" layer="37"/>
<rectangle x1="6.27888125" y1="1.04571875" x2="7.38631875" y2="1.0541" layer="37"/>
<rectangle x1="7.58951875" y1="1.04571875" x2="7.9629" y2="1.0541" layer="37"/>
<rectangle x1="9.03731875" y1="1.04571875" x2="9.49451875" y2="1.0541" layer="37"/>
<rectangle x1="-0.00508125" y1="1.0541" x2="1.88468125" y2="1.06248125" layer="37"/>
<rectangle x1="1.9685" y1="1.0541" x2="3.56108125" y2="1.06248125" layer="37"/>
<rectangle x1="4.10971875" y1="1.0541" x2="4.6355" y2="1.06248125" layer="37"/>
<rectangle x1="5.4737" y1="1.0541" x2="6.01471875" y2="1.06248125" layer="37"/>
<rectangle x1="6.2865" y1="1.0541" x2="7.3787" y2="1.06248125" layer="37"/>
<rectangle x1="7.58951875" y1="1.0541" x2="7.9629" y2="1.06248125" layer="37"/>
<rectangle x1="9.0297" y1="1.0541" x2="9.49451875" y2="1.06248125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.06248125" x2="1.87451875" y2="1.07111875" layer="37"/>
<rectangle x1="1.9685" y1="1.06248125" x2="3.57631875" y2="1.07111875" layer="37"/>
<rectangle x1="4.10971875" y1="1.06248125" x2="4.65328125" y2="1.07111875" layer="37"/>
<rectangle x1="5.45591875" y1="1.06248125" x2="6.01471875" y2="1.07111875" layer="37"/>
<rectangle x1="6.29411875" y1="1.06248125" x2="7.37108125" y2="1.07111875" layer="37"/>
<rectangle x1="7.58951875" y1="1.06248125" x2="7.9629" y2="1.07111875" layer="37"/>
<rectangle x1="9.02208125" y1="1.06248125" x2="9.49451875" y2="1.07111875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.07111875" x2="1.8669" y2="1.0795" layer="37"/>
<rectangle x1="1.9685" y1="1.07111875" x2="3.5941" y2="1.0795" layer="37"/>
<rectangle x1="4.10971875" y1="1.07111875" x2="4.66851875" y2="1.0795" layer="37"/>
<rectangle x1="5.4483" y1="1.07111875" x2="6.01471875" y2="1.0795" layer="37"/>
<rectangle x1="6.30428125" y1="1.07111875" x2="7.36091875" y2="1.0795" layer="37"/>
<rectangle x1="7.58951875" y1="1.07111875" x2="7.9629" y2="1.0795" layer="37"/>
<rectangle x1="9.01191875" y1="1.07111875" x2="9.49451875" y2="1.0795" layer="37"/>
<rectangle x1="-0.00508125" y1="1.0795" x2="1.85928125" y2="1.08788125" layer="37"/>
<rectangle x1="1.9685" y1="1.0795" x2="3.60171875" y2="1.08788125" layer="37"/>
<rectangle x1="4.10971875" y1="1.0795" x2="4.6863" y2="1.08788125" layer="37"/>
<rectangle x1="5.43051875" y1="1.0795" x2="6.01471875" y2="1.08788125" layer="37"/>
<rectangle x1="6.3119" y1="1.0795" x2="7.3533" y2="1.08788125" layer="37"/>
<rectangle x1="7.58951875" y1="1.0795" x2="7.9629" y2="1.08788125" layer="37"/>
<rectangle x1="9.0043" y1="1.0795" x2="9.49451875" y2="1.08788125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.08788125" x2="1.8415" y2="1.09651875" layer="37"/>
<rectangle x1="1.9685" y1="1.08788125" x2="3.6195" y2="1.09651875" layer="37"/>
<rectangle x1="4.10971875" y1="1.08788125" x2="4.69391875" y2="1.09651875" layer="37"/>
<rectangle x1="5.4229" y1="1.08788125" x2="6.01471875" y2="1.09651875" layer="37"/>
<rectangle x1="6.31951875" y1="1.08788125" x2="7.33551875" y2="1.09651875" layer="37"/>
<rectangle x1="7.58951875" y1="1.08788125" x2="7.9629" y2="1.09651875" layer="37"/>
<rectangle x1="8.99668125" y1="1.08788125" x2="9.49451875" y2="1.09651875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.09651875" x2="1.82371875" y2="1.1049" layer="37"/>
<rectangle x1="1.9685" y1="1.09651875" x2="3.63728125" y2="1.1049" layer="37"/>
<rectangle x1="4.10971875" y1="1.09651875" x2="4.7117" y2="1.1049" layer="37"/>
<rectangle x1="5.40511875" y1="1.09651875" x2="6.01471875" y2="1.1049" layer="37"/>
<rectangle x1="6.3373" y1="1.09651875" x2="7.32028125" y2="1.1049" layer="37"/>
<rectangle x1="7.58951875" y1="1.09651875" x2="7.9629" y2="1.1049" layer="37"/>
<rectangle x1="8.98651875" y1="1.09651875" x2="9.49451875" y2="1.1049" layer="37"/>
<rectangle x1="-0.00508125" y1="1.1049" x2="1.79831875" y2="1.11328125" layer="37"/>
<rectangle x1="1.9685" y1="1.1049" x2="3.6449" y2="1.11328125" layer="37"/>
<rectangle x1="4.10971875" y1="1.1049" x2="4.72948125" y2="1.11328125" layer="37"/>
<rectangle x1="5.3975" y1="1.1049" x2="6.01471875" y2="1.11328125" layer="37"/>
<rectangle x1="6.35508125" y1="1.1049" x2="7.3025" y2="1.11328125" layer="37"/>
<rectangle x1="7.58951875" y1="1.1049" x2="7.9629" y2="1.11328125" layer="37"/>
<rectangle x1="8.9789" y1="1.1049" x2="9.49451875" y2="1.11328125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.11328125" x2="1.7653" y2="1.12191875" layer="37"/>
<rectangle x1="1.9685" y1="1.11328125" x2="3.65251875" y2="1.12191875" layer="37"/>
<rectangle x1="4.10971875" y1="1.11328125" x2="4.7371" y2="1.12191875" layer="37"/>
<rectangle x1="5.37971875" y1="1.11328125" x2="6.01471875" y2="1.12191875" layer="37"/>
<rectangle x1="6.3881" y1="1.11328125" x2="7.26948125" y2="1.12191875" layer="37"/>
<rectangle x1="7.58951875" y1="1.11328125" x2="7.9629" y2="1.12191875" layer="37"/>
<rectangle x1="8.96111875" y1="1.11328125" x2="9.49451875" y2="1.12191875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.12191875" x2="0.3683" y2="1.1303" layer="37"/>
<rectangle x1="1.9685" y1="1.12191875" x2="2.34188125" y2="1.1303" layer="37"/>
<rectangle x1="3.05308125" y1="1.12191875" x2="3.6703" y2="1.1303" layer="37"/>
<rectangle x1="4.10971875" y1="1.12191875" x2="4.75488125" y2="1.1303" layer="37"/>
<rectangle x1="5.3721" y1="1.12191875" x2="6.01471875" y2="1.1303" layer="37"/>
<rectangle x1="6.6421" y1="1.12191875" x2="7.01548125" y2="1.1303" layer="37"/>
<rectangle x1="7.58951875" y1="1.12191875" x2="7.9629" y2="1.1303" layer="37"/>
<rectangle x1="8.96111875" y1="1.12191875" x2="9.49451875" y2="1.1303" layer="37"/>
<rectangle x1="-0.00508125" y1="1.1303" x2="0.3683" y2="1.13868125" layer="37"/>
<rectangle x1="1.9685" y1="1.1303" x2="2.34188125" y2="1.13868125" layer="37"/>
<rectangle x1="3.10388125" y1="1.1303" x2="3.67791875" y2="1.13868125" layer="37"/>
<rectangle x1="4.10971875" y1="1.1303" x2="4.77011875" y2="1.13868125" layer="37"/>
<rectangle x1="5.35431875" y1="1.1303" x2="6.01471875" y2="1.13868125" layer="37"/>
<rectangle x1="6.6421" y1="1.1303" x2="7.01548125" y2="1.13868125" layer="37"/>
<rectangle x1="7.58951875" y1="1.1303" x2="7.9629" y2="1.13868125" layer="37"/>
<rectangle x1="8.94588125" y1="1.1303" x2="9.49451875" y2="1.13868125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.13868125" x2="0.3683" y2="1.14731875" layer="37"/>
<rectangle x1="1.9685" y1="1.13868125" x2="2.34188125" y2="1.14731875" layer="37"/>
<rectangle x1="3.12928125" y1="1.13868125" x2="3.68808125" y2="1.14731875" layer="37"/>
<rectangle x1="4.10971875" y1="1.13868125" x2="4.78028125" y2="1.14731875" layer="37"/>
<rectangle x1="5.33908125" y1="1.13868125" x2="6.01471875" y2="1.14731875" layer="37"/>
<rectangle x1="6.6421" y1="1.13868125" x2="7.01548125" y2="1.14731875" layer="37"/>
<rectangle x1="7.58951875" y1="1.13868125" x2="7.9629" y2="1.14731875" layer="37"/>
<rectangle x1="8.93571875" y1="1.13868125" x2="9.49451875" y2="1.14731875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.14731875" x2="0.3683" y2="1.1557" layer="37"/>
<rectangle x1="1.9685" y1="1.14731875" x2="2.34188125" y2="1.1557" layer="37"/>
<rectangle x1="3.15468125" y1="1.14731875" x2="3.70331875" y2="1.1557" layer="37"/>
<rectangle x1="4.10971875" y1="1.14731875" x2="4.79551875" y2="1.1557" layer="37"/>
<rectangle x1="5.32891875" y1="1.14731875" x2="6.01471875" y2="1.1557" layer="37"/>
<rectangle x1="6.6421" y1="1.14731875" x2="7.01548125" y2="1.1557" layer="37"/>
<rectangle x1="7.58951875" y1="1.14731875" x2="7.9629" y2="1.1557" layer="37"/>
<rectangle x1="8.9281" y1="1.14731875" x2="9.49451875" y2="1.1557" layer="37"/>
<rectangle x1="-0.00508125" y1="1.1557" x2="0.3683" y2="1.16408125" layer="37"/>
<rectangle x1="1.9685" y1="1.1557" x2="2.34188125" y2="1.16408125" layer="37"/>
<rectangle x1="3.18008125" y1="1.1557" x2="3.71348125" y2="1.16408125" layer="37"/>
<rectangle x1="4.10971875" y1="1.1557" x2="4.80568125" y2="1.16408125" layer="37"/>
<rectangle x1="5.31368125" y1="1.1557" x2="6.01471875" y2="1.16408125" layer="37"/>
<rectangle x1="6.6421" y1="1.1557" x2="7.01548125" y2="1.16408125" layer="37"/>
<rectangle x1="7.58951875" y1="1.1557" x2="7.9629" y2="1.16408125" layer="37"/>
<rectangle x1="8.92048125" y1="1.1557" x2="9.49451875" y2="1.16408125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.16408125" x2="0.3683" y2="1.17271875" layer="37"/>
<rectangle x1="1.9685" y1="1.16408125" x2="2.34188125" y2="1.17271875" layer="37"/>
<rectangle x1="3.20548125" y1="1.16408125" x2="3.7211" y2="1.17271875" layer="37"/>
<rectangle x1="4.10971875" y1="1.16408125" x2="4.82091875" y2="1.17271875" layer="37"/>
<rectangle x1="5.2959" y1="1.16408125" x2="6.01471875" y2="1.17271875" layer="37"/>
<rectangle x1="6.6421" y1="1.16408125" x2="7.01548125" y2="1.17271875" layer="37"/>
<rectangle x1="7.58951875" y1="1.16408125" x2="7.9629" y2="1.17271875" layer="37"/>
<rectangle x1="8.91031875" y1="1.16408125" x2="9.49451875" y2="1.17271875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.17271875" x2="0.3683" y2="1.1811" layer="37"/>
<rectangle x1="1.9685" y1="1.17271875" x2="2.34188125" y2="1.1811" layer="37"/>
<rectangle x1="3.22071875" y1="1.17271875" x2="3.72871875" y2="1.1811" layer="37"/>
<rectangle x1="4.10971875" y1="1.17271875" x2="4.8387" y2="1.1811" layer="37"/>
<rectangle x1="5.28828125" y1="1.17271875" x2="6.01471875" y2="1.1811" layer="37"/>
<rectangle x1="6.6421" y1="1.17271875" x2="7.01548125" y2="1.1811" layer="37"/>
<rectangle x1="7.58951875" y1="1.17271875" x2="7.9629" y2="1.1811" layer="37"/>
<rectangle x1="8.9027" y1="1.17271875" x2="9.49451875" y2="1.1811" layer="37"/>
<rectangle x1="-0.00508125" y1="1.1811" x2="0.3683" y2="1.18948125" layer="37"/>
<rectangle x1="1.9685" y1="1.1811" x2="2.34188125" y2="1.18948125" layer="37"/>
<rectangle x1="3.2385" y1="1.1811" x2="3.73888125" y2="1.18948125" layer="37"/>
<rectangle x1="4.10971875" y1="1.1811" x2="4.84631875" y2="1.18948125" layer="37"/>
<rectangle x1="5.2705" y1="1.1811" x2="6.01471875" y2="1.18948125" layer="37"/>
<rectangle x1="6.6421" y1="1.1811" x2="7.01548125" y2="1.18948125" layer="37"/>
<rectangle x1="7.58951875" y1="1.1811" x2="7.9629" y2="1.18948125" layer="37"/>
<rectangle x1="8.89508125" y1="1.1811" x2="9.49451875" y2="1.18948125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.18948125" x2="0.3683" y2="1.19811875" layer="37"/>
<rectangle x1="1.9685" y1="1.18948125" x2="2.34188125" y2="1.19811875" layer="37"/>
<rectangle x1="3.2639" y1="1.18948125" x2="3.7465" y2="1.19811875" layer="37"/>
<rectangle x1="4.10971875" y1="1.18948125" x2="4.8641" y2="1.19811875" layer="37"/>
<rectangle x1="5.26288125" y1="1.18948125" x2="6.01471875" y2="1.19811875" layer="37"/>
<rectangle x1="6.6421" y1="1.18948125" x2="7.01548125" y2="1.19811875" layer="37"/>
<rectangle x1="7.58951875" y1="1.18948125" x2="7.9629" y2="1.19811875" layer="37"/>
<rectangle x1="8.88491875" y1="1.18948125" x2="9.49451875" y2="1.19811875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.19811875" x2="0.3683" y2="1.2065" layer="37"/>
<rectangle x1="1.9685" y1="1.19811875" x2="2.34188125" y2="1.2065" layer="37"/>
<rectangle x1="3.28168125" y1="1.19811875" x2="3.75411875" y2="1.2065" layer="37"/>
<rectangle x1="4.10971875" y1="1.19811875" x2="4.88188125" y2="1.2065" layer="37"/>
<rectangle x1="5.2451" y1="1.19811875" x2="6.01471875" y2="1.2065" layer="37"/>
<rectangle x1="6.6421" y1="1.19811875" x2="7.01548125" y2="1.2065" layer="37"/>
<rectangle x1="7.58951875" y1="1.19811875" x2="7.9629" y2="1.2065" layer="37"/>
<rectangle x1="8.8773" y1="1.19811875" x2="9.49451875" y2="1.2065" layer="37"/>
<rectangle x1="-0.00508125" y1="1.2065" x2="0.3683" y2="1.21488125" layer="37"/>
<rectangle x1="1.9685" y1="1.2065" x2="2.34188125" y2="1.21488125" layer="37"/>
<rectangle x1="3.29691875" y1="1.2065" x2="3.76428125" y2="1.21488125" layer="37"/>
<rectangle x1="4.10971875" y1="1.2065" x2="4.8895" y2="1.21488125" layer="37"/>
<rectangle x1="5.23748125" y1="1.2065" x2="6.01471875" y2="1.21488125" layer="37"/>
<rectangle x1="6.6421" y1="1.2065" x2="7.01548125" y2="1.21488125" layer="37"/>
<rectangle x1="7.58951875" y1="1.2065" x2="7.9629" y2="1.21488125" layer="37"/>
<rectangle x1="8.86968125" y1="1.2065" x2="9.49451875" y2="1.21488125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.21488125" x2="0.3683" y2="1.22351875" layer="37"/>
<rectangle x1="1.9685" y1="1.21488125" x2="2.34188125" y2="1.22351875" layer="37"/>
<rectangle x1="3.30708125" y1="1.21488125" x2="3.7719" y2="1.22351875" layer="37"/>
<rectangle x1="4.10971875" y1="1.21488125" x2="4.90728125" y2="1.22351875" layer="37"/>
<rectangle x1="5.2197" y1="1.21488125" x2="6.01471875" y2="1.22351875" layer="37"/>
<rectangle x1="6.6421" y1="1.21488125" x2="7.01548125" y2="1.22351875" layer="37"/>
<rectangle x1="7.58951875" y1="1.21488125" x2="7.9629" y2="1.22351875" layer="37"/>
<rectangle x1="8.85951875" y1="1.21488125" x2="9.49451875" y2="1.22351875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.22351875" x2="0.3683" y2="1.2319" layer="37"/>
<rectangle x1="1.9685" y1="1.22351875" x2="2.34188125" y2="1.2319" layer="37"/>
<rectangle x1="3.32231875" y1="1.22351875" x2="3.77951875" y2="1.2319" layer="37"/>
<rectangle x1="4.10971875" y1="1.22351875" x2="4.92251875" y2="1.2319" layer="37"/>
<rectangle x1="5.21208125" y1="1.22351875" x2="6.01471875" y2="1.2319" layer="37"/>
<rectangle x1="6.6421" y1="1.22351875" x2="7.01548125" y2="1.2319" layer="37"/>
<rectangle x1="7.58951875" y1="1.22351875" x2="7.9629" y2="1.2319" layer="37"/>
<rectangle x1="8.8519" y1="1.22351875" x2="9.49451875" y2="1.2319" layer="37"/>
<rectangle x1="-0.00508125" y1="1.2319" x2="0.3683" y2="1.24028125" layer="37"/>
<rectangle x1="1.9685" y1="1.2319" x2="2.34188125" y2="1.24028125" layer="37"/>
<rectangle x1="3.3401" y1="1.2319" x2="3.78968125" y2="1.24028125" layer="37"/>
<rectangle x1="4.10971875" y1="1.2319" x2="4.93268125" y2="1.24028125" layer="37"/>
<rectangle x1="5.1943" y1="1.2319" x2="6.01471875" y2="1.24028125" layer="37"/>
<rectangle x1="6.6421" y1="1.2319" x2="7.01548125" y2="1.24028125" layer="37"/>
<rectangle x1="7.58951875" y1="1.2319" x2="7.9629" y2="1.24028125" layer="37"/>
<rectangle x1="8.83411875" y1="1.2319" x2="9.49451875" y2="1.24028125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.24028125" x2="0.3683" y2="1.24891875" layer="37"/>
<rectangle x1="1.9685" y1="1.24028125" x2="2.34188125" y2="1.24891875" layer="37"/>
<rectangle x1="3.35788125" y1="1.24028125" x2="3.7973" y2="1.24891875" layer="37"/>
<rectangle x1="4.10971875" y1="1.24028125" x2="4.94791875" y2="1.24891875" layer="37"/>
<rectangle x1="5.17651875" y1="1.24028125" x2="6.01471875" y2="1.24891875" layer="37"/>
<rectangle x1="6.6421" y1="1.24028125" x2="7.01548125" y2="1.24891875" layer="37"/>
<rectangle x1="7.58951875" y1="1.24028125" x2="7.9629" y2="1.24891875" layer="37"/>
<rectangle x1="8.8265" y1="1.24028125" x2="9.49451875" y2="1.24891875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.24891875" x2="0.3683" y2="1.2573" layer="37"/>
<rectangle x1="1.9685" y1="1.24891875" x2="2.34188125" y2="1.2573" layer="37"/>
<rectangle x1="3.3655" y1="1.24891875" x2="3.7973" y2="1.2573" layer="37"/>
<rectangle x1="4.10971875" y1="1.24891875" x2="4.9657" y2="1.2573" layer="37"/>
<rectangle x1="5.1689" y1="1.24891875" x2="6.01471875" y2="1.2573" layer="37"/>
<rectangle x1="6.6421" y1="1.24891875" x2="7.01548125" y2="1.2573" layer="37"/>
<rectangle x1="7.58951875" y1="1.24891875" x2="7.9629" y2="1.2573" layer="37"/>
<rectangle x1="8.81888125" y1="1.24891875" x2="9.49451875" y2="1.2573" layer="37"/>
<rectangle x1="-0.00508125" y1="1.2573" x2="0.3683" y2="1.26568125" layer="37"/>
<rectangle x1="1.9685" y1="1.2573" x2="2.34188125" y2="1.26568125" layer="37"/>
<rectangle x1="3.37311875" y1="1.2573" x2="3.80491875" y2="1.26568125" layer="37"/>
<rectangle x1="4.10971875" y1="1.2573" x2="4.97331875" y2="1.26568125" layer="37"/>
<rectangle x1="5.15111875" y1="1.2573" x2="6.01471875" y2="1.26568125" layer="37"/>
<rectangle x1="6.6421" y1="1.2573" x2="7.01548125" y2="1.26568125" layer="37"/>
<rectangle x1="7.58951875" y1="1.2573" x2="7.9629" y2="1.26568125" layer="37"/>
<rectangle x1="8.80871875" y1="1.2573" x2="9.49451875" y2="1.26568125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.26568125" x2="0.3683" y2="1.27431875" layer="37"/>
<rectangle x1="1.9685" y1="1.26568125" x2="2.34188125" y2="1.27431875" layer="37"/>
<rectangle x1="3.3909" y1="1.26568125" x2="3.81508125" y2="1.27431875" layer="37"/>
<rectangle x1="4.10971875" y1="1.26568125" x2="4.9911" y2="1.27431875" layer="37"/>
<rectangle x1="5.1435" y1="1.26568125" x2="6.01471875" y2="1.27431875" layer="37"/>
<rectangle x1="6.6421" y1="1.26568125" x2="7.01548125" y2="1.27431875" layer="37"/>
<rectangle x1="7.58951875" y1="1.26568125" x2="7.9629" y2="1.27431875" layer="37"/>
<rectangle x1="8.8011" y1="1.26568125" x2="9.49451875" y2="1.27431875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.27431875" x2="0.3683" y2="1.2827" layer="37"/>
<rectangle x1="1.9685" y1="1.27431875" x2="2.34188125" y2="1.2827" layer="37"/>
<rectangle x1="3.39851875" y1="1.27431875" x2="3.81508125" y2="1.2827" layer="37"/>
<rectangle x1="4.10971875" y1="1.27431875" x2="5.00888125" y2="1.2827" layer="37"/>
<rectangle x1="5.12571875" y1="1.27431875" x2="6.01471875" y2="1.2827" layer="37"/>
<rectangle x1="6.6421" y1="1.27431875" x2="7.01548125" y2="1.2827" layer="37"/>
<rectangle x1="7.58951875" y1="1.27431875" x2="7.9629" y2="1.2827" layer="37"/>
<rectangle x1="8.79348125" y1="1.27431875" x2="9.49451875" y2="1.2827" layer="37"/>
<rectangle x1="-0.00508125" y1="1.2827" x2="0.3683" y2="1.29108125" layer="37"/>
<rectangle x1="1.9685" y1="1.2827" x2="2.34188125" y2="1.29108125" layer="37"/>
<rectangle x1="3.4163" y1="1.2827" x2="3.8227" y2="1.29108125" layer="37"/>
<rectangle x1="4.10971875" y1="1.2827" x2="4.4831" y2="1.29108125" layer="37"/>
<rectangle x1="4.50088125" y1="1.2827" x2="5.0165" y2="1.29108125" layer="37"/>
<rectangle x1="5.11048125" y1="1.2827" x2="5.61848125" y2="1.29108125" layer="37"/>
<rectangle x1="5.63371875" y1="1.2827" x2="6.01471875" y2="1.29108125" layer="37"/>
<rectangle x1="6.6421" y1="1.2827" x2="7.01548125" y2="1.29108125" layer="37"/>
<rectangle x1="7.58951875" y1="1.2827" x2="7.9629" y2="1.29108125" layer="37"/>
<rectangle x1="8.78331875" y1="1.2827" x2="9.49451875" y2="1.29108125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.29108125" x2="0.3683" y2="1.29971875" layer="37"/>
<rectangle x1="1.9685" y1="1.29108125" x2="2.34188125" y2="1.29971875" layer="37"/>
<rectangle x1="3.42391875" y1="1.29108125" x2="3.8227" y2="1.29971875" layer="37"/>
<rectangle x1="4.10971875" y1="1.29108125" x2="4.4831" y2="1.29971875" layer="37"/>
<rectangle x1="4.5085" y1="1.29108125" x2="5.03428125" y2="1.29971875" layer="37"/>
<rectangle x1="5.10031875" y1="1.29108125" x2="5.60831875" y2="1.29971875" layer="37"/>
<rectangle x1="5.63371875" y1="1.29108125" x2="6.01471875" y2="1.29971875" layer="37"/>
<rectangle x1="6.6421" y1="1.29108125" x2="7.01548125" y2="1.29971875" layer="37"/>
<rectangle x1="7.58951875" y1="1.29108125" x2="7.9629" y2="1.29971875" layer="37"/>
<rectangle x1="8.7757" y1="1.29108125" x2="9.49451875" y2="1.29971875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.29971875" x2="0.3683" y2="1.3081" layer="37"/>
<rectangle x1="1.9685" y1="1.29971875" x2="2.34188125" y2="1.3081" layer="37"/>
<rectangle x1="3.43408125" y1="1.29971875" x2="3.83031875" y2="1.3081" layer="37"/>
<rectangle x1="4.10971875" y1="1.29971875" x2="4.4831" y2="1.3081" layer="37"/>
<rectangle x1="4.52628125" y1="1.29971875" x2="5.04951875" y2="1.3081" layer="37"/>
<rectangle x1="5.08508125" y1="1.29971875" x2="5.59308125" y2="1.3081" layer="37"/>
<rectangle x1="5.63371875" y1="1.29971875" x2="6.01471875" y2="1.3081" layer="37"/>
<rectangle x1="6.6421" y1="1.29971875" x2="7.01548125" y2="1.3081" layer="37"/>
<rectangle x1="7.58951875" y1="1.29971875" x2="7.9629" y2="1.3081" layer="37"/>
<rectangle x1="8.76808125" y1="1.29971875" x2="9.49451875" y2="1.3081" layer="37"/>
<rectangle x1="-0.00508125" y1="1.3081" x2="0.3683" y2="1.31648125" layer="37"/>
<rectangle x1="1.9685" y1="1.3081" x2="2.34188125" y2="1.31648125" layer="37"/>
<rectangle x1="3.4417" y1="1.3081" x2="3.83031875" y2="1.31648125" layer="37"/>
<rectangle x1="4.10971875" y1="1.3081" x2="4.4831" y2="1.31648125" layer="37"/>
<rectangle x1="4.5339" y1="1.3081" x2="5.05968125" y2="1.31648125" layer="37"/>
<rectangle x1="5.07491875" y1="1.3081" x2="5.58291875" y2="1.31648125" layer="37"/>
<rectangle x1="5.63371875" y1="1.3081" x2="6.01471875" y2="1.31648125" layer="37"/>
<rectangle x1="6.6421" y1="1.3081" x2="7.01548125" y2="1.31648125" layer="37"/>
<rectangle x1="7.58951875" y1="1.3081" x2="7.9629" y2="1.31648125" layer="37"/>
<rectangle x1="8.75791875" y1="1.3081" x2="9.49451875" y2="1.31648125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.31648125" x2="0.3683" y2="1.32511875" layer="37"/>
<rectangle x1="1.9685" y1="1.31648125" x2="2.34188125" y2="1.32511875" layer="37"/>
<rectangle x1="3.44931875" y1="1.31648125" x2="3.84048125" y2="1.32511875" layer="37"/>
<rectangle x1="4.10971875" y1="1.31648125" x2="4.4831" y2="1.32511875" layer="37"/>
<rectangle x1="4.55168125" y1="1.31648125" x2="5.56768125" y2="1.32511875" layer="37"/>
<rectangle x1="5.63371875" y1="1.31648125" x2="6.01471875" y2="1.32511875" layer="37"/>
<rectangle x1="6.6421" y1="1.31648125" x2="7.01548125" y2="1.32511875" layer="37"/>
<rectangle x1="7.58951875" y1="1.31648125" x2="7.9629" y2="1.32511875" layer="37"/>
<rectangle x1="8.7503" y1="1.31648125" x2="9.49451875" y2="1.32511875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.32511875" x2="0.3683" y2="1.3335" layer="37"/>
<rectangle x1="1.9685" y1="1.32511875" x2="2.34188125" y2="1.3335" layer="37"/>
<rectangle x1="3.45948125" y1="1.32511875" x2="3.84048125" y2="1.3335" layer="37"/>
<rectangle x1="4.10971875" y1="1.32511875" x2="4.4831" y2="1.3335" layer="37"/>
<rectangle x1="4.5593" y1="1.32511875" x2="5.5499" y2="1.3335" layer="37"/>
<rectangle x1="5.63371875" y1="1.32511875" x2="6.01471875" y2="1.3335" layer="37"/>
<rectangle x1="6.6421" y1="1.32511875" x2="7.01548125" y2="1.3335" layer="37"/>
<rectangle x1="7.58951875" y1="1.32511875" x2="7.9629" y2="1.3335" layer="37"/>
<rectangle x1="8.74268125" y1="1.32511875" x2="9.49451875" y2="1.3335" layer="37"/>
<rectangle x1="-0.00508125" y1="1.3335" x2="0.3683" y2="1.34188125" layer="37"/>
<rectangle x1="1.9685" y1="1.3335" x2="2.34188125" y2="1.34188125" layer="37"/>
<rectangle x1="3.4671" y1="1.3335" x2="3.8481" y2="1.34188125" layer="37"/>
<rectangle x1="4.10971875" y1="1.3335" x2="4.4831" y2="1.34188125" layer="37"/>
<rectangle x1="4.57708125" y1="1.3335" x2="5.54228125" y2="1.34188125" layer="37"/>
<rectangle x1="5.63371875" y1="1.3335" x2="6.01471875" y2="1.34188125" layer="37"/>
<rectangle x1="6.6421" y1="1.3335" x2="7.01548125" y2="1.34188125" layer="37"/>
<rectangle x1="7.58951875" y1="1.3335" x2="7.9629" y2="1.34188125" layer="37"/>
<rectangle x1="8.73251875" y1="1.3335" x2="9.49451875" y2="1.34188125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.34188125" x2="0.3683" y2="1.35051875" layer="37"/>
<rectangle x1="1.9685" y1="1.34188125" x2="2.34188125" y2="1.35051875" layer="37"/>
<rectangle x1="3.47471875" y1="1.34188125" x2="3.8481" y2="1.35051875" layer="37"/>
<rectangle x1="4.10971875" y1="1.34188125" x2="4.4831" y2="1.35051875" layer="37"/>
<rectangle x1="4.5847" y1="1.34188125" x2="5.5245" y2="1.35051875" layer="37"/>
<rectangle x1="5.63371875" y1="1.34188125" x2="6.01471875" y2="1.35051875" layer="37"/>
<rectangle x1="6.6421" y1="1.34188125" x2="7.01548125" y2="1.35051875" layer="37"/>
<rectangle x1="7.58951875" y1="1.34188125" x2="7.9629" y2="1.35051875" layer="37"/>
<rectangle x1="8.7249" y1="1.34188125" x2="9.49451875" y2="1.35051875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.35051875" x2="0.3683" y2="1.3589" layer="37"/>
<rectangle x1="1.9685" y1="1.35051875" x2="2.34188125" y2="1.3589" layer="37"/>
<rectangle x1="3.47471875" y1="1.35051875" x2="3.85571875" y2="1.3589" layer="37"/>
<rectangle x1="4.10971875" y1="1.35051875" x2="4.4831" y2="1.3589" layer="37"/>
<rectangle x1="4.60248125" y1="1.35051875" x2="5.51688125" y2="1.3589" layer="37"/>
<rectangle x1="5.63371875" y1="1.35051875" x2="6.01471875" y2="1.3589" layer="37"/>
<rectangle x1="6.6421" y1="1.35051875" x2="7.01548125" y2="1.3589" layer="37"/>
<rectangle x1="7.58951875" y1="1.35051875" x2="7.9629" y2="1.3589" layer="37"/>
<rectangle x1="8.70711875" y1="1.35051875" x2="9.49451875" y2="1.3589" layer="37"/>
<rectangle x1="-0.00508125" y1="1.3589" x2="0.3683" y2="1.36728125" layer="37"/>
<rectangle x1="1.9685" y1="1.3589" x2="2.34188125" y2="1.36728125" layer="37"/>
<rectangle x1="3.48488125" y1="1.3589" x2="3.86588125" y2="1.36728125" layer="37"/>
<rectangle x1="4.10971875" y1="1.3589" x2="4.4831" y2="1.36728125" layer="37"/>
<rectangle x1="4.6101" y1="1.3589" x2="5.4991" y2="1.36728125" layer="37"/>
<rectangle x1="5.63371875" y1="1.3589" x2="6.01471875" y2="1.36728125" layer="37"/>
<rectangle x1="6.6421" y1="1.3589" x2="7.01548125" y2="1.36728125" layer="37"/>
<rectangle x1="7.58951875" y1="1.3589" x2="7.9629" y2="1.36728125" layer="37"/>
<rectangle x1="8.6995" y1="1.3589" x2="9.49451875" y2="1.36728125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.36728125" x2="0.3683" y2="1.37591875" layer="37"/>
<rectangle x1="1.9685" y1="1.36728125" x2="2.34188125" y2="1.37591875" layer="37"/>
<rectangle x1="3.4925" y1="1.36728125" x2="3.86588125" y2="1.37591875" layer="37"/>
<rectangle x1="4.10971875" y1="1.36728125" x2="4.4831" y2="1.37591875" layer="37"/>
<rectangle x1="4.62788125" y1="1.36728125" x2="5.48131875" y2="1.37591875" layer="37"/>
<rectangle x1="5.63371875" y1="1.36728125" x2="6.01471875" y2="1.37591875" layer="37"/>
<rectangle x1="6.6421" y1="1.36728125" x2="7.01548125" y2="1.37591875" layer="37"/>
<rectangle x1="7.58951875" y1="1.36728125" x2="7.9629" y2="1.37591875" layer="37"/>
<rectangle x1="8.69188125" y1="1.36728125" x2="9.49451875" y2="1.37591875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.37591875" x2="0.3683" y2="1.3843" layer="37"/>
<rectangle x1="1.9685" y1="1.37591875" x2="2.34188125" y2="1.3843" layer="37"/>
<rectangle x1="3.4925" y1="1.37591875" x2="3.8735" y2="1.3843" layer="37"/>
<rectangle x1="4.10971875" y1="1.37591875" x2="4.4831" y2="1.3843" layer="37"/>
<rectangle x1="4.6355" y1="1.37591875" x2="5.4737" y2="1.3843" layer="37"/>
<rectangle x1="5.63371875" y1="1.37591875" x2="6.01471875" y2="1.3843" layer="37"/>
<rectangle x1="6.6421" y1="1.37591875" x2="7.01548125" y2="1.3843" layer="37"/>
<rectangle x1="7.58951875" y1="1.37591875" x2="7.9629" y2="1.3843" layer="37"/>
<rectangle x1="8.68171875" y1="1.37591875" x2="9.49451875" y2="1.3843" layer="37"/>
<rectangle x1="-0.00508125" y1="1.3843" x2="0.3683" y2="1.39268125" layer="37"/>
<rectangle x1="1.9685" y1="1.3843" x2="2.34188125" y2="1.39268125" layer="37"/>
<rectangle x1="3.4925" y1="1.3843" x2="3.8735" y2="1.39268125" layer="37"/>
<rectangle x1="4.10971875" y1="1.3843" x2="4.4831" y2="1.39268125" layer="37"/>
<rectangle x1="4.65328125" y1="1.3843" x2="5.45591875" y2="1.39268125" layer="37"/>
<rectangle x1="5.63371875" y1="1.3843" x2="6.01471875" y2="1.39268125" layer="37"/>
<rectangle x1="6.6421" y1="1.3843" x2="7.01548125" y2="1.39268125" layer="37"/>
<rectangle x1="7.58951875" y1="1.3843" x2="7.9629" y2="1.39268125" layer="37"/>
<rectangle x1="8.6741" y1="1.3843" x2="9.49451875" y2="1.39268125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.39268125" x2="0.3683" y2="1.40131875" layer="37"/>
<rectangle x1="1.9685" y1="1.39268125" x2="2.34188125" y2="1.40131875" layer="37"/>
<rectangle x1="3.50011875" y1="1.39268125" x2="3.8735" y2="1.40131875" layer="37"/>
<rectangle x1="4.10971875" y1="1.39268125" x2="4.4831" y2="1.40131875" layer="37"/>
<rectangle x1="4.6609" y1="1.39268125" x2="5.4483" y2="1.40131875" layer="37"/>
<rectangle x1="5.63371875" y1="1.39268125" x2="6.01471875" y2="1.40131875" layer="37"/>
<rectangle x1="6.6421" y1="1.39268125" x2="7.01548125" y2="1.40131875" layer="37"/>
<rectangle x1="7.58951875" y1="1.39268125" x2="7.9629" y2="1.40131875" layer="37"/>
<rectangle x1="8.66648125" y1="1.39268125" x2="9.1059" y2="1.40131875" layer="37"/>
<rectangle x1="9.11351875" y1="1.39268125" x2="9.49451875" y2="1.40131875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.40131875" x2="0.3683" y2="1.4097" layer="37"/>
<rectangle x1="1.9685" y1="1.40131875" x2="2.34188125" y2="1.4097" layer="37"/>
<rectangle x1="3.50011875" y1="1.40131875" x2="3.8735" y2="1.4097" layer="37"/>
<rectangle x1="4.10971875" y1="1.40131875" x2="4.4831" y2="1.4097" layer="37"/>
<rectangle x1="4.67868125" y1="1.40131875" x2="5.43051875" y2="1.4097" layer="37"/>
<rectangle x1="5.63371875" y1="1.40131875" x2="6.01471875" y2="1.4097" layer="37"/>
<rectangle x1="6.6421" y1="1.40131875" x2="7.01548125" y2="1.4097" layer="37"/>
<rectangle x1="7.58951875" y1="1.40131875" x2="7.9629" y2="1.4097" layer="37"/>
<rectangle x1="8.65631875" y1="1.40131875" x2="9.09828125" y2="1.4097" layer="37"/>
<rectangle x1="9.11351875" y1="1.40131875" x2="9.49451875" y2="1.4097" layer="37"/>
<rectangle x1="-0.00508125" y1="1.4097" x2="0.3683" y2="1.41808125" layer="37"/>
<rectangle x1="1.9685" y1="1.4097" x2="2.34188125" y2="1.41808125" layer="37"/>
<rectangle x1="3.50011875" y1="1.4097" x2="3.8735" y2="1.41808125" layer="37"/>
<rectangle x1="4.10971875" y1="1.4097" x2="4.4831" y2="1.41808125" layer="37"/>
<rectangle x1="4.6863" y1="1.4097" x2="5.4229" y2="1.41808125" layer="37"/>
<rectangle x1="5.63371875" y1="1.4097" x2="6.01471875" y2="1.41808125" layer="37"/>
<rectangle x1="6.6421" y1="1.4097" x2="7.01548125" y2="1.41808125" layer="37"/>
<rectangle x1="7.58951875" y1="1.4097" x2="7.9629" y2="1.41808125" layer="37"/>
<rectangle x1="8.6487" y1="1.4097" x2="9.08811875" y2="1.41808125" layer="37"/>
<rectangle x1="9.11351875" y1="1.4097" x2="9.49451875" y2="1.41808125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.41808125" x2="0.3683" y2="1.42671875" layer="37"/>
<rectangle x1="1.9685" y1="1.41808125" x2="2.34188125" y2="1.42671875" layer="37"/>
<rectangle x1="3.50011875" y1="1.41808125" x2="3.8735" y2="1.42671875" layer="37"/>
<rectangle x1="4.10971875" y1="1.41808125" x2="4.4831" y2="1.42671875" layer="37"/>
<rectangle x1="4.70408125" y1="1.41808125" x2="5.40511875" y2="1.42671875" layer="37"/>
<rectangle x1="5.63371875" y1="1.41808125" x2="6.01471875" y2="1.42671875" layer="37"/>
<rectangle x1="6.6421" y1="1.41808125" x2="7.01548125" y2="1.42671875" layer="37"/>
<rectangle x1="7.58951875" y1="1.41808125" x2="7.9629" y2="1.42671875" layer="37"/>
<rectangle x1="8.64108125" y1="1.41808125" x2="9.0805" y2="1.42671875" layer="37"/>
<rectangle x1="9.11351875" y1="1.41808125" x2="9.49451875" y2="1.42671875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.42671875" x2="0.3683" y2="1.4351" layer="37"/>
<rectangle x1="1.9685" y1="1.42671875" x2="2.34188125" y2="1.4351" layer="37"/>
<rectangle x1="3.50011875" y1="1.42671875" x2="3.8735" y2="1.4351" layer="37"/>
<rectangle x1="4.10971875" y1="1.42671875" x2="4.4831" y2="1.4351" layer="37"/>
<rectangle x1="4.71931875" y1="1.42671875" x2="5.38988125" y2="1.4351" layer="37"/>
<rectangle x1="5.63371875" y1="1.42671875" x2="6.01471875" y2="1.4351" layer="37"/>
<rectangle x1="6.6421" y1="1.42671875" x2="7.01548125" y2="1.4351" layer="37"/>
<rectangle x1="7.58951875" y1="1.42671875" x2="7.9629" y2="1.4351" layer="37"/>
<rectangle x1="8.63091875" y1="1.42671875" x2="9.07288125" y2="1.4351" layer="37"/>
<rectangle x1="9.11351875" y1="1.42671875" x2="9.49451875" y2="1.4351" layer="37"/>
<rectangle x1="-0.00508125" y1="1.4351" x2="0.3683" y2="1.44348125" layer="37"/>
<rectangle x1="1.9685" y1="1.4351" x2="2.34188125" y2="1.44348125" layer="37"/>
<rectangle x1="3.50011875" y1="1.4351" x2="3.8735" y2="1.44348125" layer="37"/>
<rectangle x1="4.10971875" y1="1.4351" x2="4.4831" y2="1.44348125" layer="37"/>
<rectangle x1="4.72948125" y1="1.4351" x2="5.37971875" y2="1.44348125" layer="37"/>
<rectangle x1="5.63371875" y1="1.4351" x2="6.01471875" y2="1.44348125" layer="37"/>
<rectangle x1="6.6421" y1="1.4351" x2="7.01548125" y2="1.44348125" layer="37"/>
<rectangle x1="7.58951875" y1="1.4351" x2="7.9629" y2="1.44348125" layer="37"/>
<rectangle x1="8.6233" y1="1.4351" x2="9.06271875" y2="1.44348125" layer="37"/>
<rectangle x1="9.11351875" y1="1.4351" x2="9.49451875" y2="1.44348125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.44348125" x2="0.3683" y2="1.45211875" layer="37"/>
<rectangle x1="1.9685" y1="1.44348125" x2="2.34188125" y2="1.45211875" layer="37"/>
<rectangle x1="3.50011875" y1="1.44348125" x2="3.8735" y2="1.45211875" layer="37"/>
<rectangle x1="4.10971875" y1="1.44348125" x2="4.4831" y2="1.45211875" layer="37"/>
<rectangle x1="4.7371" y1="1.44348125" x2="5.36448125" y2="1.45211875" layer="37"/>
<rectangle x1="5.63371875" y1="1.44348125" x2="6.01471875" y2="1.45211875" layer="37"/>
<rectangle x1="6.6421" y1="1.44348125" x2="7.01548125" y2="1.45211875" layer="37"/>
<rectangle x1="7.58951875" y1="1.44348125" x2="7.9629" y2="1.45211875" layer="37"/>
<rectangle x1="8.61568125" y1="1.44348125" x2="9.0551" y2="1.45211875" layer="37"/>
<rectangle x1="9.11351875" y1="1.44348125" x2="9.49451875" y2="1.45211875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.45211875" x2="0.3683" y2="1.4605" layer="37"/>
<rectangle x1="1.9685" y1="1.45211875" x2="2.34188125" y2="1.4605" layer="37"/>
<rectangle x1="3.50011875" y1="1.45211875" x2="3.8735" y2="1.4605" layer="37"/>
<rectangle x1="4.10971875" y1="1.45211875" x2="4.4831" y2="1.4605" layer="37"/>
<rectangle x1="4.75488125" y1="1.45211875" x2="5.35431875" y2="1.4605" layer="37"/>
<rectangle x1="5.63371875" y1="1.45211875" x2="6.01471875" y2="1.4605" layer="37"/>
<rectangle x1="6.6421" y1="1.45211875" x2="7.01548125" y2="1.4605" layer="37"/>
<rectangle x1="7.58951875" y1="1.45211875" x2="7.9629" y2="1.4605" layer="37"/>
<rectangle x1="8.60551875" y1="1.45211875" x2="9.03731875" y2="1.4605" layer="37"/>
<rectangle x1="9.11351875" y1="1.45211875" x2="9.49451875" y2="1.4605" layer="37"/>
<rectangle x1="-0.00508125" y1="1.4605" x2="0.3683" y2="1.46888125" layer="37"/>
<rectangle x1="1.9685" y1="1.4605" x2="2.34188125" y2="1.46888125" layer="37"/>
<rectangle x1="3.50011875" y1="1.4605" x2="3.8735" y2="1.46888125" layer="37"/>
<rectangle x1="4.10971875" y1="1.4605" x2="4.4831" y2="1.46888125" layer="37"/>
<rectangle x1="4.7625" y1="1.4605" x2="5.33908125" y2="1.46888125" layer="37"/>
<rectangle x1="5.63371875" y1="1.4605" x2="6.01471875" y2="1.46888125" layer="37"/>
<rectangle x1="6.6421" y1="1.4605" x2="7.01548125" y2="1.46888125" layer="37"/>
<rectangle x1="7.58951875" y1="1.4605" x2="7.9629" y2="1.46888125" layer="37"/>
<rectangle x1="8.59028125" y1="1.4605" x2="9.0297" y2="1.46888125" layer="37"/>
<rectangle x1="9.11351875" y1="1.4605" x2="9.49451875" y2="1.46888125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.46888125" x2="0.3683" y2="1.47751875" layer="37"/>
<rectangle x1="1.9685" y1="1.46888125" x2="2.34188125" y2="1.47751875" layer="37"/>
<rectangle x1="3.50011875" y1="1.46888125" x2="3.8735" y2="1.47751875" layer="37"/>
<rectangle x1="4.10971875" y1="1.46888125" x2="4.4831" y2="1.47751875" layer="37"/>
<rectangle x1="4.78028125" y1="1.46888125" x2="5.3213" y2="1.47751875" layer="37"/>
<rectangle x1="5.63371875" y1="1.46888125" x2="6.01471875" y2="1.47751875" layer="37"/>
<rectangle x1="6.6421" y1="1.46888125" x2="7.01548125" y2="1.47751875" layer="37"/>
<rectangle x1="7.58951875" y1="1.46888125" x2="7.9629" y2="1.47751875" layer="37"/>
<rectangle x1="8.58011875" y1="1.46888125" x2="9.02208125" y2="1.47751875" layer="37"/>
<rectangle x1="9.11351875" y1="1.46888125" x2="9.49451875" y2="1.47751875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.47751875" x2="0.3683" y2="1.4859" layer="37"/>
<rectangle x1="1.9685" y1="1.47751875" x2="2.34188125" y2="1.4859" layer="37"/>
<rectangle x1="3.50011875" y1="1.47751875" x2="3.8735" y2="1.4859" layer="37"/>
<rectangle x1="4.10971875" y1="1.47751875" x2="4.4831" y2="1.4859" layer="37"/>
<rectangle x1="4.79551875" y1="1.47751875" x2="5.31368125" y2="1.4859" layer="37"/>
<rectangle x1="5.63371875" y1="1.47751875" x2="6.01471875" y2="1.4859" layer="37"/>
<rectangle x1="6.6421" y1="1.47751875" x2="7.01548125" y2="1.4859" layer="37"/>
<rectangle x1="7.58951875" y1="1.47751875" x2="7.9629" y2="1.4859" layer="37"/>
<rectangle x1="8.5725" y1="1.47751875" x2="9.01191875" y2="1.4859" layer="37"/>
<rectangle x1="9.11351875" y1="1.47751875" x2="9.49451875" y2="1.4859" layer="37"/>
<rectangle x1="-0.00508125" y1="1.4859" x2="0.3683" y2="1.49428125" layer="37"/>
<rectangle x1="1.9685" y1="1.4859" x2="2.34188125" y2="1.49428125" layer="37"/>
<rectangle x1="3.50011875" y1="1.4859" x2="3.8735" y2="1.49428125" layer="37"/>
<rectangle x1="4.10971875" y1="1.4859" x2="4.4831" y2="1.49428125" layer="37"/>
<rectangle x1="4.80568125" y1="1.4859" x2="5.2959" y2="1.49428125" layer="37"/>
<rectangle x1="5.63371875" y1="1.4859" x2="6.01471875" y2="1.49428125" layer="37"/>
<rectangle x1="6.6421" y1="1.4859" x2="7.01548125" y2="1.49428125" layer="37"/>
<rectangle x1="7.58951875" y1="1.4859" x2="7.9629" y2="1.49428125" layer="37"/>
<rectangle x1="8.56488125" y1="1.4859" x2="9.0043" y2="1.49428125" layer="37"/>
<rectangle x1="9.11351875" y1="1.4859" x2="9.49451875" y2="1.49428125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.49428125" x2="0.3683" y2="1.50291875" layer="37"/>
<rectangle x1="1.9685" y1="1.49428125" x2="2.34188125" y2="1.50291875" layer="37"/>
<rectangle x1="3.50011875" y1="1.49428125" x2="3.8735" y2="1.50291875" layer="37"/>
<rectangle x1="4.10971875" y1="1.49428125" x2="4.4831" y2="1.50291875" layer="37"/>
<rectangle x1="4.82091875" y1="1.49428125" x2="5.27811875" y2="1.50291875" layer="37"/>
<rectangle x1="5.63371875" y1="1.49428125" x2="6.01471875" y2="1.50291875" layer="37"/>
<rectangle x1="6.6421" y1="1.49428125" x2="7.01548125" y2="1.50291875" layer="37"/>
<rectangle x1="7.58951875" y1="1.49428125" x2="7.9629" y2="1.50291875" layer="37"/>
<rectangle x1="8.55471875" y1="1.49428125" x2="8.99668125" y2="1.50291875" layer="37"/>
<rectangle x1="9.11351875" y1="1.49428125" x2="9.49451875" y2="1.50291875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.50291875" x2="0.3683" y2="1.5113" layer="37"/>
<rectangle x1="1.9685" y1="1.50291875" x2="2.34188125" y2="1.5113" layer="37"/>
<rectangle x1="3.50011875" y1="1.50291875" x2="3.8735" y2="1.5113" layer="37"/>
<rectangle x1="4.10971875" y1="1.50291875" x2="4.4831" y2="1.5113" layer="37"/>
<rectangle x1="4.83108125" y1="1.50291875" x2="5.2705" y2="1.5113" layer="37"/>
<rectangle x1="5.63371875" y1="1.50291875" x2="6.01471875" y2="1.5113" layer="37"/>
<rectangle x1="6.6421" y1="1.50291875" x2="7.01548125" y2="1.5113" layer="37"/>
<rectangle x1="7.58951875" y1="1.50291875" x2="7.9629" y2="1.5113" layer="37"/>
<rectangle x1="8.5471" y1="1.50291875" x2="8.98651875" y2="1.5113" layer="37"/>
<rectangle x1="9.11351875" y1="1.50291875" x2="9.49451875" y2="1.5113" layer="37"/>
<rectangle x1="-0.00508125" y1="1.5113" x2="0.3683" y2="1.51968125" layer="37"/>
<rectangle x1="1.9685" y1="1.5113" x2="2.34188125" y2="1.51968125" layer="37"/>
<rectangle x1="3.50011875" y1="1.5113" x2="3.8735" y2="1.51968125" layer="37"/>
<rectangle x1="4.10971875" y1="1.5113" x2="4.4831" y2="1.51968125" layer="37"/>
<rectangle x1="4.84631875" y1="1.5113" x2="5.25271875" y2="1.51968125" layer="37"/>
<rectangle x1="5.63371875" y1="1.5113" x2="6.01471875" y2="1.51968125" layer="37"/>
<rectangle x1="6.6421" y1="1.5113" x2="7.01548125" y2="1.51968125" layer="37"/>
<rectangle x1="7.58951875" y1="1.5113" x2="7.9629" y2="1.51968125" layer="37"/>
<rectangle x1="8.53948125" y1="1.5113" x2="8.9789" y2="1.51968125" layer="37"/>
<rectangle x1="9.12368125" y1="1.5113" x2="9.49451875" y2="1.51968125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.51968125" x2="0.3683" y2="1.52831875" layer="37"/>
<rectangle x1="1.9685" y1="1.51968125" x2="2.34188125" y2="1.52831875" layer="37"/>
<rectangle x1="3.50011875" y1="1.51968125" x2="3.8735" y2="1.52831875" layer="37"/>
<rectangle x1="4.10971875" y1="1.51968125" x2="4.4831" y2="1.52831875" layer="37"/>
<rectangle x1="4.8641" y1="1.51968125" x2="5.23748125" y2="1.52831875" layer="37"/>
<rectangle x1="5.63371875" y1="1.51968125" x2="6.01471875" y2="1.52831875" layer="37"/>
<rectangle x1="6.6421" y1="1.51968125" x2="7.01548125" y2="1.52831875" layer="37"/>
<rectangle x1="7.58951875" y1="1.51968125" x2="7.9629" y2="1.52831875" layer="37"/>
<rectangle x1="8.52931875" y1="1.51968125" x2="8.97128125" y2="1.52831875" layer="37"/>
<rectangle x1="9.12368125" y1="1.51968125" x2="9.49451875" y2="1.52831875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.52831875" x2="0.3683" y2="1.5367" layer="37"/>
<rectangle x1="1.9685" y1="1.52831875" x2="2.34188125" y2="1.5367" layer="37"/>
<rectangle x1="3.50011875" y1="1.52831875" x2="3.8735" y2="1.5367" layer="37"/>
<rectangle x1="4.10971875" y1="1.52831875" x2="4.4831" y2="1.5367" layer="37"/>
<rectangle x1="4.88188125" y1="1.52831875" x2="5.2197" y2="1.5367" layer="37"/>
<rectangle x1="5.63371875" y1="1.52831875" x2="6.01471875" y2="1.5367" layer="37"/>
<rectangle x1="6.6421" y1="1.52831875" x2="7.01548125" y2="1.5367" layer="37"/>
<rectangle x1="7.58951875" y1="1.52831875" x2="7.9629" y2="1.5367" layer="37"/>
<rectangle x1="8.5217" y1="1.52831875" x2="8.96111875" y2="1.5367" layer="37"/>
<rectangle x1="9.12368125" y1="1.52831875" x2="9.49451875" y2="1.5367" layer="37"/>
<rectangle x1="-0.00508125" y1="1.5367" x2="0.3683" y2="1.54508125" layer="37"/>
<rectangle x1="1.9685" y1="1.5367" x2="2.34188125" y2="1.54508125" layer="37"/>
<rectangle x1="3.50011875" y1="1.5367" x2="3.8735" y2="1.54508125" layer="37"/>
<rectangle x1="4.10971875" y1="1.5367" x2="4.4831" y2="1.54508125" layer="37"/>
<rectangle x1="4.8895" y1="1.5367" x2="5.20191875" y2="1.54508125" layer="37"/>
<rectangle x1="5.63371875" y1="1.5367" x2="6.01471875" y2="1.54508125" layer="37"/>
<rectangle x1="6.6421" y1="1.5367" x2="7.01548125" y2="1.54508125" layer="37"/>
<rectangle x1="7.58951875" y1="1.5367" x2="7.9629" y2="1.54508125" layer="37"/>
<rectangle x1="8.51408125" y1="1.5367" x2="8.9535" y2="1.54508125" layer="37"/>
<rectangle x1="9.12368125" y1="1.5367" x2="9.49451875" y2="1.54508125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.54508125" x2="0.3683" y2="1.55371875" layer="37"/>
<rectangle x1="1.9685" y1="1.54508125" x2="2.34188125" y2="1.55371875" layer="37"/>
<rectangle x1="3.50011875" y1="1.54508125" x2="3.8735" y2="1.55371875" layer="37"/>
<rectangle x1="4.10971875" y1="1.54508125" x2="4.4831" y2="1.55371875" layer="37"/>
<rectangle x1="4.90728125" y1="1.54508125" x2="5.18668125" y2="1.55371875" layer="37"/>
<rectangle x1="5.63371875" y1="1.54508125" x2="6.01471875" y2="1.55371875" layer="37"/>
<rectangle x1="6.6421" y1="1.54508125" x2="7.01548125" y2="1.55371875" layer="37"/>
<rectangle x1="7.58951875" y1="1.54508125" x2="7.9629" y2="1.55371875" layer="37"/>
<rectangle x1="8.50391875" y1="1.54508125" x2="8.94588125" y2="1.55371875" layer="37"/>
<rectangle x1="9.12368125" y1="1.54508125" x2="9.49451875" y2="1.55371875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.55371875" x2="0.3683" y2="1.5621" layer="37"/>
<rectangle x1="1.9685" y1="1.55371875" x2="2.34188125" y2="1.5621" layer="37"/>
<rectangle x1="3.50011875" y1="1.55371875" x2="3.8735" y2="1.5621" layer="37"/>
<rectangle x1="4.10971875" y1="1.55371875" x2="4.4831" y2="1.5621" layer="37"/>
<rectangle x1="4.92251875" y1="1.55371875" x2="5.1689" y2="1.5621" layer="37"/>
<rectangle x1="5.63371875" y1="1.55371875" x2="6.01471875" y2="1.5621" layer="37"/>
<rectangle x1="6.6421" y1="1.55371875" x2="7.01548125" y2="1.5621" layer="37"/>
<rectangle x1="7.58951875" y1="1.55371875" x2="7.9629" y2="1.5621" layer="37"/>
<rectangle x1="8.4963" y1="1.55371875" x2="8.9281" y2="1.5621" layer="37"/>
<rectangle x1="9.12368125" y1="1.55371875" x2="9.49451875" y2="1.5621" layer="37"/>
<rectangle x1="-0.00508125" y1="1.5621" x2="0.3683" y2="1.57048125" layer="37"/>
<rectangle x1="1.9685" y1="1.5621" x2="2.34188125" y2="1.57048125" layer="37"/>
<rectangle x1="3.50011875" y1="1.5621" x2="3.8735" y2="1.57048125" layer="37"/>
<rectangle x1="4.10971875" y1="1.5621" x2="4.4831" y2="1.57048125" layer="37"/>
<rectangle x1="4.94791875" y1="1.5621" x2="5.1435" y2="1.57048125" layer="37"/>
<rectangle x1="5.63371875" y1="1.5621" x2="6.01471875" y2="1.57048125" layer="37"/>
<rectangle x1="6.6421" y1="1.5621" x2="7.01548125" y2="1.57048125" layer="37"/>
<rectangle x1="7.58951875" y1="1.5621" x2="7.9629" y2="1.57048125" layer="37"/>
<rectangle x1="8.47851875" y1="1.5621" x2="8.92048125" y2="1.57048125" layer="37"/>
<rectangle x1="9.12368125" y1="1.5621" x2="9.49451875" y2="1.57048125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.57048125" x2="0.3683" y2="1.57911875" layer="37"/>
<rectangle x1="1.9685" y1="1.57048125" x2="2.34188125" y2="1.57911875" layer="37"/>
<rectangle x1="3.50011875" y1="1.57048125" x2="3.8735" y2="1.57911875" layer="37"/>
<rectangle x1="4.10971875" y1="1.57048125" x2="4.4831" y2="1.57911875" layer="37"/>
<rectangle x1="4.97331875" y1="1.57048125" x2="5.1181" y2="1.57911875" layer="37"/>
<rectangle x1="5.63371875" y1="1.57048125" x2="6.01471875" y2="1.57911875" layer="37"/>
<rectangle x1="6.6421" y1="1.57048125" x2="7.01548125" y2="1.57911875" layer="37"/>
<rectangle x1="7.58951875" y1="1.57048125" x2="7.9629" y2="1.57911875" layer="37"/>
<rectangle x1="8.4709" y1="1.57048125" x2="8.91031875" y2="1.57911875" layer="37"/>
<rectangle x1="9.12368125" y1="1.57048125" x2="9.49451875" y2="1.57911875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.57911875" x2="0.3683" y2="1.5875" layer="37"/>
<rectangle x1="1.9685" y1="1.57911875" x2="2.34188125" y2="1.5875" layer="37"/>
<rectangle x1="3.50011875" y1="1.57911875" x2="3.8735" y2="1.5875" layer="37"/>
<rectangle x1="4.10971875" y1="1.57911875" x2="4.4831" y2="1.5875" layer="37"/>
<rectangle x1="4.99871875" y1="1.57911875" x2="5.0927" y2="1.5875" layer="37"/>
<rectangle x1="5.63371875" y1="1.57911875" x2="6.01471875" y2="1.5875" layer="37"/>
<rectangle x1="6.6421" y1="1.57911875" x2="7.01548125" y2="1.5875" layer="37"/>
<rectangle x1="7.58951875" y1="1.57911875" x2="7.9629" y2="1.5875" layer="37"/>
<rectangle x1="8.46328125" y1="1.57911875" x2="8.9027" y2="1.5875" layer="37"/>
<rectangle x1="9.12368125" y1="1.57911875" x2="9.49451875" y2="1.5875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.5875" x2="0.3683" y2="1.59588125" layer="37"/>
<rectangle x1="1.9685" y1="1.5875" x2="2.34188125" y2="1.59588125" layer="37"/>
<rectangle x1="3.50011875" y1="1.5875" x2="3.8735" y2="1.59588125" layer="37"/>
<rectangle x1="4.10971875" y1="1.5875" x2="4.4831" y2="1.59588125" layer="37"/>
<rectangle x1="5.63371875" y1="1.5875" x2="6.01471875" y2="1.59588125" layer="37"/>
<rectangle x1="6.6421" y1="1.5875" x2="7.01548125" y2="1.59588125" layer="37"/>
<rectangle x1="7.58951875" y1="1.5875" x2="7.9629" y2="1.59588125" layer="37"/>
<rectangle x1="8.45311875" y1="1.5875" x2="8.89508125" y2="1.59588125" layer="37"/>
<rectangle x1="9.12368125" y1="1.5875" x2="9.49451875" y2="1.59588125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.59588125" x2="1.3843" y2="1.60451875" layer="37"/>
<rectangle x1="1.9685" y1="1.59588125" x2="2.34188125" y2="1.60451875" layer="37"/>
<rectangle x1="3.50011875" y1="1.59588125" x2="3.8735" y2="1.60451875" layer="37"/>
<rectangle x1="4.10971875" y1="1.59588125" x2="4.4831" y2="1.60451875" layer="37"/>
<rectangle x1="5.63371875" y1="1.59588125" x2="6.01471875" y2="1.60451875" layer="37"/>
<rectangle x1="6.6421" y1="1.59588125" x2="7.01548125" y2="1.60451875" layer="37"/>
<rectangle x1="7.58951875" y1="1.59588125" x2="7.9629" y2="1.60451875" layer="37"/>
<rectangle x1="8.4455" y1="1.59588125" x2="8.88491875" y2="1.60451875" layer="37"/>
<rectangle x1="9.12368125" y1="1.59588125" x2="9.49451875" y2="1.60451875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.60451875" x2="1.41731875" y2="1.6129" layer="37"/>
<rectangle x1="1.9685" y1="1.60451875" x2="2.34188125" y2="1.6129" layer="37"/>
<rectangle x1="3.50011875" y1="1.60451875" x2="3.8735" y2="1.6129" layer="37"/>
<rectangle x1="4.10971875" y1="1.60451875" x2="4.4831" y2="1.6129" layer="37"/>
<rectangle x1="5.63371875" y1="1.60451875" x2="6.01471875" y2="1.6129" layer="37"/>
<rectangle x1="6.6421" y1="1.60451875" x2="7.01548125" y2="1.6129" layer="37"/>
<rectangle x1="7.58951875" y1="1.60451875" x2="7.9629" y2="1.6129" layer="37"/>
<rectangle x1="8.43788125" y1="1.60451875" x2="8.8773" y2="1.6129" layer="37"/>
<rectangle x1="9.12368125" y1="1.60451875" x2="9.49451875" y2="1.6129" layer="37"/>
<rectangle x1="-0.00508125" y1="1.6129" x2="1.4351" y2="1.62128125" layer="37"/>
<rectangle x1="1.9685" y1="1.6129" x2="2.34188125" y2="1.62128125" layer="37"/>
<rectangle x1="3.50011875" y1="1.6129" x2="3.8735" y2="1.62128125" layer="37"/>
<rectangle x1="4.10971875" y1="1.6129" x2="4.4831" y2="1.62128125" layer="37"/>
<rectangle x1="5.63371875" y1="1.6129" x2="6.01471875" y2="1.62128125" layer="37"/>
<rectangle x1="6.6421" y1="1.6129" x2="7.01548125" y2="1.62128125" layer="37"/>
<rectangle x1="7.58951875" y1="1.6129" x2="7.9629" y2="1.62128125" layer="37"/>
<rectangle x1="8.42771875" y1="1.6129" x2="8.86968125" y2="1.62128125" layer="37"/>
<rectangle x1="9.12368125" y1="1.6129" x2="9.49451875" y2="1.62128125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.62128125" x2="1.4605" y2="1.62991875" layer="37"/>
<rectangle x1="1.9685" y1="1.62128125" x2="2.34188125" y2="1.62991875" layer="37"/>
<rectangle x1="3.50011875" y1="1.62128125" x2="3.8735" y2="1.62991875" layer="37"/>
<rectangle x1="4.10971875" y1="1.62128125" x2="4.4831" y2="1.62991875" layer="37"/>
<rectangle x1="5.63371875" y1="1.62128125" x2="6.01471875" y2="1.62991875" layer="37"/>
<rectangle x1="6.6421" y1="1.62128125" x2="7.01548125" y2="1.62991875" layer="37"/>
<rectangle x1="7.58951875" y1="1.62128125" x2="7.9629" y2="1.62991875" layer="37"/>
<rectangle x1="8.4201" y1="1.62128125" x2="8.85951875" y2="1.62991875" layer="37"/>
<rectangle x1="9.12368125" y1="1.62128125" x2="9.49451875" y2="1.62991875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.62991875" x2="1.46811875" y2="1.6383" layer="37"/>
<rectangle x1="1.9685" y1="1.62991875" x2="2.34188125" y2="1.6383" layer="37"/>
<rectangle x1="3.50011875" y1="1.62991875" x2="3.8735" y2="1.6383" layer="37"/>
<rectangle x1="4.10971875" y1="1.62991875" x2="4.4831" y2="1.6383" layer="37"/>
<rectangle x1="5.63371875" y1="1.62991875" x2="6.01471875" y2="1.6383" layer="37"/>
<rectangle x1="6.6421" y1="1.62991875" x2="7.01548125" y2="1.6383" layer="37"/>
<rectangle x1="7.58951875" y1="1.62991875" x2="7.9629" y2="1.6383" layer="37"/>
<rectangle x1="8.41248125" y1="1.62991875" x2="8.8519" y2="1.6383" layer="37"/>
<rectangle x1="9.12368125" y1="1.62991875" x2="9.49451875" y2="1.6383" layer="37"/>
<rectangle x1="-0.00508125" y1="1.6383" x2="1.47828125" y2="1.64668125" layer="37"/>
<rectangle x1="1.9685" y1="1.6383" x2="2.34188125" y2="1.64668125" layer="37"/>
<rectangle x1="3.50011875" y1="1.6383" x2="3.8735" y2="1.64668125" layer="37"/>
<rectangle x1="4.10971875" y1="1.6383" x2="4.4831" y2="1.64668125" layer="37"/>
<rectangle x1="5.63371875" y1="1.6383" x2="6.01471875" y2="1.64668125" layer="37"/>
<rectangle x1="6.6421" y1="1.6383" x2="7.01548125" y2="1.64668125" layer="37"/>
<rectangle x1="7.58951875" y1="1.6383" x2="7.97051875" y2="1.64668125" layer="37"/>
<rectangle x1="8.40231875" y1="1.6383" x2="8.84428125" y2="1.64668125" layer="37"/>
<rectangle x1="9.12368125" y1="1.6383" x2="9.49451875" y2="1.64668125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.64668125" x2="1.4859" y2="1.65531875" layer="37"/>
<rectangle x1="1.9685" y1="1.64668125" x2="2.34188125" y2="1.65531875" layer="37"/>
<rectangle x1="3.50011875" y1="1.64668125" x2="3.8735" y2="1.65531875" layer="37"/>
<rectangle x1="4.10971875" y1="1.64668125" x2="4.4831" y2="1.65531875" layer="37"/>
<rectangle x1="5.63371875" y1="1.64668125" x2="6.01471875" y2="1.65531875" layer="37"/>
<rectangle x1="6.6421" y1="1.64668125" x2="7.01548125" y2="1.65531875" layer="37"/>
<rectangle x1="7.58951875" y1="1.64668125" x2="7.97051875" y2="1.65531875" layer="37"/>
<rectangle x1="8.3947" y1="1.64668125" x2="8.83411875" y2="1.65531875" layer="37"/>
<rectangle x1="9.12368125" y1="1.64668125" x2="9.49451875" y2="1.65531875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.65531875" x2="1.49351875" y2="1.6637" layer="37"/>
<rectangle x1="1.9685" y1="1.65531875" x2="2.34188125" y2="1.6637" layer="37"/>
<rectangle x1="3.50011875" y1="1.65531875" x2="3.8735" y2="1.6637" layer="37"/>
<rectangle x1="4.10971875" y1="1.65531875" x2="4.4831" y2="1.6637" layer="37"/>
<rectangle x1="5.63371875" y1="1.65531875" x2="6.01471875" y2="1.6637" layer="37"/>
<rectangle x1="6.6421" y1="1.65531875" x2="7.01548125" y2="1.6637" layer="37"/>
<rectangle x1="7.58951875" y1="1.65531875" x2="7.97051875" y2="1.6637" layer="37"/>
<rectangle x1="8.38708125" y1="1.65531875" x2="8.81888125" y2="1.6637" layer="37"/>
<rectangle x1="9.12368125" y1="1.65531875" x2="9.49451875" y2="1.6637" layer="37"/>
<rectangle x1="-0.00508125" y1="1.6637" x2="1.50368125" y2="1.67208125" layer="37"/>
<rectangle x1="1.9685" y1="1.6637" x2="2.34188125" y2="1.67208125" layer="37"/>
<rectangle x1="3.50011875" y1="1.6637" x2="3.8735" y2="1.67208125" layer="37"/>
<rectangle x1="4.10971875" y1="1.6637" x2="4.4831" y2="1.67208125" layer="37"/>
<rectangle x1="5.63371875" y1="1.6637" x2="6.01471875" y2="1.67208125" layer="37"/>
<rectangle x1="6.6421" y1="1.6637" x2="7.01548125" y2="1.67208125" layer="37"/>
<rectangle x1="7.58951875" y1="1.6637" x2="7.97051875" y2="1.67208125" layer="37"/>
<rectangle x1="8.3693" y1="1.6637" x2="8.80871875" y2="1.67208125" layer="37"/>
<rectangle x1="9.12368125" y1="1.6637" x2="9.49451875" y2="1.67208125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.67208125" x2="1.50368125" y2="1.68071875" layer="37"/>
<rectangle x1="1.9685" y1="1.67208125" x2="2.34188125" y2="1.68071875" layer="37"/>
<rectangle x1="3.50011875" y1="1.67208125" x2="3.8735" y2="1.68071875" layer="37"/>
<rectangle x1="4.10971875" y1="1.67208125" x2="4.4831" y2="1.68071875" layer="37"/>
<rectangle x1="5.63371875" y1="1.67208125" x2="6.01471875" y2="1.68071875" layer="37"/>
<rectangle x1="6.6421" y1="1.67208125" x2="7.01548125" y2="1.68071875" layer="37"/>
<rectangle x1="7.58951875" y1="1.67208125" x2="7.97051875" y2="1.68071875" layer="37"/>
<rectangle x1="8.3693" y1="1.67208125" x2="8.8011" y2="1.68071875" layer="37"/>
<rectangle x1="9.12368125" y1="1.67208125" x2="9.49451875" y2="1.68071875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.68071875" x2="1.5113" y2="1.6891" layer="37"/>
<rectangle x1="1.9685" y1="1.68071875" x2="2.34188125" y2="1.6891" layer="37"/>
<rectangle x1="3.50011875" y1="1.68071875" x2="3.8735" y2="1.6891" layer="37"/>
<rectangle x1="4.10971875" y1="1.68071875" x2="4.4831" y2="1.6891" layer="37"/>
<rectangle x1="5.63371875" y1="1.68071875" x2="6.01471875" y2="1.6891" layer="37"/>
<rectangle x1="6.6421" y1="1.68071875" x2="7.01548125" y2="1.6891" layer="37"/>
<rectangle x1="7.58951875" y1="1.68071875" x2="7.97051875" y2="1.6891" layer="37"/>
<rectangle x1="8.35151875" y1="1.68071875" x2="8.79348125" y2="1.6891" layer="37"/>
<rectangle x1="9.12368125" y1="1.68071875" x2="9.49451875" y2="1.6891" layer="37"/>
<rectangle x1="-0.00508125" y1="1.6891" x2="1.5113" y2="1.69748125" layer="37"/>
<rectangle x1="1.9685" y1="1.6891" x2="2.34188125" y2="1.69748125" layer="37"/>
<rectangle x1="3.50011875" y1="1.6891" x2="3.8735" y2="1.69748125" layer="37"/>
<rectangle x1="4.10971875" y1="1.6891" x2="4.4831" y2="1.69748125" layer="37"/>
<rectangle x1="5.63371875" y1="1.6891" x2="6.01471875" y2="1.69748125" layer="37"/>
<rectangle x1="6.6421" y1="1.6891" x2="7.01548125" y2="1.69748125" layer="37"/>
<rectangle x1="7.58951875" y1="1.6891" x2="7.97051875" y2="1.69748125" layer="37"/>
<rectangle x1="8.3439" y1="1.6891" x2="8.78331875" y2="1.69748125" layer="37"/>
<rectangle x1="9.12368125" y1="1.6891" x2="9.49451875" y2="1.69748125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.69748125" x2="1.5113" y2="1.70611875" layer="37"/>
<rectangle x1="1.9685" y1="1.69748125" x2="2.34188125" y2="1.70611875" layer="37"/>
<rectangle x1="3.50011875" y1="1.69748125" x2="3.8735" y2="1.70611875" layer="37"/>
<rectangle x1="4.10971875" y1="1.69748125" x2="4.4831" y2="1.70611875" layer="37"/>
<rectangle x1="5.63371875" y1="1.69748125" x2="6.01471875" y2="1.70611875" layer="37"/>
<rectangle x1="6.6421" y1="1.69748125" x2="7.01548125" y2="1.70611875" layer="37"/>
<rectangle x1="7.58951875" y1="1.69748125" x2="7.97051875" y2="1.70611875" layer="37"/>
<rectangle x1="8.33628125" y1="1.69748125" x2="8.7757" y2="1.70611875" layer="37"/>
<rectangle x1="9.12368125" y1="1.69748125" x2="9.49451875" y2="1.70611875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.70611875" x2="1.5113" y2="1.7145" layer="37"/>
<rectangle x1="1.9685" y1="1.70611875" x2="2.34188125" y2="1.7145" layer="37"/>
<rectangle x1="3.50011875" y1="1.70611875" x2="3.8735" y2="1.7145" layer="37"/>
<rectangle x1="4.10971875" y1="1.70611875" x2="4.4831" y2="1.7145" layer="37"/>
<rectangle x1="5.63371875" y1="1.70611875" x2="6.01471875" y2="1.7145" layer="37"/>
<rectangle x1="6.6421" y1="1.70611875" x2="7.01548125" y2="1.7145" layer="37"/>
<rectangle x1="7.58951875" y1="1.70611875" x2="7.97051875" y2="1.7145" layer="37"/>
<rectangle x1="8.32611875" y1="1.70611875" x2="8.76808125" y2="1.7145" layer="37"/>
<rectangle x1="9.12368125" y1="1.70611875" x2="9.49451875" y2="1.7145" layer="37"/>
<rectangle x1="-0.00508125" y1="1.7145" x2="1.5113" y2="1.72288125" layer="37"/>
<rectangle x1="1.9685" y1="1.7145" x2="2.34188125" y2="1.72288125" layer="37"/>
<rectangle x1="3.50011875" y1="1.7145" x2="3.8735" y2="1.72288125" layer="37"/>
<rectangle x1="4.10971875" y1="1.7145" x2="4.4831" y2="1.72288125" layer="37"/>
<rectangle x1="5.63371875" y1="1.7145" x2="6.01471875" y2="1.72288125" layer="37"/>
<rectangle x1="6.6421" y1="1.7145" x2="7.01548125" y2="1.72288125" layer="37"/>
<rectangle x1="7.58951875" y1="1.7145" x2="7.97051875" y2="1.72288125" layer="37"/>
<rectangle x1="8.3185" y1="1.7145" x2="8.75791875" y2="1.72288125" layer="37"/>
<rectangle x1="9.12368125" y1="1.7145" x2="9.49451875" y2="1.72288125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.72288125" x2="1.5113" y2="1.73151875" layer="37"/>
<rectangle x1="1.9685" y1="1.72288125" x2="2.34188125" y2="1.73151875" layer="37"/>
<rectangle x1="3.50011875" y1="1.72288125" x2="3.8735" y2="1.73151875" layer="37"/>
<rectangle x1="4.10971875" y1="1.72288125" x2="4.4831" y2="1.73151875" layer="37"/>
<rectangle x1="5.63371875" y1="1.72288125" x2="6.01471875" y2="1.73151875" layer="37"/>
<rectangle x1="6.6421" y1="1.72288125" x2="7.01548125" y2="1.73151875" layer="37"/>
<rectangle x1="7.58951875" y1="1.72288125" x2="7.97051875" y2="1.73151875" layer="37"/>
<rectangle x1="8.31088125" y1="1.72288125" x2="8.7503" y2="1.73151875" layer="37"/>
<rectangle x1="9.12368125" y1="1.72288125" x2="9.49451875" y2="1.73151875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.73151875" x2="1.5113" y2="1.7399" layer="37"/>
<rectangle x1="1.9685" y1="1.73151875" x2="2.34188125" y2="1.7399" layer="37"/>
<rectangle x1="3.50011875" y1="1.73151875" x2="3.8735" y2="1.7399" layer="37"/>
<rectangle x1="4.10971875" y1="1.73151875" x2="4.4831" y2="1.7399" layer="37"/>
<rectangle x1="5.63371875" y1="1.73151875" x2="6.01471875" y2="1.7399" layer="37"/>
<rectangle x1="6.6421" y1="1.73151875" x2="7.01548125" y2="1.7399" layer="37"/>
<rectangle x1="7.58951875" y1="1.73151875" x2="7.97051875" y2="1.7399" layer="37"/>
<rectangle x1="8.30071875" y1="1.73151875" x2="8.74268125" y2="1.7399" layer="37"/>
<rectangle x1="9.12368125" y1="1.73151875" x2="9.49451875" y2="1.7399" layer="37"/>
<rectangle x1="-0.00508125" y1="1.7399" x2="1.5113" y2="1.74828125" layer="37"/>
<rectangle x1="1.9685" y1="1.7399" x2="2.34188125" y2="1.74828125" layer="37"/>
<rectangle x1="3.50011875" y1="1.7399" x2="3.8735" y2="1.74828125" layer="37"/>
<rectangle x1="4.10971875" y1="1.7399" x2="4.4831" y2="1.74828125" layer="37"/>
<rectangle x1="5.63371875" y1="1.7399" x2="6.01471875" y2="1.74828125" layer="37"/>
<rectangle x1="6.6421" y1="1.7399" x2="7.01548125" y2="1.74828125" layer="37"/>
<rectangle x1="7.58951875" y1="1.7399" x2="7.97051875" y2="1.74828125" layer="37"/>
<rectangle x1="8.2931" y1="1.7399" x2="8.73251875" y2="1.74828125" layer="37"/>
<rectangle x1="9.12368125" y1="1.7399" x2="9.49451875" y2="1.74828125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.74828125" x2="1.50368125" y2="1.75691875" layer="37"/>
<rectangle x1="1.9685" y1="1.74828125" x2="2.34188125" y2="1.75691875" layer="37"/>
<rectangle x1="3.50011875" y1="1.74828125" x2="3.8735" y2="1.75691875" layer="37"/>
<rectangle x1="4.10971875" y1="1.74828125" x2="4.4831" y2="1.75691875" layer="37"/>
<rectangle x1="5.63371875" y1="1.74828125" x2="6.01471875" y2="1.75691875" layer="37"/>
<rectangle x1="6.6421" y1="1.74828125" x2="7.01548125" y2="1.75691875" layer="37"/>
<rectangle x1="7.58951875" y1="1.74828125" x2="7.97051875" y2="1.75691875" layer="37"/>
<rectangle x1="8.28548125" y1="1.74828125" x2="8.7249" y2="1.75691875" layer="37"/>
<rectangle x1="9.12368125" y1="1.74828125" x2="9.49451875" y2="1.75691875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.75691875" x2="1.50368125" y2="1.7653" layer="37"/>
<rectangle x1="1.9685" y1="1.75691875" x2="2.34188125" y2="1.7653" layer="37"/>
<rectangle x1="3.50011875" y1="1.75691875" x2="3.8735" y2="1.7653" layer="37"/>
<rectangle x1="4.10971875" y1="1.75691875" x2="4.4831" y2="1.7653" layer="37"/>
<rectangle x1="5.63371875" y1="1.75691875" x2="6.01471875" y2="1.7653" layer="37"/>
<rectangle x1="6.6421" y1="1.75691875" x2="7.01548125" y2="1.7653" layer="37"/>
<rectangle x1="7.58951875" y1="1.75691875" x2="7.97051875" y2="1.7653" layer="37"/>
<rectangle x1="8.27531875" y1="1.75691875" x2="8.70711875" y2="1.7653" layer="37"/>
<rectangle x1="9.12368125" y1="1.75691875" x2="9.49451875" y2="1.7653" layer="37"/>
<rectangle x1="-0.00508125" y1="1.7653" x2="1.49351875" y2="1.77368125" layer="37"/>
<rectangle x1="1.9685" y1="1.7653" x2="2.34188125" y2="1.77368125" layer="37"/>
<rectangle x1="3.50011875" y1="1.7653" x2="3.8735" y2="1.77368125" layer="37"/>
<rectangle x1="4.10971875" y1="1.7653" x2="4.4831" y2="1.77368125" layer="37"/>
<rectangle x1="5.63371875" y1="1.7653" x2="6.01471875" y2="1.77368125" layer="37"/>
<rectangle x1="6.6421" y1="1.7653" x2="7.01548125" y2="1.77368125" layer="37"/>
<rectangle x1="7.58951875" y1="1.7653" x2="7.97051875" y2="1.77368125" layer="37"/>
<rectangle x1="8.2677" y1="1.7653" x2="8.6995" y2="1.77368125" layer="37"/>
<rectangle x1="9.12368125" y1="1.7653" x2="9.49451875" y2="1.77368125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.77368125" x2="1.4859" y2="1.78231875" layer="37"/>
<rectangle x1="1.9685" y1="1.77368125" x2="2.34188125" y2="1.78231875" layer="37"/>
<rectangle x1="3.50011875" y1="1.77368125" x2="3.8735" y2="1.78231875" layer="37"/>
<rectangle x1="4.10971875" y1="1.77368125" x2="4.4831" y2="1.78231875" layer="37"/>
<rectangle x1="5.63371875" y1="1.77368125" x2="6.01471875" y2="1.78231875" layer="37"/>
<rectangle x1="6.6421" y1="1.77368125" x2="7.01548125" y2="1.78231875" layer="37"/>
<rectangle x1="7.58951875" y1="1.77368125" x2="7.97051875" y2="1.78231875" layer="37"/>
<rectangle x1="8.26008125" y1="1.77368125" x2="8.69188125" y2="1.78231875" layer="37"/>
<rectangle x1="9.12368125" y1="1.77368125" x2="9.49451875" y2="1.78231875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.78231875" x2="1.47828125" y2="1.7907" layer="37"/>
<rectangle x1="1.9685" y1="1.78231875" x2="2.34188125" y2="1.7907" layer="37"/>
<rectangle x1="3.50011875" y1="1.78231875" x2="3.8735" y2="1.7907" layer="37"/>
<rectangle x1="4.10971875" y1="1.78231875" x2="4.4831" y2="1.7907" layer="37"/>
<rectangle x1="5.63371875" y1="1.78231875" x2="6.01471875" y2="1.7907" layer="37"/>
<rectangle x1="6.6421" y1="1.78231875" x2="7.01548125" y2="1.7907" layer="37"/>
<rectangle x1="7.58951875" y1="1.78231875" x2="7.97051875" y2="1.7907" layer="37"/>
<rectangle x1="8.2423" y1="1.78231875" x2="8.68171875" y2="1.7907" layer="37"/>
<rectangle x1="9.12368125" y1="1.78231875" x2="9.49451875" y2="1.7907" layer="37"/>
<rectangle x1="-0.00508125" y1="1.7907" x2="1.46811875" y2="1.79908125" layer="37"/>
<rectangle x1="1.9685" y1="1.7907" x2="2.34188125" y2="1.79908125" layer="37"/>
<rectangle x1="3.50011875" y1="1.7907" x2="3.8735" y2="1.79908125" layer="37"/>
<rectangle x1="4.10971875" y1="1.7907" x2="4.4831" y2="1.79908125" layer="37"/>
<rectangle x1="5.63371875" y1="1.7907" x2="6.01471875" y2="1.79908125" layer="37"/>
<rectangle x1="6.6421" y1="1.7907" x2="7.01548125" y2="1.79908125" layer="37"/>
<rectangle x1="7.58951875" y1="1.7907" x2="7.97051875" y2="1.79908125" layer="37"/>
<rectangle x1="8.23468125" y1="1.7907" x2="8.6741" y2="1.79908125" layer="37"/>
<rectangle x1="9.12368125" y1="1.7907" x2="9.49451875" y2="1.79908125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.79908125" x2="1.45288125" y2="1.80771875" layer="37"/>
<rectangle x1="1.9685" y1="1.79908125" x2="2.34188125" y2="1.80771875" layer="37"/>
<rectangle x1="3.50011875" y1="1.79908125" x2="3.8735" y2="1.80771875" layer="37"/>
<rectangle x1="4.10971875" y1="1.79908125" x2="4.4831" y2="1.80771875" layer="37"/>
<rectangle x1="5.63371875" y1="1.79908125" x2="6.01471875" y2="1.80771875" layer="37"/>
<rectangle x1="6.6421" y1="1.79908125" x2="7.01548125" y2="1.80771875" layer="37"/>
<rectangle x1="7.58951875" y1="1.79908125" x2="7.97051875" y2="1.80771875" layer="37"/>
<rectangle x1="8.22451875" y1="1.79908125" x2="8.66648125" y2="1.80771875" layer="37"/>
<rectangle x1="9.12368125" y1="1.79908125" x2="9.49451875" y2="1.80771875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.80771875" x2="1.4351" y2="1.8161" layer="37"/>
<rectangle x1="1.9685" y1="1.80771875" x2="2.34188125" y2="1.8161" layer="37"/>
<rectangle x1="3.50011875" y1="1.80771875" x2="3.8735" y2="1.8161" layer="37"/>
<rectangle x1="4.10971875" y1="1.80771875" x2="4.4831" y2="1.8161" layer="37"/>
<rectangle x1="5.63371875" y1="1.80771875" x2="6.01471875" y2="1.8161" layer="37"/>
<rectangle x1="6.6421" y1="1.80771875" x2="7.01548125" y2="1.8161" layer="37"/>
<rectangle x1="7.58951875" y1="1.80771875" x2="7.97051875" y2="1.8161" layer="37"/>
<rectangle x1="8.2169" y1="1.80771875" x2="8.65631875" y2="1.8161" layer="37"/>
<rectangle x1="9.12368125" y1="1.80771875" x2="9.49451875" y2="1.8161" layer="37"/>
<rectangle x1="-0.00508125" y1="1.8161" x2="1.4097" y2="1.82448125" layer="37"/>
<rectangle x1="1.9685" y1="1.8161" x2="2.34188125" y2="1.82448125" layer="37"/>
<rectangle x1="3.50011875" y1="1.8161" x2="3.8735" y2="1.82448125" layer="37"/>
<rectangle x1="4.10971875" y1="1.8161" x2="4.4831" y2="1.82448125" layer="37"/>
<rectangle x1="5.63371875" y1="1.8161" x2="6.01471875" y2="1.82448125" layer="37"/>
<rectangle x1="6.6421" y1="1.8161" x2="7.01548125" y2="1.82448125" layer="37"/>
<rectangle x1="7.58951875" y1="1.8161" x2="7.97051875" y2="1.82448125" layer="37"/>
<rectangle x1="8.20928125" y1="1.8161" x2="8.6487" y2="1.82448125" layer="37"/>
<rectangle x1="9.12368125" y1="1.8161" x2="9.49451875" y2="1.82448125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.82448125" x2="0.3683" y2="1.83311875" layer="37"/>
<rectangle x1="1.9685" y1="1.82448125" x2="2.34188125" y2="1.83311875" layer="37"/>
<rectangle x1="3.50011875" y1="1.82448125" x2="3.8735" y2="1.83311875" layer="37"/>
<rectangle x1="4.10971875" y1="1.82448125" x2="4.4831" y2="1.83311875" layer="37"/>
<rectangle x1="5.63371875" y1="1.82448125" x2="6.01471875" y2="1.83311875" layer="37"/>
<rectangle x1="6.6421" y1="1.82448125" x2="7.01548125" y2="1.83311875" layer="37"/>
<rectangle x1="7.58951875" y1="1.82448125" x2="7.97051875" y2="1.83311875" layer="37"/>
<rectangle x1="8.19911875" y1="1.82448125" x2="8.64108125" y2="1.83311875" layer="37"/>
<rectangle x1="9.12368125" y1="1.82448125" x2="9.49451875" y2="1.83311875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.83311875" x2="0.3683" y2="1.8415" layer="37"/>
<rectangle x1="1.9685" y1="1.83311875" x2="2.34188125" y2="1.8415" layer="37"/>
<rectangle x1="3.50011875" y1="1.83311875" x2="3.8735" y2="1.8415" layer="37"/>
<rectangle x1="4.10971875" y1="1.83311875" x2="4.4831" y2="1.8415" layer="37"/>
<rectangle x1="5.63371875" y1="1.83311875" x2="6.01471875" y2="1.8415" layer="37"/>
<rectangle x1="6.6421" y1="1.83311875" x2="7.01548125" y2="1.8415" layer="37"/>
<rectangle x1="7.58951875" y1="1.83311875" x2="7.97051875" y2="1.8415" layer="37"/>
<rectangle x1="8.1915" y1="1.83311875" x2="8.63091875" y2="1.8415" layer="37"/>
<rectangle x1="9.12368125" y1="1.83311875" x2="9.49451875" y2="1.8415" layer="37"/>
<rectangle x1="-0.00508125" y1="1.8415" x2="0.3683" y2="1.84988125" layer="37"/>
<rectangle x1="1.9685" y1="1.8415" x2="2.34188125" y2="1.84988125" layer="37"/>
<rectangle x1="3.50011875" y1="1.8415" x2="3.8735" y2="1.84988125" layer="37"/>
<rectangle x1="4.10971875" y1="1.8415" x2="4.4831" y2="1.84988125" layer="37"/>
<rectangle x1="5.63371875" y1="1.8415" x2="6.01471875" y2="1.84988125" layer="37"/>
<rectangle x1="6.6421" y1="1.8415" x2="7.01548125" y2="1.84988125" layer="37"/>
<rectangle x1="7.58951875" y1="1.8415" x2="7.97051875" y2="1.84988125" layer="37"/>
<rectangle x1="8.18388125" y1="1.8415" x2="8.6233" y2="1.84988125" layer="37"/>
<rectangle x1="9.12368125" y1="1.8415" x2="9.49451875" y2="1.84988125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.84988125" x2="0.3683" y2="1.85851875" layer="37"/>
<rectangle x1="1.9685" y1="1.84988125" x2="2.34188125" y2="1.85851875" layer="37"/>
<rectangle x1="3.50011875" y1="1.84988125" x2="3.8735" y2="1.85851875" layer="37"/>
<rectangle x1="4.10971875" y1="1.84988125" x2="4.4831" y2="1.85851875" layer="37"/>
<rectangle x1="5.63371875" y1="1.84988125" x2="6.01471875" y2="1.85851875" layer="37"/>
<rectangle x1="6.6421" y1="1.84988125" x2="7.01548125" y2="1.85851875" layer="37"/>
<rectangle x1="7.58951875" y1="1.84988125" x2="7.97051875" y2="1.85851875" layer="37"/>
<rectangle x1="8.17371875" y1="1.84988125" x2="8.61568125" y2="1.85851875" layer="37"/>
<rectangle x1="9.12368125" y1="1.84988125" x2="9.49451875" y2="1.85851875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.85851875" x2="0.3683" y2="1.8669" layer="37"/>
<rectangle x1="1.9685" y1="1.85851875" x2="2.34188125" y2="1.8669" layer="37"/>
<rectangle x1="3.50011875" y1="1.85851875" x2="3.8735" y2="1.8669" layer="37"/>
<rectangle x1="4.10971875" y1="1.85851875" x2="4.4831" y2="1.8669" layer="37"/>
<rectangle x1="5.63371875" y1="1.85851875" x2="6.01471875" y2="1.8669" layer="37"/>
<rectangle x1="6.6421" y1="1.85851875" x2="7.01548125" y2="1.8669" layer="37"/>
<rectangle x1="7.58951875" y1="1.85851875" x2="7.97051875" y2="1.8669" layer="37"/>
<rectangle x1="8.1661" y1="1.85851875" x2="8.5979" y2="1.8669" layer="37"/>
<rectangle x1="9.12368125" y1="1.85851875" x2="9.49451875" y2="1.8669" layer="37"/>
<rectangle x1="-0.00508125" y1="1.8669" x2="0.3683" y2="1.87528125" layer="37"/>
<rectangle x1="1.9685" y1="1.8669" x2="2.34188125" y2="1.87528125" layer="37"/>
<rectangle x1="3.50011875" y1="1.8669" x2="3.8735" y2="1.87528125" layer="37"/>
<rectangle x1="4.10971875" y1="1.8669" x2="4.4831" y2="1.87528125" layer="37"/>
<rectangle x1="5.63371875" y1="1.8669" x2="6.01471875" y2="1.87528125" layer="37"/>
<rectangle x1="6.6421" y1="1.8669" x2="7.01548125" y2="1.87528125" layer="37"/>
<rectangle x1="7.58951875" y1="1.8669" x2="7.97051875" y2="1.87528125" layer="37"/>
<rectangle x1="8.15848125" y1="1.8669" x2="8.5979" y2="1.87528125" layer="37"/>
<rectangle x1="9.12368125" y1="1.8669" x2="9.49451875" y2="1.87528125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.87528125" x2="0.3683" y2="1.88391875" layer="37"/>
<rectangle x1="1.9685" y1="1.87528125" x2="2.34188125" y2="1.88391875" layer="37"/>
<rectangle x1="3.50011875" y1="1.87528125" x2="3.8735" y2="1.88391875" layer="37"/>
<rectangle x1="4.10971875" y1="1.87528125" x2="4.4831" y2="1.88391875" layer="37"/>
<rectangle x1="5.63371875" y1="1.87528125" x2="6.01471875" y2="1.88391875" layer="37"/>
<rectangle x1="6.6421" y1="1.87528125" x2="7.01548125" y2="1.88391875" layer="37"/>
<rectangle x1="7.58951875" y1="1.87528125" x2="7.97051875" y2="1.88391875" layer="37"/>
<rectangle x1="8.14831875" y1="1.87528125" x2="8.58011875" y2="1.88391875" layer="37"/>
<rectangle x1="9.12368125" y1="1.87528125" x2="9.49451875" y2="1.88391875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.88391875" x2="0.3683" y2="1.8923" layer="37"/>
<rectangle x1="1.9685" y1="1.88391875" x2="2.34188125" y2="1.8923" layer="37"/>
<rectangle x1="3.50011875" y1="1.88391875" x2="3.8735" y2="1.8923" layer="37"/>
<rectangle x1="4.10971875" y1="1.88391875" x2="4.4831" y2="1.8923" layer="37"/>
<rectangle x1="5.63371875" y1="1.88391875" x2="6.01471875" y2="1.8923" layer="37"/>
<rectangle x1="6.6421" y1="1.88391875" x2="7.01548125" y2="1.8923" layer="37"/>
<rectangle x1="7.58951875" y1="1.88391875" x2="7.97051875" y2="1.8923" layer="37"/>
<rectangle x1="8.1407" y1="1.88391875" x2="8.5725" y2="1.8923" layer="37"/>
<rectangle x1="9.12368125" y1="1.88391875" x2="9.49451875" y2="1.8923" layer="37"/>
<rectangle x1="-0.00508125" y1="1.8923" x2="0.3683" y2="1.90068125" layer="37"/>
<rectangle x1="1.9685" y1="1.8923" x2="2.34188125" y2="1.90068125" layer="37"/>
<rectangle x1="3.50011875" y1="1.8923" x2="3.8735" y2="1.90068125" layer="37"/>
<rectangle x1="4.10971875" y1="1.8923" x2="4.4831" y2="1.90068125" layer="37"/>
<rectangle x1="5.63371875" y1="1.8923" x2="6.01471875" y2="1.90068125" layer="37"/>
<rectangle x1="6.6421" y1="1.8923" x2="7.01548125" y2="1.90068125" layer="37"/>
<rectangle x1="7.58951875" y1="1.8923" x2="7.97051875" y2="1.90068125" layer="37"/>
<rectangle x1="8.12291875" y1="1.8923" x2="8.56488125" y2="1.90068125" layer="37"/>
<rectangle x1="9.12368125" y1="1.8923" x2="9.49451875" y2="1.90068125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.90068125" x2="0.3683" y2="1.90931875" layer="37"/>
<rectangle x1="1.9685" y1="1.90068125" x2="2.34188125" y2="1.90931875" layer="37"/>
<rectangle x1="3.50011875" y1="1.90068125" x2="3.8735" y2="1.90931875" layer="37"/>
<rectangle x1="4.10971875" y1="1.90068125" x2="4.4831" y2="1.90931875" layer="37"/>
<rectangle x1="5.63371875" y1="1.90068125" x2="6.01471875" y2="1.90931875" layer="37"/>
<rectangle x1="6.6421" y1="1.90068125" x2="7.01548125" y2="1.90931875" layer="37"/>
<rectangle x1="7.58951875" y1="1.90068125" x2="7.97051875" y2="1.90931875" layer="37"/>
<rectangle x1="8.1153" y1="1.90068125" x2="8.55471875" y2="1.90931875" layer="37"/>
<rectangle x1="9.12368125" y1="1.90068125" x2="9.49451875" y2="1.90931875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.90931875" x2="0.3683" y2="1.9177" layer="37"/>
<rectangle x1="1.9685" y1="1.90931875" x2="2.34188125" y2="1.9177" layer="37"/>
<rectangle x1="3.50011875" y1="1.90931875" x2="3.8735" y2="1.9177" layer="37"/>
<rectangle x1="4.10971875" y1="1.90931875" x2="4.4831" y2="1.9177" layer="37"/>
<rectangle x1="5.63371875" y1="1.90931875" x2="6.01471875" y2="1.9177" layer="37"/>
<rectangle x1="6.6421" y1="1.90931875" x2="7.01548125" y2="1.9177" layer="37"/>
<rectangle x1="7.58951875" y1="1.90931875" x2="7.97051875" y2="1.9177" layer="37"/>
<rectangle x1="8.10768125" y1="1.90931875" x2="8.5471" y2="1.9177" layer="37"/>
<rectangle x1="9.12368125" y1="1.90931875" x2="9.49451875" y2="1.9177" layer="37"/>
<rectangle x1="-0.00508125" y1="1.9177" x2="0.3683" y2="1.92608125" layer="37"/>
<rectangle x1="1.9685" y1="1.9177" x2="2.34188125" y2="1.92608125" layer="37"/>
<rectangle x1="3.50011875" y1="1.9177" x2="3.8735" y2="1.92608125" layer="37"/>
<rectangle x1="4.10971875" y1="1.9177" x2="4.4831" y2="1.92608125" layer="37"/>
<rectangle x1="5.63371875" y1="1.9177" x2="6.01471875" y2="1.92608125" layer="37"/>
<rectangle x1="6.6421" y1="1.9177" x2="7.01548125" y2="1.92608125" layer="37"/>
<rectangle x1="7.58951875" y1="1.9177" x2="7.97051875" y2="1.92608125" layer="37"/>
<rectangle x1="8.09751875" y1="1.9177" x2="8.53948125" y2="1.92608125" layer="37"/>
<rectangle x1="9.12368125" y1="1.9177" x2="9.49451875" y2="1.92608125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.92608125" x2="0.3683" y2="1.93471875" layer="37"/>
<rectangle x1="1.9685" y1="1.92608125" x2="2.34188125" y2="1.93471875" layer="37"/>
<rectangle x1="3.50011875" y1="1.92608125" x2="3.8735" y2="1.93471875" layer="37"/>
<rectangle x1="4.10971875" y1="1.92608125" x2="4.4831" y2="1.93471875" layer="37"/>
<rectangle x1="5.63371875" y1="1.92608125" x2="6.01471875" y2="1.93471875" layer="37"/>
<rectangle x1="6.6421" y1="1.92608125" x2="7.01548125" y2="1.93471875" layer="37"/>
<rectangle x1="7.58951875" y1="1.92608125" x2="7.97051875" y2="1.93471875" layer="37"/>
<rectangle x1="8.0899" y1="1.92608125" x2="8.52931875" y2="1.93471875" layer="37"/>
<rectangle x1="9.12368125" y1="1.92608125" x2="9.49451875" y2="1.93471875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.93471875" x2="0.3683" y2="1.9431" layer="37"/>
<rectangle x1="1.9685" y1="1.93471875" x2="2.34188125" y2="1.9431" layer="37"/>
<rectangle x1="3.50011875" y1="1.93471875" x2="3.8735" y2="1.9431" layer="37"/>
<rectangle x1="4.10971875" y1="1.93471875" x2="4.4831" y2="1.9431" layer="37"/>
<rectangle x1="5.63371875" y1="1.93471875" x2="6.01471875" y2="1.9431" layer="37"/>
<rectangle x1="6.6421" y1="1.93471875" x2="7.01548125" y2="1.9431" layer="37"/>
<rectangle x1="7.58951875" y1="1.93471875" x2="7.97051875" y2="1.9431" layer="37"/>
<rectangle x1="8.08228125" y1="1.93471875" x2="8.5217" y2="1.9431" layer="37"/>
<rectangle x1="9.12368125" y1="1.93471875" x2="9.49451875" y2="1.9431" layer="37"/>
<rectangle x1="-0.00508125" y1="1.9431" x2="0.3683" y2="1.95148125" layer="37"/>
<rectangle x1="1.9685" y1="1.9431" x2="2.34188125" y2="1.95148125" layer="37"/>
<rectangle x1="3.50011875" y1="1.9431" x2="3.8735" y2="1.95148125" layer="37"/>
<rectangle x1="4.10971875" y1="1.9431" x2="4.4831" y2="1.95148125" layer="37"/>
<rectangle x1="5.63371875" y1="1.9431" x2="6.01471875" y2="1.95148125" layer="37"/>
<rectangle x1="6.6421" y1="1.9431" x2="7.01548125" y2="1.95148125" layer="37"/>
<rectangle x1="7.58951875" y1="1.9431" x2="7.97051875" y2="1.95148125" layer="37"/>
<rectangle x1="8.07211875" y1="1.9431" x2="8.51408125" y2="1.95148125" layer="37"/>
<rectangle x1="9.12368125" y1="1.9431" x2="9.49451875" y2="1.95148125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.95148125" x2="0.3683" y2="1.96011875" layer="37"/>
<rectangle x1="1.9685" y1="1.95148125" x2="2.34188125" y2="1.96011875" layer="37"/>
<rectangle x1="3.50011875" y1="1.95148125" x2="3.8735" y2="1.96011875" layer="37"/>
<rectangle x1="4.10971875" y1="1.95148125" x2="4.4831" y2="1.96011875" layer="37"/>
<rectangle x1="5.63371875" y1="1.95148125" x2="6.01471875" y2="1.96011875" layer="37"/>
<rectangle x1="6.6421" y1="1.95148125" x2="7.01548125" y2="1.96011875" layer="37"/>
<rectangle x1="7.58951875" y1="1.95148125" x2="7.97051875" y2="1.96011875" layer="37"/>
<rectangle x1="8.0645" y1="1.95148125" x2="8.50391875" y2="1.96011875" layer="37"/>
<rectangle x1="9.12368125" y1="1.95148125" x2="9.49451875" y2="1.96011875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.96011875" x2="0.3683" y2="1.9685" layer="37"/>
<rectangle x1="1.9685" y1="1.96011875" x2="2.34188125" y2="1.9685" layer="37"/>
<rectangle x1="3.50011875" y1="1.96011875" x2="3.8735" y2="1.9685" layer="37"/>
<rectangle x1="4.10971875" y1="1.96011875" x2="4.4831" y2="1.9685" layer="37"/>
<rectangle x1="5.63371875" y1="1.96011875" x2="6.01471875" y2="1.9685" layer="37"/>
<rectangle x1="6.6421" y1="1.96011875" x2="7.01548125" y2="1.9685" layer="37"/>
<rectangle x1="7.58951875" y1="1.96011875" x2="7.97051875" y2="1.9685" layer="37"/>
<rectangle x1="8.05688125" y1="1.96011875" x2="8.4963" y2="1.9685" layer="37"/>
<rectangle x1="9.12368125" y1="1.96011875" x2="9.49451875" y2="1.9685" layer="37"/>
<rectangle x1="-0.00508125" y1="1.9685" x2="0.3683" y2="1.97688125" layer="37"/>
<rectangle x1="1.9685" y1="1.9685" x2="2.34188125" y2="1.97688125" layer="37"/>
<rectangle x1="3.50011875" y1="1.9685" x2="3.8735" y2="1.97688125" layer="37"/>
<rectangle x1="4.10971875" y1="1.9685" x2="4.4831" y2="1.97688125" layer="37"/>
<rectangle x1="5.63371875" y1="1.9685" x2="6.01471875" y2="1.97688125" layer="37"/>
<rectangle x1="6.6421" y1="1.9685" x2="7.01548125" y2="1.97688125" layer="37"/>
<rectangle x1="7.58951875" y1="1.9685" x2="7.97051875" y2="1.97688125" layer="37"/>
<rectangle x1="8.04671875" y1="1.9685" x2="8.48868125" y2="1.97688125" layer="37"/>
<rectangle x1="9.12368125" y1="1.9685" x2="9.49451875" y2="1.97688125" layer="37"/>
<rectangle x1="-0.00508125" y1="1.97688125" x2="0.3683" y2="1.98551875" layer="37"/>
<rectangle x1="1.9685" y1="1.97688125" x2="2.34188125" y2="1.98551875" layer="37"/>
<rectangle x1="3.50011875" y1="1.97688125" x2="3.8735" y2="1.98551875" layer="37"/>
<rectangle x1="4.10971875" y1="1.97688125" x2="4.4831" y2="1.98551875" layer="37"/>
<rectangle x1="5.63371875" y1="1.97688125" x2="6.01471875" y2="1.98551875" layer="37"/>
<rectangle x1="6.6421" y1="1.97688125" x2="7.01548125" y2="1.98551875" layer="37"/>
<rectangle x1="7.58951875" y1="1.97688125" x2="7.97051875" y2="1.98551875" layer="37"/>
<rectangle x1="8.0391" y1="1.97688125" x2="8.4709" y2="1.98551875" layer="37"/>
<rectangle x1="9.12368125" y1="1.97688125" x2="9.49451875" y2="1.98551875" layer="37"/>
<rectangle x1="-0.00508125" y1="1.98551875" x2="0.3683" y2="1.9939" layer="37"/>
<rectangle x1="1.9685" y1="1.98551875" x2="2.34188125" y2="1.9939" layer="37"/>
<rectangle x1="3.50011875" y1="1.98551875" x2="3.8735" y2="1.9939" layer="37"/>
<rectangle x1="4.10971875" y1="1.98551875" x2="4.4831" y2="1.9939" layer="37"/>
<rectangle x1="5.63371875" y1="1.98551875" x2="6.01471875" y2="1.9939" layer="37"/>
<rectangle x1="6.6421" y1="1.98551875" x2="7.01548125" y2="1.9939" layer="37"/>
<rectangle x1="7.58951875" y1="1.98551875" x2="7.97051875" y2="1.9939" layer="37"/>
<rectangle x1="8.02131875" y1="1.98551875" x2="8.46328125" y2="1.9939" layer="37"/>
<rectangle x1="9.12368125" y1="1.98551875" x2="9.49451875" y2="1.9939" layer="37"/>
<rectangle x1="-0.00508125" y1="1.9939" x2="0.3683" y2="2.00228125" layer="37"/>
<rectangle x1="1.9685" y1="1.9939" x2="2.34188125" y2="2.00228125" layer="37"/>
<rectangle x1="3.50011875" y1="1.9939" x2="3.8735" y2="2.00228125" layer="37"/>
<rectangle x1="4.10971875" y1="1.9939" x2="4.4831" y2="2.00228125" layer="37"/>
<rectangle x1="5.63371875" y1="1.9939" x2="6.01471875" y2="2.00228125" layer="37"/>
<rectangle x1="6.6421" y1="1.9939" x2="7.01548125" y2="2.00228125" layer="37"/>
<rectangle x1="7.58951875" y1="1.9939" x2="7.97051875" y2="2.00228125" layer="37"/>
<rectangle x1="8.0137" y1="1.9939" x2="8.45311875" y2="2.00228125" layer="37"/>
<rectangle x1="9.12368125" y1="1.9939" x2="9.49451875" y2="2.00228125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.00228125" x2="0.3683" y2="2.01091875" layer="37"/>
<rectangle x1="1.9685" y1="2.00228125" x2="2.34188125" y2="2.01091875" layer="37"/>
<rectangle x1="3.50011875" y1="2.00228125" x2="3.8735" y2="2.01091875" layer="37"/>
<rectangle x1="4.10971875" y1="2.00228125" x2="4.4831" y2="2.01091875" layer="37"/>
<rectangle x1="5.64388125" y1="2.00228125" x2="6.01471875" y2="2.01091875" layer="37"/>
<rectangle x1="6.6421" y1="2.00228125" x2="7.01548125" y2="2.01091875" layer="37"/>
<rectangle x1="7.58951875" y1="2.00228125" x2="7.97051875" y2="2.01091875" layer="37"/>
<rectangle x1="8.00608125" y1="2.00228125" x2="8.4455" y2="2.01091875" layer="37"/>
<rectangle x1="9.12368125" y1="2.00228125" x2="9.49451875" y2="2.01091875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.01091875" x2="0.3683" y2="2.0193" layer="37"/>
<rectangle x1="1.9685" y1="2.01091875" x2="2.34188125" y2="2.0193" layer="37"/>
<rectangle x1="3.50011875" y1="2.01091875" x2="3.8735" y2="2.0193" layer="37"/>
<rectangle x1="4.10971875" y1="2.01091875" x2="4.4831" y2="2.0193" layer="37"/>
<rectangle x1="5.64388125" y1="2.01091875" x2="6.01471875" y2="2.0193" layer="37"/>
<rectangle x1="6.6421" y1="2.01091875" x2="7.01548125" y2="2.0193" layer="37"/>
<rectangle x1="7.58951875" y1="2.01091875" x2="7.97051875" y2="2.0193" layer="37"/>
<rectangle x1="7.99591875" y1="2.01091875" x2="8.43788125" y2="2.0193" layer="37"/>
<rectangle x1="9.12368125" y1="2.01091875" x2="9.49451875" y2="2.0193" layer="37"/>
<rectangle x1="-0.00508125" y1="2.0193" x2="0.3683" y2="2.02768125" layer="37"/>
<rectangle x1="1.9685" y1="2.0193" x2="2.34188125" y2="2.02768125" layer="37"/>
<rectangle x1="3.50011875" y1="2.0193" x2="3.8735" y2="2.02768125" layer="37"/>
<rectangle x1="4.10971875" y1="2.0193" x2="4.4831" y2="2.02768125" layer="37"/>
<rectangle x1="5.64388125" y1="2.0193" x2="6.01471875" y2="2.02768125" layer="37"/>
<rectangle x1="6.6421" y1="2.0193" x2="7.01548125" y2="2.02768125" layer="37"/>
<rectangle x1="7.58951875" y1="2.0193" x2="7.97051875" y2="2.02768125" layer="37"/>
<rectangle x1="7.9883" y1="2.0193" x2="8.42771875" y2="2.02768125" layer="37"/>
<rectangle x1="9.12368125" y1="2.0193" x2="9.49451875" y2="2.02768125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.02768125" x2="0.3683" y2="2.03631875" layer="37"/>
<rectangle x1="1.9685" y1="2.02768125" x2="2.34188125" y2="2.03631875" layer="37"/>
<rectangle x1="3.50011875" y1="2.02768125" x2="3.8735" y2="2.03631875" layer="37"/>
<rectangle x1="4.10971875" y1="2.02768125" x2="4.4831" y2="2.03631875" layer="37"/>
<rectangle x1="5.64388125" y1="2.02768125" x2="6.01471875" y2="2.03631875" layer="37"/>
<rectangle x1="6.6421" y1="2.02768125" x2="7.01548125" y2="2.03631875" layer="37"/>
<rectangle x1="7.58951875" y1="2.02768125" x2="7.97051875" y2="2.03631875" layer="37"/>
<rectangle x1="7.98068125" y1="2.02768125" x2="8.4201" y2="2.03631875" layer="37"/>
<rectangle x1="9.12368125" y1="2.02768125" x2="9.49451875" y2="2.03631875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.03631875" x2="0.3683" y2="2.0447" layer="37"/>
<rectangle x1="1.9685" y1="2.03631875" x2="2.34188125" y2="2.0447" layer="37"/>
<rectangle x1="3.4925" y1="2.03631875" x2="3.86588125" y2="2.0447" layer="37"/>
<rectangle x1="4.10971875" y1="2.03631875" x2="4.4831" y2="2.0447" layer="37"/>
<rectangle x1="5.64388125" y1="2.03631875" x2="6.01471875" y2="2.0447" layer="37"/>
<rectangle x1="6.6421" y1="2.03631875" x2="7.01548125" y2="2.0447" layer="37"/>
<rectangle x1="7.58951875" y1="2.03631875" x2="8.41248125" y2="2.0447" layer="37"/>
<rectangle x1="9.12368125" y1="2.03631875" x2="9.49451875" y2="2.0447" layer="37"/>
<rectangle x1="-0.00508125" y1="2.0447" x2="0.3683" y2="2.05308125" layer="37"/>
<rectangle x1="1.9685" y1="2.0447" x2="2.34188125" y2="2.05308125" layer="37"/>
<rectangle x1="3.4925" y1="2.0447" x2="3.86588125" y2="2.05308125" layer="37"/>
<rectangle x1="4.10971875" y1="2.0447" x2="4.4831" y2="2.05308125" layer="37"/>
<rectangle x1="5.64388125" y1="2.0447" x2="6.01471875" y2="2.05308125" layer="37"/>
<rectangle x1="6.6421" y1="2.0447" x2="7.01548125" y2="2.05308125" layer="37"/>
<rectangle x1="7.58951875" y1="2.0447" x2="8.40231875" y2="2.05308125" layer="37"/>
<rectangle x1="9.12368125" y1="2.0447" x2="9.49451875" y2="2.05308125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.05308125" x2="0.3683" y2="2.06171875" layer="37"/>
<rectangle x1="1.9685" y1="2.05308125" x2="2.34188125" y2="2.06171875" layer="37"/>
<rectangle x1="3.48488125" y1="2.05308125" x2="3.85571875" y2="2.06171875" layer="37"/>
<rectangle x1="4.10971875" y1="2.05308125" x2="4.4831" y2="2.06171875" layer="37"/>
<rectangle x1="5.64388125" y1="2.05308125" x2="6.01471875" y2="2.06171875" layer="37"/>
<rectangle x1="6.6421" y1="2.05308125" x2="7.01548125" y2="2.06171875" layer="37"/>
<rectangle x1="7.58951875" y1="2.05308125" x2="8.3947" y2="2.06171875" layer="37"/>
<rectangle x1="9.12368125" y1="2.05308125" x2="9.49451875" y2="2.06171875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.06171875" x2="0.3683" y2="2.0701" layer="37"/>
<rectangle x1="1.9685" y1="2.06171875" x2="2.34188125" y2="2.0701" layer="37"/>
<rectangle x1="3.48488125" y1="2.06171875" x2="3.85571875" y2="2.0701" layer="37"/>
<rectangle x1="4.10971875" y1="2.06171875" x2="4.4831" y2="2.0701" layer="37"/>
<rectangle x1="5.64388125" y1="2.06171875" x2="6.01471875" y2="2.0701" layer="37"/>
<rectangle x1="6.6421" y1="2.06171875" x2="7.01548125" y2="2.0701" layer="37"/>
<rectangle x1="7.58951875" y1="2.06171875" x2="8.38708125" y2="2.0701" layer="37"/>
<rectangle x1="9.12368125" y1="2.06171875" x2="9.49451875" y2="2.0701" layer="37"/>
<rectangle x1="-0.00508125" y1="2.0701" x2="0.3683" y2="2.07848125" layer="37"/>
<rectangle x1="1.9685" y1="2.0701" x2="2.34188125" y2="2.07848125" layer="37"/>
<rectangle x1="3.47471875" y1="2.0701" x2="3.8481" y2="2.07848125" layer="37"/>
<rectangle x1="4.10971875" y1="2.0701" x2="4.4831" y2="2.07848125" layer="37"/>
<rectangle x1="5.64388125" y1="2.0701" x2="6.01471875" y2="2.07848125" layer="37"/>
<rectangle x1="6.6421" y1="2.0701" x2="7.01548125" y2="2.07848125" layer="37"/>
<rectangle x1="7.58951875" y1="2.0701" x2="8.37691875" y2="2.07848125" layer="37"/>
<rectangle x1="9.12368125" y1="2.0701" x2="9.49451875" y2="2.07848125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.07848125" x2="0.3683" y2="2.08711875" layer="37"/>
<rectangle x1="1.9685" y1="2.07848125" x2="2.34188125" y2="2.08711875" layer="37"/>
<rectangle x1="3.4671" y1="2.07848125" x2="3.8481" y2="2.08711875" layer="37"/>
<rectangle x1="4.10971875" y1="2.07848125" x2="4.4831" y2="2.08711875" layer="37"/>
<rectangle x1="5.64388125" y1="2.07848125" x2="6.01471875" y2="2.08711875" layer="37"/>
<rectangle x1="6.6421" y1="2.07848125" x2="7.01548125" y2="2.08711875" layer="37"/>
<rectangle x1="7.58951875" y1="2.07848125" x2="8.3693" y2="2.08711875" layer="37"/>
<rectangle x1="9.12368125" y1="2.07848125" x2="9.49451875" y2="2.08711875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.08711875" x2="0.3683" y2="2.0955" layer="37"/>
<rectangle x1="1.9685" y1="2.08711875" x2="2.34188125" y2="2.0955" layer="37"/>
<rectangle x1="3.45948125" y1="2.08711875" x2="3.84048125" y2="2.0955" layer="37"/>
<rectangle x1="4.10971875" y1="2.08711875" x2="4.4831" y2="2.0955" layer="37"/>
<rectangle x1="5.64388125" y1="2.08711875" x2="6.01471875" y2="2.0955" layer="37"/>
<rectangle x1="6.6421" y1="2.08711875" x2="7.01548125" y2="2.0955" layer="37"/>
<rectangle x1="7.58951875" y1="2.08711875" x2="8.35151875" y2="2.0955" layer="37"/>
<rectangle x1="9.12368125" y1="2.08711875" x2="9.49451875" y2="2.0955" layer="37"/>
<rectangle x1="-0.00508125" y1="2.0955" x2="0.3683" y2="2.10388125" layer="37"/>
<rectangle x1="1.9685" y1="2.0955" x2="2.34188125" y2="2.10388125" layer="37"/>
<rectangle x1="3.45948125" y1="2.0955" x2="3.84048125" y2="2.10388125" layer="37"/>
<rectangle x1="4.10971875" y1="2.0955" x2="4.4831" y2="2.10388125" layer="37"/>
<rectangle x1="5.64388125" y1="2.0955" x2="6.01471875" y2="2.10388125" layer="37"/>
<rectangle x1="6.6421" y1="2.0955" x2="7.01548125" y2="2.10388125" layer="37"/>
<rectangle x1="7.58951875" y1="2.0955" x2="8.3439" y2="2.10388125" layer="37"/>
<rectangle x1="9.12368125" y1="2.0955" x2="9.49451875" y2="2.10388125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.10388125" x2="0.3683" y2="2.11251875" layer="37"/>
<rectangle x1="1.9685" y1="2.10388125" x2="2.34188125" y2="2.11251875" layer="37"/>
<rectangle x1="3.44931875" y1="2.10388125" x2="3.84048125" y2="2.11251875" layer="37"/>
<rectangle x1="4.10971875" y1="2.10388125" x2="4.4831" y2="2.11251875" layer="37"/>
<rectangle x1="5.64388125" y1="2.10388125" x2="6.01471875" y2="2.11251875" layer="37"/>
<rectangle x1="6.6421" y1="2.10388125" x2="7.01548125" y2="2.11251875" layer="37"/>
<rectangle x1="7.58951875" y1="2.10388125" x2="8.33628125" y2="2.11251875" layer="37"/>
<rectangle x1="9.12368125" y1="2.10388125" x2="9.49451875" y2="2.11251875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.11251875" x2="0.3683" y2="2.1209" layer="37"/>
<rectangle x1="1.9685" y1="2.11251875" x2="2.34188125" y2="2.1209" layer="37"/>
<rectangle x1="3.4417" y1="2.11251875" x2="3.83031875" y2="2.1209" layer="37"/>
<rectangle x1="4.10971875" y1="2.11251875" x2="4.4831" y2="2.1209" layer="37"/>
<rectangle x1="5.64388125" y1="2.11251875" x2="6.01471875" y2="2.1209" layer="37"/>
<rectangle x1="6.6421" y1="2.11251875" x2="7.01548125" y2="2.1209" layer="37"/>
<rectangle x1="7.58951875" y1="2.11251875" x2="8.32611875" y2="2.1209" layer="37"/>
<rectangle x1="9.12368125" y1="2.11251875" x2="9.49451875" y2="2.1209" layer="37"/>
<rectangle x1="-0.00508125" y1="2.1209" x2="0.3683" y2="2.12928125" layer="37"/>
<rectangle x1="1.9685" y1="2.1209" x2="2.34188125" y2="2.12928125" layer="37"/>
<rectangle x1="3.42391875" y1="2.1209" x2="3.83031875" y2="2.12928125" layer="37"/>
<rectangle x1="4.10971875" y1="2.1209" x2="4.4831" y2="2.12928125" layer="37"/>
<rectangle x1="5.64388125" y1="2.1209" x2="6.01471875" y2="2.12928125" layer="37"/>
<rectangle x1="6.6421" y1="2.1209" x2="7.01548125" y2="2.12928125" layer="37"/>
<rectangle x1="7.58951875" y1="2.1209" x2="8.3185" y2="2.12928125" layer="37"/>
<rectangle x1="9.12368125" y1="2.1209" x2="9.49451875" y2="2.12928125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.12928125" x2="0.3683" y2="2.13791875" layer="37"/>
<rectangle x1="1.9685" y1="2.12928125" x2="2.34188125" y2="2.13791875" layer="37"/>
<rectangle x1="3.4163" y1="2.12928125" x2="3.8227" y2="2.13791875" layer="37"/>
<rectangle x1="4.10971875" y1="2.12928125" x2="4.4831" y2="2.13791875" layer="37"/>
<rectangle x1="5.64388125" y1="2.12928125" x2="6.01471875" y2="2.13791875" layer="37"/>
<rectangle x1="6.6421" y1="2.12928125" x2="7.01548125" y2="2.13791875" layer="37"/>
<rectangle x1="7.58951875" y1="2.12928125" x2="8.31088125" y2="2.13791875" layer="37"/>
<rectangle x1="9.12368125" y1="2.12928125" x2="9.49451875" y2="2.13791875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.13791875" x2="0.3683" y2="2.1463" layer="37"/>
<rectangle x1="1.9685" y1="2.13791875" x2="2.34188125" y2="2.1463" layer="37"/>
<rectangle x1="3.40868125" y1="2.13791875" x2="3.8227" y2="2.1463" layer="37"/>
<rectangle x1="4.10971875" y1="2.13791875" x2="4.4831" y2="2.1463" layer="37"/>
<rectangle x1="5.64388125" y1="2.13791875" x2="6.01471875" y2="2.1463" layer="37"/>
<rectangle x1="6.6421" y1="2.13791875" x2="7.01548125" y2="2.1463" layer="37"/>
<rectangle x1="7.58951875" y1="2.13791875" x2="8.30071875" y2="2.1463" layer="37"/>
<rectangle x1="9.12368125" y1="2.13791875" x2="9.49451875" y2="2.1463" layer="37"/>
<rectangle x1="-0.00508125" y1="2.1463" x2="0.3683" y2="2.15468125" layer="37"/>
<rectangle x1="1.9685" y1="2.1463" x2="2.34188125" y2="2.15468125" layer="37"/>
<rectangle x1="3.39851875" y1="2.1463" x2="3.81508125" y2="2.15468125" layer="37"/>
<rectangle x1="4.10971875" y1="2.1463" x2="4.4831" y2="2.15468125" layer="37"/>
<rectangle x1="5.64388125" y1="2.1463" x2="6.01471875" y2="2.15468125" layer="37"/>
<rectangle x1="6.6421" y1="2.1463" x2="7.01548125" y2="2.15468125" layer="37"/>
<rectangle x1="7.58951875" y1="2.1463" x2="8.2931" y2="2.15468125" layer="37"/>
<rectangle x1="9.12368125" y1="2.1463" x2="9.49451875" y2="2.15468125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.15468125" x2="0.3683" y2="2.16331875" layer="37"/>
<rectangle x1="1.9685" y1="2.15468125" x2="2.34188125" y2="2.16331875" layer="37"/>
<rectangle x1="3.38328125" y1="2.15468125" x2="3.80491875" y2="2.16331875" layer="37"/>
<rectangle x1="4.10971875" y1="2.15468125" x2="4.47548125" y2="2.16331875" layer="37"/>
<rectangle x1="5.64388125" y1="2.15468125" x2="6.01471875" y2="2.16331875" layer="37"/>
<rectangle x1="6.6421" y1="2.15468125" x2="7.01548125" y2="2.16331875" layer="37"/>
<rectangle x1="7.58951875" y1="2.15468125" x2="8.28548125" y2="2.16331875" layer="37"/>
<rectangle x1="9.12368125" y1="2.15468125" x2="9.49451875" y2="2.16331875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.16331875" x2="0.3683" y2="2.1717" layer="37"/>
<rectangle x1="1.9685" y1="2.16331875" x2="2.34188125" y2="2.1717" layer="37"/>
<rectangle x1="3.37311875" y1="2.16331875" x2="3.80491875" y2="2.1717" layer="37"/>
<rectangle x1="4.10971875" y1="2.16331875" x2="4.47548125" y2="2.1717" layer="37"/>
<rectangle x1="5.64388125" y1="2.16331875" x2="6.01471875" y2="2.1717" layer="37"/>
<rectangle x1="6.6421" y1="2.16331875" x2="7.01548125" y2="2.1717" layer="37"/>
<rectangle x1="7.58951875" y1="2.16331875" x2="8.27531875" y2="2.1717" layer="37"/>
<rectangle x1="9.12368125" y1="2.16331875" x2="9.49451875" y2="2.1717" layer="37"/>
<rectangle x1="-0.00508125" y1="2.1717" x2="0.3683" y2="2.18008125" layer="37"/>
<rectangle x1="1.9685" y1="2.1717" x2="2.34188125" y2="2.18008125" layer="37"/>
<rectangle x1="3.35788125" y1="2.1717" x2="3.7973" y2="2.18008125" layer="37"/>
<rectangle x1="4.10971875" y1="2.1717" x2="4.47548125" y2="2.18008125" layer="37"/>
<rectangle x1="5.64388125" y1="2.1717" x2="6.01471875" y2="2.18008125" layer="37"/>
<rectangle x1="6.6421" y1="2.1717" x2="7.01548125" y2="2.18008125" layer="37"/>
<rectangle x1="7.58951875" y1="2.1717" x2="8.2677" y2="2.18008125" layer="37"/>
<rectangle x1="9.12368125" y1="2.1717" x2="9.49451875" y2="2.18008125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.18008125" x2="0.3683" y2="2.18871875" layer="37"/>
<rectangle x1="1.9685" y1="2.18008125" x2="2.34188125" y2="2.18871875" layer="37"/>
<rectangle x1="3.34771875" y1="2.18008125" x2="3.78968125" y2="2.18871875" layer="37"/>
<rectangle x1="4.10971875" y1="2.18008125" x2="4.47548125" y2="2.18871875" layer="37"/>
<rectangle x1="5.64388125" y1="2.18008125" x2="6.01471875" y2="2.18871875" layer="37"/>
<rectangle x1="6.6421" y1="2.18008125" x2="7.01548125" y2="2.18871875" layer="37"/>
<rectangle x1="7.58951875" y1="2.18008125" x2="8.26008125" y2="2.18871875" layer="37"/>
<rectangle x1="9.12368125" y1="2.18008125" x2="9.49451875" y2="2.18871875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.18871875" x2="0.3683" y2="2.1971" layer="37"/>
<rectangle x1="1.9685" y1="2.18871875" x2="2.34188125" y2="2.1971" layer="37"/>
<rectangle x1="3.33248125" y1="2.18871875" x2="3.78968125" y2="2.1971" layer="37"/>
<rectangle x1="4.10971875" y1="2.18871875" x2="4.47548125" y2="2.1971" layer="37"/>
<rectangle x1="5.64388125" y1="2.18871875" x2="6.01471875" y2="2.1971" layer="37"/>
<rectangle x1="6.6421" y1="2.18871875" x2="7.01548125" y2="2.1971" layer="37"/>
<rectangle x1="7.58951875" y1="2.18871875" x2="8.24991875" y2="2.1971" layer="37"/>
<rectangle x1="9.12368125" y1="2.18871875" x2="9.49451875" y2="2.1971" layer="37"/>
<rectangle x1="-0.00508125" y1="2.1971" x2="0.3683" y2="2.20548125" layer="37"/>
<rectangle x1="1.9685" y1="2.1971" x2="2.34188125" y2="2.20548125" layer="37"/>
<rectangle x1="3.3147" y1="2.1971" x2="3.77951875" y2="2.20548125" layer="37"/>
<rectangle x1="4.10971875" y1="2.1971" x2="4.47548125" y2="2.20548125" layer="37"/>
<rectangle x1="5.64388125" y1="2.1971" x2="6.01471875" y2="2.20548125" layer="37"/>
<rectangle x1="6.6421" y1="2.1971" x2="7.01548125" y2="2.20548125" layer="37"/>
<rectangle x1="7.58951875" y1="2.1971" x2="8.2423" y2="2.20548125" layer="37"/>
<rectangle x1="9.12368125" y1="2.1971" x2="9.49451875" y2="2.20548125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.20548125" x2="0.3683" y2="2.21411875" layer="37"/>
<rectangle x1="1.9685" y1="2.20548125" x2="2.34188125" y2="2.21411875" layer="37"/>
<rectangle x1="3.30708125" y1="2.20548125" x2="3.7719" y2="2.21411875" layer="37"/>
<rectangle x1="4.10971875" y1="2.20548125" x2="4.47548125" y2="2.21411875" layer="37"/>
<rectangle x1="5.64388125" y1="2.20548125" x2="6.01471875" y2="2.21411875" layer="37"/>
<rectangle x1="6.6421" y1="2.20548125" x2="7.01548125" y2="2.21411875" layer="37"/>
<rectangle x1="7.58951875" y1="2.20548125" x2="8.23468125" y2="2.21411875" layer="37"/>
<rectangle x1="9.12368125" y1="2.20548125" x2="9.49451875" y2="2.21411875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.21411875" x2="0.3683" y2="2.2225" layer="37"/>
<rectangle x1="1.9685" y1="2.21411875" x2="2.34188125" y2="2.2225" layer="37"/>
<rectangle x1="3.2893" y1="2.21411875" x2="3.76428125" y2="2.2225" layer="37"/>
<rectangle x1="4.10971875" y1="2.21411875" x2="4.47548125" y2="2.2225" layer="37"/>
<rectangle x1="5.64388125" y1="2.21411875" x2="6.01471875" y2="2.2225" layer="37"/>
<rectangle x1="6.6421" y1="2.21411875" x2="7.01548125" y2="2.2225" layer="37"/>
<rectangle x1="7.58951875" y1="2.21411875" x2="8.2169" y2="2.2225" layer="37"/>
<rectangle x1="9.12368125" y1="2.21411875" x2="9.49451875" y2="2.2225" layer="37"/>
<rectangle x1="-0.00508125" y1="2.2225" x2="0.3683" y2="2.23088125" layer="37"/>
<rectangle x1="1.9685" y1="2.2225" x2="2.34188125" y2="2.23088125" layer="37"/>
<rectangle x1="3.27151875" y1="2.2225" x2="3.75411875" y2="2.23088125" layer="37"/>
<rectangle x1="4.10971875" y1="2.2225" x2="4.47548125" y2="2.23088125" layer="37"/>
<rectangle x1="5.64388125" y1="2.2225" x2="6.01471875" y2="2.23088125" layer="37"/>
<rectangle x1="6.6421" y1="2.2225" x2="7.01548125" y2="2.23088125" layer="37"/>
<rectangle x1="7.58951875" y1="2.2225" x2="8.20928125" y2="2.23088125" layer="37"/>
<rectangle x1="9.12368125" y1="2.2225" x2="9.49451875" y2="2.23088125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.23088125" x2="0.3683" y2="2.23951875" layer="37"/>
<rectangle x1="1.9685" y1="2.23088125" x2="2.34188125" y2="2.23951875" layer="37"/>
<rectangle x1="3.25628125" y1="2.23088125" x2="3.7465" y2="2.23951875" layer="37"/>
<rectangle x1="4.10971875" y1="2.23088125" x2="4.47548125" y2="2.23951875" layer="37"/>
<rectangle x1="5.64388125" y1="2.23088125" x2="6.01471875" y2="2.23951875" layer="37"/>
<rectangle x1="6.6421" y1="2.23088125" x2="7.01548125" y2="2.23951875" layer="37"/>
<rectangle x1="7.58951875" y1="2.23088125" x2="8.19911875" y2="2.23951875" layer="37"/>
<rectangle x1="9.12368125" y1="2.23088125" x2="9.49451875" y2="2.23951875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.23951875" x2="0.3683" y2="2.2479" layer="37"/>
<rectangle x1="1.9685" y1="2.23951875" x2="2.34188125" y2="2.2479" layer="37"/>
<rectangle x1="3.2385" y1="2.23951875" x2="3.73888125" y2="2.2479" layer="37"/>
<rectangle x1="4.10971875" y1="2.23951875" x2="4.47548125" y2="2.2479" layer="37"/>
<rectangle x1="5.64388125" y1="2.23951875" x2="6.01471875" y2="2.2479" layer="37"/>
<rectangle x1="6.6421" y1="2.23951875" x2="7.01548125" y2="2.2479" layer="37"/>
<rectangle x1="7.58951875" y1="2.23951875" x2="8.1915" y2="2.2479" layer="37"/>
<rectangle x1="9.12368125" y1="2.23951875" x2="9.49451875" y2="2.2479" layer="37"/>
<rectangle x1="-0.00508125" y1="2.2479" x2="0.3683" y2="2.25628125" layer="37"/>
<rectangle x1="1.9685" y1="2.2479" x2="2.34188125" y2="2.25628125" layer="37"/>
<rectangle x1="3.2131" y1="2.2479" x2="3.72871875" y2="2.25628125" layer="37"/>
<rectangle x1="4.10971875" y1="2.2479" x2="4.47548125" y2="2.25628125" layer="37"/>
<rectangle x1="5.64388125" y1="2.2479" x2="6.01471875" y2="2.25628125" layer="37"/>
<rectangle x1="6.6421" y1="2.2479" x2="7.01548125" y2="2.25628125" layer="37"/>
<rectangle x1="7.58951875" y1="2.2479" x2="8.18388125" y2="2.25628125" layer="37"/>
<rectangle x1="9.12368125" y1="2.2479" x2="9.49451875" y2="2.25628125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.25628125" x2="0.3683" y2="2.26491875" layer="37"/>
<rectangle x1="1.9685" y1="2.25628125" x2="2.34188125" y2="2.26491875" layer="37"/>
<rectangle x1="3.19531875" y1="2.25628125" x2="3.7211" y2="2.26491875" layer="37"/>
<rectangle x1="4.10971875" y1="2.25628125" x2="4.47548125" y2="2.26491875" layer="37"/>
<rectangle x1="5.64388125" y1="2.25628125" x2="6.01471875" y2="2.26491875" layer="37"/>
<rectangle x1="6.6421" y1="2.25628125" x2="7.01548125" y2="2.26491875" layer="37"/>
<rectangle x1="7.58951875" y1="2.25628125" x2="8.17371875" y2="2.26491875" layer="37"/>
<rectangle x1="9.12368125" y1="2.25628125" x2="9.49451875" y2="2.26491875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.26491875" x2="0.3683" y2="2.2733" layer="37"/>
<rectangle x1="1.9685" y1="2.26491875" x2="2.34188125" y2="2.2733" layer="37"/>
<rectangle x1="3.16991875" y1="2.26491875" x2="3.71348125" y2="2.2733" layer="37"/>
<rectangle x1="4.10971875" y1="2.26491875" x2="4.47548125" y2="2.2733" layer="37"/>
<rectangle x1="5.64388125" y1="2.26491875" x2="6.01471875" y2="2.2733" layer="37"/>
<rectangle x1="6.6421" y1="2.26491875" x2="7.01548125" y2="2.2733" layer="37"/>
<rectangle x1="7.58951875" y1="2.26491875" x2="8.1661" y2="2.2733" layer="37"/>
<rectangle x1="9.12368125" y1="2.26491875" x2="9.49451875" y2="2.2733" layer="37"/>
<rectangle x1="-0.00508125" y1="2.2733" x2="0.3683" y2="2.28168125" layer="37"/>
<rectangle x1="1.9685" y1="2.2733" x2="2.34188125" y2="2.28168125" layer="37"/>
<rectangle x1="3.14451875" y1="2.2733" x2="3.70331875" y2="2.28168125" layer="37"/>
<rectangle x1="4.10971875" y1="2.2733" x2="4.47548125" y2="2.28168125" layer="37"/>
<rectangle x1="5.64388125" y1="2.2733" x2="6.01471875" y2="2.28168125" layer="37"/>
<rectangle x1="6.6421" y1="2.2733" x2="7.01548125" y2="2.28168125" layer="37"/>
<rectangle x1="7.58951875" y1="2.2733" x2="8.15848125" y2="2.28168125" layer="37"/>
<rectangle x1="9.12368125" y1="2.2733" x2="9.49451875" y2="2.28168125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.28168125" x2="0.3683" y2="2.29031875" layer="37"/>
<rectangle x1="1.9685" y1="2.28168125" x2="2.34188125" y2="2.29031875" layer="37"/>
<rectangle x1="3.11911875" y1="2.28168125" x2="3.68808125" y2="2.29031875" layer="37"/>
<rectangle x1="4.10971875" y1="2.28168125" x2="4.47548125" y2="2.29031875" layer="37"/>
<rectangle x1="5.64388125" y1="2.28168125" x2="6.01471875" y2="2.29031875" layer="37"/>
<rectangle x1="6.6421" y1="2.28168125" x2="7.01548125" y2="2.29031875" layer="37"/>
<rectangle x1="7.58951875" y1="2.28168125" x2="8.14831875" y2="2.29031875" layer="37"/>
<rectangle x1="9.12368125" y1="2.28168125" x2="9.49451875" y2="2.29031875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.29031875" x2="0.3683" y2="2.2987" layer="37"/>
<rectangle x1="1.9685" y1="2.29031875" x2="2.34188125" y2="2.2987" layer="37"/>
<rectangle x1="3.0861" y1="2.29031875" x2="3.67791875" y2="2.2987" layer="37"/>
<rectangle x1="4.10971875" y1="2.29031875" x2="4.47548125" y2="2.2987" layer="37"/>
<rectangle x1="5.64388125" y1="2.29031875" x2="6.01471875" y2="2.2987" layer="37"/>
<rectangle x1="6.6421" y1="2.29031875" x2="7.01548125" y2="2.2987" layer="37"/>
<rectangle x1="7.58951875" y1="2.29031875" x2="8.1407" y2="2.2987" layer="37"/>
<rectangle x1="9.12368125" y1="2.29031875" x2="9.49451875" y2="2.2987" layer="37"/>
<rectangle x1="-0.00508125" y1="2.2987" x2="0.3683" y2="2.30708125" layer="37"/>
<rectangle x1="1.9685" y1="2.2987" x2="2.34188125" y2="2.30708125" layer="37"/>
<rectangle x1="3.0353" y1="2.2987" x2="3.6703" y2="2.30708125" layer="37"/>
<rectangle x1="4.10971875" y1="2.2987" x2="4.47548125" y2="2.30708125" layer="37"/>
<rectangle x1="5.64388125" y1="2.2987" x2="6.01471875" y2="2.30708125" layer="37"/>
<rectangle x1="6.6421" y1="2.2987" x2="7.01548125" y2="2.30708125" layer="37"/>
<rectangle x1="7.58951875" y1="2.2987" x2="8.13308125" y2="2.30708125" layer="37"/>
<rectangle x1="9.12368125" y1="2.2987" x2="9.49451875" y2="2.30708125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.30708125" x2="1.7653" y2="2.31571875" layer="37"/>
<rectangle x1="1.9685" y1="2.30708125" x2="3.66268125" y2="2.31571875" layer="37"/>
<rectangle x1="4.10971875" y1="2.30708125" x2="4.47548125" y2="2.31571875" layer="37"/>
<rectangle x1="5.64388125" y1="2.30708125" x2="6.01471875" y2="2.31571875" layer="37"/>
<rectangle x1="6.38048125" y1="2.30708125" x2="7.2771" y2="2.31571875" layer="37"/>
<rectangle x1="7.58951875" y1="2.30708125" x2="8.12291875" y2="2.31571875" layer="37"/>
<rectangle x1="9.12368125" y1="2.30708125" x2="9.49451875" y2="2.31571875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.31571875" x2="1.80848125" y2="2.3241" layer="37"/>
<rectangle x1="1.9685" y1="2.31571875" x2="3.6449" y2="2.3241" layer="37"/>
<rectangle x1="4.10971875" y1="2.31571875" x2="4.47548125" y2="2.3241" layer="37"/>
<rectangle x1="5.64388125" y1="2.31571875" x2="6.01471875" y2="2.3241" layer="37"/>
<rectangle x1="6.35508125" y1="2.31571875" x2="7.31011875" y2="2.3241" layer="37"/>
<rectangle x1="7.58951875" y1="2.31571875" x2="8.1153" y2="2.3241" layer="37"/>
<rectangle x1="9.12368125" y1="2.31571875" x2="9.49451875" y2="2.3241" layer="37"/>
<rectangle x1="-0.00508125" y1="2.3241" x2="1.83388125" y2="2.33248125" layer="37"/>
<rectangle x1="1.9685" y1="2.3241" x2="3.63728125" y2="2.33248125" layer="37"/>
<rectangle x1="4.10971875" y1="2.3241" x2="4.47548125" y2="2.33248125" layer="37"/>
<rectangle x1="5.64388125" y1="2.3241" x2="6.01471875" y2="2.33248125" layer="37"/>
<rectangle x1="6.3373" y1="2.3241" x2="7.32028125" y2="2.33248125" layer="37"/>
<rectangle x1="7.58951875" y1="2.3241" x2="8.09751875" y2="2.33248125" layer="37"/>
<rectangle x1="9.12368125" y1="2.3241" x2="9.49451875" y2="2.33248125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.33248125" x2="1.84911875" y2="2.34111875" layer="37"/>
<rectangle x1="1.9685" y1="2.33248125" x2="3.6195" y2="2.34111875" layer="37"/>
<rectangle x1="4.10971875" y1="2.33248125" x2="4.47548125" y2="2.34111875" layer="37"/>
<rectangle x1="5.64388125" y1="2.33248125" x2="6.01471875" y2="2.34111875" layer="37"/>
<rectangle x1="6.31951875" y1="2.33248125" x2="7.33551875" y2="2.34111875" layer="37"/>
<rectangle x1="7.58951875" y1="2.33248125" x2="8.0899" y2="2.34111875" layer="37"/>
<rectangle x1="9.12368125" y1="2.33248125" x2="9.49451875" y2="2.34111875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.34111875" x2="1.85928125" y2="2.3495" layer="37"/>
<rectangle x1="1.9685" y1="2.34111875" x2="3.61188125" y2="2.3495" layer="37"/>
<rectangle x1="4.10971875" y1="2.34111875" x2="4.47548125" y2="2.3495" layer="37"/>
<rectangle x1="5.64388125" y1="2.34111875" x2="6.01471875" y2="2.3495" layer="37"/>
<rectangle x1="6.3119" y1="2.34111875" x2="7.34568125" y2="2.3495" layer="37"/>
<rectangle x1="7.58951875" y1="2.34111875" x2="8.08228125" y2="2.3495" layer="37"/>
<rectangle x1="9.12368125" y1="2.34111875" x2="9.49451875" y2="2.3495" layer="37"/>
<rectangle x1="-0.00508125" y1="2.3495" x2="1.8669" y2="2.35788125" layer="37"/>
<rectangle x1="1.9685" y1="2.3495" x2="3.5941" y2="2.35788125" layer="37"/>
<rectangle x1="4.10971875" y1="2.3495" x2="4.47548125" y2="2.35788125" layer="37"/>
<rectangle x1="5.64388125" y1="2.3495" x2="6.01471875" y2="2.35788125" layer="37"/>
<rectangle x1="6.30428125" y1="2.3495" x2="7.3533" y2="2.35788125" layer="37"/>
<rectangle x1="7.58951875" y1="2.3495" x2="8.07211875" y2="2.35788125" layer="37"/>
<rectangle x1="9.12368125" y1="2.3495" x2="9.49451875" y2="2.35788125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.35788125" x2="1.87451875" y2="2.36651875" layer="37"/>
<rectangle x1="1.9685" y1="2.35788125" x2="3.57631875" y2="2.36651875" layer="37"/>
<rectangle x1="4.10971875" y1="2.35788125" x2="4.47548125" y2="2.36651875" layer="37"/>
<rectangle x1="5.64388125" y1="2.35788125" x2="6.01471875" y2="2.36651875" layer="37"/>
<rectangle x1="6.29411875" y1="2.35788125" x2="7.36091875" y2="2.36651875" layer="37"/>
<rectangle x1="7.58951875" y1="2.35788125" x2="8.0645" y2="2.36651875" layer="37"/>
<rectangle x1="9.12368125" y1="2.35788125" x2="9.49451875" y2="2.36651875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.36651875" x2="1.88468125" y2="2.3749" layer="37"/>
<rectangle x1="1.9685" y1="2.36651875" x2="3.5687" y2="2.3749" layer="37"/>
<rectangle x1="4.10971875" y1="2.36651875" x2="4.47548125" y2="2.3749" layer="37"/>
<rectangle x1="5.64388125" y1="2.36651875" x2="6.01471875" y2="2.3749" layer="37"/>
<rectangle x1="6.2865" y1="2.36651875" x2="7.37108125" y2="2.3749" layer="37"/>
<rectangle x1="7.58951875" y1="2.36651875" x2="8.05688125" y2="2.3749" layer="37"/>
<rectangle x1="9.12368125" y1="2.36651875" x2="9.49451875" y2="2.3749" layer="37"/>
<rectangle x1="-0.00508125" y1="2.3749" x2="1.88468125" y2="2.38328125" layer="37"/>
<rectangle x1="1.9685" y1="2.3749" x2="3.55091875" y2="2.38328125" layer="37"/>
<rectangle x1="4.10971875" y1="2.3749" x2="4.47548125" y2="2.38328125" layer="37"/>
<rectangle x1="5.64388125" y1="2.3749" x2="6.01471875" y2="2.38328125" layer="37"/>
<rectangle x1="6.27888125" y1="2.3749" x2="7.3787" y2="2.38328125" layer="37"/>
<rectangle x1="7.58951875" y1="2.3749" x2="8.04671875" y2="2.38328125" layer="37"/>
<rectangle x1="9.12368125" y1="2.3749" x2="9.49451875" y2="2.38328125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.38328125" x2="1.8923" y2="2.39191875" layer="37"/>
<rectangle x1="1.9685" y1="2.38328125" x2="3.53568125" y2="2.39191875" layer="37"/>
<rectangle x1="4.10971875" y1="2.38328125" x2="4.47548125" y2="2.39191875" layer="37"/>
<rectangle x1="5.64388125" y1="2.38328125" x2="6.01471875" y2="2.39191875" layer="37"/>
<rectangle x1="6.26871875" y1="2.38328125" x2="7.38631875" y2="2.39191875" layer="37"/>
<rectangle x1="7.58951875" y1="2.38328125" x2="8.0391" y2="2.39191875" layer="37"/>
<rectangle x1="9.12368125" y1="2.38328125" x2="9.49451875" y2="2.39191875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.39191875" x2="1.8923" y2="2.4003" layer="37"/>
<rectangle x1="1.9685" y1="2.39191875" x2="3.5179" y2="2.4003" layer="37"/>
<rectangle x1="4.10971875" y1="2.39191875" x2="4.47548125" y2="2.4003" layer="37"/>
<rectangle x1="5.64388125" y1="2.39191875" x2="6.01471875" y2="2.4003" layer="37"/>
<rectangle x1="6.2611" y1="2.39191875" x2="7.39648125" y2="2.4003" layer="37"/>
<rectangle x1="7.58951875" y1="2.39191875" x2="8.03148125" y2="2.4003" layer="37"/>
<rectangle x1="9.12368125" y1="2.39191875" x2="9.49451875" y2="2.4003" layer="37"/>
<rectangle x1="-0.00508125" y1="2.4003" x2="1.8923" y2="2.40868125" layer="37"/>
<rectangle x1="1.9685" y1="2.4003" x2="3.51028125" y2="2.40868125" layer="37"/>
<rectangle x1="4.10971875" y1="2.4003" x2="4.47548125" y2="2.40868125" layer="37"/>
<rectangle x1="5.64388125" y1="2.4003" x2="6.01471875" y2="2.40868125" layer="37"/>
<rectangle x1="6.2611" y1="2.4003" x2="7.39648125" y2="2.40868125" layer="37"/>
<rectangle x1="7.58951875" y1="2.4003" x2="8.02131875" y2="2.40868125" layer="37"/>
<rectangle x1="9.12368125" y1="2.4003" x2="9.49451875" y2="2.40868125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.40868125" x2="1.8923" y2="2.41731875" layer="37"/>
<rectangle x1="1.9685" y1="2.40868125" x2="3.4925" y2="2.41731875" layer="37"/>
<rectangle x1="4.10971875" y1="2.40868125" x2="4.47548125" y2="2.41731875" layer="37"/>
<rectangle x1="5.64388125" y1="2.40868125" x2="6.01471875" y2="2.41731875" layer="37"/>
<rectangle x1="6.2611" y1="2.40868125" x2="7.4041" y2="2.41731875" layer="37"/>
<rectangle x1="7.58951875" y1="2.40868125" x2="8.0137" y2="2.41731875" layer="37"/>
<rectangle x1="9.12368125" y1="2.40868125" x2="9.49451875" y2="2.41731875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.41731875" x2="1.8923" y2="2.4257" layer="37"/>
<rectangle x1="1.9685" y1="2.41731875" x2="3.47471875" y2="2.4257" layer="37"/>
<rectangle x1="4.10971875" y1="2.41731875" x2="4.47548125" y2="2.4257" layer="37"/>
<rectangle x1="5.64388125" y1="2.41731875" x2="6.01471875" y2="2.4257" layer="37"/>
<rectangle x1="6.2611" y1="2.41731875" x2="7.4041" y2="2.4257" layer="37"/>
<rectangle x1="7.58951875" y1="2.41731875" x2="8.00608125" y2="2.4257" layer="37"/>
<rectangle x1="9.12368125" y1="2.41731875" x2="9.49451875" y2="2.4257" layer="37"/>
<rectangle x1="-0.00508125" y1="2.4257" x2="1.8923" y2="2.43408125" layer="37"/>
<rectangle x1="1.9685" y1="2.4257" x2="3.45948125" y2="2.43408125" layer="37"/>
<rectangle x1="4.10971875" y1="2.4257" x2="4.47548125" y2="2.43408125" layer="37"/>
<rectangle x1="5.64388125" y1="2.4257" x2="6.01471875" y2="2.43408125" layer="37"/>
<rectangle x1="6.2611" y1="2.4257" x2="7.39648125" y2="2.43408125" layer="37"/>
<rectangle x1="7.58951875" y1="2.4257" x2="7.99591875" y2="2.43408125" layer="37"/>
<rectangle x1="9.12368125" y1="2.4257" x2="9.49451875" y2="2.43408125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.43408125" x2="1.8923" y2="2.44271875" layer="37"/>
<rectangle x1="1.9685" y1="2.43408125" x2="3.4417" y2="2.44271875" layer="37"/>
<rectangle x1="4.10971875" y1="2.43408125" x2="4.47548125" y2="2.44271875" layer="37"/>
<rectangle x1="5.64388125" y1="2.43408125" x2="6.01471875" y2="2.44271875" layer="37"/>
<rectangle x1="6.2611" y1="2.43408125" x2="7.39648125" y2="2.44271875" layer="37"/>
<rectangle x1="7.58951875" y1="2.43408125" x2="7.9883" y2="2.44271875" layer="37"/>
<rectangle x1="9.12368125" y1="2.43408125" x2="9.49451875" y2="2.44271875" layer="37"/>
<rectangle x1="-0.00508125" y1="2.44271875" x2="1.8923" y2="2.4511" layer="37"/>
<rectangle x1="1.9685" y1="2.44271875" x2="3.4163" y2="2.4511" layer="37"/>
<rectangle x1="4.10971875" y1="2.44271875" x2="4.4831" y2="2.4511" layer="37"/>
<rectangle x1="5.64388125" y1="2.44271875" x2="6.01471875" y2="2.4511" layer="37"/>
<rectangle x1="6.2611" y1="2.44271875" x2="7.39648125" y2="2.4511" layer="37"/>
<rectangle x1="7.58951875" y1="2.44271875" x2="7.98068125" y2="2.4511" layer="37"/>
<rectangle x1="9.12368125" y1="2.44271875" x2="9.49451875" y2="2.4511" layer="37"/>
<rectangle x1="-0.00508125" y1="2.4511" x2="1.8923" y2="2.45948125" layer="37"/>
<rectangle x1="1.9685" y1="2.4511" x2="3.39851875" y2="2.45948125" layer="37"/>
<rectangle x1="4.10971875" y1="2.4511" x2="4.47548125" y2="2.45948125" layer="37"/>
<rectangle x1="5.64388125" y1="2.4511" x2="6.01471875" y2="2.45948125" layer="37"/>
<rectangle x1="6.26871875" y1="2.4511" x2="7.39648125" y2="2.45948125" layer="37"/>
<rectangle x1="7.58951875" y1="2.4511" x2="7.9629" y2="2.45948125" layer="37"/>
<rectangle x1="9.1313" y1="2.4511" x2="9.49451875" y2="2.45948125" layer="37"/>
<rectangle x1="-0.00508125" y1="2.45948125" x2="1.88468125" y2="2.46811875" layer="37"/>
<rectangle x1="1.9685" y1="2.45948125" x2="3.38328125" y2="2.46811875" layer="37"/>
<rectangle x1="4.10971875" y1="2.45948125" x2="4.47548125" y2="2.46811875" layer="37"/>
<rectangle x1="5.6515" y1="2.45948125" x2="6.0071" y2="2.46811875" layer="37"/>
<rectangle x1="6.26871875" y1="2.45948125" x2="7.39648125" y2="2.46811875" layer="37"/>
<rectangle x1="7.59968125" y1="2.45948125" x2="7.95528125" y2="2.46811875" layer="37"/>
<rectangle x1="9.1313" y1="2.45948125" x2="9.49451875" y2="2.46811875" layer="37"/>
<rectangle x1="0.00508125" y1="2.46811875" x2="1.88468125" y2="2.4765" layer="37"/>
<rectangle x1="1.97611875" y1="2.46811875" x2="3.35788125" y2="2.4765" layer="37"/>
<rectangle x1="4.11988125" y1="2.46811875" x2="4.46531875" y2="2.4765" layer="37"/>
<rectangle x1="5.6515" y1="2.46811875" x2="6.0071" y2="2.4765" layer="37"/>
<rectangle x1="6.27888125" y1="2.46811875" x2="7.38631875" y2="2.4765" layer="37"/>
<rectangle x1="7.59968125" y1="2.46811875" x2="7.94511875" y2="2.4765" layer="37"/>
<rectangle x1="9.13891875" y1="2.46811875" x2="9.4869" y2="2.4765" layer="37"/>
<rectangle x1="0.0127" y1="2.4765" x2="1.87451875" y2="2.48488125" layer="37"/>
<rectangle x1="1.98628125" y1="2.4765" x2="3.33248125" y2="2.48488125" layer="37"/>
<rectangle x1="4.1275" y1="2.4765" x2="4.4577" y2="2.48488125" layer="37"/>
<rectangle x1="5.65911875" y1="2.4765" x2="5.99948125" y2="2.48488125" layer="37"/>
<rectangle x1="6.27888125" y1="2.4765" x2="7.3787" y2="2.48488125" layer="37"/>
<rectangle x1="7.6073" y1="2.4765" x2="7.9375" y2="2.48488125" layer="37"/>
<rectangle x1="9.14908125" y1="2.4765" x2="9.47928125" y2="2.48488125" layer="37"/>
<rectangle x1="0.02031875" y1="2.48488125" x2="1.8669" y2="2.49351875" layer="37"/>
<rectangle x1="1.9939" y1="2.48488125" x2="3.30708125" y2="2.49351875" layer="37"/>
<rectangle x1="4.13511875" y1="2.48488125" x2="4.45008125" y2="2.49351875" layer="37"/>
<rectangle x1="5.66928125" y1="2.48488125" x2="5.98931875" y2="2.49351875" layer="37"/>
<rectangle x1="6.2865" y1="2.48488125" x2="7.36091875" y2="2.49351875" layer="37"/>
<rectangle x1="7.61491875" y1="2.48488125" x2="7.92988125" y2="2.49351875" layer="37"/>
<rectangle x1="9.1567" y1="2.48488125" x2="9.46911875" y2="2.49351875" layer="37"/>
<rectangle x1="0.0381" y1="2.49351875" x2="1.85928125" y2="2.5019" layer="37"/>
<rectangle x1="2.01168125" y1="2.49351875" x2="3.28168125" y2="2.5019" layer="37"/>
<rectangle x1="4.14528125" y1="2.49351875" x2="4.4323" y2="2.5019" layer="37"/>
<rectangle x1="5.6769" y1="2.49351875" x2="5.97408125" y2="2.5019" layer="37"/>
<rectangle x1="6.29411875" y1="2.49351875" x2="7.3533" y2="2.5019" layer="37"/>
<rectangle x1="7.6327" y1="2.49351875" x2="7.91971875" y2="2.5019" layer="37"/>
<rectangle x1="9.16431875" y1="2.49351875" x2="9.4615" y2="2.5019" layer="37"/>
<rectangle x1="0.05588125" y1="2.5019" x2="1.84911875" y2="2.51028125" layer="37"/>
<rectangle x1="2.02691875" y1="2.5019" x2="3.25628125" y2="2.51028125" layer="37"/>
<rectangle x1="4.16051875" y1="2.5019" x2="4.42468125" y2="2.51028125" layer="37"/>
<rectangle x1="5.69468125" y1="2.5019" x2="5.96391875" y2="2.51028125" layer="37"/>
<rectangle x1="6.30428125" y1="2.5019" x2="7.33551875" y2="2.51028125" layer="37"/>
<rectangle x1="7.64031875" y1="2.5019" x2="7.90448125" y2="2.51028125" layer="37"/>
<rectangle x1="9.1821" y1="2.5019" x2="9.45388125" y2="2.51028125" layer="37"/>
<rectangle x1="0.07111875" y1="2.51028125" x2="1.83388125" y2="2.51891875" layer="37"/>
<rectangle x1="2.0447" y1="2.51028125" x2="3.22071875" y2="2.51891875" layer="37"/>
<rectangle x1="4.18591875" y1="2.51028125" x2="4.4069" y2="2.51891875" layer="37"/>
<rectangle x1="5.70991875" y1="2.51028125" x2="5.93851875" y2="2.51891875" layer="37"/>
<rectangle x1="6.31951875" y1="2.51028125" x2="7.32028125" y2="2.51891875" layer="37"/>
<rectangle x1="7.6581" y1="2.51028125" x2="7.8867" y2="2.51891875" layer="37"/>
<rectangle x1="9.19988125" y1="2.51028125" x2="9.42848125" y2="2.51891875" layer="37"/>
<rectangle x1="0.0889" y1="2.51891875" x2="1.8161" y2="2.5273" layer="37"/>
<rectangle x1="2.06248125" y1="2.51891875" x2="3.18008125" y2="2.5273" layer="37"/>
<rectangle x1="4.2037" y1="2.51891875" x2="4.3815" y2="2.5273" layer="37"/>
<rectangle x1="5.7277" y1="2.51891875" x2="5.91311875" y2="2.5273" layer="37"/>
<rectangle x1="6.3373" y1="2.51891875" x2="7.3025" y2="2.5273" layer="37"/>
<rectangle x1="7.6835" y1="2.51891875" x2="7.8613" y2="2.5273" layer="37"/>
<rectangle x1="9.22528125" y1="2.51891875" x2="9.4107" y2="2.5273" layer="37"/>
<rectangle x1="0.1143" y1="2.5273" x2="1.7907" y2="2.53568125" layer="37"/>
<rectangle x1="2.08788125" y1="2.5273" x2="3.12928125" y2="2.53568125" layer="37"/>
<rectangle x1="4.2291" y1="2.5273" x2="4.3561" y2="2.53568125" layer="37"/>
<rectangle x1="5.76071875" y1="2.5273" x2="5.88771875" y2="2.53568125" layer="37"/>
<rectangle x1="6.3627" y1="2.5273" x2="7.2771" y2="2.53568125" layer="37"/>
<rectangle x1="7.7343" y1="2.5273" x2="7.8105" y2="2.53568125" layer="37"/>
<rectangle x1="9.27608125" y1="2.5273" x2="9.3599" y2="2.53568125" layer="37"/>
<rectangle x1="5.81151875" y1="2.53568125" x2="5.83691875" y2="2.54431875" layer="37"/>
<rectangle x1="0.02031875" y1="2.71348125" x2="0.45211875" y2="2.72211875" layer="37"/>
<rectangle x1="0.51308125" y1="2.71348125" x2="0.5461" y2="2.72211875" layer="37"/>
<rectangle x1="0.61468125" y1="2.71348125" x2="0.6477" y2="2.72211875" layer="37"/>
<rectangle x1="0.7239" y1="2.71348125" x2="0.75691875" y2="2.72211875" layer="37"/>
<rectangle x1="0.83311875" y1="2.71348125" x2="0.86868125" y2="2.72211875" layer="37"/>
<rectangle x1="0.96011875" y1="2.71348125" x2="0.99568125" y2="2.72211875" layer="37"/>
<rectangle x1="1.0795" y1="2.71348125" x2="1.11251875" y2="2.72211875" layer="37"/>
<rectangle x1="1.1811" y1="2.71348125" x2="1.83388125" y2="2.72211875" layer="37"/>
<rectangle x1="1.9685" y1="2.71348125" x2="2.78891875" y2="2.72211875" layer="37"/>
<rectangle x1="2.83971875" y1="2.71348125" x2="2.87528125" y2="2.72211875" layer="37"/>
<rectangle x1="2.92608125" y1="2.71348125" x2="3.68808125" y2="2.72211875" layer="37"/>
<rectangle x1="3.7719" y1="2.71348125" x2="4.4323" y2="2.72211875" layer="37"/>
<rectangle x1="5.10031875" y1="2.71348125" x2="5.72008125" y2="2.72211875" layer="37"/>
<rectangle x1="5.81151875" y1="2.71348125" x2="6.57351875" y2="2.72211875" layer="37"/>
<rectangle x1="6.6167" y1="2.71348125" x2="6.64971875" y2="2.72211875" layer="37"/>
<rectangle x1="6.70051875" y1="2.71348125" x2="7.52348125" y2="2.72211875" layer="37"/>
<rectangle x1="7.6581" y1="2.71348125" x2="8.30071875" y2="2.72211875" layer="37"/>
<rectangle x1="8.37691875" y1="2.71348125" x2="8.41248125" y2="2.72211875" layer="37"/>
<rectangle x1="8.4963" y1="2.71348125" x2="8.52931875" y2="2.72211875" layer="37"/>
<rectangle x1="8.61568125" y1="2.71348125" x2="8.6487" y2="2.72211875" layer="37"/>
<rectangle x1="8.73251875" y1="2.71348125" x2="8.76808125" y2="2.72211875" layer="37"/>
<rectangle x1="8.83411875" y1="2.71348125" x2="8.86968125" y2="2.72211875" layer="37"/>
<rectangle x1="8.93571875" y1="2.71348125" x2="8.97128125" y2="2.72211875" layer="37"/>
<rectangle x1="9.03731875" y1="2.71348125" x2="9.49451875" y2="2.72211875" layer="37"/>
<rectangle x1="0.02031875" y1="2.72211875" x2="0.45211875" y2="2.7305" layer="37"/>
<rectangle x1="0.51308125" y1="2.72211875" x2="0.5461" y2="2.7305" layer="37"/>
<rectangle x1="0.61468125" y1="2.72211875" x2="0.6477" y2="2.7305" layer="37"/>
<rectangle x1="0.7239" y1="2.72211875" x2="0.75691875" y2="2.7305" layer="37"/>
<rectangle x1="0.83311875" y1="2.72211875" x2="0.86868125" y2="2.7305" layer="37"/>
<rectangle x1="0.96011875" y1="2.72211875" x2="0.99568125" y2="2.7305" layer="37"/>
<rectangle x1="1.0795" y1="2.72211875" x2="1.11251875" y2="2.7305" layer="37"/>
<rectangle x1="1.1811" y1="2.72211875" x2="1.83388125" y2="2.7305" layer="37"/>
<rectangle x1="1.9685" y1="2.72211875" x2="2.78891875" y2="2.7305" layer="37"/>
<rectangle x1="2.83971875" y1="2.72211875" x2="2.87528125" y2="2.7305" layer="37"/>
<rectangle x1="2.92608125" y1="2.72211875" x2="3.68808125" y2="2.7305" layer="37"/>
<rectangle x1="3.7719" y1="2.72211875" x2="4.4323" y2="2.7305" layer="37"/>
<rectangle x1="5.10031875" y1="2.72211875" x2="5.72008125" y2="2.7305" layer="37"/>
<rectangle x1="5.81151875" y1="2.72211875" x2="6.57351875" y2="2.7305" layer="37"/>
<rectangle x1="6.6167" y1="2.72211875" x2="6.64971875" y2="2.7305" layer="37"/>
<rectangle x1="6.70051875" y1="2.72211875" x2="7.52348125" y2="2.7305" layer="37"/>
<rectangle x1="7.6581" y1="2.72211875" x2="8.30071875" y2="2.7305" layer="37"/>
<rectangle x1="8.37691875" y1="2.72211875" x2="8.41248125" y2="2.7305" layer="37"/>
<rectangle x1="8.4963" y1="2.72211875" x2="8.52931875" y2="2.7305" layer="37"/>
<rectangle x1="8.61568125" y1="2.72211875" x2="8.6487" y2="2.7305" layer="37"/>
<rectangle x1="8.73251875" y1="2.72211875" x2="8.76808125" y2="2.7305" layer="37"/>
<rectangle x1="8.83411875" y1="2.72211875" x2="8.86968125" y2="2.7305" layer="37"/>
<rectangle x1="8.93571875" y1="2.72211875" x2="8.97128125" y2="2.7305" layer="37"/>
<rectangle x1="9.03731875" y1="2.72211875" x2="9.49451875" y2="2.7305" layer="37"/>
<rectangle x1="0.02031875" y1="2.7305" x2="0.45211875" y2="2.73888125" layer="37"/>
<rectangle x1="0.51308125" y1="2.7305" x2="0.5461" y2="2.73888125" layer="37"/>
<rectangle x1="0.61468125" y1="2.7305" x2="0.6477" y2="2.73888125" layer="37"/>
<rectangle x1="0.7239" y1="2.7305" x2="0.75691875" y2="2.73888125" layer="37"/>
<rectangle x1="0.83311875" y1="2.7305" x2="0.86868125" y2="2.73888125" layer="37"/>
<rectangle x1="0.96011875" y1="2.7305" x2="0.99568125" y2="2.73888125" layer="37"/>
<rectangle x1="1.0795" y1="2.7305" x2="1.11251875" y2="2.73888125" layer="37"/>
<rectangle x1="1.1811" y1="2.7305" x2="1.83388125" y2="2.73888125" layer="37"/>
<rectangle x1="1.9685" y1="2.7305" x2="2.78891875" y2="2.73888125" layer="37"/>
<rectangle x1="2.83971875" y1="2.7305" x2="2.87528125" y2="2.73888125" layer="37"/>
<rectangle x1="2.92608125" y1="2.7305" x2="3.68808125" y2="2.73888125" layer="37"/>
<rectangle x1="3.7719" y1="2.7305" x2="4.4323" y2="2.73888125" layer="37"/>
<rectangle x1="5.10031875" y1="2.7305" x2="5.72008125" y2="2.73888125" layer="37"/>
<rectangle x1="5.81151875" y1="2.7305" x2="6.57351875" y2="2.73888125" layer="37"/>
<rectangle x1="6.6167" y1="2.7305" x2="6.64971875" y2="2.73888125" layer="37"/>
<rectangle x1="6.70051875" y1="2.7305" x2="7.52348125" y2="2.73888125" layer="37"/>
<rectangle x1="7.6581" y1="2.7305" x2="8.30071875" y2="2.73888125" layer="37"/>
<rectangle x1="8.37691875" y1="2.7305" x2="8.41248125" y2="2.73888125" layer="37"/>
<rectangle x1="8.4963" y1="2.7305" x2="8.52931875" y2="2.73888125" layer="37"/>
<rectangle x1="8.61568125" y1="2.7305" x2="8.6487" y2="2.73888125" layer="37"/>
<rectangle x1="8.73251875" y1="2.7305" x2="8.76808125" y2="2.73888125" layer="37"/>
<rectangle x1="8.83411875" y1="2.7305" x2="8.86968125" y2="2.73888125" layer="37"/>
<rectangle x1="8.93571875" y1="2.7305" x2="8.97128125" y2="2.73888125" layer="37"/>
<rectangle x1="9.03731875" y1="2.7305" x2="9.49451875" y2="2.73888125" layer="37"/>
<rectangle x1="0.02031875" y1="2.73888125" x2="0.45211875" y2="2.74751875" layer="37"/>
<rectangle x1="0.51308125" y1="2.73888125" x2="0.5461" y2="2.74751875" layer="37"/>
<rectangle x1="0.61468125" y1="2.73888125" x2="0.6477" y2="2.74751875" layer="37"/>
<rectangle x1="0.7239" y1="2.73888125" x2="0.75691875" y2="2.74751875" layer="37"/>
<rectangle x1="0.83311875" y1="2.73888125" x2="0.86868125" y2="2.74751875" layer="37"/>
<rectangle x1="0.96011875" y1="2.73888125" x2="0.99568125" y2="2.74751875" layer="37"/>
<rectangle x1="1.0795" y1="2.73888125" x2="1.11251875" y2="2.74751875" layer="37"/>
<rectangle x1="1.1811" y1="2.73888125" x2="1.83388125" y2="2.74751875" layer="37"/>
<rectangle x1="1.9685" y1="2.73888125" x2="2.78891875" y2="2.74751875" layer="37"/>
<rectangle x1="2.83971875" y1="2.73888125" x2="2.87528125" y2="2.74751875" layer="37"/>
<rectangle x1="2.92608125" y1="2.73888125" x2="3.68808125" y2="2.74751875" layer="37"/>
<rectangle x1="3.7719" y1="2.73888125" x2="4.4323" y2="2.74751875" layer="37"/>
<rectangle x1="5.10031875" y1="2.73888125" x2="5.72008125" y2="2.74751875" layer="37"/>
<rectangle x1="5.81151875" y1="2.73888125" x2="6.57351875" y2="2.74751875" layer="37"/>
<rectangle x1="6.6167" y1="2.73888125" x2="6.64971875" y2="2.74751875" layer="37"/>
<rectangle x1="6.70051875" y1="2.73888125" x2="7.52348125" y2="2.74751875" layer="37"/>
<rectangle x1="7.6581" y1="2.73888125" x2="8.30071875" y2="2.74751875" layer="37"/>
<rectangle x1="8.37691875" y1="2.73888125" x2="8.41248125" y2="2.74751875" layer="37"/>
<rectangle x1="8.4963" y1="2.73888125" x2="8.52931875" y2="2.74751875" layer="37"/>
<rectangle x1="8.61568125" y1="2.73888125" x2="8.6487" y2="2.74751875" layer="37"/>
<rectangle x1="8.73251875" y1="2.73888125" x2="8.76808125" y2="2.74751875" layer="37"/>
<rectangle x1="8.83411875" y1="2.73888125" x2="8.86968125" y2="2.74751875" layer="37"/>
<rectangle x1="8.93571875" y1="2.73888125" x2="8.97128125" y2="2.74751875" layer="37"/>
<rectangle x1="9.03731875" y1="2.73888125" x2="9.49451875" y2="2.74751875" layer="37"/>
<rectangle x1="0.02031875" y1="2.74751875" x2="0.45211875" y2="2.7559" layer="37"/>
<rectangle x1="0.51308125" y1="2.74751875" x2="0.5461" y2="2.7559" layer="37"/>
<rectangle x1="0.61468125" y1="2.74751875" x2="0.6477" y2="2.7559" layer="37"/>
<rectangle x1="0.7239" y1="2.74751875" x2="0.75691875" y2="2.7559" layer="37"/>
<rectangle x1="0.83311875" y1="2.74751875" x2="0.86868125" y2="2.7559" layer="37"/>
<rectangle x1="0.96011875" y1="2.74751875" x2="0.99568125" y2="2.7559" layer="37"/>
<rectangle x1="1.0795" y1="2.74751875" x2="1.11251875" y2="2.7559" layer="37"/>
<rectangle x1="1.1811" y1="2.74751875" x2="1.83388125" y2="2.7559" layer="37"/>
<rectangle x1="1.9685" y1="2.74751875" x2="2.78891875" y2="2.7559" layer="37"/>
<rectangle x1="2.83971875" y1="2.74751875" x2="2.87528125" y2="2.7559" layer="37"/>
<rectangle x1="2.92608125" y1="2.74751875" x2="3.68808125" y2="2.7559" layer="37"/>
<rectangle x1="3.7719" y1="2.74751875" x2="4.4323" y2="2.7559" layer="37"/>
<rectangle x1="5.10031875" y1="2.74751875" x2="5.72008125" y2="2.7559" layer="37"/>
<rectangle x1="5.81151875" y1="2.74751875" x2="6.57351875" y2="2.7559" layer="37"/>
<rectangle x1="6.6167" y1="2.74751875" x2="6.64971875" y2="2.7559" layer="37"/>
<rectangle x1="6.70051875" y1="2.74751875" x2="7.52348125" y2="2.7559" layer="37"/>
<rectangle x1="7.6581" y1="2.74751875" x2="8.30071875" y2="2.7559" layer="37"/>
<rectangle x1="8.37691875" y1="2.74751875" x2="8.41248125" y2="2.7559" layer="37"/>
<rectangle x1="8.4963" y1="2.74751875" x2="8.52931875" y2="2.7559" layer="37"/>
<rectangle x1="8.61568125" y1="2.74751875" x2="8.6487" y2="2.7559" layer="37"/>
<rectangle x1="8.73251875" y1="2.74751875" x2="8.76808125" y2="2.7559" layer="37"/>
<rectangle x1="8.83411875" y1="2.74751875" x2="8.86968125" y2="2.7559" layer="37"/>
<rectangle x1="8.93571875" y1="2.74751875" x2="8.97128125" y2="2.7559" layer="37"/>
<rectangle x1="9.03731875" y1="2.74751875" x2="9.49451875" y2="2.7559" layer="37"/>
<rectangle x1="0.02031875" y1="2.7559" x2="0.45211875" y2="2.76428125" layer="37"/>
<rectangle x1="0.51308125" y1="2.7559" x2="0.5461" y2="2.76428125" layer="37"/>
<rectangle x1="0.61468125" y1="2.7559" x2="0.6477" y2="2.76428125" layer="37"/>
<rectangle x1="0.7239" y1="2.7559" x2="0.75691875" y2="2.76428125" layer="37"/>
<rectangle x1="0.83311875" y1="2.7559" x2="0.86868125" y2="2.76428125" layer="37"/>
<rectangle x1="0.96011875" y1="2.7559" x2="0.99568125" y2="2.76428125" layer="37"/>
<rectangle x1="1.0795" y1="2.7559" x2="1.11251875" y2="2.76428125" layer="37"/>
<rectangle x1="1.1811" y1="2.7559" x2="1.83388125" y2="2.76428125" layer="37"/>
<rectangle x1="1.9685" y1="2.7559" x2="2.78891875" y2="2.76428125" layer="37"/>
<rectangle x1="2.83971875" y1="2.7559" x2="2.87528125" y2="2.76428125" layer="37"/>
<rectangle x1="2.92608125" y1="2.7559" x2="3.68808125" y2="2.76428125" layer="37"/>
<rectangle x1="3.7719" y1="2.7559" x2="4.4323" y2="2.76428125" layer="37"/>
<rectangle x1="5.10031875" y1="2.7559" x2="5.72008125" y2="2.76428125" layer="37"/>
<rectangle x1="5.81151875" y1="2.7559" x2="6.57351875" y2="2.76428125" layer="37"/>
<rectangle x1="6.6167" y1="2.7559" x2="6.64971875" y2="2.76428125" layer="37"/>
<rectangle x1="6.70051875" y1="2.7559" x2="7.52348125" y2="2.76428125" layer="37"/>
<rectangle x1="7.6581" y1="2.7559" x2="8.30071875" y2="2.76428125" layer="37"/>
<rectangle x1="8.37691875" y1="2.7559" x2="8.41248125" y2="2.76428125" layer="37"/>
<rectangle x1="8.4963" y1="2.7559" x2="8.52931875" y2="2.76428125" layer="37"/>
<rectangle x1="8.61568125" y1="2.7559" x2="8.6487" y2="2.76428125" layer="37"/>
<rectangle x1="8.73251875" y1="2.7559" x2="8.76808125" y2="2.76428125" layer="37"/>
<rectangle x1="8.83411875" y1="2.7559" x2="8.86968125" y2="2.76428125" layer="37"/>
<rectangle x1="8.93571875" y1="2.7559" x2="8.97128125" y2="2.76428125" layer="37"/>
<rectangle x1="9.03731875" y1="2.7559" x2="9.49451875" y2="2.76428125" layer="37"/>
<rectangle x1="0.02031875" y1="2.76428125" x2="0.45211875" y2="2.77291875" layer="37"/>
<rectangle x1="0.51308125" y1="2.76428125" x2="0.5461" y2="2.77291875" layer="37"/>
<rectangle x1="0.61468125" y1="2.76428125" x2="0.6477" y2="2.77291875" layer="37"/>
<rectangle x1="0.7239" y1="2.76428125" x2="0.75691875" y2="2.77291875" layer="37"/>
<rectangle x1="0.83311875" y1="2.76428125" x2="0.86868125" y2="2.77291875" layer="37"/>
<rectangle x1="0.96011875" y1="2.76428125" x2="0.99568125" y2="2.77291875" layer="37"/>
<rectangle x1="1.0795" y1="2.76428125" x2="1.11251875" y2="2.77291875" layer="37"/>
<rectangle x1="1.1811" y1="2.76428125" x2="1.83388125" y2="2.77291875" layer="37"/>
<rectangle x1="1.9685" y1="2.76428125" x2="2.78891875" y2="2.77291875" layer="37"/>
<rectangle x1="2.83971875" y1="2.76428125" x2="2.87528125" y2="2.77291875" layer="37"/>
<rectangle x1="2.92608125" y1="2.76428125" x2="3.68808125" y2="2.77291875" layer="37"/>
<rectangle x1="3.7719" y1="2.76428125" x2="4.4323" y2="2.77291875" layer="37"/>
<rectangle x1="5.10031875" y1="2.76428125" x2="5.72008125" y2="2.77291875" layer="37"/>
<rectangle x1="5.81151875" y1="2.76428125" x2="6.57351875" y2="2.77291875" layer="37"/>
<rectangle x1="6.6167" y1="2.76428125" x2="6.64971875" y2="2.77291875" layer="37"/>
<rectangle x1="6.70051875" y1="2.76428125" x2="7.52348125" y2="2.77291875" layer="37"/>
<rectangle x1="7.6581" y1="2.76428125" x2="8.30071875" y2="2.77291875" layer="37"/>
<rectangle x1="8.37691875" y1="2.76428125" x2="8.41248125" y2="2.77291875" layer="37"/>
<rectangle x1="8.4963" y1="2.76428125" x2="8.52931875" y2="2.77291875" layer="37"/>
<rectangle x1="8.61568125" y1="2.76428125" x2="8.6487" y2="2.77291875" layer="37"/>
<rectangle x1="8.73251875" y1="2.76428125" x2="8.76808125" y2="2.77291875" layer="37"/>
<rectangle x1="8.83411875" y1="2.76428125" x2="8.86968125" y2="2.77291875" layer="37"/>
<rectangle x1="8.93571875" y1="2.76428125" x2="8.97128125" y2="2.77291875" layer="37"/>
<rectangle x1="9.03731875" y1="2.76428125" x2="9.49451875" y2="2.77291875" layer="37"/>
<rectangle x1="0.02031875" y1="2.77291875" x2="0.45211875" y2="2.7813" layer="37"/>
<rectangle x1="0.51308125" y1="2.77291875" x2="0.5461" y2="2.7813" layer="37"/>
<rectangle x1="0.61468125" y1="2.77291875" x2="0.6477" y2="2.7813" layer="37"/>
<rectangle x1="0.7239" y1="2.77291875" x2="0.75691875" y2="2.7813" layer="37"/>
<rectangle x1="0.83311875" y1="2.77291875" x2="0.86868125" y2="2.7813" layer="37"/>
<rectangle x1="0.96011875" y1="2.77291875" x2="0.99568125" y2="2.7813" layer="37"/>
<rectangle x1="1.0795" y1="2.77291875" x2="1.11251875" y2="2.7813" layer="37"/>
<rectangle x1="1.1811" y1="2.77291875" x2="1.83388125" y2="2.7813" layer="37"/>
<rectangle x1="1.9685" y1="2.77291875" x2="2.78891875" y2="2.7813" layer="37"/>
<rectangle x1="2.83971875" y1="2.77291875" x2="2.87528125" y2="2.7813" layer="37"/>
<rectangle x1="2.92608125" y1="2.77291875" x2="3.68808125" y2="2.7813" layer="37"/>
<rectangle x1="3.7719" y1="2.77291875" x2="4.4323" y2="2.7813" layer="37"/>
<rectangle x1="5.10031875" y1="2.77291875" x2="5.72008125" y2="2.7813" layer="37"/>
<rectangle x1="5.81151875" y1="2.77291875" x2="6.57351875" y2="2.7813" layer="37"/>
<rectangle x1="6.6167" y1="2.77291875" x2="6.64971875" y2="2.7813" layer="37"/>
<rectangle x1="6.70051875" y1="2.77291875" x2="7.52348125" y2="2.7813" layer="37"/>
<rectangle x1="7.6581" y1="2.77291875" x2="8.30071875" y2="2.7813" layer="37"/>
<rectangle x1="8.37691875" y1="2.77291875" x2="8.41248125" y2="2.7813" layer="37"/>
<rectangle x1="8.4963" y1="2.77291875" x2="8.52931875" y2="2.7813" layer="37"/>
<rectangle x1="8.61568125" y1="2.77291875" x2="8.6487" y2="2.7813" layer="37"/>
<rectangle x1="8.73251875" y1="2.77291875" x2="8.76808125" y2="2.7813" layer="37"/>
<rectangle x1="8.83411875" y1="2.77291875" x2="8.86968125" y2="2.7813" layer="37"/>
<rectangle x1="8.93571875" y1="2.77291875" x2="8.97128125" y2="2.7813" layer="37"/>
<rectangle x1="9.03731875" y1="2.77291875" x2="9.49451875" y2="2.7813" layer="37"/>
<rectangle x1="0.02031875" y1="2.7813" x2="0.45211875" y2="2.78968125" layer="37"/>
<rectangle x1="0.51308125" y1="2.7813" x2="0.5461" y2="2.78968125" layer="37"/>
<rectangle x1="0.61468125" y1="2.7813" x2="0.6477" y2="2.78968125" layer="37"/>
<rectangle x1="0.7239" y1="2.7813" x2="0.75691875" y2="2.78968125" layer="37"/>
<rectangle x1="0.83311875" y1="2.7813" x2="0.86868125" y2="2.78968125" layer="37"/>
<rectangle x1="0.96011875" y1="2.7813" x2="0.99568125" y2="2.78968125" layer="37"/>
<rectangle x1="1.0795" y1="2.7813" x2="1.11251875" y2="2.78968125" layer="37"/>
<rectangle x1="1.1811" y1="2.7813" x2="1.83388125" y2="2.78968125" layer="37"/>
<rectangle x1="1.9685" y1="2.7813" x2="2.78891875" y2="2.78968125" layer="37"/>
<rectangle x1="2.83971875" y1="2.7813" x2="2.87528125" y2="2.78968125" layer="37"/>
<rectangle x1="2.92608125" y1="2.7813" x2="3.68808125" y2="2.78968125" layer="37"/>
<rectangle x1="3.7719" y1="2.7813" x2="4.4323" y2="2.78968125" layer="37"/>
<rectangle x1="5.10031875" y1="2.7813" x2="5.72008125" y2="2.78968125" layer="37"/>
<rectangle x1="5.81151875" y1="2.7813" x2="6.57351875" y2="2.78968125" layer="37"/>
<rectangle x1="6.6167" y1="2.7813" x2="6.64971875" y2="2.78968125" layer="37"/>
<rectangle x1="6.70051875" y1="2.7813" x2="7.52348125" y2="2.78968125" layer="37"/>
<rectangle x1="7.6581" y1="2.7813" x2="8.30071875" y2="2.78968125" layer="37"/>
<rectangle x1="8.37691875" y1="2.7813" x2="8.41248125" y2="2.78968125" layer="37"/>
<rectangle x1="8.4963" y1="2.7813" x2="8.52931875" y2="2.78968125" layer="37"/>
<rectangle x1="8.61568125" y1="2.7813" x2="8.6487" y2="2.78968125" layer="37"/>
<rectangle x1="8.73251875" y1="2.7813" x2="8.76808125" y2="2.78968125" layer="37"/>
<rectangle x1="8.83411875" y1="2.7813" x2="8.86968125" y2="2.78968125" layer="37"/>
<rectangle x1="8.93571875" y1="2.7813" x2="8.97128125" y2="2.78968125" layer="37"/>
<rectangle x1="9.03731875" y1="2.7813" x2="9.49451875" y2="2.78968125" layer="37"/>
<rectangle x1="0.02031875" y1="2.78968125" x2="0.45211875" y2="2.79831875" layer="37"/>
<rectangle x1="0.51308125" y1="2.78968125" x2="0.5461" y2="2.79831875" layer="37"/>
<rectangle x1="0.61468125" y1="2.78968125" x2="0.6477" y2="2.79831875" layer="37"/>
<rectangle x1="0.7239" y1="2.78968125" x2="0.75691875" y2="2.79831875" layer="37"/>
<rectangle x1="0.83311875" y1="2.78968125" x2="0.86868125" y2="2.79831875" layer="37"/>
<rectangle x1="0.96011875" y1="2.78968125" x2="0.99568125" y2="2.79831875" layer="37"/>
<rectangle x1="1.0795" y1="2.78968125" x2="1.11251875" y2="2.79831875" layer="37"/>
<rectangle x1="1.1811" y1="2.78968125" x2="1.83388125" y2="2.79831875" layer="37"/>
<rectangle x1="1.9685" y1="2.78968125" x2="2.78891875" y2="2.79831875" layer="37"/>
<rectangle x1="2.83971875" y1="2.78968125" x2="2.87528125" y2="2.79831875" layer="37"/>
<rectangle x1="2.92608125" y1="2.78968125" x2="3.68808125" y2="2.79831875" layer="37"/>
<rectangle x1="3.7719" y1="2.78968125" x2="4.4323" y2="2.79831875" layer="37"/>
<rectangle x1="5.10031875" y1="2.78968125" x2="5.72008125" y2="2.79831875" layer="37"/>
<rectangle x1="5.81151875" y1="2.78968125" x2="6.57351875" y2="2.79831875" layer="37"/>
<rectangle x1="6.6167" y1="2.78968125" x2="6.64971875" y2="2.79831875" layer="37"/>
<rectangle x1="6.70051875" y1="2.78968125" x2="7.52348125" y2="2.79831875" layer="37"/>
<rectangle x1="7.6581" y1="2.78968125" x2="8.30071875" y2="2.79831875" layer="37"/>
<rectangle x1="8.37691875" y1="2.78968125" x2="8.41248125" y2="2.79831875" layer="37"/>
<rectangle x1="8.4963" y1="2.78968125" x2="8.52931875" y2="2.79831875" layer="37"/>
<rectangle x1="8.61568125" y1="2.78968125" x2="8.6487" y2="2.79831875" layer="37"/>
<rectangle x1="8.73251875" y1="2.78968125" x2="8.76808125" y2="2.79831875" layer="37"/>
<rectangle x1="8.83411875" y1="2.78968125" x2="8.86968125" y2="2.79831875" layer="37"/>
<rectangle x1="8.93571875" y1="2.78968125" x2="8.97128125" y2="2.79831875" layer="37"/>
<rectangle x1="9.03731875" y1="2.78968125" x2="9.49451875" y2="2.79831875" layer="37"/>
<rectangle x1="0.02031875" y1="2.79831875" x2="0.45211875" y2="2.8067" layer="37"/>
<rectangle x1="0.51308125" y1="2.79831875" x2="0.5461" y2="2.8067" layer="37"/>
<rectangle x1="0.61468125" y1="2.79831875" x2="0.6477" y2="2.8067" layer="37"/>
<rectangle x1="0.7239" y1="2.79831875" x2="0.75691875" y2="2.8067" layer="37"/>
<rectangle x1="0.83311875" y1="2.79831875" x2="0.86868125" y2="2.8067" layer="37"/>
<rectangle x1="0.96011875" y1="2.79831875" x2="0.99568125" y2="2.8067" layer="37"/>
<rectangle x1="1.0795" y1="2.79831875" x2="1.11251875" y2="2.8067" layer="37"/>
<rectangle x1="1.1811" y1="2.79831875" x2="1.83388125" y2="2.8067" layer="37"/>
<rectangle x1="1.9685" y1="2.79831875" x2="2.78891875" y2="2.8067" layer="37"/>
<rectangle x1="2.83971875" y1="2.79831875" x2="2.87528125" y2="2.8067" layer="37"/>
<rectangle x1="2.92608125" y1="2.79831875" x2="3.68808125" y2="2.8067" layer="37"/>
<rectangle x1="3.7719" y1="2.79831875" x2="4.4323" y2="2.8067" layer="37"/>
<rectangle x1="5.10031875" y1="2.79831875" x2="5.72008125" y2="2.8067" layer="37"/>
<rectangle x1="5.81151875" y1="2.79831875" x2="6.57351875" y2="2.8067" layer="37"/>
<rectangle x1="6.6167" y1="2.79831875" x2="6.64971875" y2="2.8067" layer="37"/>
<rectangle x1="6.70051875" y1="2.79831875" x2="7.52348125" y2="2.8067" layer="37"/>
<rectangle x1="7.6581" y1="2.79831875" x2="8.30071875" y2="2.8067" layer="37"/>
<rectangle x1="8.37691875" y1="2.79831875" x2="8.41248125" y2="2.8067" layer="37"/>
<rectangle x1="8.4963" y1="2.79831875" x2="8.52931875" y2="2.8067" layer="37"/>
<rectangle x1="8.61568125" y1="2.79831875" x2="8.6487" y2="2.8067" layer="37"/>
<rectangle x1="8.73251875" y1="2.79831875" x2="8.76808125" y2="2.8067" layer="37"/>
<rectangle x1="8.83411875" y1="2.79831875" x2="8.86968125" y2="2.8067" layer="37"/>
<rectangle x1="8.93571875" y1="2.79831875" x2="8.97128125" y2="2.8067" layer="37"/>
<rectangle x1="9.03731875" y1="2.79831875" x2="9.49451875" y2="2.8067" layer="37"/>
<rectangle x1="0.41148125" y1="2.8067" x2="0.45211875" y2="2.81508125" layer="37"/>
<rectangle x1="0.51308125" y1="2.8067" x2="0.5461" y2="2.81508125" layer="37"/>
<rectangle x1="0.61468125" y1="2.8067" x2="0.6477" y2="2.81508125" layer="37"/>
<rectangle x1="0.7239" y1="2.8067" x2="0.75691875" y2="2.81508125" layer="37"/>
<rectangle x1="0.83311875" y1="2.8067" x2="0.86868125" y2="2.81508125" layer="37"/>
<rectangle x1="0.96011875" y1="2.8067" x2="0.99568125" y2="2.81508125" layer="37"/>
<rectangle x1="1.0795" y1="2.8067" x2="1.11251875" y2="2.81508125" layer="37"/>
<rectangle x1="1.1811" y1="2.8067" x2="1.21411875" y2="2.81508125" layer="37"/>
<rectangle x1="1.79831875" y1="2.8067" x2="1.83388125" y2="2.81508125" layer="37"/>
<rectangle x1="1.9685" y1="2.8067" x2="2.00151875" y2="2.81508125" layer="37"/>
<rectangle x1="2.7559" y1="2.8067" x2="2.78891875" y2="2.81508125" layer="37"/>
<rectangle x1="2.83971875" y1="2.8067" x2="2.87528125" y2="2.81508125" layer="37"/>
<rectangle x1="2.92608125" y1="2.8067" x2="2.9591" y2="2.81508125" layer="37"/>
<rectangle x1="3.65251875" y1="2.8067" x2="3.68808125" y2="2.81508125" layer="37"/>
<rectangle x1="3.7719" y1="2.8067" x2="3.80491875" y2="2.81508125" layer="37"/>
<rectangle x1="4.4069" y1="2.8067" x2="4.4323" y2="2.81508125" layer="37"/>
<rectangle x1="5.10031875" y1="2.8067" x2="5.13588125" y2="2.81508125" layer="37"/>
<rectangle x1="5.68451875" y1="2.8067" x2="5.72008125" y2="2.81508125" layer="37"/>
<rectangle x1="5.81151875" y1="2.8067" x2="5.84708125" y2="2.81508125" layer="37"/>
<rectangle x1="6.5405" y1="2.8067" x2="6.57351875" y2="2.81508125" layer="37"/>
<rectangle x1="6.6167" y1="2.8067" x2="6.64971875" y2="2.81508125" layer="37"/>
<rectangle x1="6.70051875" y1="2.8067" x2="6.73608125" y2="2.81508125" layer="37"/>
<rectangle x1="7.48791875" y1="2.8067" x2="7.52348125" y2="2.81508125" layer="37"/>
<rectangle x1="7.6581" y1="2.8067" x2="7.69111875" y2="2.81508125" layer="37"/>
<rectangle x1="8.2677" y1="2.8067" x2="8.30071875" y2="2.81508125" layer="37"/>
<rectangle x1="8.37691875" y1="2.8067" x2="8.41248125" y2="2.81508125" layer="37"/>
<rectangle x1="8.4963" y1="2.8067" x2="8.52931875" y2="2.81508125" layer="37"/>
<rectangle x1="8.61568125" y1="2.8067" x2="8.6487" y2="2.81508125" layer="37"/>
<rectangle x1="8.73251875" y1="2.8067" x2="8.76808125" y2="2.81508125" layer="37"/>
<rectangle x1="8.83411875" y1="2.8067" x2="8.86968125" y2="2.81508125" layer="37"/>
<rectangle x1="8.93571875" y1="2.8067" x2="8.97128125" y2="2.81508125" layer="37"/>
<rectangle x1="9.03731875" y1="2.8067" x2="9.07288125" y2="2.81508125" layer="37"/>
<rectangle x1="0.4191" y1="2.81508125" x2="0.45211875" y2="2.82371875" layer="37"/>
<rectangle x1="0.51308125" y1="2.81508125" x2="0.5461" y2="2.82371875" layer="37"/>
<rectangle x1="0.61468125" y1="2.81508125" x2="0.6477" y2="2.82371875" layer="37"/>
<rectangle x1="0.7239" y1="2.81508125" x2="0.75691875" y2="2.82371875" layer="37"/>
<rectangle x1="0.83311875" y1="2.81508125" x2="0.86868125" y2="2.82371875" layer="37"/>
<rectangle x1="0.96011875" y1="2.81508125" x2="0.99568125" y2="2.82371875" layer="37"/>
<rectangle x1="1.0795" y1="2.81508125" x2="1.11251875" y2="2.82371875" layer="37"/>
<rectangle x1="1.1811" y1="2.81508125" x2="1.21411875" y2="2.82371875" layer="37"/>
<rectangle x1="1.79831875" y1="2.81508125" x2="1.83388125" y2="2.82371875" layer="37"/>
<rectangle x1="1.9685" y1="2.81508125" x2="2.00151875" y2="2.82371875" layer="37"/>
<rectangle x1="2.7559" y1="2.81508125" x2="2.78891875" y2="2.82371875" layer="37"/>
<rectangle x1="2.83971875" y1="2.81508125" x2="2.87528125" y2="2.82371875" layer="37"/>
<rectangle x1="2.92608125" y1="2.81508125" x2="2.9591" y2="2.82371875" layer="37"/>
<rectangle x1="3.65251875" y1="2.81508125" x2="3.68808125" y2="2.82371875" layer="37"/>
<rectangle x1="3.7719" y1="2.81508125" x2="3.80491875" y2="2.82371875" layer="37"/>
<rectangle x1="4.4069" y1="2.81508125" x2="4.4323" y2="2.82371875" layer="37"/>
<rectangle x1="5.10031875" y1="2.81508125" x2="5.13588125" y2="2.82371875" layer="37"/>
<rectangle x1="5.68451875" y1="2.81508125" x2="5.72008125" y2="2.82371875" layer="37"/>
<rectangle x1="5.81151875" y1="2.81508125" x2="5.84708125" y2="2.82371875" layer="37"/>
<rectangle x1="6.5405" y1="2.81508125" x2="6.57351875" y2="2.82371875" layer="37"/>
<rectangle x1="6.6167" y1="2.81508125" x2="6.64971875" y2="2.82371875" layer="37"/>
<rectangle x1="6.70051875" y1="2.81508125" x2="6.73608125" y2="2.82371875" layer="37"/>
<rectangle x1="7.48791875" y1="2.81508125" x2="7.52348125" y2="2.82371875" layer="37"/>
<rectangle x1="7.6581" y1="2.81508125" x2="7.69111875" y2="2.82371875" layer="37"/>
<rectangle x1="8.2677" y1="2.81508125" x2="8.30071875" y2="2.82371875" layer="37"/>
<rectangle x1="8.37691875" y1="2.81508125" x2="8.41248125" y2="2.82371875" layer="37"/>
<rectangle x1="8.4963" y1="2.81508125" x2="8.52931875" y2="2.82371875" layer="37"/>
<rectangle x1="8.61568125" y1="2.81508125" x2="8.6487" y2="2.82371875" layer="37"/>
<rectangle x1="8.73251875" y1="2.81508125" x2="8.76808125" y2="2.82371875" layer="37"/>
<rectangle x1="8.83411875" y1="2.81508125" x2="8.86968125" y2="2.82371875" layer="37"/>
<rectangle x1="8.93571875" y1="2.81508125" x2="8.97128125" y2="2.82371875" layer="37"/>
<rectangle x1="9.03731875" y1="2.81508125" x2="9.07288125" y2="2.82371875" layer="37"/>
<rectangle x1="0.4191" y1="2.82371875" x2="0.45211875" y2="2.8321" layer="37"/>
<rectangle x1="0.51308125" y1="2.82371875" x2="0.5461" y2="2.8321" layer="37"/>
<rectangle x1="0.61468125" y1="2.82371875" x2="0.6477" y2="2.8321" layer="37"/>
<rectangle x1="0.7239" y1="2.82371875" x2="0.75691875" y2="2.8321" layer="37"/>
<rectangle x1="0.83311875" y1="2.82371875" x2="0.86868125" y2="2.8321" layer="37"/>
<rectangle x1="0.96011875" y1="2.82371875" x2="0.99568125" y2="2.8321" layer="37"/>
<rectangle x1="1.0795" y1="2.82371875" x2="1.11251875" y2="2.8321" layer="37"/>
<rectangle x1="1.1811" y1="2.82371875" x2="1.21411875" y2="2.8321" layer="37"/>
<rectangle x1="1.79831875" y1="2.82371875" x2="1.83388125" y2="2.8321" layer="37"/>
<rectangle x1="1.9685" y1="2.82371875" x2="2.00151875" y2="2.8321" layer="37"/>
<rectangle x1="2.7559" y1="2.82371875" x2="2.78891875" y2="2.8321" layer="37"/>
<rectangle x1="2.83971875" y1="2.82371875" x2="2.87528125" y2="2.8321" layer="37"/>
<rectangle x1="2.92608125" y1="2.82371875" x2="2.9591" y2="2.8321" layer="37"/>
<rectangle x1="3.65251875" y1="2.82371875" x2="3.68808125" y2="2.8321" layer="37"/>
<rectangle x1="3.7719" y1="2.82371875" x2="3.80491875" y2="2.8321" layer="37"/>
<rectangle x1="4.4069" y1="2.82371875" x2="4.4323" y2="2.8321" layer="37"/>
<rectangle x1="5.10031875" y1="2.82371875" x2="5.13588125" y2="2.8321" layer="37"/>
<rectangle x1="5.68451875" y1="2.82371875" x2="5.72008125" y2="2.8321" layer="37"/>
<rectangle x1="5.81151875" y1="2.82371875" x2="5.84708125" y2="2.8321" layer="37"/>
<rectangle x1="6.5405" y1="2.82371875" x2="6.57351875" y2="2.8321" layer="37"/>
<rectangle x1="6.6167" y1="2.82371875" x2="6.64971875" y2="2.8321" layer="37"/>
<rectangle x1="6.70051875" y1="2.82371875" x2="6.73608125" y2="2.8321" layer="37"/>
<rectangle x1="7.48791875" y1="2.82371875" x2="7.52348125" y2="2.8321" layer="37"/>
<rectangle x1="7.6581" y1="2.82371875" x2="7.69111875" y2="2.8321" layer="37"/>
<rectangle x1="8.2677" y1="2.82371875" x2="8.30071875" y2="2.8321" layer="37"/>
<rectangle x1="8.37691875" y1="2.82371875" x2="8.41248125" y2="2.8321" layer="37"/>
<rectangle x1="8.4963" y1="2.82371875" x2="8.52931875" y2="2.8321" layer="37"/>
<rectangle x1="8.61568125" y1="2.82371875" x2="8.6487" y2="2.8321" layer="37"/>
<rectangle x1="8.73251875" y1="2.82371875" x2="8.76808125" y2="2.8321" layer="37"/>
<rectangle x1="8.83411875" y1="2.82371875" x2="8.86968125" y2="2.8321" layer="37"/>
<rectangle x1="8.93571875" y1="2.82371875" x2="8.97128125" y2="2.8321" layer="37"/>
<rectangle x1="9.03731875" y1="2.82371875" x2="9.07288125" y2="2.8321" layer="37"/>
<rectangle x1="0.4191" y1="2.8321" x2="0.45211875" y2="2.84048125" layer="37"/>
<rectangle x1="0.51308125" y1="2.8321" x2="0.5461" y2="2.84048125" layer="37"/>
<rectangle x1="0.61468125" y1="2.8321" x2="0.6477" y2="2.84048125" layer="37"/>
<rectangle x1="0.7239" y1="2.8321" x2="0.75691875" y2="2.84048125" layer="37"/>
<rectangle x1="0.83311875" y1="2.8321" x2="0.86868125" y2="2.84048125" layer="37"/>
<rectangle x1="0.96011875" y1="2.8321" x2="0.99568125" y2="2.84048125" layer="37"/>
<rectangle x1="1.0795" y1="2.8321" x2="1.11251875" y2="2.84048125" layer="37"/>
<rectangle x1="1.1811" y1="2.8321" x2="1.21411875" y2="2.84048125" layer="37"/>
<rectangle x1="1.79831875" y1="2.8321" x2="1.83388125" y2="2.84048125" layer="37"/>
<rectangle x1="1.9685" y1="2.8321" x2="2.00151875" y2="2.84048125" layer="37"/>
<rectangle x1="2.7559" y1="2.8321" x2="2.78891875" y2="2.84048125" layer="37"/>
<rectangle x1="2.83971875" y1="2.8321" x2="2.87528125" y2="2.84048125" layer="37"/>
<rectangle x1="2.92608125" y1="2.8321" x2="2.9591" y2="2.84048125" layer="37"/>
<rectangle x1="3.65251875" y1="2.8321" x2="3.68808125" y2="2.84048125" layer="37"/>
<rectangle x1="3.7719" y1="2.8321" x2="3.80491875" y2="2.84048125" layer="37"/>
<rectangle x1="4.4069" y1="2.8321" x2="4.4323" y2="2.84048125" layer="37"/>
<rectangle x1="5.10031875" y1="2.8321" x2="5.13588125" y2="2.84048125" layer="37"/>
<rectangle x1="5.68451875" y1="2.8321" x2="5.72008125" y2="2.84048125" layer="37"/>
<rectangle x1="5.81151875" y1="2.8321" x2="5.84708125" y2="2.84048125" layer="37"/>
<rectangle x1="6.5405" y1="2.8321" x2="6.57351875" y2="2.84048125" layer="37"/>
<rectangle x1="6.6167" y1="2.8321" x2="6.64971875" y2="2.84048125" layer="37"/>
<rectangle x1="6.70051875" y1="2.8321" x2="6.73608125" y2="2.84048125" layer="37"/>
<rectangle x1="7.48791875" y1="2.8321" x2="7.52348125" y2="2.84048125" layer="37"/>
<rectangle x1="7.6581" y1="2.8321" x2="7.69111875" y2="2.84048125" layer="37"/>
<rectangle x1="8.2677" y1="2.8321" x2="8.30071875" y2="2.84048125" layer="37"/>
<rectangle x1="8.37691875" y1="2.8321" x2="8.41248125" y2="2.84048125" layer="37"/>
<rectangle x1="8.4963" y1="2.8321" x2="8.52931875" y2="2.84048125" layer="37"/>
<rectangle x1="8.61568125" y1="2.8321" x2="8.6487" y2="2.84048125" layer="37"/>
<rectangle x1="8.73251875" y1="2.8321" x2="8.76808125" y2="2.84048125" layer="37"/>
<rectangle x1="8.83411875" y1="2.8321" x2="8.86968125" y2="2.84048125" layer="37"/>
<rectangle x1="8.93571875" y1="2.8321" x2="8.97128125" y2="2.84048125" layer="37"/>
<rectangle x1="9.03731875" y1="2.8321" x2="9.07288125" y2="2.84048125" layer="37"/>
<rectangle x1="0.4191" y1="2.84048125" x2="0.45211875" y2="2.84911875" layer="37"/>
<rectangle x1="0.51308125" y1="2.84048125" x2="0.5461" y2="2.84911875" layer="37"/>
<rectangle x1="0.61468125" y1="2.84048125" x2="0.6477" y2="2.84911875" layer="37"/>
<rectangle x1="0.7239" y1="2.84048125" x2="0.75691875" y2="2.84911875" layer="37"/>
<rectangle x1="0.83311875" y1="2.84048125" x2="0.86868125" y2="2.84911875" layer="37"/>
<rectangle x1="0.96011875" y1="2.84048125" x2="0.99568125" y2="2.84911875" layer="37"/>
<rectangle x1="1.0795" y1="2.84048125" x2="1.11251875" y2="2.84911875" layer="37"/>
<rectangle x1="1.1811" y1="2.84048125" x2="1.21411875" y2="2.84911875" layer="37"/>
<rectangle x1="1.79831875" y1="2.84048125" x2="1.83388125" y2="2.84911875" layer="37"/>
<rectangle x1="1.9685" y1="2.84048125" x2="2.00151875" y2="2.84911875" layer="37"/>
<rectangle x1="2.7559" y1="2.84048125" x2="2.78891875" y2="2.84911875" layer="37"/>
<rectangle x1="2.83971875" y1="2.84048125" x2="2.87528125" y2="2.84911875" layer="37"/>
<rectangle x1="2.92608125" y1="2.84048125" x2="2.9591" y2="2.84911875" layer="37"/>
<rectangle x1="3.65251875" y1="2.84048125" x2="3.68808125" y2="2.84911875" layer="37"/>
<rectangle x1="3.7719" y1="2.84048125" x2="3.80491875" y2="2.84911875" layer="37"/>
<rectangle x1="4.4069" y1="2.84048125" x2="4.4323" y2="2.84911875" layer="37"/>
<rectangle x1="5.10031875" y1="2.84048125" x2="5.13588125" y2="2.84911875" layer="37"/>
<rectangle x1="5.68451875" y1="2.84048125" x2="5.72008125" y2="2.84911875" layer="37"/>
<rectangle x1="5.81151875" y1="2.84048125" x2="5.84708125" y2="2.84911875" layer="37"/>
<rectangle x1="6.5405" y1="2.84048125" x2="6.57351875" y2="2.84911875" layer="37"/>
<rectangle x1="6.6167" y1="2.84048125" x2="6.64971875" y2="2.84911875" layer="37"/>
<rectangle x1="6.70051875" y1="2.84048125" x2="6.73608125" y2="2.84911875" layer="37"/>
<rectangle x1="7.48791875" y1="2.84048125" x2="7.52348125" y2="2.84911875" layer="37"/>
<rectangle x1="7.6581" y1="2.84048125" x2="7.69111875" y2="2.84911875" layer="37"/>
<rectangle x1="8.2677" y1="2.84048125" x2="8.30071875" y2="2.84911875" layer="37"/>
<rectangle x1="8.37691875" y1="2.84048125" x2="8.41248125" y2="2.84911875" layer="37"/>
<rectangle x1="8.4963" y1="2.84048125" x2="8.52931875" y2="2.84911875" layer="37"/>
<rectangle x1="8.61568125" y1="2.84048125" x2="8.6487" y2="2.84911875" layer="37"/>
<rectangle x1="8.73251875" y1="2.84048125" x2="8.76808125" y2="2.84911875" layer="37"/>
<rectangle x1="8.83411875" y1="2.84048125" x2="8.86968125" y2="2.84911875" layer="37"/>
<rectangle x1="8.93571875" y1="2.84048125" x2="8.97128125" y2="2.84911875" layer="37"/>
<rectangle x1="9.03731875" y1="2.84048125" x2="9.07288125" y2="2.84911875" layer="37"/>
<rectangle x1="0.4191" y1="2.84911875" x2="0.45211875" y2="2.8575" layer="37"/>
<rectangle x1="0.51308125" y1="2.84911875" x2="0.5461" y2="2.8575" layer="37"/>
<rectangle x1="0.61468125" y1="2.84911875" x2="0.6477" y2="2.8575" layer="37"/>
<rectangle x1="0.7239" y1="2.84911875" x2="0.75691875" y2="2.8575" layer="37"/>
<rectangle x1="0.83311875" y1="2.84911875" x2="0.86868125" y2="2.8575" layer="37"/>
<rectangle x1="0.96011875" y1="2.84911875" x2="0.99568125" y2="2.8575" layer="37"/>
<rectangle x1="1.0795" y1="2.84911875" x2="1.11251875" y2="2.8575" layer="37"/>
<rectangle x1="1.1811" y1="2.84911875" x2="1.21411875" y2="2.8575" layer="37"/>
<rectangle x1="1.79831875" y1="2.84911875" x2="1.83388125" y2="2.8575" layer="37"/>
<rectangle x1="1.9685" y1="2.84911875" x2="2.00151875" y2="2.8575" layer="37"/>
<rectangle x1="2.7559" y1="2.84911875" x2="2.78891875" y2="2.8575" layer="37"/>
<rectangle x1="2.83971875" y1="2.84911875" x2="2.87528125" y2="2.8575" layer="37"/>
<rectangle x1="2.92608125" y1="2.84911875" x2="2.9591" y2="2.8575" layer="37"/>
<rectangle x1="3.65251875" y1="2.84911875" x2="3.68808125" y2="2.8575" layer="37"/>
<rectangle x1="3.7719" y1="2.84911875" x2="3.80491875" y2="2.8575" layer="37"/>
<rectangle x1="4.4069" y1="2.84911875" x2="4.4323" y2="2.8575" layer="37"/>
<rectangle x1="5.10031875" y1="2.84911875" x2="5.13588125" y2="2.8575" layer="37"/>
<rectangle x1="5.68451875" y1="2.84911875" x2="5.72008125" y2="2.8575" layer="37"/>
<rectangle x1="5.81151875" y1="2.84911875" x2="5.84708125" y2="2.8575" layer="37"/>
<rectangle x1="6.5405" y1="2.84911875" x2="6.57351875" y2="2.8575" layer="37"/>
<rectangle x1="6.6167" y1="2.84911875" x2="6.64971875" y2="2.8575" layer="37"/>
<rectangle x1="6.70051875" y1="2.84911875" x2="6.73608125" y2="2.8575" layer="37"/>
<rectangle x1="7.48791875" y1="2.84911875" x2="7.52348125" y2="2.8575" layer="37"/>
<rectangle x1="7.6581" y1="2.84911875" x2="7.69111875" y2="2.8575" layer="37"/>
<rectangle x1="8.2677" y1="2.84911875" x2="8.30071875" y2="2.8575" layer="37"/>
<rectangle x1="8.37691875" y1="2.84911875" x2="8.41248125" y2="2.8575" layer="37"/>
<rectangle x1="8.4963" y1="2.84911875" x2="8.52931875" y2="2.8575" layer="37"/>
<rectangle x1="8.61568125" y1="2.84911875" x2="8.6487" y2="2.8575" layer="37"/>
<rectangle x1="8.73251875" y1="2.84911875" x2="8.76808125" y2="2.8575" layer="37"/>
<rectangle x1="8.83411875" y1="2.84911875" x2="8.86968125" y2="2.8575" layer="37"/>
<rectangle x1="8.93571875" y1="2.84911875" x2="8.97128125" y2="2.8575" layer="37"/>
<rectangle x1="9.03731875" y1="2.84911875" x2="9.07288125" y2="2.8575" layer="37"/>
<rectangle x1="0.4191" y1="2.8575" x2="0.45211875" y2="2.86588125" layer="37"/>
<rectangle x1="0.51308125" y1="2.8575" x2="0.5461" y2="2.86588125" layer="37"/>
<rectangle x1="0.61468125" y1="2.8575" x2="0.6477" y2="2.86588125" layer="37"/>
<rectangle x1="0.7239" y1="2.8575" x2="0.75691875" y2="2.86588125" layer="37"/>
<rectangle x1="0.83311875" y1="2.8575" x2="0.86868125" y2="2.86588125" layer="37"/>
<rectangle x1="0.96011875" y1="2.8575" x2="0.99568125" y2="2.86588125" layer="37"/>
<rectangle x1="1.0795" y1="2.8575" x2="1.11251875" y2="2.86588125" layer="37"/>
<rectangle x1="1.1811" y1="2.8575" x2="1.21411875" y2="2.86588125" layer="37"/>
<rectangle x1="1.79831875" y1="2.8575" x2="1.83388125" y2="2.86588125" layer="37"/>
<rectangle x1="1.9685" y1="2.8575" x2="2.00151875" y2="2.86588125" layer="37"/>
<rectangle x1="2.7559" y1="2.8575" x2="2.78891875" y2="2.86588125" layer="37"/>
<rectangle x1="2.83971875" y1="2.8575" x2="2.87528125" y2="2.86588125" layer="37"/>
<rectangle x1="2.92608125" y1="2.8575" x2="2.9591" y2="2.86588125" layer="37"/>
<rectangle x1="3.65251875" y1="2.8575" x2="3.68808125" y2="2.86588125" layer="37"/>
<rectangle x1="3.7719" y1="2.8575" x2="3.80491875" y2="2.86588125" layer="37"/>
<rectangle x1="4.4069" y1="2.8575" x2="4.4323" y2="2.86588125" layer="37"/>
<rectangle x1="5.10031875" y1="2.8575" x2="5.13588125" y2="2.86588125" layer="37"/>
<rectangle x1="5.68451875" y1="2.8575" x2="5.72008125" y2="2.86588125" layer="37"/>
<rectangle x1="5.81151875" y1="2.8575" x2="5.84708125" y2="2.86588125" layer="37"/>
<rectangle x1="6.5405" y1="2.8575" x2="6.57351875" y2="2.86588125" layer="37"/>
<rectangle x1="6.6167" y1="2.8575" x2="6.64971875" y2="2.86588125" layer="37"/>
<rectangle x1="6.70051875" y1="2.8575" x2="6.73608125" y2="2.86588125" layer="37"/>
<rectangle x1="7.48791875" y1="2.8575" x2="7.52348125" y2="2.86588125" layer="37"/>
<rectangle x1="7.6581" y1="2.8575" x2="7.69111875" y2="2.86588125" layer="37"/>
<rectangle x1="8.2677" y1="2.8575" x2="8.30071875" y2="2.86588125" layer="37"/>
<rectangle x1="8.37691875" y1="2.8575" x2="8.41248125" y2="2.86588125" layer="37"/>
<rectangle x1="8.4963" y1="2.8575" x2="8.52931875" y2="2.86588125" layer="37"/>
<rectangle x1="8.61568125" y1="2.8575" x2="8.6487" y2="2.86588125" layer="37"/>
<rectangle x1="8.73251875" y1="2.8575" x2="8.76808125" y2="2.86588125" layer="37"/>
<rectangle x1="8.83411875" y1="2.8575" x2="8.86968125" y2="2.86588125" layer="37"/>
<rectangle x1="8.93571875" y1="2.8575" x2="8.97128125" y2="2.86588125" layer="37"/>
<rectangle x1="9.03731875" y1="2.8575" x2="9.07288125" y2="2.86588125" layer="37"/>
<rectangle x1="0.4191" y1="2.86588125" x2="0.45211875" y2="2.87451875" layer="37"/>
<rectangle x1="0.51308125" y1="2.86588125" x2="0.5461" y2="2.87451875" layer="37"/>
<rectangle x1="0.61468125" y1="2.86588125" x2="0.6477" y2="2.87451875" layer="37"/>
<rectangle x1="0.7239" y1="2.86588125" x2="0.75691875" y2="2.87451875" layer="37"/>
<rectangle x1="0.83311875" y1="2.86588125" x2="0.86868125" y2="2.87451875" layer="37"/>
<rectangle x1="0.96011875" y1="2.86588125" x2="0.99568125" y2="2.87451875" layer="37"/>
<rectangle x1="1.0795" y1="2.86588125" x2="1.11251875" y2="2.87451875" layer="37"/>
<rectangle x1="1.1811" y1="2.86588125" x2="1.21411875" y2="2.87451875" layer="37"/>
<rectangle x1="1.79831875" y1="2.86588125" x2="1.83388125" y2="2.87451875" layer="37"/>
<rectangle x1="1.9685" y1="2.86588125" x2="2.00151875" y2="2.87451875" layer="37"/>
<rectangle x1="2.7559" y1="2.86588125" x2="2.78891875" y2="2.87451875" layer="37"/>
<rectangle x1="2.83971875" y1="2.86588125" x2="2.87528125" y2="2.87451875" layer="37"/>
<rectangle x1="2.92608125" y1="2.86588125" x2="2.9591" y2="2.87451875" layer="37"/>
<rectangle x1="3.65251875" y1="2.86588125" x2="3.68808125" y2="2.87451875" layer="37"/>
<rectangle x1="3.7719" y1="2.86588125" x2="3.80491875" y2="2.87451875" layer="37"/>
<rectangle x1="4.4069" y1="2.86588125" x2="4.4323" y2="2.87451875" layer="37"/>
<rectangle x1="5.10031875" y1="2.86588125" x2="5.13588125" y2="2.87451875" layer="37"/>
<rectangle x1="5.68451875" y1="2.86588125" x2="5.72008125" y2="2.87451875" layer="37"/>
<rectangle x1="5.81151875" y1="2.86588125" x2="5.84708125" y2="2.87451875" layer="37"/>
<rectangle x1="6.5405" y1="2.86588125" x2="6.57351875" y2="2.87451875" layer="37"/>
<rectangle x1="6.6167" y1="2.86588125" x2="6.64971875" y2="2.87451875" layer="37"/>
<rectangle x1="6.70051875" y1="2.86588125" x2="6.73608125" y2="2.87451875" layer="37"/>
<rectangle x1="7.48791875" y1="2.86588125" x2="7.52348125" y2="2.87451875" layer="37"/>
<rectangle x1="7.6581" y1="2.86588125" x2="7.69111875" y2="2.87451875" layer="37"/>
<rectangle x1="8.2677" y1="2.86588125" x2="8.30071875" y2="2.87451875" layer="37"/>
<rectangle x1="8.37691875" y1="2.86588125" x2="8.41248125" y2="2.87451875" layer="37"/>
<rectangle x1="8.4963" y1="2.86588125" x2="8.52931875" y2="2.87451875" layer="37"/>
<rectangle x1="8.61568125" y1="2.86588125" x2="8.6487" y2="2.87451875" layer="37"/>
<rectangle x1="8.73251875" y1="2.86588125" x2="8.76808125" y2="2.87451875" layer="37"/>
<rectangle x1="8.83411875" y1="2.86588125" x2="8.86968125" y2="2.87451875" layer="37"/>
<rectangle x1="8.93571875" y1="2.86588125" x2="8.97128125" y2="2.87451875" layer="37"/>
<rectangle x1="9.03731875" y1="2.86588125" x2="9.07288125" y2="2.87451875" layer="37"/>
<rectangle x1="0.4191" y1="2.87451875" x2="0.45211875" y2="2.8829" layer="37"/>
<rectangle x1="0.51308125" y1="2.87451875" x2="0.5461" y2="2.8829" layer="37"/>
<rectangle x1="0.61468125" y1="2.87451875" x2="0.6477" y2="2.8829" layer="37"/>
<rectangle x1="0.7239" y1="2.87451875" x2="0.75691875" y2="2.8829" layer="37"/>
<rectangle x1="0.83311875" y1="2.87451875" x2="0.86868125" y2="2.8829" layer="37"/>
<rectangle x1="0.96011875" y1="2.87451875" x2="0.99568125" y2="2.8829" layer="37"/>
<rectangle x1="1.0795" y1="2.87451875" x2="1.11251875" y2="2.8829" layer="37"/>
<rectangle x1="1.1811" y1="2.87451875" x2="1.21411875" y2="2.8829" layer="37"/>
<rectangle x1="1.79831875" y1="2.87451875" x2="1.83388125" y2="2.8829" layer="37"/>
<rectangle x1="1.9685" y1="2.87451875" x2="2.00151875" y2="2.8829" layer="37"/>
<rectangle x1="2.7559" y1="2.87451875" x2="2.78891875" y2="2.8829" layer="37"/>
<rectangle x1="2.83971875" y1="2.87451875" x2="2.87528125" y2="2.8829" layer="37"/>
<rectangle x1="2.92608125" y1="2.87451875" x2="2.9591" y2="2.8829" layer="37"/>
<rectangle x1="3.65251875" y1="2.87451875" x2="3.68808125" y2="2.8829" layer="37"/>
<rectangle x1="3.7719" y1="2.87451875" x2="3.80491875" y2="2.8829" layer="37"/>
<rectangle x1="4.4069" y1="2.87451875" x2="4.4323" y2="2.8829" layer="37"/>
<rectangle x1="5.10031875" y1="2.87451875" x2="5.13588125" y2="2.8829" layer="37"/>
<rectangle x1="5.68451875" y1="2.87451875" x2="5.72008125" y2="2.8829" layer="37"/>
<rectangle x1="5.81151875" y1="2.87451875" x2="5.84708125" y2="2.8829" layer="37"/>
<rectangle x1="6.5405" y1="2.87451875" x2="6.57351875" y2="2.8829" layer="37"/>
<rectangle x1="6.6167" y1="2.87451875" x2="6.64971875" y2="2.8829" layer="37"/>
<rectangle x1="6.70051875" y1="2.87451875" x2="6.73608125" y2="2.8829" layer="37"/>
<rectangle x1="7.48791875" y1="2.87451875" x2="7.52348125" y2="2.8829" layer="37"/>
<rectangle x1="7.6581" y1="2.87451875" x2="7.69111875" y2="2.8829" layer="37"/>
<rectangle x1="8.2677" y1="2.87451875" x2="8.30071875" y2="2.8829" layer="37"/>
<rectangle x1="8.37691875" y1="2.87451875" x2="8.41248125" y2="2.8829" layer="37"/>
<rectangle x1="8.4963" y1="2.87451875" x2="8.52931875" y2="2.8829" layer="37"/>
<rectangle x1="8.61568125" y1="2.87451875" x2="8.6487" y2="2.8829" layer="37"/>
<rectangle x1="8.73251875" y1="2.87451875" x2="8.76808125" y2="2.8829" layer="37"/>
<rectangle x1="8.83411875" y1="2.87451875" x2="8.86968125" y2="2.8829" layer="37"/>
<rectangle x1="8.93571875" y1="2.87451875" x2="8.97128125" y2="2.8829" layer="37"/>
<rectangle x1="9.03731875" y1="2.87451875" x2="9.07288125" y2="2.8829" layer="37"/>
<rectangle x1="0.4191" y1="2.8829" x2="0.45211875" y2="2.89128125" layer="37"/>
<rectangle x1="0.51308125" y1="2.8829" x2="0.5461" y2="2.89128125" layer="37"/>
<rectangle x1="0.61468125" y1="2.8829" x2="0.6477" y2="2.89128125" layer="37"/>
<rectangle x1="0.7239" y1="2.8829" x2="0.75691875" y2="2.89128125" layer="37"/>
<rectangle x1="0.83311875" y1="2.8829" x2="0.86868125" y2="2.89128125" layer="37"/>
<rectangle x1="0.96011875" y1="2.8829" x2="0.99568125" y2="2.89128125" layer="37"/>
<rectangle x1="1.0795" y1="2.8829" x2="1.11251875" y2="2.89128125" layer="37"/>
<rectangle x1="1.1811" y1="2.8829" x2="1.21411875" y2="2.89128125" layer="37"/>
<rectangle x1="1.79831875" y1="2.8829" x2="1.83388125" y2="2.89128125" layer="37"/>
<rectangle x1="1.9685" y1="2.8829" x2="2.00151875" y2="2.89128125" layer="37"/>
<rectangle x1="2.7559" y1="2.8829" x2="2.78891875" y2="2.89128125" layer="37"/>
<rectangle x1="2.83971875" y1="2.8829" x2="2.87528125" y2="2.89128125" layer="37"/>
<rectangle x1="2.92608125" y1="2.8829" x2="2.9591" y2="2.89128125" layer="37"/>
<rectangle x1="3.65251875" y1="2.8829" x2="3.68808125" y2="2.89128125" layer="37"/>
<rectangle x1="3.7719" y1="2.8829" x2="3.80491875" y2="2.89128125" layer="37"/>
<rectangle x1="4.4069" y1="2.8829" x2="4.4323" y2="2.89128125" layer="37"/>
<rectangle x1="5.10031875" y1="2.8829" x2="5.13588125" y2="2.89128125" layer="37"/>
<rectangle x1="5.68451875" y1="2.8829" x2="5.72008125" y2="2.89128125" layer="37"/>
<rectangle x1="5.81151875" y1="2.8829" x2="5.84708125" y2="2.89128125" layer="37"/>
<rectangle x1="6.5405" y1="2.8829" x2="6.57351875" y2="2.89128125" layer="37"/>
<rectangle x1="6.6167" y1="2.8829" x2="6.64971875" y2="2.89128125" layer="37"/>
<rectangle x1="6.70051875" y1="2.8829" x2="6.73608125" y2="2.89128125" layer="37"/>
<rectangle x1="7.48791875" y1="2.8829" x2="7.52348125" y2="2.89128125" layer="37"/>
<rectangle x1="7.6581" y1="2.8829" x2="7.69111875" y2="2.89128125" layer="37"/>
<rectangle x1="8.2677" y1="2.8829" x2="8.30071875" y2="2.89128125" layer="37"/>
<rectangle x1="8.37691875" y1="2.8829" x2="8.41248125" y2="2.89128125" layer="37"/>
<rectangle x1="8.4963" y1="2.8829" x2="8.52931875" y2="2.89128125" layer="37"/>
<rectangle x1="8.61568125" y1="2.8829" x2="8.6487" y2="2.89128125" layer="37"/>
<rectangle x1="8.73251875" y1="2.8829" x2="8.76808125" y2="2.89128125" layer="37"/>
<rectangle x1="8.83411875" y1="2.8829" x2="8.86968125" y2="2.89128125" layer="37"/>
<rectangle x1="8.93571875" y1="2.8829" x2="8.97128125" y2="2.89128125" layer="37"/>
<rectangle x1="9.03731875" y1="2.8829" x2="9.07288125" y2="2.89128125" layer="37"/>
<rectangle x1="0.4191" y1="2.89128125" x2="0.45211875" y2="2.89991875" layer="37"/>
<rectangle x1="0.51308125" y1="2.89128125" x2="0.5461" y2="2.89991875" layer="37"/>
<rectangle x1="0.61468125" y1="2.89128125" x2="0.6477" y2="2.89991875" layer="37"/>
<rectangle x1="0.7239" y1="2.89128125" x2="0.75691875" y2="2.89991875" layer="37"/>
<rectangle x1="0.83311875" y1="2.89128125" x2="0.86868125" y2="2.89991875" layer="37"/>
<rectangle x1="0.96011875" y1="2.89128125" x2="0.99568125" y2="2.89991875" layer="37"/>
<rectangle x1="1.0795" y1="2.89128125" x2="1.11251875" y2="2.89991875" layer="37"/>
<rectangle x1="1.1811" y1="2.89128125" x2="1.21411875" y2="2.89991875" layer="37"/>
<rectangle x1="1.79831875" y1="2.89128125" x2="1.83388125" y2="2.89991875" layer="37"/>
<rectangle x1="1.9685" y1="2.89128125" x2="2.00151875" y2="2.89991875" layer="37"/>
<rectangle x1="2.7559" y1="2.89128125" x2="2.78891875" y2="2.89991875" layer="37"/>
<rectangle x1="2.83971875" y1="2.89128125" x2="2.87528125" y2="2.89991875" layer="37"/>
<rectangle x1="2.92608125" y1="2.89128125" x2="2.9591" y2="2.89991875" layer="37"/>
<rectangle x1="3.65251875" y1="2.89128125" x2="3.68808125" y2="2.89991875" layer="37"/>
<rectangle x1="3.7719" y1="2.89128125" x2="3.80491875" y2="2.89991875" layer="37"/>
<rectangle x1="4.4069" y1="2.89128125" x2="4.4323" y2="2.89991875" layer="37"/>
<rectangle x1="5.10031875" y1="2.89128125" x2="5.13588125" y2="2.89991875" layer="37"/>
<rectangle x1="5.68451875" y1="2.89128125" x2="5.72008125" y2="2.89991875" layer="37"/>
<rectangle x1="5.81151875" y1="2.89128125" x2="5.84708125" y2="2.89991875" layer="37"/>
<rectangle x1="6.5405" y1="2.89128125" x2="6.57351875" y2="2.89991875" layer="37"/>
<rectangle x1="6.6167" y1="2.89128125" x2="6.64971875" y2="2.89991875" layer="37"/>
<rectangle x1="6.70051875" y1="2.89128125" x2="6.73608125" y2="2.89991875" layer="37"/>
<rectangle x1="7.48791875" y1="2.89128125" x2="7.52348125" y2="2.89991875" layer="37"/>
<rectangle x1="7.6581" y1="2.89128125" x2="7.69111875" y2="2.89991875" layer="37"/>
<rectangle x1="8.2677" y1="2.89128125" x2="8.30071875" y2="2.89991875" layer="37"/>
<rectangle x1="8.37691875" y1="2.89128125" x2="8.41248125" y2="2.89991875" layer="37"/>
<rectangle x1="8.4963" y1="2.89128125" x2="8.52931875" y2="2.89991875" layer="37"/>
<rectangle x1="8.61568125" y1="2.89128125" x2="8.6487" y2="2.89991875" layer="37"/>
<rectangle x1="8.73251875" y1="2.89128125" x2="8.76808125" y2="2.89991875" layer="37"/>
<rectangle x1="8.83411875" y1="2.89128125" x2="8.86968125" y2="2.89991875" layer="37"/>
<rectangle x1="8.93571875" y1="2.89128125" x2="8.97128125" y2="2.89991875" layer="37"/>
<rectangle x1="9.03731875" y1="2.89128125" x2="9.07288125" y2="2.89991875" layer="37"/>
<rectangle x1="0.4191" y1="2.89991875" x2="0.45211875" y2="2.9083" layer="37"/>
<rectangle x1="0.51308125" y1="2.89991875" x2="0.5461" y2="2.9083" layer="37"/>
<rectangle x1="0.61468125" y1="2.89991875" x2="0.6477" y2="2.9083" layer="37"/>
<rectangle x1="0.7239" y1="2.89991875" x2="0.75691875" y2="2.9083" layer="37"/>
<rectangle x1="0.83311875" y1="2.89991875" x2="0.86868125" y2="2.9083" layer="37"/>
<rectangle x1="0.96011875" y1="2.89991875" x2="0.99568125" y2="2.9083" layer="37"/>
<rectangle x1="1.0795" y1="2.89991875" x2="1.11251875" y2="2.9083" layer="37"/>
<rectangle x1="1.1811" y1="2.89991875" x2="1.21411875" y2="2.9083" layer="37"/>
<rectangle x1="1.79831875" y1="2.89991875" x2="1.83388125" y2="2.9083" layer="37"/>
<rectangle x1="1.9685" y1="2.89991875" x2="2.00151875" y2="2.9083" layer="37"/>
<rectangle x1="2.7559" y1="2.89991875" x2="2.78891875" y2="2.9083" layer="37"/>
<rectangle x1="2.83971875" y1="2.89991875" x2="2.87528125" y2="2.9083" layer="37"/>
<rectangle x1="2.92608125" y1="2.89991875" x2="2.9591" y2="2.9083" layer="37"/>
<rectangle x1="3.65251875" y1="2.89991875" x2="3.68808125" y2="2.9083" layer="37"/>
<rectangle x1="3.7719" y1="2.89991875" x2="3.80491875" y2="2.9083" layer="37"/>
<rectangle x1="4.4069" y1="2.89991875" x2="4.4323" y2="2.9083" layer="37"/>
<rectangle x1="5.10031875" y1="2.89991875" x2="5.13588125" y2="2.9083" layer="37"/>
<rectangle x1="5.68451875" y1="2.89991875" x2="5.72008125" y2="2.9083" layer="37"/>
<rectangle x1="5.81151875" y1="2.89991875" x2="5.84708125" y2="2.9083" layer="37"/>
<rectangle x1="6.5405" y1="2.89991875" x2="6.57351875" y2="2.9083" layer="37"/>
<rectangle x1="6.6167" y1="2.89991875" x2="6.64971875" y2="2.9083" layer="37"/>
<rectangle x1="6.70051875" y1="2.89991875" x2="6.73608125" y2="2.9083" layer="37"/>
<rectangle x1="7.48791875" y1="2.89991875" x2="7.52348125" y2="2.9083" layer="37"/>
<rectangle x1="7.6581" y1="2.89991875" x2="7.69111875" y2="2.9083" layer="37"/>
<rectangle x1="8.2677" y1="2.89991875" x2="8.30071875" y2="2.9083" layer="37"/>
<rectangle x1="8.37691875" y1="2.89991875" x2="8.41248125" y2="2.9083" layer="37"/>
<rectangle x1="8.4963" y1="2.89991875" x2="8.52931875" y2="2.9083" layer="37"/>
<rectangle x1="8.61568125" y1="2.89991875" x2="8.6487" y2="2.9083" layer="37"/>
<rectangle x1="8.73251875" y1="2.89991875" x2="8.76808125" y2="2.9083" layer="37"/>
<rectangle x1="8.83411875" y1="2.89991875" x2="8.86968125" y2="2.9083" layer="37"/>
<rectangle x1="8.93571875" y1="2.89991875" x2="8.97128125" y2="2.9083" layer="37"/>
<rectangle x1="9.03731875" y1="2.89991875" x2="9.07288125" y2="2.9083" layer="37"/>
<rectangle x1="0.4191" y1="2.9083" x2="0.45211875" y2="2.91668125" layer="37"/>
<rectangle x1="0.51308125" y1="2.9083" x2="0.5461" y2="2.91668125" layer="37"/>
<rectangle x1="0.61468125" y1="2.9083" x2="0.6477" y2="2.91668125" layer="37"/>
<rectangle x1="0.7239" y1="2.9083" x2="0.75691875" y2="2.91668125" layer="37"/>
<rectangle x1="0.83311875" y1="2.9083" x2="0.86868125" y2="2.91668125" layer="37"/>
<rectangle x1="0.96011875" y1="2.9083" x2="0.99568125" y2="2.91668125" layer="37"/>
<rectangle x1="1.0795" y1="2.9083" x2="1.11251875" y2="2.91668125" layer="37"/>
<rectangle x1="1.1811" y1="2.9083" x2="1.21411875" y2="2.91668125" layer="37"/>
<rectangle x1="1.79831875" y1="2.9083" x2="1.83388125" y2="2.91668125" layer="37"/>
<rectangle x1="1.9685" y1="2.9083" x2="2.00151875" y2="2.91668125" layer="37"/>
<rectangle x1="2.7559" y1="2.9083" x2="2.78891875" y2="2.91668125" layer="37"/>
<rectangle x1="2.83971875" y1="2.9083" x2="2.87528125" y2="2.91668125" layer="37"/>
<rectangle x1="2.92608125" y1="2.9083" x2="2.9591" y2="2.91668125" layer="37"/>
<rectangle x1="3.65251875" y1="2.9083" x2="3.68808125" y2="2.91668125" layer="37"/>
<rectangle x1="3.7719" y1="2.9083" x2="3.80491875" y2="2.91668125" layer="37"/>
<rectangle x1="4.4069" y1="2.9083" x2="4.4323" y2="2.91668125" layer="37"/>
<rectangle x1="5.10031875" y1="2.9083" x2="5.13588125" y2="2.91668125" layer="37"/>
<rectangle x1="5.68451875" y1="2.9083" x2="5.72008125" y2="2.91668125" layer="37"/>
<rectangle x1="5.81151875" y1="2.9083" x2="5.84708125" y2="2.91668125" layer="37"/>
<rectangle x1="6.5405" y1="2.9083" x2="6.57351875" y2="2.91668125" layer="37"/>
<rectangle x1="6.6167" y1="2.9083" x2="6.64971875" y2="2.91668125" layer="37"/>
<rectangle x1="6.70051875" y1="2.9083" x2="6.73608125" y2="2.91668125" layer="37"/>
<rectangle x1="7.48791875" y1="2.9083" x2="7.52348125" y2="2.91668125" layer="37"/>
<rectangle x1="7.6581" y1="2.9083" x2="7.69111875" y2="2.91668125" layer="37"/>
<rectangle x1="8.2677" y1="2.9083" x2="8.30071875" y2="2.91668125" layer="37"/>
<rectangle x1="8.37691875" y1="2.9083" x2="8.41248125" y2="2.91668125" layer="37"/>
<rectangle x1="8.4963" y1="2.9083" x2="8.52931875" y2="2.91668125" layer="37"/>
<rectangle x1="8.61568125" y1="2.9083" x2="8.6487" y2="2.91668125" layer="37"/>
<rectangle x1="8.73251875" y1="2.9083" x2="8.76808125" y2="2.91668125" layer="37"/>
<rectangle x1="8.83411875" y1="2.9083" x2="8.86968125" y2="2.91668125" layer="37"/>
<rectangle x1="8.93571875" y1="2.9083" x2="8.97128125" y2="2.91668125" layer="37"/>
<rectangle x1="9.03731875" y1="2.9083" x2="9.07288125" y2="2.91668125" layer="37"/>
<rectangle x1="0.4191" y1="2.91668125" x2="0.45211875" y2="2.92531875" layer="37"/>
<rectangle x1="0.51308125" y1="2.91668125" x2="0.5461" y2="2.92531875" layer="37"/>
<rectangle x1="0.61468125" y1="2.91668125" x2="0.6477" y2="2.92531875" layer="37"/>
<rectangle x1="0.7239" y1="2.91668125" x2="0.75691875" y2="2.92531875" layer="37"/>
<rectangle x1="0.83311875" y1="2.91668125" x2="0.86868125" y2="2.92531875" layer="37"/>
<rectangle x1="0.96011875" y1="2.91668125" x2="0.99568125" y2="2.92531875" layer="37"/>
<rectangle x1="1.0795" y1="2.91668125" x2="1.11251875" y2="2.92531875" layer="37"/>
<rectangle x1="1.1811" y1="2.91668125" x2="1.21411875" y2="2.92531875" layer="37"/>
<rectangle x1="1.79831875" y1="2.91668125" x2="1.83388125" y2="2.92531875" layer="37"/>
<rectangle x1="1.9685" y1="2.91668125" x2="2.00151875" y2="2.92531875" layer="37"/>
<rectangle x1="2.7559" y1="2.91668125" x2="2.78891875" y2="2.92531875" layer="37"/>
<rectangle x1="2.83971875" y1="2.91668125" x2="2.87528125" y2="2.92531875" layer="37"/>
<rectangle x1="2.92608125" y1="2.91668125" x2="2.9591" y2="2.92531875" layer="37"/>
<rectangle x1="3.65251875" y1="2.91668125" x2="3.68808125" y2="2.92531875" layer="37"/>
<rectangle x1="3.7719" y1="2.91668125" x2="3.80491875" y2="2.92531875" layer="37"/>
<rectangle x1="4.4069" y1="2.91668125" x2="4.4323" y2="2.92531875" layer="37"/>
<rectangle x1="5.10031875" y1="2.91668125" x2="5.13588125" y2="2.92531875" layer="37"/>
<rectangle x1="5.68451875" y1="2.91668125" x2="5.72008125" y2="2.92531875" layer="37"/>
<rectangle x1="5.81151875" y1="2.91668125" x2="5.84708125" y2="2.92531875" layer="37"/>
<rectangle x1="6.5405" y1="2.91668125" x2="6.57351875" y2="2.92531875" layer="37"/>
<rectangle x1="6.6167" y1="2.91668125" x2="6.64971875" y2="2.92531875" layer="37"/>
<rectangle x1="6.70051875" y1="2.91668125" x2="6.73608125" y2="2.92531875" layer="37"/>
<rectangle x1="7.48791875" y1="2.91668125" x2="7.52348125" y2="2.92531875" layer="37"/>
<rectangle x1="7.6581" y1="2.91668125" x2="7.69111875" y2="2.92531875" layer="37"/>
<rectangle x1="8.2677" y1="2.91668125" x2="8.30071875" y2="2.92531875" layer="37"/>
<rectangle x1="8.37691875" y1="2.91668125" x2="8.41248125" y2="2.92531875" layer="37"/>
<rectangle x1="8.4963" y1="2.91668125" x2="8.52931875" y2="2.92531875" layer="37"/>
<rectangle x1="8.61568125" y1="2.91668125" x2="8.6487" y2="2.92531875" layer="37"/>
<rectangle x1="8.73251875" y1="2.91668125" x2="8.76808125" y2="2.92531875" layer="37"/>
<rectangle x1="8.83411875" y1="2.91668125" x2="8.86968125" y2="2.92531875" layer="37"/>
<rectangle x1="8.93571875" y1="2.91668125" x2="8.97128125" y2="2.92531875" layer="37"/>
<rectangle x1="9.03731875" y1="2.91668125" x2="9.07288125" y2="2.92531875" layer="37"/>
<rectangle x1="0.4191" y1="2.92531875" x2="0.45211875" y2="2.9337" layer="37"/>
<rectangle x1="0.51308125" y1="2.92531875" x2="0.5461" y2="2.9337" layer="37"/>
<rectangle x1="0.61468125" y1="2.92531875" x2="0.6477" y2="2.9337" layer="37"/>
<rectangle x1="0.7239" y1="2.92531875" x2="0.75691875" y2="2.9337" layer="37"/>
<rectangle x1="0.83311875" y1="2.92531875" x2="0.86868125" y2="2.9337" layer="37"/>
<rectangle x1="0.96011875" y1="2.92531875" x2="0.99568125" y2="2.9337" layer="37"/>
<rectangle x1="1.0795" y1="2.92531875" x2="1.11251875" y2="2.9337" layer="37"/>
<rectangle x1="1.1811" y1="2.92531875" x2="1.21411875" y2="2.9337" layer="37"/>
<rectangle x1="1.79831875" y1="2.92531875" x2="1.83388125" y2="2.9337" layer="37"/>
<rectangle x1="1.9685" y1="2.92531875" x2="2.00151875" y2="2.9337" layer="37"/>
<rectangle x1="2.7559" y1="2.92531875" x2="2.78891875" y2="2.9337" layer="37"/>
<rectangle x1="2.83971875" y1="2.92531875" x2="2.87528125" y2="2.9337" layer="37"/>
<rectangle x1="2.92608125" y1="2.92531875" x2="2.9591" y2="2.9337" layer="37"/>
<rectangle x1="3.65251875" y1="2.92531875" x2="3.68808125" y2="2.9337" layer="37"/>
<rectangle x1="3.7719" y1="2.92531875" x2="3.80491875" y2="2.9337" layer="37"/>
<rectangle x1="4.4069" y1="2.92531875" x2="4.4323" y2="2.9337" layer="37"/>
<rectangle x1="5.10031875" y1="2.92531875" x2="5.13588125" y2="2.9337" layer="37"/>
<rectangle x1="5.68451875" y1="2.92531875" x2="5.72008125" y2="2.9337" layer="37"/>
<rectangle x1="5.81151875" y1="2.92531875" x2="5.84708125" y2="2.9337" layer="37"/>
<rectangle x1="6.5405" y1="2.92531875" x2="6.57351875" y2="2.9337" layer="37"/>
<rectangle x1="6.6167" y1="2.92531875" x2="6.64971875" y2="2.9337" layer="37"/>
<rectangle x1="6.70051875" y1="2.92531875" x2="6.73608125" y2="2.9337" layer="37"/>
<rectangle x1="7.48791875" y1="2.92531875" x2="7.52348125" y2="2.9337" layer="37"/>
<rectangle x1="7.6581" y1="2.92531875" x2="7.69111875" y2="2.9337" layer="37"/>
<rectangle x1="8.2677" y1="2.92531875" x2="8.30071875" y2="2.9337" layer="37"/>
<rectangle x1="8.37691875" y1="2.92531875" x2="8.41248125" y2="2.9337" layer="37"/>
<rectangle x1="8.4963" y1="2.92531875" x2="8.52931875" y2="2.9337" layer="37"/>
<rectangle x1="8.61568125" y1="2.92531875" x2="8.6487" y2="2.9337" layer="37"/>
<rectangle x1="8.73251875" y1="2.92531875" x2="8.76808125" y2="2.9337" layer="37"/>
<rectangle x1="8.83411875" y1="2.92531875" x2="8.86968125" y2="2.9337" layer="37"/>
<rectangle x1="8.93571875" y1="2.92531875" x2="8.97128125" y2="2.9337" layer="37"/>
<rectangle x1="9.03731875" y1="2.92531875" x2="9.07288125" y2="2.9337" layer="37"/>
<rectangle x1="0.4191" y1="2.9337" x2="0.45211875" y2="2.94208125" layer="37"/>
<rectangle x1="0.51308125" y1="2.9337" x2="0.5461" y2="2.94208125" layer="37"/>
<rectangle x1="0.61468125" y1="2.9337" x2="0.6477" y2="2.94208125" layer="37"/>
<rectangle x1="0.7239" y1="2.9337" x2="0.75691875" y2="2.94208125" layer="37"/>
<rectangle x1="0.83311875" y1="2.9337" x2="0.86868125" y2="2.94208125" layer="37"/>
<rectangle x1="0.96011875" y1="2.9337" x2="0.99568125" y2="2.94208125" layer="37"/>
<rectangle x1="1.0795" y1="2.9337" x2="1.11251875" y2="2.94208125" layer="37"/>
<rectangle x1="1.1811" y1="2.9337" x2="1.21411875" y2="2.94208125" layer="37"/>
<rectangle x1="1.79831875" y1="2.9337" x2="1.83388125" y2="2.94208125" layer="37"/>
<rectangle x1="1.9685" y1="2.9337" x2="2.00151875" y2="2.94208125" layer="37"/>
<rectangle x1="2.7559" y1="2.9337" x2="2.78891875" y2="2.94208125" layer="37"/>
<rectangle x1="2.83971875" y1="2.9337" x2="2.87528125" y2="2.94208125" layer="37"/>
<rectangle x1="2.92608125" y1="2.9337" x2="2.9591" y2="2.94208125" layer="37"/>
<rectangle x1="3.65251875" y1="2.9337" x2="3.68808125" y2="2.94208125" layer="37"/>
<rectangle x1="3.7719" y1="2.9337" x2="3.80491875" y2="2.94208125" layer="37"/>
<rectangle x1="4.4069" y1="2.9337" x2="4.4323" y2="2.94208125" layer="37"/>
<rectangle x1="5.10031875" y1="2.9337" x2="5.13588125" y2="2.94208125" layer="37"/>
<rectangle x1="5.68451875" y1="2.9337" x2="5.72008125" y2="2.94208125" layer="37"/>
<rectangle x1="5.81151875" y1="2.9337" x2="5.84708125" y2="2.94208125" layer="37"/>
<rectangle x1="6.5405" y1="2.9337" x2="6.57351875" y2="2.94208125" layer="37"/>
<rectangle x1="6.6167" y1="2.9337" x2="6.64971875" y2="2.94208125" layer="37"/>
<rectangle x1="6.70051875" y1="2.9337" x2="6.73608125" y2="2.94208125" layer="37"/>
<rectangle x1="7.48791875" y1="2.9337" x2="7.52348125" y2="2.94208125" layer="37"/>
<rectangle x1="7.6581" y1="2.9337" x2="7.69111875" y2="2.94208125" layer="37"/>
<rectangle x1="8.2677" y1="2.9337" x2="8.30071875" y2="2.94208125" layer="37"/>
<rectangle x1="8.37691875" y1="2.9337" x2="8.41248125" y2="2.94208125" layer="37"/>
<rectangle x1="8.4963" y1="2.9337" x2="8.52931875" y2="2.94208125" layer="37"/>
<rectangle x1="8.61568125" y1="2.9337" x2="8.6487" y2="2.94208125" layer="37"/>
<rectangle x1="8.73251875" y1="2.9337" x2="8.76808125" y2="2.94208125" layer="37"/>
<rectangle x1="8.83411875" y1="2.9337" x2="8.86968125" y2="2.94208125" layer="37"/>
<rectangle x1="8.93571875" y1="2.9337" x2="8.97128125" y2="2.94208125" layer="37"/>
<rectangle x1="9.03731875" y1="2.9337" x2="9.07288125" y2="2.94208125" layer="37"/>
<rectangle x1="0.3937" y1="2.94208125" x2="1.24968125" y2="2.95071875" layer="37"/>
<rectangle x1="1.74751875" y1="2.94208125" x2="2.06248125" y2="2.95071875" layer="37"/>
<rectangle x1="2.73811875" y1="2.94208125" x2="2.97688125" y2="2.95071875" layer="37"/>
<rectangle x1="3.60171875" y1="2.94208125" x2="3.85571875" y2="2.95071875" layer="37"/>
<rectangle x1="4.4069" y1="2.94208125" x2="4.4323" y2="2.95071875" layer="37"/>
<rectangle x1="5.10031875" y1="2.94208125" x2="5.13588125" y2="2.95071875" layer="37"/>
<rectangle x1="5.64388125" y1="2.94208125" x2="5.89788125" y2="2.95071875" layer="37"/>
<rectangle x1="6.5151" y1="2.94208125" x2="6.76148125" y2="2.95071875" layer="37"/>
<rectangle x1="7.4295" y1="2.94208125" x2="7.74191875" y2="2.95071875" layer="37"/>
<rectangle x1="8.23468125" y1="2.94208125" x2="9.09828125" y2="2.95071875" layer="37"/>
<rectangle x1="0.3937" y1="2.95071875" x2="1.24968125" y2="2.9591" layer="37"/>
<rectangle x1="1.74751875" y1="2.95071875" x2="2.06248125" y2="2.9591" layer="37"/>
<rectangle x1="2.73811875" y1="2.95071875" x2="2.97688125" y2="2.9591" layer="37"/>
<rectangle x1="3.60171875" y1="2.95071875" x2="3.65251875" y2="2.9591" layer="37"/>
<rectangle x1="3.68808125" y1="2.95071875" x2="3.77951875" y2="2.9591" layer="37"/>
<rectangle x1="3.8227" y1="2.95071875" x2="3.85571875" y2="2.9591" layer="37"/>
<rectangle x1="4.4069" y1="2.95071875" x2="4.4323" y2="2.9591" layer="37"/>
<rectangle x1="5.10031875" y1="2.95071875" x2="5.13588125" y2="2.9591" layer="37"/>
<rectangle x1="5.64388125" y1="2.95071875" x2="5.69468125" y2="2.9591" layer="37"/>
<rectangle x1="5.72008125" y1="2.95071875" x2="5.81151875" y2="2.9591" layer="37"/>
<rectangle x1="5.86231875" y1="2.95071875" x2="5.89788125" y2="2.9591" layer="37"/>
<rectangle x1="6.5151" y1="2.95071875" x2="6.76148125" y2="2.9591" layer="37"/>
<rectangle x1="7.4295" y1="2.95071875" x2="7.74191875" y2="2.9591" layer="37"/>
<rectangle x1="8.23468125" y1="2.95071875" x2="9.09828125" y2="2.9591" layer="37"/>
<rectangle x1="0.3937" y1="2.9591" x2="1.24968125" y2="2.96748125" layer="37"/>
<rectangle x1="1.74751875" y1="2.9591" x2="2.06248125" y2="2.96748125" layer="37"/>
<rectangle x1="2.73811875" y1="2.9591" x2="2.97688125" y2="2.96748125" layer="37"/>
<rectangle x1="3.60171875" y1="2.9591" x2="3.65251875" y2="2.96748125" layer="37"/>
<rectangle x1="3.68808125" y1="2.9591" x2="3.7719" y2="2.96748125" layer="37"/>
<rectangle x1="3.8227" y1="2.9591" x2="3.85571875" y2="2.96748125" layer="37"/>
<rectangle x1="4.4069" y1="2.9591" x2="4.4323" y2="2.96748125" layer="37"/>
<rectangle x1="5.10031875" y1="2.9591" x2="5.13588125" y2="2.96748125" layer="37"/>
<rectangle x1="5.64388125" y1="2.9591" x2="5.69468125" y2="2.96748125" layer="37"/>
<rectangle x1="5.72008125" y1="2.9591" x2="5.81151875" y2="2.96748125" layer="37"/>
<rectangle x1="5.86231875" y1="2.9591" x2="5.89788125" y2="2.96748125" layer="37"/>
<rectangle x1="6.5151" y1="2.9591" x2="6.76148125" y2="2.96748125" layer="37"/>
<rectangle x1="7.4295" y1="2.9591" x2="7.74191875" y2="2.96748125" layer="37"/>
<rectangle x1="8.23468125" y1="2.9591" x2="9.09828125" y2="2.96748125" layer="37"/>
<rectangle x1="0.3937" y1="2.96748125" x2="1.24968125" y2="2.97611875" layer="37"/>
<rectangle x1="1.74751875" y1="2.96748125" x2="2.06248125" y2="2.97611875" layer="37"/>
<rectangle x1="2.73811875" y1="2.96748125" x2="2.97688125" y2="2.97611875" layer="37"/>
<rectangle x1="3.60171875" y1="2.96748125" x2="3.65251875" y2="2.97611875" layer="37"/>
<rectangle x1="3.68808125" y1="2.96748125" x2="3.76428125" y2="2.97611875" layer="37"/>
<rectangle x1="3.8227" y1="2.96748125" x2="3.85571875" y2="2.97611875" layer="37"/>
<rectangle x1="4.4069" y1="2.96748125" x2="4.4323" y2="2.97611875" layer="37"/>
<rectangle x1="5.10031875" y1="2.96748125" x2="5.13588125" y2="2.97611875" layer="37"/>
<rectangle x1="5.64388125" y1="2.96748125" x2="5.69468125" y2="2.97611875" layer="37"/>
<rectangle x1="5.72008125" y1="2.96748125" x2="5.8039" y2="2.97611875" layer="37"/>
<rectangle x1="5.86231875" y1="2.96748125" x2="5.89788125" y2="2.97611875" layer="37"/>
<rectangle x1="6.5151" y1="2.96748125" x2="6.76148125" y2="2.97611875" layer="37"/>
<rectangle x1="7.4295" y1="2.96748125" x2="7.74191875" y2="2.97611875" layer="37"/>
<rectangle x1="8.23468125" y1="2.96748125" x2="9.09828125" y2="2.97611875" layer="37"/>
<rectangle x1="0.3937" y1="2.97611875" x2="1.24968125" y2="2.9845" layer="37"/>
<rectangle x1="1.74751875" y1="2.97611875" x2="2.06248125" y2="2.9845" layer="37"/>
<rectangle x1="2.73811875" y1="2.97611875" x2="2.97688125" y2="2.9845" layer="37"/>
<rectangle x1="3.60171875" y1="2.97611875" x2="3.65251875" y2="2.9845" layer="37"/>
<rectangle x1="3.68808125" y1="2.97611875" x2="3.75411875" y2="2.9845" layer="37"/>
<rectangle x1="3.8227" y1="2.97611875" x2="3.85571875" y2="2.9845" layer="37"/>
<rectangle x1="4.4069" y1="2.97611875" x2="4.4323" y2="2.9845" layer="37"/>
<rectangle x1="4.99871875" y1="2.97611875" x2="5.00888125" y2="2.9845" layer="37"/>
<rectangle x1="5.10031875" y1="2.97611875" x2="5.13588125" y2="2.9845" layer="37"/>
<rectangle x1="5.64388125" y1="2.97611875" x2="5.69468125" y2="2.9845" layer="37"/>
<rectangle x1="5.72008125" y1="2.97611875" x2="5.78611875" y2="2.9845" layer="37"/>
<rectangle x1="5.86231875" y1="2.97611875" x2="5.89788125" y2="2.9845" layer="37"/>
<rectangle x1="6.5151" y1="2.97611875" x2="6.76148125" y2="2.9845" layer="37"/>
<rectangle x1="7.4295" y1="2.97611875" x2="7.74191875" y2="2.9845" layer="37"/>
<rectangle x1="8.23468125" y1="2.97611875" x2="9.09828125" y2="2.9845" layer="37"/>
<rectangle x1="0.3937" y1="2.9845" x2="1.24968125" y2="2.99288125" layer="37"/>
<rectangle x1="1.74751875" y1="2.9845" x2="2.06248125" y2="2.99288125" layer="37"/>
<rectangle x1="2.73811875" y1="2.9845" x2="2.97688125" y2="2.99288125" layer="37"/>
<rectangle x1="3.60171875" y1="2.9845" x2="3.65251875" y2="2.99288125" layer="37"/>
<rectangle x1="3.68808125" y1="2.9845" x2="3.7465" y2="2.99288125" layer="37"/>
<rectangle x1="3.8227" y1="2.9845" x2="3.85571875" y2="2.99288125" layer="37"/>
<rectangle x1="4.4069" y1="2.9845" x2="4.4323" y2="2.99288125" layer="37"/>
<rectangle x1="5.10031875" y1="2.9845" x2="5.13588125" y2="2.99288125" layer="37"/>
<rectangle x1="5.64388125" y1="2.9845" x2="5.69468125" y2="2.99288125" layer="37"/>
<rectangle x1="5.72008125" y1="2.9845" x2="5.78611875" y2="2.99288125" layer="37"/>
<rectangle x1="5.86231875" y1="2.9845" x2="5.89788125" y2="2.99288125" layer="37"/>
<rectangle x1="6.5151" y1="2.9845" x2="6.76148125" y2="2.99288125" layer="37"/>
<rectangle x1="7.4295" y1="2.9845" x2="7.74191875" y2="2.99288125" layer="37"/>
<rectangle x1="8.23468125" y1="2.9845" x2="9.09828125" y2="2.99288125" layer="37"/>
<rectangle x1="0.3937" y1="2.99288125" x2="1.24968125" y2="3.00151875" layer="37"/>
<rectangle x1="1.74751875" y1="2.99288125" x2="2.06248125" y2="3.00151875" layer="37"/>
<rectangle x1="2.73811875" y1="2.99288125" x2="2.97688125" y2="3.00151875" layer="37"/>
<rectangle x1="3.60171875" y1="2.99288125" x2="3.65251875" y2="3.00151875" layer="37"/>
<rectangle x1="3.68808125" y1="2.99288125" x2="3.7465" y2="3.00151875" layer="37"/>
<rectangle x1="3.8227" y1="2.99288125" x2="3.85571875" y2="3.00151875" layer="37"/>
<rectangle x1="4.4069" y1="2.99288125" x2="4.4323" y2="3.00151875" layer="37"/>
<rectangle x1="5.10031875" y1="2.99288125" x2="5.13588125" y2="3.00151875" layer="37"/>
<rectangle x1="5.64388125" y1="2.99288125" x2="5.69468125" y2="3.00151875" layer="37"/>
<rectangle x1="5.72008125" y1="2.99288125" x2="5.78611875" y2="3.00151875" layer="37"/>
<rectangle x1="5.86231875" y1="2.99288125" x2="5.89788125" y2="3.00151875" layer="37"/>
<rectangle x1="6.5151" y1="2.99288125" x2="6.76148125" y2="3.00151875" layer="37"/>
<rectangle x1="7.4295" y1="2.99288125" x2="7.74191875" y2="3.00151875" layer="37"/>
<rectangle x1="8.23468125" y1="2.99288125" x2="9.09828125" y2="3.00151875" layer="37"/>
<rectangle x1="0.3937" y1="3.00151875" x2="1.24968125" y2="3.0099" layer="37"/>
<rectangle x1="1.74751875" y1="3.00151875" x2="2.06248125" y2="3.0099" layer="37"/>
<rectangle x1="2.73811875" y1="3.00151875" x2="2.97688125" y2="3.0099" layer="37"/>
<rectangle x1="3.60171875" y1="3.00151875" x2="3.65251875" y2="3.0099" layer="37"/>
<rectangle x1="3.68808125" y1="3.00151875" x2="3.7465" y2="3.0099" layer="37"/>
<rectangle x1="3.8227" y1="3.00151875" x2="3.85571875" y2="3.0099" layer="37"/>
<rectangle x1="4.4069" y1="3.00151875" x2="4.4323" y2="3.0099" layer="37"/>
<rectangle x1="5.0165" y1="3.00151875" x2="5.02411875" y2="3.0099" layer="37"/>
<rectangle x1="5.10031875" y1="3.00151875" x2="5.13588125" y2="3.0099" layer="37"/>
<rectangle x1="5.64388125" y1="3.00151875" x2="5.69468125" y2="3.0099" layer="37"/>
<rectangle x1="5.72008125" y1="3.00151875" x2="5.78611875" y2="3.0099" layer="37"/>
<rectangle x1="5.86231875" y1="3.00151875" x2="5.89788125" y2="3.0099" layer="37"/>
<rectangle x1="6.5151" y1="3.00151875" x2="6.76148125" y2="3.0099" layer="37"/>
<rectangle x1="7.4295" y1="3.00151875" x2="7.74191875" y2="3.0099" layer="37"/>
<rectangle x1="8.23468125" y1="3.00151875" x2="9.09828125" y2="3.0099" layer="37"/>
<rectangle x1="0.3937" y1="3.0099" x2="1.24968125" y2="3.01828125" layer="37"/>
<rectangle x1="1.74751875" y1="3.0099" x2="2.06248125" y2="3.01828125" layer="37"/>
<rectangle x1="2.73811875" y1="3.0099" x2="2.97688125" y2="3.01828125" layer="37"/>
<rectangle x1="3.60171875" y1="3.0099" x2="3.65251875" y2="3.01828125" layer="37"/>
<rectangle x1="3.68808125" y1="3.0099" x2="3.7465" y2="3.01828125" layer="37"/>
<rectangle x1="3.8227" y1="3.0099" x2="3.85571875" y2="3.01828125" layer="37"/>
<rectangle x1="4.4069" y1="3.0099" x2="4.4323" y2="3.01828125" layer="37"/>
<rectangle x1="4.4831" y1="3.0099" x2="4.54151875" y2="3.01828125" layer="37"/>
<rectangle x1="4.5593" y1="3.0099" x2="4.6609" y2="3.01828125" layer="37"/>
<rectangle x1="4.67868125" y1="3.0099" x2="4.70408125" y2="3.01828125" layer="37"/>
<rectangle x1="4.72948125" y1="3.0099" x2="4.99871875" y2="3.01828125" layer="37"/>
<rectangle x1="5.0165" y1="3.0099" x2="5.05968125" y2="3.01828125" layer="37"/>
<rectangle x1="5.10031875" y1="3.0099" x2="5.13588125" y2="3.01828125" layer="37"/>
<rectangle x1="5.64388125" y1="3.0099" x2="5.69468125" y2="3.01828125" layer="37"/>
<rectangle x1="5.72008125" y1="3.0099" x2="5.78611875" y2="3.01828125" layer="37"/>
<rectangle x1="5.86231875" y1="3.0099" x2="5.89788125" y2="3.01828125" layer="37"/>
<rectangle x1="6.5151" y1="3.0099" x2="6.76148125" y2="3.01828125" layer="37"/>
<rectangle x1="7.4295" y1="3.0099" x2="7.74191875" y2="3.01828125" layer="37"/>
<rectangle x1="8.23468125" y1="3.0099" x2="9.09828125" y2="3.01828125" layer="37"/>
<rectangle x1="0.3937" y1="3.01828125" x2="1.24968125" y2="3.02691875" layer="37"/>
<rectangle x1="1.74751875" y1="3.01828125" x2="2.06248125" y2="3.02691875" layer="37"/>
<rectangle x1="2.73811875" y1="3.01828125" x2="2.97688125" y2="3.02691875" layer="37"/>
<rectangle x1="3.60171875" y1="3.01828125" x2="3.65251875" y2="3.02691875" layer="37"/>
<rectangle x1="3.68808125" y1="3.01828125" x2="3.7465" y2="3.02691875" layer="37"/>
<rectangle x1="3.8227" y1="3.01828125" x2="3.85571875" y2="3.02691875" layer="37"/>
<rectangle x1="4.4069" y1="3.01828125" x2="4.4323" y2="3.02691875" layer="37"/>
<rectangle x1="4.4831" y1="3.01828125" x2="4.54151875" y2="3.02691875" layer="37"/>
<rectangle x1="4.5593" y1="3.01828125" x2="4.6609" y2="3.02691875" layer="37"/>
<rectangle x1="4.67868125" y1="3.01828125" x2="4.70408125" y2="3.02691875" layer="37"/>
<rectangle x1="4.72948125" y1="3.01828125" x2="4.99871875" y2="3.02691875" layer="37"/>
<rectangle x1="5.0165" y1="3.01828125" x2="5.05968125" y2="3.02691875" layer="37"/>
<rectangle x1="5.10031875" y1="3.01828125" x2="5.13588125" y2="3.02691875" layer="37"/>
<rectangle x1="5.64388125" y1="3.01828125" x2="5.69468125" y2="3.02691875" layer="37"/>
<rectangle x1="5.72008125" y1="3.01828125" x2="5.78611875" y2="3.02691875" layer="37"/>
<rectangle x1="5.86231875" y1="3.01828125" x2="5.89788125" y2="3.02691875" layer="37"/>
<rectangle x1="6.5151" y1="3.01828125" x2="6.76148125" y2="3.02691875" layer="37"/>
<rectangle x1="7.4295" y1="3.01828125" x2="7.74191875" y2="3.02691875" layer="37"/>
<rectangle x1="8.23468125" y1="3.01828125" x2="9.09828125" y2="3.02691875" layer="37"/>
<rectangle x1="0.3937" y1="3.02691875" x2="1.24968125" y2="3.0353" layer="37"/>
<rectangle x1="1.74751875" y1="3.02691875" x2="2.06248125" y2="3.0353" layer="37"/>
<rectangle x1="2.73811875" y1="3.02691875" x2="2.97688125" y2="3.0353" layer="37"/>
<rectangle x1="3.60171875" y1="3.02691875" x2="3.65251875" y2="3.0353" layer="37"/>
<rectangle x1="3.68808125" y1="3.02691875" x2="3.7465" y2="3.0353" layer="37"/>
<rectangle x1="3.8227" y1="3.02691875" x2="3.85571875" y2="3.0353" layer="37"/>
<rectangle x1="4.4069" y1="3.02691875" x2="4.4323" y2="3.0353" layer="37"/>
<rectangle x1="4.4831" y1="3.02691875" x2="4.54151875" y2="3.0353" layer="37"/>
<rectangle x1="4.5593" y1="3.02691875" x2="4.6609" y2="3.0353" layer="37"/>
<rectangle x1="4.67868125" y1="3.02691875" x2="4.70408125" y2="3.0353" layer="37"/>
<rectangle x1="4.72948125" y1="3.02691875" x2="4.99871875" y2="3.0353" layer="37"/>
<rectangle x1="5.0165" y1="3.02691875" x2="5.05968125" y2="3.0353" layer="37"/>
<rectangle x1="5.10031875" y1="3.02691875" x2="5.13588125" y2="3.0353" layer="37"/>
<rectangle x1="5.64388125" y1="3.02691875" x2="5.69468125" y2="3.0353" layer="37"/>
<rectangle x1="5.72008125" y1="3.02691875" x2="5.78611875" y2="3.0353" layer="37"/>
<rectangle x1="5.86231875" y1="3.02691875" x2="5.89788125" y2="3.0353" layer="37"/>
<rectangle x1="6.5151" y1="3.02691875" x2="6.76148125" y2="3.0353" layer="37"/>
<rectangle x1="7.4295" y1="3.02691875" x2="7.74191875" y2="3.0353" layer="37"/>
<rectangle x1="8.23468125" y1="3.02691875" x2="9.09828125" y2="3.0353" layer="37"/>
<rectangle x1="0.3937" y1="3.0353" x2="1.24968125" y2="3.04368125" layer="37"/>
<rectangle x1="1.74751875" y1="3.0353" x2="2.06248125" y2="3.04368125" layer="37"/>
<rectangle x1="2.73811875" y1="3.0353" x2="2.97688125" y2="3.04368125" layer="37"/>
<rectangle x1="3.60171875" y1="3.0353" x2="3.65251875" y2="3.04368125" layer="37"/>
<rectangle x1="3.68808125" y1="3.0353" x2="3.7465" y2="3.04368125" layer="37"/>
<rectangle x1="3.8227" y1="3.0353" x2="3.85571875" y2="3.04368125" layer="37"/>
<rectangle x1="4.4069" y1="3.0353" x2="4.54151875" y2="3.04368125" layer="37"/>
<rectangle x1="4.5593" y1="3.0353" x2="4.6609" y2="3.04368125" layer="37"/>
<rectangle x1="4.67868125" y1="3.0353" x2="4.70408125" y2="3.04368125" layer="37"/>
<rectangle x1="4.72948125" y1="3.0353" x2="4.99871875" y2="3.04368125" layer="37"/>
<rectangle x1="5.0165" y1="3.0353" x2="5.13588125" y2="3.04368125" layer="37"/>
<rectangle x1="5.64388125" y1="3.0353" x2="5.69468125" y2="3.04368125" layer="37"/>
<rectangle x1="5.72008125" y1="3.0353" x2="5.78611875" y2="3.04368125" layer="37"/>
<rectangle x1="5.86231875" y1="3.0353" x2="5.89788125" y2="3.04368125" layer="37"/>
<rectangle x1="6.5151" y1="3.0353" x2="6.76148125" y2="3.04368125" layer="37"/>
<rectangle x1="7.4295" y1="3.0353" x2="7.74191875" y2="3.04368125" layer="37"/>
<rectangle x1="8.23468125" y1="3.0353" x2="9.09828125" y2="3.04368125" layer="37"/>
<rectangle x1="0.3937" y1="3.04368125" x2="1.24968125" y2="3.05231875" layer="37"/>
<rectangle x1="1.74751875" y1="3.04368125" x2="2.06248125" y2="3.05231875" layer="37"/>
<rectangle x1="2.73811875" y1="3.04368125" x2="2.97688125" y2="3.05231875" layer="37"/>
<rectangle x1="3.60171875" y1="3.04368125" x2="3.65251875" y2="3.05231875" layer="37"/>
<rectangle x1="3.68808125" y1="3.04368125" x2="3.7465" y2="3.05231875" layer="37"/>
<rectangle x1="3.8227" y1="3.04368125" x2="3.85571875" y2="3.05231875" layer="37"/>
<rectangle x1="4.4069" y1="3.04368125" x2="4.54151875" y2="3.05231875" layer="37"/>
<rectangle x1="4.5593" y1="3.04368125" x2="4.6609" y2="3.05231875" layer="37"/>
<rectangle x1="4.67868125" y1="3.04368125" x2="4.70408125" y2="3.05231875" layer="37"/>
<rectangle x1="4.72948125" y1="3.04368125" x2="4.99871875" y2="3.05231875" layer="37"/>
<rectangle x1="5.0165" y1="3.04368125" x2="5.13588125" y2="3.05231875" layer="37"/>
<rectangle x1="5.64388125" y1="3.04368125" x2="5.69468125" y2="3.05231875" layer="37"/>
<rectangle x1="5.72008125" y1="3.04368125" x2="5.78611875" y2="3.05231875" layer="37"/>
<rectangle x1="5.86231875" y1="3.04368125" x2="5.89788125" y2="3.05231875" layer="37"/>
<rectangle x1="6.5151" y1="3.04368125" x2="6.76148125" y2="3.05231875" layer="37"/>
<rectangle x1="7.4295" y1="3.04368125" x2="7.74191875" y2="3.05231875" layer="37"/>
<rectangle x1="8.23468125" y1="3.04368125" x2="9.09828125" y2="3.05231875" layer="37"/>
<rectangle x1="0.3937" y1="3.05231875" x2="1.24968125" y2="3.0607" layer="37"/>
<rectangle x1="1.74751875" y1="3.05231875" x2="2.06248125" y2="3.0607" layer="37"/>
<rectangle x1="2.73811875" y1="3.05231875" x2="2.97688125" y2="3.0607" layer="37"/>
<rectangle x1="3.60171875" y1="3.05231875" x2="3.65251875" y2="3.0607" layer="37"/>
<rectangle x1="3.68808125" y1="3.05231875" x2="3.7465" y2="3.0607" layer="37"/>
<rectangle x1="3.8227" y1="3.05231875" x2="3.85571875" y2="3.0607" layer="37"/>
<rectangle x1="4.4069" y1="3.05231875" x2="4.54151875" y2="3.0607" layer="37"/>
<rectangle x1="4.5593" y1="3.05231875" x2="4.6609" y2="3.0607" layer="37"/>
<rectangle x1="4.67868125" y1="3.05231875" x2="4.70408125" y2="3.0607" layer="37"/>
<rectangle x1="4.72948125" y1="3.05231875" x2="4.99871875" y2="3.0607" layer="37"/>
<rectangle x1="5.0165" y1="3.05231875" x2="5.13588125" y2="3.0607" layer="37"/>
<rectangle x1="5.64388125" y1="3.05231875" x2="5.69468125" y2="3.0607" layer="37"/>
<rectangle x1="5.72008125" y1="3.05231875" x2="5.78611875" y2="3.0607" layer="37"/>
<rectangle x1="5.86231875" y1="3.05231875" x2="5.89788125" y2="3.0607" layer="37"/>
<rectangle x1="6.5151" y1="3.05231875" x2="6.76148125" y2="3.0607" layer="37"/>
<rectangle x1="7.4295" y1="3.05231875" x2="7.74191875" y2="3.0607" layer="37"/>
<rectangle x1="8.23468125" y1="3.05231875" x2="9.09828125" y2="3.0607" layer="37"/>
<rectangle x1="0.3937" y1="3.0607" x2="1.24968125" y2="3.06908125" layer="37"/>
<rectangle x1="1.74751875" y1="3.0607" x2="2.06248125" y2="3.06908125" layer="37"/>
<rectangle x1="2.73811875" y1="3.0607" x2="2.97688125" y2="3.06908125" layer="37"/>
<rectangle x1="3.60171875" y1="3.0607" x2="3.65251875" y2="3.06908125" layer="37"/>
<rectangle x1="3.68808125" y1="3.0607" x2="3.7465" y2="3.06908125" layer="37"/>
<rectangle x1="3.8227" y1="3.0607" x2="3.85571875" y2="3.06908125" layer="37"/>
<rectangle x1="4.4069" y1="3.0607" x2="4.54151875" y2="3.06908125" layer="37"/>
<rectangle x1="4.5593" y1="3.0607" x2="4.6609" y2="3.06908125" layer="37"/>
<rectangle x1="4.67868125" y1="3.0607" x2="4.70408125" y2="3.06908125" layer="37"/>
<rectangle x1="4.72948125" y1="3.0607" x2="4.99871875" y2="3.06908125" layer="37"/>
<rectangle x1="5.0165" y1="3.0607" x2="5.13588125" y2="3.06908125" layer="37"/>
<rectangle x1="5.64388125" y1="3.0607" x2="5.69468125" y2="3.06908125" layer="37"/>
<rectangle x1="5.72008125" y1="3.0607" x2="5.78611875" y2="3.06908125" layer="37"/>
<rectangle x1="5.86231875" y1="3.0607" x2="5.89788125" y2="3.06908125" layer="37"/>
<rectangle x1="6.5151" y1="3.0607" x2="6.76148125" y2="3.06908125" layer="37"/>
<rectangle x1="7.4295" y1="3.0607" x2="7.74191875" y2="3.06908125" layer="37"/>
<rectangle x1="8.23468125" y1="3.0607" x2="9.09828125" y2="3.06908125" layer="37"/>
<rectangle x1="0.3937" y1="3.06908125" x2="1.24968125" y2="3.07771875" layer="37"/>
<rectangle x1="1.74751875" y1="3.06908125" x2="2.06248125" y2="3.07771875" layer="37"/>
<rectangle x1="2.73811875" y1="3.06908125" x2="2.97688125" y2="3.07771875" layer="37"/>
<rectangle x1="3.60171875" y1="3.06908125" x2="3.65251875" y2="3.07771875" layer="37"/>
<rectangle x1="3.68808125" y1="3.06908125" x2="3.75411875" y2="3.07771875" layer="37"/>
<rectangle x1="3.8227" y1="3.06908125" x2="3.85571875" y2="3.07771875" layer="37"/>
<rectangle x1="4.4069" y1="3.06908125" x2="4.54151875" y2="3.07771875" layer="37"/>
<rectangle x1="4.5593" y1="3.06908125" x2="4.6609" y2="3.07771875" layer="37"/>
<rectangle x1="4.67868125" y1="3.06908125" x2="4.70408125" y2="3.07771875" layer="37"/>
<rectangle x1="4.72948125" y1="3.06908125" x2="4.99871875" y2="3.07771875" layer="37"/>
<rectangle x1="5.0165" y1="3.06908125" x2="5.13588125" y2="3.07771875" layer="37"/>
<rectangle x1="5.64388125" y1="3.06908125" x2="5.69468125" y2="3.07771875" layer="37"/>
<rectangle x1="5.72008125" y1="3.06908125" x2="5.79628125" y2="3.07771875" layer="37"/>
<rectangle x1="5.86231875" y1="3.06908125" x2="5.89788125" y2="3.07771875" layer="37"/>
<rectangle x1="6.5151" y1="3.06908125" x2="6.76148125" y2="3.07771875" layer="37"/>
<rectangle x1="7.4295" y1="3.06908125" x2="7.74191875" y2="3.07771875" layer="37"/>
<rectangle x1="8.23468125" y1="3.06908125" x2="9.09828125" y2="3.07771875" layer="37"/>
<rectangle x1="1.74751875" y1="3.07771875" x2="2.06248125" y2="3.0861" layer="37"/>
<rectangle x1="2.73811875" y1="3.07771875" x2="2.97688125" y2="3.0861" layer="37"/>
<rectangle x1="3.60171875" y1="3.07771875" x2="3.65251875" y2="3.0861" layer="37"/>
<rectangle x1="3.71348125" y1="3.07771875" x2="3.76428125" y2="3.0861" layer="37"/>
<rectangle x1="3.8227" y1="3.07771875" x2="3.85571875" y2="3.0861" layer="37"/>
<rectangle x1="4.4831" y1="3.07771875" x2="4.54151875" y2="3.0861" layer="37"/>
<rectangle x1="4.5593" y1="3.07771875" x2="4.6609" y2="3.0861" layer="37"/>
<rectangle x1="4.67868125" y1="3.07771875" x2="4.70408125" y2="3.0861" layer="37"/>
<rectangle x1="4.72948125" y1="3.07771875" x2="4.99871875" y2="3.0861" layer="37"/>
<rectangle x1="5.0165" y1="3.07771875" x2="5.05968125" y2="3.0861" layer="37"/>
<rectangle x1="5.64388125" y1="3.07771875" x2="5.69468125" y2="3.0861" layer="37"/>
<rectangle x1="5.74548125" y1="3.07771875" x2="5.8039" y2="3.0861" layer="37"/>
<rectangle x1="5.86231875" y1="3.07771875" x2="5.89788125" y2="3.0861" layer="37"/>
<rectangle x1="6.5151" y1="3.07771875" x2="6.76148125" y2="3.0861" layer="37"/>
<rectangle x1="7.4295" y1="3.07771875" x2="7.74191875" y2="3.0861" layer="37"/>
<rectangle x1="1.74751875" y1="3.0861" x2="2.06248125" y2="3.09448125" layer="37"/>
<rectangle x1="2.73811875" y1="3.0861" x2="2.97688125" y2="3.09448125" layer="37"/>
<rectangle x1="3.60171875" y1="3.0861" x2="3.65251875" y2="3.09448125" layer="37"/>
<rectangle x1="3.71348125" y1="3.0861" x2="3.75411875" y2="3.09448125" layer="37"/>
<rectangle x1="3.8227" y1="3.0861" x2="3.85571875" y2="3.09448125" layer="37"/>
<rectangle x1="4.4831" y1="3.0861" x2="4.54151875" y2="3.09448125" layer="37"/>
<rectangle x1="4.5593" y1="3.0861" x2="4.6609" y2="3.09448125" layer="37"/>
<rectangle x1="4.67868125" y1="3.0861" x2="4.70408125" y2="3.09448125" layer="37"/>
<rectangle x1="4.72948125" y1="3.0861" x2="4.99871875" y2="3.09448125" layer="37"/>
<rectangle x1="5.0165" y1="3.0861" x2="5.05968125" y2="3.09448125" layer="37"/>
<rectangle x1="5.64388125" y1="3.0861" x2="5.69468125" y2="3.09448125" layer="37"/>
<rectangle x1="5.74548125" y1="3.0861" x2="5.79628125" y2="3.09448125" layer="37"/>
<rectangle x1="5.86231875" y1="3.0861" x2="5.89788125" y2="3.09448125" layer="37"/>
<rectangle x1="6.5151" y1="3.0861" x2="6.76148125" y2="3.09448125" layer="37"/>
<rectangle x1="7.4295" y1="3.0861" x2="7.74191875" y2="3.09448125" layer="37"/>
<rectangle x1="1.74751875" y1="3.09448125" x2="2.06248125" y2="3.10311875" layer="37"/>
<rectangle x1="2.73811875" y1="3.09448125" x2="2.97688125" y2="3.10311875" layer="37"/>
<rectangle x1="3.60171875" y1="3.09448125" x2="3.65251875" y2="3.10311875" layer="37"/>
<rectangle x1="3.70331875" y1="3.09448125" x2="3.7465" y2="3.10311875" layer="37"/>
<rectangle x1="3.8227" y1="3.09448125" x2="3.85571875" y2="3.10311875" layer="37"/>
<rectangle x1="4.4831" y1="3.09448125" x2="4.54151875" y2="3.10311875" layer="37"/>
<rectangle x1="4.5593" y1="3.09448125" x2="4.6609" y2="3.10311875" layer="37"/>
<rectangle x1="4.67868125" y1="3.09448125" x2="4.70408125" y2="3.10311875" layer="37"/>
<rectangle x1="4.72948125" y1="3.09448125" x2="4.99871875" y2="3.10311875" layer="37"/>
<rectangle x1="5.0165" y1="3.09448125" x2="5.05968125" y2="3.10311875" layer="37"/>
<rectangle x1="5.64388125" y1="3.09448125" x2="5.69468125" y2="3.10311875" layer="37"/>
<rectangle x1="5.74548125" y1="3.09448125" x2="5.7785" y2="3.10311875" layer="37"/>
<rectangle x1="5.86231875" y1="3.09448125" x2="5.89788125" y2="3.10311875" layer="37"/>
<rectangle x1="6.5151" y1="3.09448125" x2="6.76148125" y2="3.10311875" layer="37"/>
<rectangle x1="7.4295" y1="3.09448125" x2="7.74191875" y2="3.10311875" layer="37"/>
<rectangle x1="1.74751875" y1="3.10311875" x2="2.06248125" y2="3.1115" layer="37"/>
<rectangle x1="2.73811875" y1="3.10311875" x2="2.97688125" y2="3.1115" layer="37"/>
<rectangle x1="3.60171875" y1="3.10311875" x2="3.65251875" y2="3.1115" layer="37"/>
<rectangle x1="3.6957" y1="3.10311875" x2="3.72871875" y2="3.1115" layer="37"/>
<rectangle x1="3.8227" y1="3.10311875" x2="3.85571875" y2="3.1115" layer="37"/>
<rectangle x1="4.4831" y1="3.10311875" x2="4.54151875" y2="3.1115" layer="37"/>
<rectangle x1="4.5593" y1="3.10311875" x2="4.6609" y2="3.1115" layer="37"/>
<rectangle x1="4.67868125" y1="3.10311875" x2="4.70408125" y2="3.1115" layer="37"/>
<rectangle x1="4.72948125" y1="3.10311875" x2="4.99871875" y2="3.1115" layer="37"/>
<rectangle x1="5.0165" y1="3.10311875" x2="5.05968125" y2="3.1115" layer="37"/>
<rectangle x1="5.64388125" y1="3.10311875" x2="5.69468125" y2="3.1115" layer="37"/>
<rectangle x1="5.73531875" y1="3.10311875" x2="5.77088125" y2="3.1115" layer="37"/>
<rectangle x1="5.86231875" y1="3.10311875" x2="5.89788125" y2="3.1115" layer="37"/>
<rectangle x1="6.5151" y1="3.10311875" x2="6.76148125" y2="3.1115" layer="37"/>
<rectangle x1="7.4295" y1="3.10311875" x2="7.74191875" y2="3.1115" layer="37"/>
<rectangle x1="1.74751875" y1="3.1115" x2="2.06248125" y2="3.11988125" layer="37"/>
<rectangle x1="2.73811875" y1="3.1115" x2="2.97688125" y2="3.11988125" layer="37"/>
<rectangle x1="3.60171875" y1="3.1115" x2="3.65251875" y2="3.11988125" layer="37"/>
<rectangle x1="3.68808125" y1="3.1115" x2="3.7211" y2="3.11988125" layer="37"/>
<rectangle x1="3.8227" y1="3.1115" x2="3.85571875" y2="3.11988125" layer="37"/>
<rectangle x1="4.4831" y1="3.1115" x2="4.54151875" y2="3.11988125" layer="37"/>
<rectangle x1="4.5593" y1="3.1115" x2="4.6609" y2="3.11988125" layer="37"/>
<rectangle x1="4.67868125" y1="3.1115" x2="4.70408125" y2="3.11988125" layer="37"/>
<rectangle x1="4.72948125" y1="3.1115" x2="4.99871875" y2="3.11988125" layer="37"/>
<rectangle x1="5.0165" y1="3.1115" x2="5.05968125" y2="3.11988125" layer="37"/>
<rectangle x1="5.64388125" y1="3.1115" x2="5.69468125" y2="3.11988125" layer="37"/>
<rectangle x1="5.72008125" y1="3.1115" x2="5.7531" y2="3.11988125" layer="37"/>
<rectangle x1="5.86231875" y1="3.1115" x2="5.89788125" y2="3.11988125" layer="37"/>
<rectangle x1="6.5151" y1="3.1115" x2="6.76148125" y2="3.11988125" layer="37"/>
<rectangle x1="7.4295" y1="3.1115" x2="7.74191875" y2="3.11988125" layer="37"/>
<rectangle x1="1.74751875" y1="3.11988125" x2="2.06248125" y2="3.12851875" layer="37"/>
<rectangle x1="2.73811875" y1="3.11988125" x2="2.97688125" y2="3.12851875" layer="37"/>
<rectangle x1="3.60171875" y1="3.11988125" x2="3.85571875" y2="3.12851875" layer="37"/>
<rectangle x1="4.4831" y1="3.11988125" x2="4.54151875" y2="3.12851875" layer="37"/>
<rectangle x1="4.5593" y1="3.11988125" x2="4.6609" y2="3.12851875" layer="37"/>
<rectangle x1="4.67868125" y1="3.11988125" x2="4.70408125" y2="3.12851875" layer="37"/>
<rectangle x1="4.72948125" y1="3.11988125" x2="4.99871875" y2="3.12851875" layer="37"/>
<rectangle x1="5.0165" y1="3.11988125" x2="5.05968125" y2="3.12851875" layer="37"/>
<rectangle x1="5.64388125" y1="3.11988125" x2="5.89788125" y2="3.12851875" layer="37"/>
<rectangle x1="6.5151" y1="3.11988125" x2="6.76148125" y2="3.12851875" layer="37"/>
<rectangle x1="7.4295" y1="3.11988125" x2="7.74191875" y2="3.12851875" layer="37"/>
<rectangle x1="1.74751875" y1="3.12851875" x2="2.06248125" y2="3.1369" layer="37"/>
<rectangle x1="2.73811875" y1="3.12851875" x2="2.97688125" y2="3.1369" layer="37"/>
<rectangle x1="3.60171875" y1="3.12851875" x2="3.85571875" y2="3.1369" layer="37"/>
<rectangle x1="5.64388125" y1="3.12851875" x2="5.89788125" y2="3.1369" layer="37"/>
<rectangle x1="6.5151" y1="3.12851875" x2="6.76148125" y2="3.1369" layer="37"/>
<rectangle x1="7.4295" y1="3.12851875" x2="7.74191875" y2="3.1369" layer="37"/>
<rectangle x1="1.74751875" y1="3.1369" x2="2.06248125" y2="3.14528125" layer="37"/>
<rectangle x1="2.73811875" y1="3.1369" x2="2.97688125" y2="3.14528125" layer="37"/>
<rectangle x1="3.60171875" y1="3.1369" x2="3.85571875" y2="3.14528125" layer="37"/>
<rectangle x1="5.64388125" y1="3.1369" x2="5.89788125" y2="3.14528125" layer="37"/>
<rectangle x1="6.5151" y1="3.1369" x2="6.76148125" y2="3.14528125" layer="37"/>
<rectangle x1="7.4295" y1="3.1369" x2="7.74191875" y2="3.14528125" layer="37"/>
<rectangle x1="1.74751875" y1="3.14528125" x2="2.06248125" y2="3.15391875" layer="37"/>
<rectangle x1="2.73811875" y1="3.14528125" x2="2.97688125" y2="3.15391875" layer="37"/>
<rectangle x1="3.60171875" y1="3.14528125" x2="3.85571875" y2="3.15391875" layer="37"/>
<rectangle x1="5.64388125" y1="3.14528125" x2="5.89788125" y2="3.15391875" layer="37"/>
<rectangle x1="6.5151" y1="3.14528125" x2="6.76148125" y2="3.15391875" layer="37"/>
<rectangle x1="7.4295" y1="3.14528125" x2="7.74191875" y2="3.15391875" layer="37"/>
<rectangle x1="1.74751875" y1="3.15391875" x2="2.06248125" y2="3.1623" layer="37"/>
<rectangle x1="2.73811875" y1="3.15391875" x2="2.97688125" y2="3.1623" layer="37"/>
<rectangle x1="3.60171875" y1="3.15391875" x2="3.85571875" y2="3.1623" layer="37"/>
<rectangle x1="5.64388125" y1="3.15391875" x2="5.89788125" y2="3.1623" layer="37"/>
<rectangle x1="6.5151" y1="3.15391875" x2="6.76148125" y2="3.1623" layer="37"/>
<rectangle x1="7.4295" y1="3.15391875" x2="7.74191875" y2="3.1623" layer="37"/>
<rectangle x1="1.74751875" y1="3.1623" x2="2.06248125" y2="3.17068125" layer="37"/>
<rectangle x1="3.5941" y1="3.1623" x2="3.85571875" y2="3.17068125" layer="37"/>
<rectangle x1="5.64388125" y1="3.1623" x2="5.89788125" y2="3.17068125" layer="37"/>
<rectangle x1="7.4295" y1="3.1623" x2="7.74191875" y2="3.17068125" layer="37"/>
<rectangle x1="1.74751875" y1="3.17068125" x2="2.06248125" y2="3.17931875" layer="37"/>
<rectangle x1="3.5941" y1="3.17068125" x2="3.85571875" y2="3.17931875" layer="37"/>
<rectangle x1="5.64388125" y1="3.17068125" x2="5.89788125" y2="3.17931875" layer="37"/>
<rectangle x1="7.4295" y1="3.17068125" x2="7.74191875" y2="3.17931875" layer="37"/>
<rectangle x1="1.74751875" y1="3.17931875" x2="2.06248125" y2="3.1877" layer="37"/>
<rectangle x1="3.60171875" y1="3.17931875" x2="3.85571875" y2="3.1877" layer="37"/>
<rectangle x1="5.63371875" y1="3.17931875" x2="5.89788125" y2="3.1877" layer="37"/>
<rectangle x1="7.4295" y1="3.17931875" x2="7.74191875" y2="3.1877" layer="37"/>
<rectangle x1="1.74751875" y1="3.1877" x2="2.06248125" y2="3.19608125" layer="37"/>
<rectangle x1="3.60171875" y1="3.1877" x2="3.85571875" y2="3.19608125" layer="37"/>
<rectangle x1="5.64388125" y1="3.1877" x2="5.88771875" y2="3.19608125" layer="37"/>
<rectangle x1="7.4295" y1="3.1877" x2="7.74191875" y2="3.19608125" layer="37"/>
<rectangle x1="1.74751875" y1="3.19608125" x2="2.06248125" y2="3.20471875" layer="37"/>
<rectangle x1="3.6195" y1="3.19608125" x2="3.84048125" y2="3.20471875" layer="37"/>
<rectangle x1="5.6515" y1="3.19608125" x2="5.8801" y2="3.20471875" layer="37"/>
<rectangle x1="7.4295" y1="3.19608125" x2="7.74191875" y2="3.20471875" layer="37"/>
<rectangle x1="1.74751875" y1="3.20471875" x2="2.06248125" y2="3.2131" layer="37"/>
<rectangle x1="3.63728125" y1="3.20471875" x2="3.8227" y2="3.2131" layer="37"/>
<rectangle x1="5.66928125" y1="3.20471875" x2="5.86231875" y2="3.2131" layer="37"/>
<rectangle x1="7.4295" y1="3.20471875" x2="7.74191875" y2="3.2131" layer="37"/>
<rectangle x1="1.74751875" y1="3.2131" x2="2.06248125" y2="3.22148125" layer="37"/>
<rectangle x1="3.66268125" y1="3.2131" x2="3.7973" y2="3.22148125" layer="37"/>
<rectangle x1="5.69468125" y1="3.2131" x2="5.83691875" y2="3.22148125" layer="37"/>
<rectangle x1="7.4295" y1="3.2131" x2="7.74191875" y2="3.22148125" layer="37"/>
<rectangle x1="1.74751875" y1="3.22148125" x2="2.06248125" y2="3.23011875" layer="37"/>
<rectangle x1="7.4295" y1="3.22148125" x2="7.74191875" y2="3.23011875" layer="37"/>
<rectangle x1="1.74751875" y1="3.23011875" x2="2.06248125" y2="3.2385" layer="37"/>
<rectangle x1="7.4295" y1="3.23011875" x2="7.74191875" y2="3.2385" layer="37"/>
<rectangle x1="1.74751875" y1="3.2385" x2="2.06248125" y2="3.24688125" layer="37"/>
<rectangle x1="7.4295" y1="3.2385" x2="7.74191875" y2="3.24688125" layer="37"/>
<rectangle x1="1.74751875" y1="3.24688125" x2="2.06248125" y2="3.25551875" layer="37"/>
<rectangle x1="7.4295" y1="3.24688125" x2="7.74191875" y2="3.25551875" layer="37"/>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="LOGO-9.5MMX4.5MM">
<rectangle x1="0.07111875" y1="-0.00431875" x2="0.17271875" y2="0.00431875" layer="94"/>
<rectangle x1="0.98551875" y1="-0.00431875" x2="1.09728125" y2="0.00431875" layer="94"/>
<rectangle x1="1.50368125" y1="-0.00431875" x2="2.1971" y2="0.00431875" layer="94"/>
<rectangle x1="2.61111875" y1="-0.00431875" x2="3.47471875" y2="0.00431875" layer="94"/>
<rectangle x1="4.0513" y1="-0.00431875" x2="4.75488125" y2="0.00431875" layer="94"/>
<rectangle x1="6.37031875" y1="-0.00431875" x2="6.9469" y2="0.00431875" layer="94"/>
<rectangle x1="7.3279" y1="-0.00431875" x2="8.02131875" y2="0.00431875" layer="94"/>
<rectangle x1="8.56488125" y1="-0.00431875" x2="9.2583" y2="0.00431875" layer="94"/>
<rectangle x1="0.04571875" y1="0.00431875" x2="0.19811875" y2="0.0127" layer="94"/>
<rectangle x1="0.97028125" y1="0.00431875" x2="1.11251875" y2="0.0127" layer="94"/>
<rectangle x1="1.4605" y1="0.00431875" x2="2.24028125" y2="0.0127" layer="94"/>
<rectangle x1="2.59588125" y1="0.00431875" x2="3.5179" y2="0.0127" layer="94"/>
<rectangle x1="4.01828125" y1="0.00431875" x2="4.7879" y2="0.0127" layer="94"/>
<rectangle x1="6.35508125" y1="0.00431875" x2="6.96468125" y2="0.0127" layer="94"/>
<rectangle x1="7.29488125" y1="0.00431875" x2="8.0645" y2="0.0127" layer="94"/>
<rectangle x1="8.5217" y1="0.00431875" x2="9.29131875" y2="0.0127" layer="94"/>
<rectangle x1="0.02031875" y1="0.0127" x2="0.22351875" y2="0.02108125" layer="94"/>
<rectangle x1="0.9525" y1="0.0127" x2="1.1303" y2="0.02108125" layer="94"/>
<rectangle x1="1.4351" y1="0.0127" x2="2.26568125" y2="0.02108125" layer="94"/>
<rectangle x1="2.5781" y1="0.0127" x2="3.5433" y2="0.02108125" layer="94"/>
<rectangle x1="3.98271875" y1="0.0127" x2="4.82091875" y2="0.02108125" layer="94"/>
<rectangle x1="6.3373" y1="0.0127" x2="6.97991875" y2="0.02108125" layer="94"/>
<rectangle x1="7.25931875" y1="0.0127" x2="8.09751875" y2="0.02108125" layer="94"/>
<rectangle x1="8.48868125" y1="0.0127" x2="9.32688125" y2="0.02108125" layer="94"/>
<rectangle x1="0.0127" y1="0.02108125" x2="0.23368125" y2="0.02971875" layer="94"/>
<rectangle x1="0.93471875" y1="0.02108125" x2="1.13791875" y2="0.02971875" layer="94"/>
<rectangle x1="1.4097" y1="0.02108125" x2="2.29108125" y2="0.02971875" layer="94"/>
<rectangle x1="2.56031875" y1="0.02108125" x2="3.5687" y2="0.02971875" layer="94"/>
<rectangle x1="3.96748125" y1="0.02108125" x2="4.84631875" y2="0.02971875" layer="94"/>
<rectangle x1="6.32968125" y1="0.02108125" x2="6.99008125" y2="0.02971875" layer="94"/>
<rectangle x1="7.23391875" y1="0.02108125" x2="8.1153" y2="0.02971875" layer="94"/>
<rectangle x1="8.46328125" y1="0.02108125" x2="9.35228125" y2="0.02971875" layer="94"/>
<rectangle x1="0.0127" y1="0.02971875" x2="0.23368125" y2="0.0381" layer="94"/>
<rectangle x1="0.9271" y1="0.02971875" x2="1.14808125" y2="0.0381" layer="94"/>
<rectangle x1="1.39191875" y1="0.02971875" x2="2.30631875" y2="0.0381" layer="94"/>
<rectangle x1="2.56031875" y1="0.02971875" x2="3.58648125" y2="0.0381" layer="94"/>
<rectangle x1="3.94208125" y1="0.02971875" x2="4.8641" y2="0.0381" layer="94"/>
<rectangle x1="6.31951875" y1="0.02971875" x2="6.9977" y2="0.0381" layer="94"/>
<rectangle x1="7.21868125" y1="0.02971875" x2="8.1407" y2="0.0381" layer="94"/>
<rectangle x1="8.4455" y1="0.02971875" x2="9.36751875" y2="0.0381" layer="94"/>
<rectangle x1="0.0127" y1="0.0381" x2="0.23368125" y2="0.04648125" layer="94"/>
<rectangle x1="0.91948125" y1="0.0381" x2="1.14808125" y2="0.04648125" layer="94"/>
<rectangle x1="1.36651875" y1="0.0381" x2="2.33171875" y2="0.04648125" layer="94"/>
<rectangle x1="2.56031875" y1="0.0381" x2="3.60171875" y2="0.04648125" layer="94"/>
<rectangle x1="3.9243" y1="0.0381" x2="4.88188125" y2="0.04648125" layer="94"/>
<rectangle x1="5.63371875" y1="0.0381" x2="5.7277" y2="0.04648125" layer="94"/>
<rectangle x1="6.31951875" y1="0.0381" x2="6.9977" y2="0.04648125" layer="94"/>
<rectangle x1="7.2009" y1="0.0381" x2="8.15848125" y2="0.04648125" layer="94"/>
<rectangle x1="8.42771875" y1="0.0381" x2="9.3853" y2="0.04648125" layer="94"/>
<rectangle x1="0.0127" y1="0.04648125" x2="0.23368125" y2="0.05511875" layer="94"/>
<rectangle x1="0.90931875" y1="0.04648125" x2="1.14808125" y2="0.05511875" layer="94"/>
<rectangle x1="1.35128125" y1="0.04648125" x2="2.3495" y2="0.05511875" layer="94"/>
<rectangle x1="2.56031875" y1="0.04648125" x2="3.62711875" y2="0.05511875" layer="94"/>
<rectangle x1="3.90651875" y1="0.04648125" x2="4.89711875" y2="0.05511875" layer="94"/>
<rectangle x1="5.6007" y1="0.04648125" x2="5.76071875" y2="0.05511875" layer="94"/>
<rectangle x1="6.31951875" y1="0.04648125" x2="6.9977" y2="0.05511875" layer="94"/>
<rectangle x1="7.1755" y1="0.04648125" x2="8.17371875" y2="0.05511875" layer="94"/>
<rectangle x1="8.41248125" y1="0.04648125" x2="9.40308125" y2="0.05511875" layer="94"/>
<rectangle x1="0.0127" y1="0.05511875" x2="0.23368125" y2="0.0635" layer="94"/>
<rectangle x1="0.9017" y1="0.05511875" x2="1.13791875" y2="0.0635" layer="94"/>
<rectangle x1="1.3335" y1="0.05511875" x2="2.36728125" y2="0.0635" layer="94"/>
<rectangle x1="2.56031875" y1="0.05511875" x2="3.63728125" y2="0.0635" layer="94"/>
<rectangle x1="3.89128125" y1="0.05511875" x2="4.9149" y2="0.0635" layer="94"/>
<rectangle x1="5.58291875" y1="0.05511875" x2="5.7785" y2="0.0635" layer="94"/>
<rectangle x1="6.31951875" y1="0.05511875" x2="6.99008125" y2="0.0635" layer="94"/>
<rectangle x1="7.16788125" y1="0.05511875" x2="8.18388125" y2="0.0635" layer="94"/>
<rectangle x1="8.3947" y1="0.05511875" x2="9.41831875" y2="0.0635" layer="94"/>
<rectangle x1="0.0127" y1="0.0635" x2="0.23368125" y2="0.07188125" layer="94"/>
<rectangle x1="0.89408125" y1="0.0635" x2="1.1303" y2="0.07188125" layer="94"/>
<rectangle x1="1.32588125" y1="0.0635" x2="2.3749" y2="0.07188125" layer="94"/>
<rectangle x1="2.56031875" y1="0.0635" x2="3.65251875" y2="0.07188125" layer="94"/>
<rectangle x1="3.8735" y1="0.0635" x2="4.93268125" y2="0.07188125" layer="94"/>
<rectangle x1="5.5753" y1="0.0635" x2="5.78611875" y2="0.07188125" layer="94"/>
<rectangle x1="6.32968125" y1="0.0635" x2="6.97991875" y2="0.07188125" layer="94"/>
<rectangle x1="7.1501" y1="0.0635" x2="8.19911875" y2="0.07188125" layer="94"/>
<rectangle x1="8.38708125" y1="0.0635" x2="9.4361" y2="0.07188125" layer="94"/>
<rectangle x1="0.0127" y1="0.07188125" x2="0.23368125" y2="0.08051875" layer="94"/>
<rectangle x1="0.88391875" y1="0.07188125" x2="1.12268125" y2="0.08051875" layer="94"/>
<rectangle x1="1.3081" y1="0.07188125" x2="2.39268125" y2="0.08051875" layer="94"/>
<rectangle x1="2.56031875" y1="0.07188125" x2="3.66268125" y2="0.08051875" layer="94"/>
<rectangle x1="3.86588125" y1="0.07188125" x2="4.9403" y2="0.08051875" layer="94"/>
<rectangle x1="5.56768125" y1="0.07188125" x2="5.78611875" y2="0.08051875" layer="94"/>
<rectangle x1="6.34491875" y1="0.07188125" x2="6.9723" y2="0.08051875" layer="94"/>
<rectangle x1="7.14248125" y1="0.07188125" x2="8.20928125" y2="0.08051875" layer="94"/>
<rectangle x1="8.3693" y1="0.07188125" x2="9.44371875" y2="0.08051875" layer="94"/>
<rectangle x1="0.0127" y1="0.08051875" x2="0.23368125" y2="0.0889" layer="94"/>
<rectangle x1="0.86868125" y1="0.08051875" x2="1.11251875" y2="0.0889" layer="94"/>
<rectangle x1="1.30048125" y1="0.08051875" x2="2.4003" y2="0.0889" layer="94"/>
<rectangle x1="2.56031875" y1="0.08051875" x2="3.67791875" y2="0.0889" layer="94"/>
<rectangle x1="3.85571875" y1="0.08051875" x2="4.95808125" y2="0.0889" layer="94"/>
<rectangle x1="5.56768125" y1="0.08051875" x2="5.78611875" y2="0.0889" layer="94"/>
<rectangle x1="6.3627" y1="0.08051875" x2="6.95451875" y2="0.0889" layer="94"/>
<rectangle x1="7.1247" y1="0.08051875" x2="8.2169" y2="0.0889" layer="94"/>
<rectangle x1="8.36168125" y1="0.08051875" x2="9.45388125" y2="0.0889" layer="94"/>
<rectangle x1="0.0127" y1="0.0889" x2="0.23368125" y2="0.09728125" layer="94"/>
<rectangle x1="0.85851875" y1="0.0889" x2="1.1049" y2="0.09728125" layer="94"/>
<rectangle x1="1.29031875" y1="0.0889" x2="2.40791875" y2="0.09728125" layer="94"/>
<rectangle x1="2.56031875" y1="0.0889" x2="3.68808125" y2="0.09728125" layer="94"/>
<rectangle x1="3.8481" y1="0.0889" x2="4.95808125" y2="0.09728125" layer="94"/>
<rectangle x1="5.56768125" y1="0.0889" x2="5.78611875" y2="0.09728125" layer="94"/>
<rectangle x1="6.3881" y1="0.0889" x2="6.9215" y2="0.09728125" layer="94"/>
<rectangle x1="7.11708125" y1="0.0889" x2="8.22451875" y2="0.09728125" layer="94"/>
<rectangle x1="8.35151875" y1="0.0889" x2="9.4615" y2="0.09728125" layer="94"/>
<rectangle x1="0.0127" y1="0.09728125" x2="0.23368125" y2="0.10591875" layer="94"/>
<rectangle x1="0.8509" y1="0.09728125" x2="1.09728125" y2="0.10591875" layer="94"/>
<rectangle x1="1.2827" y1="0.09728125" x2="1.55448125" y2="0.10591875" layer="94"/>
<rectangle x1="2.1463" y1="0.09728125" x2="2.41808125" y2="0.10591875" layer="94"/>
<rectangle x1="2.56031875" y1="0.09728125" x2="2.7813" y2="0.10591875" layer="94"/>
<rectangle x1="3.42391875" y1="0.09728125" x2="3.68808125" y2="0.10591875" layer="94"/>
<rectangle x1="3.84048125" y1="0.09728125" x2="4.1021" y2="0.10591875" layer="94"/>
<rectangle x1="4.70408125" y1="0.09728125" x2="4.9657" y2="0.10591875" layer="94"/>
<rectangle x1="5.56768125" y1="0.09728125" x2="5.78611875" y2="0.10591875" layer="94"/>
<rectangle x1="6.54811875" y1="0.09728125" x2="6.7691" y2="0.10591875" layer="94"/>
<rectangle x1="7.11708125" y1="0.09728125" x2="7.3787" y2="0.10591875" layer="94"/>
<rectangle x1="7.98068125" y1="0.09728125" x2="8.23468125" y2="0.10591875" layer="94"/>
<rectangle x1="8.3439" y1="0.09728125" x2="8.60551875" y2="0.10591875" layer="94"/>
<rectangle x1="9.2075" y1="0.09728125" x2="9.46911875" y2="0.10591875" layer="94"/>
<rectangle x1="0.0127" y1="0.10591875" x2="0.23368125" y2="0.1143" layer="94"/>
<rectangle x1="0.84328125" y1="0.10591875" x2="1.08711875" y2="0.1143" layer="94"/>
<rectangle x1="1.2827" y1="0.10591875" x2="1.52908125" y2="0.1143" layer="94"/>
<rectangle x1="2.16408125" y1="0.10591875" x2="2.41808125" y2="0.1143" layer="94"/>
<rectangle x1="2.56031875" y1="0.10591875" x2="2.7813" y2="0.1143" layer="94"/>
<rectangle x1="3.44931875" y1="0.10591875" x2="3.6957" y2="0.1143" layer="94"/>
<rectangle x1="3.84048125" y1="0.10591875" x2="4.08431875" y2="0.1143" layer="94"/>
<rectangle x1="4.71931875" y1="0.10591875" x2="4.9657" y2="0.1143" layer="94"/>
<rectangle x1="5.56768125" y1="0.10591875" x2="5.78611875" y2="0.1143" layer="94"/>
<rectangle x1="6.54811875" y1="0.10591875" x2="6.7691" y2="0.1143" layer="94"/>
<rectangle x1="7.10691875" y1="0.10591875" x2="7.36091875" y2="0.1143" layer="94"/>
<rectangle x1="7.99591875" y1="0.10591875" x2="8.2423" y2="0.1143" layer="94"/>
<rectangle x1="8.3439" y1="0.10591875" x2="8.59028125" y2="0.1143" layer="94"/>
<rectangle x1="9.22528125" y1="0.10591875" x2="9.46911875" y2="0.1143" layer="94"/>
<rectangle x1="0.0127" y1="0.1143" x2="0.23368125" y2="0.12268125" layer="94"/>
<rectangle x1="0.83311875" y1="0.1143" x2="1.07188125" y2="0.12268125" layer="94"/>
<rectangle x1="1.2827" y1="0.1143" x2="1.5113" y2="0.12268125" layer="94"/>
<rectangle x1="2.18948125" y1="0.1143" x2="2.41808125" y2="0.12268125" layer="94"/>
<rectangle x1="2.56031875" y1="0.1143" x2="2.7813" y2="0.12268125" layer="94"/>
<rectangle x1="3.4671" y1="0.1143" x2="3.6957" y2="0.12268125" layer="94"/>
<rectangle x1="3.84048125" y1="0.1143" x2="4.06908125" y2="0.12268125" layer="94"/>
<rectangle x1="4.7371" y1="0.1143" x2="4.97331875" y2="0.12268125" layer="94"/>
<rectangle x1="5.56768125" y1="0.1143" x2="5.78611875" y2="0.12268125" layer="94"/>
<rectangle x1="6.54811875" y1="0.1143" x2="6.7691" y2="0.12268125" layer="94"/>
<rectangle x1="7.10691875" y1="0.1143" x2="7.33551875" y2="0.12268125" layer="94"/>
<rectangle x1="8.00608125" y1="0.1143" x2="8.2423" y2="0.12268125" layer="94"/>
<rectangle x1="8.33628125" y1="0.1143" x2="8.5725" y2="0.12268125" layer="94"/>
<rectangle x1="9.24051875" y1="0.1143" x2="9.47928125" y2="0.12268125" layer="94"/>
<rectangle x1="0.0127" y1="0.12268125" x2="0.23368125" y2="0.13131875" layer="94"/>
<rectangle x1="0.8255" y1="0.12268125" x2="1.06171875" y2="0.13131875" layer="94"/>
<rectangle x1="1.2827" y1="0.12268125" x2="1.50368125" y2="0.13131875" layer="94"/>
<rectangle x1="2.1971" y1="0.12268125" x2="2.41808125" y2="0.13131875" layer="94"/>
<rectangle x1="2.56031875" y1="0.12268125" x2="2.7813" y2="0.13131875" layer="94"/>
<rectangle x1="3.47471875" y1="0.12268125" x2="3.6957" y2="0.13131875" layer="94"/>
<rectangle x1="3.84048125" y1="0.12268125" x2="4.05891875" y2="0.13131875" layer="94"/>
<rectangle x1="4.75488125" y1="0.12268125" x2="4.97331875" y2="0.13131875" layer="94"/>
<rectangle x1="5.56768125" y1="0.12268125" x2="5.78611875" y2="0.13131875" layer="94"/>
<rectangle x1="6.54811875" y1="0.12268125" x2="6.7691" y2="0.13131875" layer="94"/>
<rectangle x1="7.10691875" y1="0.12268125" x2="7.3279" y2="0.13131875" layer="94"/>
<rectangle x1="8.0137" y1="0.12268125" x2="8.2423" y2="0.13131875" layer="94"/>
<rectangle x1="8.33628125" y1="0.12268125" x2="8.56488125" y2="0.13131875" layer="94"/>
<rectangle x1="9.25068125" y1="0.12268125" x2="9.47928125" y2="0.13131875" layer="94"/>
<rectangle x1="0.0127" y1="0.13131875" x2="0.23368125" y2="0.1397" layer="94"/>
<rectangle x1="0.81788125" y1="0.13131875" x2="1.0541" y2="0.1397" layer="94"/>
<rectangle x1="1.2827" y1="0.13131875" x2="1.50368125" y2="0.1397" layer="94"/>
<rectangle x1="2.1971" y1="0.13131875" x2="2.41808125" y2="0.1397" layer="94"/>
<rectangle x1="2.56031875" y1="0.13131875" x2="2.7813" y2="0.1397" layer="94"/>
<rectangle x1="3.47471875" y1="0.13131875" x2="3.6957" y2="0.1397" layer="94"/>
<rectangle x1="3.84048125" y1="0.13131875" x2="4.05891875" y2="0.1397" layer="94"/>
<rectangle x1="4.75488125" y1="0.13131875" x2="4.97331875" y2="0.1397" layer="94"/>
<rectangle x1="5.56768125" y1="0.13131875" x2="5.78611875" y2="0.1397" layer="94"/>
<rectangle x1="6.54811875" y1="0.13131875" x2="6.7691" y2="0.1397" layer="94"/>
<rectangle x1="7.10691875" y1="0.13131875" x2="7.3279" y2="0.1397" layer="94"/>
<rectangle x1="8.02131875" y1="0.13131875" x2="8.2423" y2="0.1397" layer="94"/>
<rectangle x1="8.33628125" y1="0.13131875" x2="8.56488125" y2="0.1397" layer="94"/>
<rectangle x1="9.25068125" y1="0.13131875" x2="9.47928125" y2="0.1397" layer="94"/>
<rectangle x1="0.0127" y1="0.1397" x2="0.23368125" y2="0.14808125" layer="94"/>
<rectangle x1="0.8001" y1="0.1397" x2="1.04648125" y2="0.14808125" layer="94"/>
<rectangle x1="1.2827" y1="0.1397" x2="1.50368125" y2="0.14808125" layer="94"/>
<rectangle x1="2.1971" y1="0.1397" x2="2.41808125" y2="0.14808125" layer="94"/>
<rectangle x1="2.56031875" y1="0.1397" x2="2.7813" y2="0.14808125" layer="94"/>
<rectangle x1="3.47471875" y1="0.1397" x2="3.6957" y2="0.14808125" layer="94"/>
<rectangle x1="3.84048125" y1="0.1397" x2="4.05891875" y2="0.14808125" layer="94"/>
<rectangle x1="4.75488125" y1="0.1397" x2="4.97331875" y2="0.14808125" layer="94"/>
<rectangle x1="5.56768125" y1="0.1397" x2="5.78611875" y2="0.14808125" layer="94"/>
<rectangle x1="6.54811875" y1="0.1397" x2="6.7691" y2="0.14808125" layer="94"/>
<rectangle x1="7.10691875" y1="0.1397" x2="7.3279" y2="0.14808125" layer="94"/>
<rectangle x1="8.02131875" y1="0.1397" x2="8.2423" y2="0.14808125" layer="94"/>
<rectangle x1="8.33628125" y1="0.1397" x2="8.55471875" y2="0.14808125" layer="94"/>
<rectangle x1="9.25068125" y1="0.1397" x2="9.47928125" y2="0.14808125" layer="94"/>
<rectangle x1="0.0127" y1="0.14808125" x2="0.23368125" y2="0.15671875" layer="94"/>
<rectangle x1="0.79248125" y1="0.14808125" x2="1.03631875" y2="0.15671875" layer="94"/>
<rectangle x1="1.2827" y1="0.14808125" x2="1.50368125" y2="0.15671875" layer="94"/>
<rectangle x1="2.1971" y1="0.14808125" x2="2.41808125" y2="0.15671875" layer="94"/>
<rectangle x1="2.56031875" y1="0.14808125" x2="2.7813" y2="0.15671875" layer="94"/>
<rectangle x1="3.47471875" y1="0.14808125" x2="3.6957" y2="0.15671875" layer="94"/>
<rectangle x1="3.84048125" y1="0.14808125" x2="4.05891875" y2="0.15671875" layer="94"/>
<rectangle x1="4.75488125" y1="0.14808125" x2="4.97331875" y2="0.15671875" layer="94"/>
<rectangle x1="5.56768125" y1="0.14808125" x2="5.78611875" y2="0.15671875" layer="94"/>
<rectangle x1="6.54811875" y1="0.14808125" x2="6.7691" y2="0.15671875" layer="94"/>
<rectangle x1="7.10691875" y1="0.14808125" x2="7.3279" y2="0.15671875" layer="94"/>
<rectangle x1="8.03148125" y1="0.14808125" x2="8.2423" y2="0.15671875" layer="94"/>
<rectangle x1="8.33628125" y1="0.14808125" x2="8.55471875" y2="0.15671875" layer="94"/>
<rectangle x1="9.25068125" y1="0.14808125" x2="9.47928125" y2="0.15671875" layer="94"/>
<rectangle x1="0.0127" y1="0.15671875" x2="0.23368125" y2="0.1651" layer="94"/>
<rectangle x1="0.78231875" y1="0.15671875" x2="1.0287" y2="0.1651" layer="94"/>
<rectangle x1="1.2827" y1="0.15671875" x2="1.50368125" y2="0.1651" layer="94"/>
<rectangle x1="2.1971" y1="0.15671875" x2="2.41808125" y2="0.1651" layer="94"/>
<rectangle x1="2.56031875" y1="0.15671875" x2="2.7813" y2="0.1651" layer="94"/>
<rectangle x1="3.47471875" y1="0.15671875" x2="3.6957" y2="0.1651" layer="94"/>
<rectangle x1="3.84048125" y1="0.15671875" x2="4.05891875" y2="0.1651" layer="94"/>
<rectangle x1="4.75488125" y1="0.15671875" x2="4.97331875" y2="0.1651" layer="94"/>
<rectangle x1="5.56768125" y1="0.15671875" x2="5.78611875" y2="0.1651" layer="94"/>
<rectangle x1="6.54811875" y1="0.15671875" x2="6.7691" y2="0.1651" layer="94"/>
<rectangle x1="7.10691875" y1="0.15671875" x2="7.3279" y2="0.1651" layer="94"/>
<rectangle x1="8.03148125" y1="0.15671875" x2="8.2423" y2="0.1651" layer="94"/>
<rectangle x1="8.3439" y1="0.15671875" x2="8.5471" y2="0.1651" layer="94"/>
<rectangle x1="9.25068125" y1="0.15671875" x2="9.47928125" y2="0.1651" layer="94"/>
<rectangle x1="0.0127" y1="0.1651" x2="0.23368125" y2="0.17348125" layer="94"/>
<rectangle x1="0.7747" y1="0.1651" x2="1.02108125" y2="0.17348125" layer="94"/>
<rectangle x1="1.2827" y1="0.1651" x2="1.50368125" y2="0.17348125" layer="94"/>
<rectangle x1="2.1971" y1="0.1651" x2="2.41808125" y2="0.17348125" layer="94"/>
<rectangle x1="2.56031875" y1="0.1651" x2="2.7813" y2="0.17348125" layer="94"/>
<rectangle x1="3.47471875" y1="0.1651" x2="3.6957" y2="0.17348125" layer="94"/>
<rectangle x1="3.84048125" y1="0.1651" x2="4.05891875" y2="0.17348125" layer="94"/>
<rectangle x1="4.75488125" y1="0.1651" x2="4.97331875" y2="0.17348125" layer="94"/>
<rectangle x1="5.56768125" y1="0.1651" x2="5.78611875" y2="0.17348125" layer="94"/>
<rectangle x1="6.54811875" y1="0.1651" x2="6.7691" y2="0.17348125" layer="94"/>
<rectangle x1="7.10691875" y1="0.1651" x2="7.3279" y2="0.17348125" layer="94"/>
<rectangle x1="8.0391" y1="0.1651" x2="8.23468125" y2="0.17348125" layer="94"/>
<rectangle x1="8.3439" y1="0.1651" x2="8.5471" y2="0.17348125" layer="94"/>
<rectangle x1="9.25068125" y1="0.1651" x2="9.47928125" y2="0.17348125" layer="94"/>
<rectangle x1="0.0127" y1="0.17348125" x2="0.23368125" y2="0.18211875" layer="94"/>
<rectangle x1="0.76708125" y1="0.17348125" x2="1.01091875" y2="0.18211875" layer="94"/>
<rectangle x1="1.2827" y1="0.17348125" x2="1.50368125" y2="0.18211875" layer="94"/>
<rectangle x1="2.1971" y1="0.17348125" x2="2.41808125" y2="0.18211875" layer="94"/>
<rectangle x1="2.56031875" y1="0.17348125" x2="2.7813" y2="0.18211875" layer="94"/>
<rectangle x1="3.47471875" y1="0.17348125" x2="3.6957" y2="0.18211875" layer="94"/>
<rectangle x1="3.84048125" y1="0.17348125" x2="4.05891875" y2="0.18211875" layer="94"/>
<rectangle x1="4.75488125" y1="0.17348125" x2="4.97331875" y2="0.18211875" layer="94"/>
<rectangle x1="5.56768125" y1="0.17348125" x2="5.78611875" y2="0.18211875" layer="94"/>
<rectangle x1="6.54811875" y1="0.17348125" x2="6.7691" y2="0.18211875" layer="94"/>
<rectangle x1="7.10691875" y1="0.17348125" x2="7.3279" y2="0.18211875" layer="94"/>
<rectangle x1="8.0391" y1="0.17348125" x2="8.2169" y2="0.18211875" layer="94"/>
<rectangle x1="8.35151875" y1="0.17348125" x2="8.53948125" y2="0.18211875" layer="94"/>
<rectangle x1="9.25068125" y1="0.17348125" x2="9.47928125" y2="0.18211875" layer="94"/>
<rectangle x1="0.0127" y1="0.18211875" x2="0.23368125" y2="0.1905" layer="94"/>
<rectangle x1="0.75691875" y1="0.18211875" x2="1.0033" y2="0.1905" layer="94"/>
<rectangle x1="1.2827" y1="0.18211875" x2="1.50368125" y2="0.1905" layer="94"/>
<rectangle x1="2.1971" y1="0.18211875" x2="2.41808125" y2="0.1905" layer="94"/>
<rectangle x1="2.56031875" y1="0.18211875" x2="2.7813" y2="0.1905" layer="94"/>
<rectangle x1="3.47471875" y1="0.18211875" x2="3.6957" y2="0.1905" layer="94"/>
<rectangle x1="3.84048125" y1="0.18211875" x2="4.05891875" y2="0.1905" layer="94"/>
<rectangle x1="4.75488125" y1="0.18211875" x2="4.97331875" y2="0.1905" layer="94"/>
<rectangle x1="5.56768125" y1="0.18211875" x2="5.78611875" y2="0.1905" layer="94"/>
<rectangle x1="6.54811875" y1="0.18211875" x2="6.7691" y2="0.1905" layer="94"/>
<rectangle x1="7.10691875" y1="0.18211875" x2="7.3279" y2="0.1905" layer="94"/>
<rectangle x1="8.0645" y1="0.18211875" x2="8.19911875" y2="0.1905" layer="94"/>
<rectangle x1="8.37691875" y1="0.18211875" x2="8.5217" y2="0.1905" layer="94"/>
<rectangle x1="9.25068125" y1="0.18211875" x2="9.47928125" y2="0.1905" layer="94"/>
<rectangle x1="0.0127" y1="0.1905" x2="0.23368125" y2="0.19888125" layer="94"/>
<rectangle x1="0.7493" y1="0.1905" x2="0.99568125" y2="0.19888125" layer="94"/>
<rectangle x1="1.2827" y1="0.1905" x2="1.50368125" y2="0.19888125" layer="94"/>
<rectangle x1="2.1971" y1="0.1905" x2="2.41808125" y2="0.19888125" layer="94"/>
<rectangle x1="2.56031875" y1="0.1905" x2="2.7813" y2="0.19888125" layer="94"/>
<rectangle x1="3.47471875" y1="0.1905" x2="3.6957" y2="0.19888125" layer="94"/>
<rectangle x1="3.84048125" y1="0.1905" x2="4.05891875" y2="0.19888125" layer="94"/>
<rectangle x1="4.75488125" y1="0.1905" x2="4.97331875" y2="0.19888125" layer="94"/>
<rectangle x1="5.56768125" y1="0.1905" x2="5.78611875" y2="0.19888125" layer="94"/>
<rectangle x1="6.54811875" y1="0.1905" x2="6.7691" y2="0.19888125" layer="94"/>
<rectangle x1="7.10691875" y1="0.1905" x2="7.3279" y2="0.19888125" layer="94"/>
<rectangle x1="8.09751875" y1="0.1905" x2="8.1661" y2="0.19888125" layer="94"/>
<rectangle x1="8.41248125" y1="0.1905" x2="8.48868125" y2="0.19888125" layer="94"/>
<rectangle x1="9.25068125" y1="0.1905" x2="9.47928125" y2="0.19888125" layer="94"/>
<rectangle x1="0.0127" y1="0.19888125" x2="0.23368125" y2="0.20751875" layer="94"/>
<rectangle x1="0.74168125" y1="0.19888125" x2="0.98551875" y2="0.20751875" layer="94"/>
<rectangle x1="1.2827" y1="0.19888125" x2="1.50368125" y2="0.20751875" layer="94"/>
<rectangle x1="2.1971" y1="0.19888125" x2="2.41808125" y2="0.20751875" layer="94"/>
<rectangle x1="2.56031875" y1="0.19888125" x2="2.7813" y2="0.20751875" layer="94"/>
<rectangle x1="3.47471875" y1="0.19888125" x2="3.6957" y2="0.20751875" layer="94"/>
<rectangle x1="3.84048125" y1="0.19888125" x2="4.05891875" y2="0.20751875" layer="94"/>
<rectangle x1="4.75488125" y1="0.19888125" x2="4.97331875" y2="0.20751875" layer="94"/>
<rectangle x1="5.56768125" y1="0.19888125" x2="5.78611875" y2="0.20751875" layer="94"/>
<rectangle x1="6.54811875" y1="0.19888125" x2="6.7691" y2="0.20751875" layer="94"/>
<rectangle x1="7.10691875" y1="0.19888125" x2="7.3279" y2="0.20751875" layer="94"/>
<rectangle x1="9.25068125" y1="0.19888125" x2="9.47928125" y2="0.20751875" layer="94"/>
<rectangle x1="0.0127" y1="0.20751875" x2="0.23368125" y2="0.2159" layer="94"/>
<rectangle x1="0.7239" y1="0.20751875" x2="0.97028125" y2="0.2159" layer="94"/>
<rectangle x1="1.2827" y1="0.20751875" x2="1.50368125" y2="0.2159" layer="94"/>
<rectangle x1="2.1971" y1="0.20751875" x2="2.41808125" y2="0.2159" layer="94"/>
<rectangle x1="2.56031875" y1="0.20751875" x2="2.7813" y2="0.2159" layer="94"/>
<rectangle x1="3.47471875" y1="0.20751875" x2="3.6957" y2="0.2159" layer="94"/>
<rectangle x1="3.84048125" y1="0.20751875" x2="4.05891875" y2="0.2159" layer="94"/>
<rectangle x1="4.75488125" y1="0.20751875" x2="4.97331875" y2="0.2159" layer="94"/>
<rectangle x1="5.56768125" y1="0.20751875" x2="5.78611875" y2="0.2159" layer="94"/>
<rectangle x1="6.54811875" y1="0.20751875" x2="6.7691" y2="0.2159" layer="94"/>
<rectangle x1="7.10691875" y1="0.20751875" x2="7.3279" y2="0.2159" layer="94"/>
<rectangle x1="9.25068125" y1="0.20751875" x2="9.47928125" y2="0.2159" layer="94"/>
<rectangle x1="0.0127" y1="0.2159" x2="0.23368125" y2="0.22428125" layer="94"/>
<rectangle x1="0.71628125" y1="0.2159" x2="0.96011875" y2="0.22428125" layer="94"/>
<rectangle x1="1.2827" y1="0.2159" x2="1.50368125" y2="0.22428125" layer="94"/>
<rectangle x1="2.1971" y1="0.2159" x2="2.41808125" y2="0.22428125" layer="94"/>
<rectangle x1="2.56031875" y1="0.2159" x2="2.7813" y2="0.22428125" layer="94"/>
<rectangle x1="3.47471875" y1="0.2159" x2="3.6957" y2="0.22428125" layer="94"/>
<rectangle x1="3.84048125" y1="0.2159" x2="4.05891875" y2="0.22428125" layer="94"/>
<rectangle x1="4.75488125" y1="0.2159" x2="4.97331875" y2="0.22428125" layer="94"/>
<rectangle x1="5.56768125" y1="0.2159" x2="5.78611875" y2="0.22428125" layer="94"/>
<rectangle x1="6.54811875" y1="0.2159" x2="6.7691" y2="0.22428125" layer="94"/>
<rectangle x1="7.10691875" y1="0.2159" x2="7.3279" y2="0.22428125" layer="94"/>
<rectangle x1="9.25068125" y1="0.2159" x2="9.47928125" y2="0.22428125" layer="94"/>
<rectangle x1="0.0127" y1="0.22428125" x2="0.23368125" y2="0.23291875" layer="94"/>
<rectangle x1="0.70611875" y1="0.22428125" x2="0.9525" y2="0.23291875" layer="94"/>
<rectangle x1="1.2827" y1="0.22428125" x2="1.50368125" y2="0.23291875" layer="94"/>
<rectangle x1="2.1971" y1="0.22428125" x2="2.41808125" y2="0.23291875" layer="94"/>
<rectangle x1="2.56031875" y1="0.22428125" x2="2.7813" y2="0.23291875" layer="94"/>
<rectangle x1="3.47471875" y1="0.22428125" x2="3.6957" y2="0.23291875" layer="94"/>
<rectangle x1="3.84048125" y1="0.22428125" x2="4.05891875" y2="0.23291875" layer="94"/>
<rectangle x1="4.75488125" y1="0.22428125" x2="4.97331875" y2="0.23291875" layer="94"/>
<rectangle x1="5.56768125" y1="0.22428125" x2="5.78611875" y2="0.23291875" layer="94"/>
<rectangle x1="6.54811875" y1="0.22428125" x2="6.7691" y2="0.23291875" layer="94"/>
<rectangle x1="7.10691875" y1="0.22428125" x2="7.3279" y2="0.23291875" layer="94"/>
<rectangle x1="9.25068125" y1="0.22428125" x2="9.47928125" y2="0.23291875" layer="94"/>
<rectangle x1="0.0127" y1="0.23291875" x2="0.23368125" y2="0.2413" layer="94"/>
<rectangle x1="0.6985" y1="0.23291875" x2="0.94488125" y2="0.2413" layer="94"/>
<rectangle x1="1.2827" y1="0.23291875" x2="1.50368125" y2="0.2413" layer="94"/>
<rectangle x1="2.1971" y1="0.23291875" x2="2.41808125" y2="0.2413" layer="94"/>
<rectangle x1="2.56031875" y1="0.23291875" x2="2.7813" y2="0.2413" layer="94"/>
<rectangle x1="3.47471875" y1="0.23291875" x2="3.6957" y2="0.2413" layer="94"/>
<rectangle x1="3.84048125" y1="0.23291875" x2="4.05891875" y2="0.2413" layer="94"/>
<rectangle x1="4.75488125" y1="0.23291875" x2="4.97331875" y2="0.2413" layer="94"/>
<rectangle x1="5.56768125" y1="0.23291875" x2="5.78611875" y2="0.2413" layer="94"/>
<rectangle x1="6.54811875" y1="0.23291875" x2="6.7691" y2="0.2413" layer="94"/>
<rectangle x1="7.10691875" y1="0.23291875" x2="7.3279" y2="0.2413" layer="94"/>
<rectangle x1="9.25068125" y1="0.23291875" x2="9.47928125" y2="0.2413" layer="94"/>
<rectangle x1="0.0127" y1="0.2413" x2="0.23368125" y2="0.24968125" layer="94"/>
<rectangle x1="0.69088125" y1="0.2413" x2="0.93471875" y2="0.24968125" layer="94"/>
<rectangle x1="1.2827" y1="0.2413" x2="1.50368125" y2="0.24968125" layer="94"/>
<rectangle x1="2.1971" y1="0.2413" x2="2.41808125" y2="0.24968125" layer="94"/>
<rectangle x1="2.56031875" y1="0.2413" x2="2.7813" y2="0.24968125" layer="94"/>
<rectangle x1="3.47471875" y1="0.2413" x2="3.6957" y2="0.24968125" layer="94"/>
<rectangle x1="3.84048125" y1="0.2413" x2="4.05891875" y2="0.24968125" layer="94"/>
<rectangle x1="4.75488125" y1="0.2413" x2="4.97331875" y2="0.24968125" layer="94"/>
<rectangle x1="5.56768125" y1="0.2413" x2="5.78611875" y2="0.24968125" layer="94"/>
<rectangle x1="6.54811875" y1="0.2413" x2="6.7691" y2="0.24968125" layer="94"/>
<rectangle x1="7.10691875" y1="0.2413" x2="7.3279" y2="0.24968125" layer="94"/>
<rectangle x1="9.25068125" y1="0.2413" x2="9.47928125" y2="0.24968125" layer="94"/>
<rectangle x1="0.0127" y1="0.24968125" x2="0.23368125" y2="0.25831875" layer="94"/>
<rectangle x1="0.68071875" y1="0.24968125" x2="0.9271" y2="0.25831875" layer="94"/>
<rectangle x1="1.2827" y1="0.24968125" x2="1.50368125" y2="0.25831875" layer="94"/>
<rectangle x1="2.1971" y1="0.24968125" x2="2.41808125" y2="0.25831875" layer="94"/>
<rectangle x1="2.56031875" y1="0.24968125" x2="2.7813" y2="0.25831875" layer="94"/>
<rectangle x1="3.47471875" y1="0.24968125" x2="3.6957" y2="0.25831875" layer="94"/>
<rectangle x1="3.84048125" y1="0.24968125" x2="4.05891875" y2="0.25831875" layer="94"/>
<rectangle x1="4.75488125" y1="0.24968125" x2="4.97331875" y2="0.25831875" layer="94"/>
<rectangle x1="5.56768125" y1="0.24968125" x2="5.78611875" y2="0.25831875" layer="94"/>
<rectangle x1="6.54811875" y1="0.24968125" x2="6.7691" y2="0.25831875" layer="94"/>
<rectangle x1="7.10691875" y1="0.24968125" x2="7.3279" y2="0.25831875" layer="94"/>
<rectangle x1="9.25068125" y1="0.24968125" x2="9.47928125" y2="0.25831875" layer="94"/>
<rectangle x1="0.0127" y1="0.25831875" x2="0.23368125" y2="0.2667" layer="94"/>
<rectangle x1="0.6731" y1="0.25831875" x2="0.91948125" y2="0.2667" layer="94"/>
<rectangle x1="1.2827" y1="0.25831875" x2="1.50368125" y2="0.2667" layer="94"/>
<rectangle x1="2.1971" y1="0.25831875" x2="2.41808125" y2="0.2667" layer="94"/>
<rectangle x1="2.56031875" y1="0.25831875" x2="2.7813" y2="0.2667" layer="94"/>
<rectangle x1="3.47471875" y1="0.25831875" x2="3.6957" y2="0.2667" layer="94"/>
<rectangle x1="3.84048125" y1="0.25831875" x2="4.05891875" y2="0.2667" layer="94"/>
<rectangle x1="4.75488125" y1="0.25831875" x2="4.97331875" y2="0.2667" layer="94"/>
<rectangle x1="5.56768125" y1="0.25831875" x2="5.78611875" y2="0.2667" layer="94"/>
<rectangle x1="6.54811875" y1="0.25831875" x2="6.7691" y2="0.2667" layer="94"/>
<rectangle x1="7.10691875" y1="0.25831875" x2="7.3279" y2="0.2667" layer="94"/>
<rectangle x1="9.25068125" y1="0.25831875" x2="9.47928125" y2="0.2667" layer="94"/>
<rectangle x1="0.0127" y1="0.2667" x2="0.23368125" y2="0.27508125" layer="94"/>
<rectangle x1="0.66548125" y1="0.2667" x2="0.90931875" y2="0.27508125" layer="94"/>
<rectangle x1="1.2827" y1="0.2667" x2="1.50368125" y2="0.27508125" layer="94"/>
<rectangle x1="2.1971" y1="0.2667" x2="2.41808125" y2="0.27508125" layer="94"/>
<rectangle x1="2.56031875" y1="0.2667" x2="2.7813" y2="0.27508125" layer="94"/>
<rectangle x1="3.47471875" y1="0.2667" x2="3.6957" y2="0.27508125" layer="94"/>
<rectangle x1="3.84048125" y1="0.2667" x2="4.05891875" y2="0.27508125" layer="94"/>
<rectangle x1="4.75488125" y1="0.2667" x2="4.97331875" y2="0.27508125" layer="94"/>
<rectangle x1="5.56768125" y1="0.2667" x2="5.78611875" y2="0.27508125" layer="94"/>
<rectangle x1="6.54811875" y1="0.2667" x2="6.7691" y2="0.27508125" layer="94"/>
<rectangle x1="7.10691875" y1="0.2667" x2="7.3279" y2="0.27508125" layer="94"/>
<rectangle x1="9.25068125" y1="0.2667" x2="9.47928125" y2="0.27508125" layer="94"/>
<rectangle x1="0.0127" y1="0.27508125" x2="0.23368125" y2="0.28371875" layer="94"/>
<rectangle x1="0.6477" y1="0.27508125" x2="0.89408125" y2="0.28371875" layer="94"/>
<rectangle x1="1.2827" y1="0.27508125" x2="1.50368125" y2="0.28371875" layer="94"/>
<rectangle x1="2.1971" y1="0.27508125" x2="2.41808125" y2="0.28371875" layer="94"/>
<rectangle x1="2.56031875" y1="0.27508125" x2="2.7813" y2="0.28371875" layer="94"/>
<rectangle x1="3.45948125" y1="0.27508125" x2="3.6957" y2="0.28371875" layer="94"/>
<rectangle x1="3.84048125" y1="0.27508125" x2="4.05891875" y2="0.28371875" layer="94"/>
<rectangle x1="4.75488125" y1="0.27508125" x2="4.97331875" y2="0.28371875" layer="94"/>
<rectangle x1="5.56768125" y1="0.27508125" x2="5.78611875" y2="0.28371875" layer="94"/>
<rectangle x1="6.54811875" y1="0.27508125" x2="6.7691" y2="0.28371875" layer="94"/>
<rectangle x1="7.10691875" y1="0.27508125" x2="7.3279" y2="0.28371875" layer="94"/>
<rectangle x1="9.24051875" y1="0.27508125" x2="9.47928125" y2="0.28371875" layer="94"/>
<rectangle x1="0.0127" y1="0.28371875" x2="0.23368125" y2="0.2921" layer="94"/>
<rectangle x1="0.64008125" y1="0.28371875" x2="0.88391875" y2="0.2921" layer="94"/>
<rectangle x1="1.2827" y1="0.28371875" x2="1.50368125" y2="0.2921" layer="94"/>
<rectangle x1="2.1971" y1="0.28371875" x2="2.41808125" y2="0.2921" layer="94"/>
<rectangle x1="2.56031875" y1="0.28371875" x2="2.7813" y2="0.2921" layer="94"/>
<rectangle x1="3.4417" y1="0.28371875" x2="3.68808125" y2="0.2921" layer="94"/>
<rectangle x1="3.84048125" y1="0.28371875" x2="4.05891875" y2="0.2921" layer="94"/>
<rectangle x1="4.75488125" y1="0.28371875" x2="4.97331875" y2="0.2921" layer="94"/>
<rectangle x1="5.56768125" y1="0.28371875" x2="5.78611875" y2="0.2921" layer="94"/>
<rectangle x1="6.54811875" y1="0.28371875" x2="6.7691" y2="0.2921" layer="94"/>
<rectangle x1="7.10691875" y1="0.28371875" x2="7.3279" y2="0.2921" layer="94"/>
<rectangle x1="9.21511875" y1="0.28371875" x2="9.46911875" y2="0.2921" layer="94"/>
<rectangle x1="0.0127" y1="0.2921" x2="0.23368125" y2="0.30048125" layer="94"/>
<rectangle x1="0.62991875" y1="0.2921" x2="0.8763" y2="0.30048125" layer="94"/>
<rectangle x1="1.2827" y1="0.2921" x2="1.50368125" y2="0.30048125" layer="94"/>
<rectangle x1="2.1971" y1="0.2921" x2="2.41808125" y2="0.30048125" layer="94"/>
<rectangle x1="2.56031875" y1="0.2921" x2="2.7813" y2="0.30048125" layer="94"/>
<rectangle x1="3.4163" y1="0.2921" x2="3.68808125" y2="0.30048125" layer="94"/>
<rectangle x1="3.84048125" y1="0.2921" x2="4.05891875" y2="0.30048125" layer="94"/>
<rectangle x1="4.75488125" y1="0.2921" x2="4.97331875" y2="0.30048125" layer="94"/>
<rectangle x1="5.56768125" y1="0.2921" x2="5.78611875" y2="0.30048125" layer="94"/>
<rectangle x1="6.54811875" y1="0.2921" x2="6.7691" y2="0.30048125" layer="94"/>
<rectangle x1="7.10691875" y1="0.2921" x2="7.3279" y2="0.30048125" layer="94"/>
<rectangle x1="9.19988125" y1="0.2921" x2="9.46911875" y2="0.30048125" layer="94"/>
<rectangle x1="0.0127" y1="0.30048125" x2="0.88391875" y2="0.30911875" layer="94"/>
<rectangle x1="1.2827" y1="0.30048125" x2="1.50368125" y2="0.30911875" layer="94"/>
<rectangle x1="2.1971" y1="0.30048125" x2="2.41808125" y2="0.30911875" layer="94"/>
<rectangle x1="2.56031875" y1="0.30048125" x2="3.67791875" y2="0.30911875" layer="94"/>
<rectangle x1="3.84048125" y1="0.30048125" x2="4.05891875" y2="0.30911875" layer="94"/>
<rectangle x1="4.75488125" y1="0.30048125" x2="4.97331875" y2="0.30911875" layer="94"/>
<rectangle x1="5.56768125" y1="0.30048125" x2="5.78611875" y2="0.30911875" layer="94"/>
<rectangle x1="6.54811875" y1="0.30048125" x2="6.7691" y2="0.30911875" layer="94"/>
<rectangle x1="7.10691875" y1="0.30048125" x2="7.3279" y2="0.30911875" layer="94"/>
<rectangle x1="8.5725" y1="0.30048125" x2="9.4615" y2="0.30911875" layer="94"/>
<rectangle x1="0.0127" y1="0.30911875" x2="0.94488125" y2="0.3175" layer="94"/>
<rectangle x1="1.2827" y1="0.30911875" x2="1.50368125" y2="0.3175" layer="94"/>
<rectangle x1="2.1971" y1="0.30911875" x2="2.41808125" y2="0.3175" layer="94"/>
<rectangle x1="2.56031875" y1="0.30911875" x2="3.66268125" y2="0.3175" layer="94"/>
<rectangle x1="3.84048125" y1="0.30911875" x2="4.05891875" y2="0.3175" layer="94"/>
<rectangle x1="4.75488125" y1="0.30911875" x2="4.97331875" y2="0.3175" layer="94"/>
<rectangle x1="5.56768125" y1="0.30911875" x2="5.78611875" y2="0.3175" layer="94"/>
<rectangle x1="6.54811875" y1="0.30911875" x2="6.7691" y2="0.3175" layer="94"/>
<rectangle x1="7.10691875" y1="0.30911875" x2="7.3279" y2="0.3175" layer="94"/>
<rectangle x1="8.52931875" y1="0.30911875" x2="9.45388125" y2="0.3175" layer="94"/>
<rectangle x1="0.0127" y1="0.3175" x2="0.9779" y2="0.32588125" layer="94"/>
<rectangle x1="1.2827" y1="0.3175" x2="1.50368125" y2="0.32588125" layer="94"/>
<rectangle x1="2.1971" y1="0.3175" x2="2.41808125" y2="0.32588125" layer="94"/>
<rectangle x1="2.56031875" y1="0.3175" x2="3.65251875" y2="0.32588125" layer="94"/>
<rectangle x1="3.84048125" y1="0.3175" x2="4.05891875" y2="0.32588125" layer="94"/>
<rectangle x1="4.75488125" y1="0.3175" x2="4.97331875" y2="0.32588125" layer="94"/>
<rectangle x1="5.56768125" y1="0.3175" x2="5.78611875" y2="0.32588125" layer="94"/>
<rectangle x1="6.54811875" y1="0.3175" x2="6.7691" y2="0.32588125" layer="94"/>
<rectangle x1="7.10691875" y1="0.3175" x2="7.3279" y2="0.32588125" layer="94"/>
<rectangle x1="8.4963" y1="0.3175" x2="9.44371875" y2="0.32588125" layer="94"/>
<rectangle x1="0.0127" y1="0.32588125" x2="1.01091875" y2="0.33451875" layer="94"/>
<rectangle x1="1.2827" y1="0.32588125" x2="1.50368125" y2="0.33451875" layer="94"/>
<rectangle x1="2.1971" y1="0.32588125" x2="2.41808125" y2="0.33451875" layer="94"/>
<rectangle x1="2.56031875" y1="0.32588125" x2="3.6449" y2="0.33451875" layer="94"/>
<rectangle x1="3.84048125" y1="0.32588125" x2="4.05891875" y2="0.33451875" layer="94"/>
<rectangle x1="4.75488125" y1="0.32588125" x2="4.97331875" y2="0.33451875" layer="94"/>
<rectangle x1="5.56768125" y1="0.32588125" x2="5.78611875" y2="0.33451875" layer="94"/>
<rectangle x1="6.54811875" y1="0.32588125" x2="6.7691" y2="0.33451875" layer="94"/>
<rectangle x1="7.10691875" y1="0.32588125" x2="7.3279" y2="0.33451875" layer="94"/>
<rectangle x1="8.4709" y1="0.32588125" x2="9.42848125" y2="0.33451875" layer="94"/>
<rectangle x1="0.0127" y1="0.33451875" x2="1.04648125" y2="0.3429" layer="94"/>
<rectangle x1="1.2827" y1="0.33451875" x2="1.50368125" y2="0.3429" layer="94"/>
<rectangle x1="2.1971" y1="0.33451875" x2="2.41808125" y2="0.3429" layer="94"/>
<rectangle x1="2.56031875" y1="0.33451875" x2="3.6195" y2="0.3429" layer="94"/>
<rectangle x1="3.84048125" y1="0.33451875" x2="4.05891875" y2="0.3429" layer="94"/>
<rectangle x1="4.75488125" y1="0.33451875" x2="4.97331875" y2="0.3429" layer="94"/>
<rectangle x1="5.56768125" y1="0.33451875" x2="5.78611875" y2="0.3429" layer="94"/>
<rectangle x1="6.54811875" y1="0.33451875" x2="6.7691" y2="0.3429" layer="94"/>
<rectangle x1="7.10691875" y1="0.33451875" x2="7.3279" y2="0.3429" layer="94"/>
<rectangle x1="8.45311875" y1="0.33451875" x2="9.41831875" y2="0.3429" layer="94"/>
<rectangle x1="0.0127" y1="0.3429" x2="1.06171875" y2="0.35128125" layer="94"/>
<rectangle x1="1.2827" y1="0.3429" x2="1.50368125" y2="0.35128125" layer="94"/>
<rectangle x1="2.1971" y1="0.3429" x2="2.41808125" y2="0.35128125" layer="94"/>
<rectangle x1="2.56031875" y1="0.3429" x2="3.58648125" y2="0.35128125" layer="94"/>
<rectangle x1="3.84048125" y1="0.3429" x2="4.05891875" y2="0.35128125" layer="94"/>
<rectangle x1="4.75488125" y1="0.3429" x2="4.97331875" y2="0.35128125" layer="94"/>
<rectangle x1="5.56768125" y1="0.3429" x2="5.78611875" y2="0.35128125" layer="94"/>
<rectangle x1="6.54811875" y1="0.3429" x2="6.7691" y2="0.35128125" layer="94"/>
<rectangle x1="7.10691875" y1="0.3429" x2="7.3279" y2="0.35128125" layer="94"/>
<rectangle x1="8.43788125" y1="0.3429" x2="9.40308125" y2="0.35128125" layer="94"/>
<rectangle x1="0.0127" y1="0.35128125" x2="1.07188125" y2="0.35991875" layer="94"/>
<rectangle x1="1.2827" y1="0.35128125" x2="1.50368125" y2="0.35991875" layer="94"/>
<rectangle x1="2.1971" y1="0.35128125" x2="2.41808125" y2="0.35991875" layer="94"/>
<rectangle x1="2.56031875" y1="0.35128125" x2="3.60171875" y2="0.35991875" layer="94"/>
<rectangle x1="3.84048125" y1="0.35128125" x2="4.05891875" y2="0.35991875" layer="94"/>
<rectangle x1="4.75488125" y1="0.35128125" x2="4.97331875" y2="0.35991875" layer="94"/>
<rectangle x1="5.56768125" y1="0.35128125" x2="5.78611875" y2="0.35991875" layer="94"/>
<rectangle x1="6.54811875" y1="0.35128125" x2="6.7691" y2="0.35991875" layer="94"/>
<rectangle x1="7.10691875" y1="0.35128125" x2="7.3279" y2="0.35991875" layer="94"/>
<rectangle x1="8.41248125" y1="0.35128125" x2="9.3853" y2="0.35991875" layer="94"/>
<rectangle x1="0.0127" y1="0.35991875" x2="1.08711875" y2="0.3683" layer="94"/>
<rectangle x1="1.2827" y1="0.35991875" x2="1.50368125" y2="0.3683" layer="94"/>
<rectangle x1="2.1971" y1="0.35991875" x2="2.41808125" y2="0.3683" layer="94"/>
<rectangle x1="2.56031875" y1="0.35991875" x2="3.62711875" y2="0.3683" layer="94"/>
<rectangle x1="3.84048125" y1="0.35991875" x2="4.05891875" y2="0.3683" layer="94"/>
<rectangle x1="4.75488125" y1="0.35991875" x2="4.97331875" y2="0.3683" layer="94"/>
<rectangle x1="5.56768125" y1="0.35991875" x2="5.78611875" y2="0.3683" layer="94"/>
<rectangle x1="6.54811875" y1="0.35991875" x2="6.7691" y2="0.3683" layer="94"/>
<rectangle x1="7.10691875" y1="0.35991875" x2="7.3279" y2="0.3683" layer="94"/>
<rectangle x1="8.40231875" y1="0.35991875" x2="9.3599" y2="0.3683" layer="94"/>
<rectangle x1="0.0127" y1="0.3683" x2="1.09728125" y2="0.37668125" layer="94"/>
<rectangle x1="1.2827" y1="0.3683" x2="1.50368125" y2="0.37668125" layer="94"/>
<rectangle x1="2.1971" y1="0.3683" x2="2.41808125" y2="0.37668125" layer="94"/>
<rectangle x1="2.56031875" y1="0.3683" x2="3.6449" y2="0.37668125" layer="94"/>
<rectangle x1="3.84048125" y1="0.3683" x2="4.05891875" y2="0.37668125" layer="94"/>
<rectangle x1="4.75488125" y1="0.3683" x2="4.97331875" y2="0.37668125" layer="94"/>
<rectangle x1="5.56768125" y1="0.3683" x2="5.78611875" y2="0.37668125" layer="94"/>
<rectangle x1="6.54811875" y1="0.3683" x2="6.7691" y2="0.37668125" layer="94"/>
<rectangle x1="7.10691875" y1="0.3683" x2="7.3279" y2="0.37668125" layer="94"/>
<rectangle x1="8.38708125" y1="0.3683" x2="9.34211875" y2="0.37668125" layer="94"/>
<rectangle x1="0.0127" y1="0.37668125" x2="1.11251875" y2="0.38531875" layer="94"/>
<rectangle x1="1.2827" y1="0.37668125" x2="1.50368125" y2="0.38531875" layer="94"/>
<rectangle x1="2.1971" y1="0.37668125" x2="2.41808125" y2="0.38531875" layer="94"/>
<rectangle x1="2.56031875" y1="0.37668125" x2="3.65251875" y2="0.38531875" layer="94"/>
<rectangle x1="3.84048125" y1="0.37668125" x2="4.05891875" y2="0.38531875" layer="94"/>
<rectangle x1="4.75488125" y1="0.37668125" x2="4.97331875" y2="0.38531875" layer="94"/>
<rectangle x1="5.56768125" y1="0.37668125" x2="5.78611875" y2="0.38531875" layer="94"/>
<rectangle x1="6.54811875" y1="0.37668125" x2="6.7691" y2="0.38531875" layer="94"/>
<rectangle x1="7.10691875" y1="0.37668125" x2="7.3279" y2="0.38531875" layer="94"/>
<rectangle x1="8.3693" y1="0.37668125" x2="9.31671875" y2="0.38531875" layer="94"/>
<rectangle x1="0.0127" y1="0.38531875" x2="1.12268125" y2="0.3937" layer="94"/>
<rectangle x1="1.2827" y1="0.38531875" x2="1.50368125" y2="0.3937" layer="94"/>
<rectangle x1="2.1971" y1="0.38531875" x2="2.41808125" y2="0.3937" layer="94"/>
<rectangle x1="2.56031875" y1="0.38531875" x2="3.66268125" y2="0.3937" layer="94"/>
<rectangle x1="3.84048125" y1="0.38531875" x2="4.05891875" y2="0.3937" layer="94"/>
<rectangle x1="4.75488125" y1="0.38531875" x2="4.97331875" y2="0.3937" layer="94"/>
<rectangle x1="5.56768125" y1="0.38531875" x2="5.78611875" y2="0.3937" layer="94"/>
<rectangle x1="6.54811875" y1="0.38531875" x2="6.7691" y2="0.3937" layer="94"/>
<rectangle x1="7.10691875" y1="0.38531875" x2="7.3279" y2="0.3937" layer="94"/>
<rectangle x1="8.36168125" y1="0.38531875" x2="9.2837" y2="0.3937" layer="94"/>
<rectangle x1="0.0127" y1="0.3937" x2="1.1303" y2="0.40208125" layer="94"/>
<rectangle x1="1.2827" y1="0.3937" x2="1.50368125" y2="0.40208125" layer="94"/>
<rectangle x1="2.1971" y1="0.3937" x2="2.41808125" y2="0.40208125" layer="94"/>
<rectangle x1="2.56031875" y1="0.3937" x2="3.67791875" y2="0.40208125" layer="94"/>
<rectangle x1="3.84048125" y1="0.3937" x2="4.05891875" y2="0.40208125" layer="94"/>
<rectangle x1="4.75488125" y1="0.3937" x2="4.97331875" y2="0.40208125" layer="94"/>
<rectangle x1="5.56768125" y1="0.3937" x2="5.78611875" y2="0.40208125" layer="94"/>
<rectangle x1="6.54811875" y1="0.3937" x2="6.7691" y2="0.40208125" layer="94"/>
<rectangle x1="7.10691875" y1="0.3937" x2="7.3279" y2="0.40208125" layer="94"/>
<rectangle x1="8.35151875" y1="0.3937" x2="9.24051875" y2="0.40208125" layer="94"/>
<rectangle x1="0.0127" y1="0.40208125" x2="0.23368125" y2="0.41071875" layer="94"/>
<rectangle x1="0.86868125" y1="0.40208125" x2="1.13791875" y2="0.41071875" layer="94"/>
<rectangle x1="1.2827" y1="0.40208125" x2="1.50368125" y2="0.41071875" layer="94"/>
<rectangle x1="2.1971" y1="0.40208125" x2="2.41808125" y2="0.41071875" layer="94"/>
<rectangle x1="2.56031875" y1="0.40208125" x2="2.7813" y2="0.41071875" layer="94"/>
<rectangle x1="3.4163" y1="0.40208125" x2="3.68808125" y2="0.41071875" layer="94"/>
<rectangle x1="3.84048125" y1="0.40208125" x2="4.05891875" y2="0.41071875" layer="94"/>
<rectangle x1="4.75488125" y1="0.40208125" x2="4.97331875" y2="0.41071875" layer="94"/>
<rectangle x1="5.56768125" y1="0.40208125" x2="5.78611875" y2="0.41071875" layer="94"/>
<rectangle x1="6.54811875" y1="0.40208125" x2="6.7691" y2="0.41071875" layer="94"/>
<rectangle x1="7.10691875" y1="0.40208125" x2="7.3279" y2="0.41071875" layer="94"/>
<rectangle x1="8.10768125" y1="0.40208125" x2="8.1661" y2="0.41071875" layer="94"/>
<rectangle x1="8.3439" y1="0.40208125" x2="8.61568125" y2="0.41071875" layer="94"/>
<rectangle x1="0.0127" y1="0.41071875" x2="0.23368125" y2="0.4191" layer="94"/>
<rectangle x1="0.9017" y1="0.41071875" x2="1.14808125" y2="0.4191" layer="94"/>
<rectangle x1="1.2827" y1="0.41071875" x2="1.50368125" y2="0.4191" layer="94"/>
<rectangle x1="2.1971" y1="0.41071875" x2="2.41808125" y2="0.4191" layer="94"/>
<rectangle x1="2.56031875" y1="0.41071875" x2="2.7813" y2="0.4191" layer="94"/>
<rectangle x1="3.44931875" y1="0.41071875" x2="3.68808125" y2="0.4191" layer="94"/>
<rectangle x1="3.84048125" y1="0.41071875" x2="4.05891875" y2="0.4191" layer="94"/>
<rectangle x1="4.75488125" y1="0.41071875" x2="4.97331875" y2="0.4191" layer="94"/>
<rectangle x1="5.56768125" y1="0.41071875" x2="5.78611875" y2="0.4191" layer="94"/>
<rectangle x1="6.54811875" y1="0.41071875" x2="6.7691" y2="0.4191" layer="94"/>
<rectangle x1="7.10691875" y1="0.41071875" x2="7.3279" y2="0.4191" layer="94"/>
<rectangle x1="8.0645" y1="0.41071875" x2="8.20928125" y2="0.4191" layer="94"/>
<rectangle x1="8.3439" y1="0.41071875" x2="8.59028125" y2="0.4191" layer="94"/>
<rectangle x1="0.0127" y1="0.4191" x2="0.23368125" y2="0.42748125" layer="94"/>
<rectangle x1="0.91948125" y1="0.4191" x2="1.14808125" y2="0.42748125" layer="94"/>
<rectangle x1="1.2827" y1="0.4191" x2="1.50368125" y2="0.42748125" layer="94"/>
<rectangle x1="2.1971" y1="0.4191" x2="2.41808125" y2="0.42748125" layer="94"/>
<rectangle x1="2.56031875" y1="0.4191" x2="2.7813" y2="0.42748125" layer="94"/>
<rectangle x1="3.4671" y1="0.4191" x2="3.6957" y2="0.42748125" layer="94"/>
<rectangle x1="3.84048125" y1="0.4191" x2="4.05891875" y2="0.42748125" layer="94"/>
<rectangle x1="4.75488125" y1="0.4191" x2="4.97331875" y2="0.42748125" layer="94"/>
<rectangle x1="5.56768125" y1="0.4191" x2="5.78611875" y2="0.42748125" layer="94"/>
<rectangle x1="6.54811875" y1="0.4191" x2="6.7691" y2="0.42748125" layer="94"/>
<rectangle x1="7.10691875" y1="0.4191" x2="7.3279" y2="0.42748125" layer="94"/>
<rectangle x1="8.04671875" y1="0.4191" x2="8.22451875" y2="0.42748125" layer="94"/>
<rectangle x1="8.3439" y1="0.4191" x2="8.5725" y2="0.42748125" layer="94"/>
<rectangle x1="0.0127" y1="0.42748125" x2="0.23368125" y2="0.43611875" layer="94"/>
<rectangle x1="0.9271" y1="0.42748125" x2="1.14808125" y2="0.43611875" layer="94"/>
<rectangle x1="1.2827" y1="0.42748125" x2="1.50368125" y2="0.43611875" layer="94"/>
<rectangle x1="2.1971" y1="0.42748125" x2="2.41808125" y2="0.43611875" layer="94"/>
<rectangle x1="2.56031875" y1="0.42748125" x2="2.7813" y2="0.43611875" layer="94"/>
<rectangle x1="3.47471875" y1="0.42748125" x2="3.6957" y2="0.43611875" layer="94"/>
<rectangle x1="3.84048125" y1="0.42748125" x2="4.05891875" y2="0.43611875" layer="94"/>
<rectangle x1="4.75488125" y1="0.42748125" x2="4.97331875" y2="0.43611875" layer="94"/>
<rectangle x1="5.56768125" y1="0.42748125" x2="5.78611875" y2="0.43611875" layer="94"/>
<rectangle x1="6.54811875" y1="0.42748125" x2="6.7691" y2="0.43611875" layer="94"/>
<rectangle x1="7.10691875" y1="0.42748125" x2="7.3279" y2="0.43611875" layer="94"/>
<rectangle x1="8.03148125" y1="0.42748125" x2="8.2423" y2="0.43611875" layer="94"/>
<rectangle x1="8.3439" y1="0.42748125" x2="8.56488125" y2="0.43611875" layer="94"/>
<rectangle x1="0.0127" y1="0.43611875" x2="0.23368125" y2="0.4445" layer="94"/>
<rectangle x1="0.9271" y1="0.43611875" x2="1.14808125" y2="0.4445" layer="94"/>
<rectangle x1="1.2827" y1="0.43611875" x2="1.50368125" y2="0.4445" layer="94"/>
<rectangle x1="2.1971" y1="0.43611875" x2="2.41808125" y2="0.4445" layer="94"/>
<rectangle x1="2.56031875" y1="0.43611875" x2="2.7813" y2="0.4445" layer="94"/>
<rectangle x1="3.47471875" y1="0.43611875" x2="3.6957" y2="0.4445" layer="94"/>
<rectangle x1="3.84048125" y1="0.43611875" x2="4.05891875" y2="0.4445" layer="94"/>
<rectangle x1="4.75488125" y1="0.43611875" x2="4.97331875" y2="0.4445" layer="94"/>
<rectangle x1="5.56768125" y1="0.43611875" x2="5.78611875" y2="0.4445" layer="94"/>
<rectangle x1="6.54811875" y1="0.43611875" x2="6.7691" y2="0.4445" layer="94"/>
<rectangle x1="7.10691875" y1="0.43611875" x2="7.3279" y2="0.4445" layer="94"/>
<rectangle x1="8.02131875" y1="0.43611875" x2="8.2423" y2="0.4445" layer="94"/>
<rectangle x1="8.3439" y1="0.43611875" x2="8.56488125" y2="0.4445" layer="94"/>
<rectangle x1="0.0127" y1="0.4445" x2="0.23368125" y2="0.45288125" layer="94"/>
<rectangle x1="0.9271" y1="0.4445" x2="1.14808125" y2="0.45288125" layer="94"/>
<rectangle x1="1.2827" y1="0.4445" x2="1.50368125" y2="0.45288125" layer="94"/>
<rectangle x1="2.1971" y1="0.4445" x2="2.41808125" y2="0.45288125" layer="94"/>
<rectangle x1="2.56031875" y1="0.4445" x2="2.7813" y2="0.45288125" layer="94"/>
<rectangle x1="3.47471875" y1="0.4445" x2="3.6957" y2="0.45288125" layer="94"/>
<rectangle x1="3.84048125" y1="0.4445" x2="4.05891875" y2="0.45288125" layer="94"/>
<rectangle x1="4.75488125" y1="0.4445" x2="4.97331875" y2="0.45288125" layer="94"/>
<rectangle x1="5.56768125" y1="0.4445" x2="5.78611875" y2="0.45288125" layer="94"/>
<rectangle x1="6.54811875" y1="0.4445" x2="6.7691" y2="0.45288125" layer="94"/>
<rectangle x1="7.10691875" y1="0.4445" x2="7.3279" y2="0.45288125" layer="94"/>
<rectangle x1="8.02131875" y1="0.4445" x2="8.2423" y2="0.45288125" layer="94"/>
<rectangle x1="8.3439" y1="0.4445" x2="8.56488125" y2="0.45288125" layer="94"/>
<rectangle x1="0.0127" y1="0.45288125" x2="0.23368125" y2="0.46151875" layer="94"/>
<rectangle x1="0.9271" y1="0.45288125" x2="1.14808125" y2="0.46151875" layer="94"/>
<rectangle x1="1.2827" y1="0.45288125" x2="1.50368125" y2="0.46151875" layer="94"/>
<rectangle x1="2.1971" y1="0.45288125" x2="2.41808125" y2="0.46151875" layer="94"/>
<rectangle x1="2.56031875" y1="0.45288125" x2="2.7813" y2="0.46151875" layer="94"/>
<rectangle x1="3.47471875" y1="0.45288125" x2="3.6957" y2="0.46151875" layer="94"/>
<rectangle x1="3.84048125" y1="0.45288125" x2="4.05891875" y2="0.46151875" layer="94"/>
<rectangle x1="4.75488125" y1="0.45288125" x2="4.97331875" y2="0.46151875" layer="94"/>
<rectangle x1="5.56768125" y1="0.45288125" x2="5.78611875" y2="0.46151875" layer="94"/>
<rectangle x1="6.54811875" y1="0.45288125" x2="6.7691" y2="0.46151875" layer="94"/>
<rectangle x1="7.10691875" y1="0.45288125" x2="7.3279" y2="0.46151875" layer="94"/>
<rectangle x1="8.02131875" y1="0.45288125" x2="8.2423" y2="0.46151875" layer="94"/>
<rectangle x1="8.3439" y1="0.45288125" x2="8.56488125" y2="0.46151875" layer="94"/>
<rectangle x1="0.0127" y1="0.46151875" x2="0.23368125" y2="0.4699" layer="94"/>
<rectangle x1="0.9271" y1="0.46151875" x2="1.14808125" y2="0.4699" layer="94"/>
<rectangle x1="1.2827" y1="0.46151875" x2="1.50368125" y2="0.4699" layer="94"/>
<rectangle x1="2.1971" y1="0.46151875" x2="2.41808125" y2="0.4699" layer="94"/>
<rectangle x1="2.56031875" y1="0.46151875" x2="2.7813" y2="0.4699" layer="94"/>
<rectangle x1="3.47471875" y1="0.46151875" x2="3.6957" y2="0.4699" layer="94"/>
<rectangle x1="3.84048125" y1="0.46151875" x2="4.05891875" y2="0.4699" layer="94"/>
<rectangle x1="4.75488125" y1="0.46151875" x2="4.97331875" y2="0.4699" layer="94"/>
<rectangle x1="5.56768125" y1="0.46151875" x2="5.78611875" y2="0.4699" layer="94"/>
<rectangle x1="6.54811875" y1="0.46151875" x2="6.7691" y2="0.4699" layer="94"/>
<rectangle x1="7.10691875" y1="0.46151875" x2="7.3279" y2="0.4699" layer="94"/>
<rectangle x1="8.02131875" y1="0.46151875" x2="8.2423" y2="0.4699" layer="94"/>
<rectangle x1="8.3439" y1="0.46151875" x2="8.56488125" y2="0.4699" layer="94"/>
<rectangle x1="0.0127" y1="0.4699" x2="0.23368125" y2="0.47828125" layer="94"/>
<rectangle x1="0.9271" y1="0.4699" x2="1.14808125" y2="0.47828125" layer="94"/>
<rectangle x1="1.2827" y1="0.4699" x2="1.50368125" y2="0.47828125" layer="94"/>
<rectangle x1="2.1971" y1="0.4699" x2="2.41808125" y2="0.47828125" layer="94"/>
<rectangle x1="2.56031875" y1="0.4699" x2="2.7813" y2="0.47828125" layer="94"/>
<rectangle x1="3.47471875" y1="0.4699" x2="3.6957" y2="0.47828125" layer="94"/>
<rectangle x1="3.84048125" y1="0.4699" x2="4.05891875" y2="0.47828125" layer="94"/>
<rectangle x1="4.75488125" y1="0.4699" x2="4.97331875" y2="0.47828125" layer="94"/>
<rectangle x1="5.56768125" y1="0.4699" x2="5.78611875" y2="0.47828125" layer="94"/>
<rectangle x1="6.54811875" y1="0.4699" x2="6.7691" y2="0.47828125" layer="94"/>
<rectangle x1="7.10691875" y1="0.4699" x2="7.3279" y2="0.47828125" layer="94"/>
<rectangle x1="8.02131875" y1="0.4699" x2="8.2423" y2="0.47828125" layer="94"/>
<rectangle x1="8.3439" y1="0.4699" x2="8.56488125" y2="0.47828125" layer="94"/>
<rectangle x1="0.0127" y1="0.47828125" x2="0.23368125" y2="0.48691875" layer="94"/>
<rectangle x1="0.9271" y1="0.47828125" x2="1.14808125" y2="0.48691875" layer="94"/>
<rectangle x1="1.2827" y1="0.47828125" x2="1.50368125" y2="0.48691875" layer="94"/>
<rectangle x1="2.1971" y1="0.47828125" x2="2.41808125" y2="0.48691875" layer="94"/>
<rectangle x1="2.56031875" y1="0.47828125" x2="2.7813" y2="0.48691875" layer="94"/>
<rectangle x1="3.47471875" y1="0.47828125" x2="3.6957" y2="0.48691875" layer="94"/>
<rectangle x1="3.84048125" y1="0.47828125" x2="4.05891875" y2="0.48691875" layer="94"/>
<rectangle x1="4.75488125" y1="0.47828125" x2="4.97331875" y2="0.48691875" layer="94"/>
<rectangle x1="5.56768125" y1="0.47828125" x2="5.78611875" y2="0.48691875" layer="94"/>
<rectangle x1="6.54811875" y1="0.47828125" x2="6.7691" y2="0.48691875" layer="94"/>
<rectangle x1="7.10691875" y1="0.47828125" x2="7.3279" y2="0.48691875" layer="94"/>
<rectangle x1="8.02131875" y1="0.47828125" x2="8.2423" y2="0.48691875" layer="94"/>
<rectangle x1="8.3439" y1="0.47828125" x2="8.56488125" y2="0.48691875" layer="94"/>
<rectangle x1="0.0127" y1="0.48691875" x2="0.23368125" y2="0.4953" layer="94"/>
<rectangle x1="0.9271" y1="0.48691875" x2="1.14808125" y2="0.4953" layer="94"/>
<rectangle x1="1.2827" y1="0.48691875" x2="1.50368125" y2="0.4953" layer="94"/>
<rectangle x1="2.1971" y1="0.48691875" x2="2.41808125" y2="0.4953" layer="94"/>
<rectangle x1="2.56031875" y1="0.48691875" x2="2.7813" y2="0.4953" layer="94"/>
<rectangle x1="3.47471875" y1="0.48691875" x2="3.6957" y2="0.4953" layer="94"/>
<rectangle x1="3.84048125" y1="0.48691875" x2="4.05891875" y2="0.4953" layer="94"/>
<rectangle x1="4.75488125" y1="0.48691875" x2="4.97331875" y2="0.4953" layer="94"/>
<rectangle x1="5.56768125" y1="0.48691875" x2="5.78611875" y2="0.4953" layer="94"/>
<rectangle x1="6.54811875" y1="0.48691875" x2="6.7691" y2="0.4953" layer="94"/>
<rectangle x1="7.10691875" y1="0.48691875" x2="7.3279" y2="0.4953" layer="94"/>
<rectangle x1="8.02131875" y1="0.48691875" x2="8.2423" y2="0.4953" layer="94"/>
<rectangle x1="8.3439" y1="0.48691875" x2="8.56488125" y2="0.4953" layer="94"/>
<rectangle x1="0.0127" y1="0.4953" x2="0.23368125" y2="0.50368125" layer="94"/>
<rectangle x1="0.9271" y1="0.4953" x2="1.14808125" y2="0.50368125" layer="94"/>
<rectangle x1="1.2827" y1="0.4953" x2="1.50368125" y2="0.50368125" layer="94"/>
<rectangle x1="2.1971" y1="0.4953" x2="2.41808125" y2="0.50368125" layer="94"/>
<rectangle x1="2.56031875" y1="0.4953" x2="2.7813" y2="0.50368125" layer="94"/>
<rectangle x1="3.47471875" y1="0.4953" x2="3.6957" y2="0.50368125" layer="94"/>
<rectangle x1="3.84048125" y1="0.4953" x2="4.05891875" y2="0.50368125" layer="94"/>
<rectangle x1="4.75488125" y1="0.4953" x2="4.97331875" y2="0.50368125" layer="94"/>
<rectangle x1="5.56768125" y1="0.4953" x2="5.78611875" y2="0.50368125" layer="94"/>
<rectangle x1="6.54811875" y1="0.4953" x2="6.7691" y2="0.50368125" layer="94"/>
<rectangle x1="7.10691875" y1="0.4953" x2="7.3279" y2="0.50368125" layer="94"/>
<rectangle x1="8.02131875" y1="0.4953" x2="8.2423" y2="0.50368125" layer="94"/>
<rectangle x1="8.3439" y1="0.4953" x2="8.56488125" y2="0.50368125" layer="94"/>
<rectangle x1="0.0127" y1="0.50368125" x2="0.23368125" y2="0.51231875" layer="94"/>
<rectangle x1="0.9271" y1="0.50368125" x2="1.14808125" y2="0.51231875" layer="94"/>
<rectangle x1="1.2827" y1="0.50368125" x2="1.50368125" y2="0.51231875" layer="94"/>
<rectangle x1="2.1971" y1="0.50368125" x2="2.41808125" y2="0.51231875" layer="94"/>
<rectangle x1="2.56031875" y1="0.50368125" x2="2.7813" y2="0.51231875" layer="94"/>
<rectangle x1="3.47471875" y1="0.50368125" x2="3.6957" y2="0.51231875" layer="94"/>
<rectangle x1="3.84048125" y1="0.50368125" x2="4.05891875" y2="0.51231875" layer="94"/>
<rectangle x1="4.75488125" y1="0.50368125" x2="4.97331875" y2="0.51231875" layer="94"/>
<rectangle x1="5.56768125" y1="0.50368125" x2="5.78611875" y2="0.51231875" layer="94"/>
<rectangle x1="6.54811875" y1="0.50368125" x2="6.7691" y2="0.51231875" layer="94"/>
<rectangle x1="7.10691875" y1="0.50368125" x2="7.3279" y2="0.51231875" layer="94"/>
<rectangle x1="8.02131875" y1="0.50368125" x2="8.2423" y2="0.51231875" layer="94"/>
<rectangle x1="8.3439" y1="0.50368125" x2="8.56488125" y2="0.51231875" layer="94"/>
<rectangle x1="9.31671875" y1="0.50368125" x2="9.4107" y2="0.51231875" layer="94"/>
<rectangle x1="0.0127" y1="0.51231875" x2="0.23368125" y2="0.5207" layer="94"/>
<rectangle x1="0.9271" y1="0.51231875" x2="1.14808125" y2="0.5207" layer="94"/>
<rectangle x1="1.2827" y1="0.51231875" x2="1.50368125" y2="0.5207" layer="94"/>
<rectangle x1="2.1971" y1="0.51231875" x2="2.41808125" y2="0.5207" layer="94"/>
<rectangle x1="2.56031875" y1="0.51231875" x2="2.7813" y2="0.5207" layer="94"/>
<rectangle x1="3.47471875" y1="0.51231875" x2="3.6957" y2="0.5207" layer="94"/>
<rectangle x1="3.84048125" y1="0.51231875" x2="4.05891875" y2="0.5207" layer="94"/>
<rectangle x1="4.75488125" y1="0.51231875" x2="4.97331875" y2="0.5207" layer="94"/>
<rectangle x1="5.56768125" y1="0.51231875" x2="5.78611875" y2="0.5207" layer="94"/>
<rectangle x1="6.54811875" y1="0.51231875" x2="6.7691" y2="0.5207" layer="94"/>
<rectangle x1="7.10691875" y1="0.51231875" x2="7.3279" y2="0.5207" layer="94"/>
<rectangle x1="8.02131875" y1="0.51231875" x2="8.2423" y2="0.5207" layer="94"/>
<rectangle x1="8.3439" y1="0.51231875" x2="8.56488125" y2="0.5207" layer="94"/>
<rectangle x1="9.2837" y1="0.51231875" x2="9.44371875" y2="0.5207" layer="94"/>
<rectangle x1="0.0127" y1="0.5207" x2="0.23368125" y2="0.52908125" layer="94"/>
<rectangle x1="0.9271" y1="0.5207" x2="1.14808125" y2="0.52908125" layer="94"/>
<rectangle x1="1.2827" y1="0.5207" x2="1.50368125" y2="0.52908125" layer="94"/>
<rectangle x1="2.1971" y1="0.5207" x2="2.41808125" y2="0.52908125" layer="94"/>
<rectangle x1="2.56031875" y1="0.5207" x2="2.7813" y2="0.52908125" layer="94"/>
<rectangle x1="3.47471875" y1="0.5207" x2="3.6957" y2="0.52908125" layer="94"/>
<rectangle x1="3.84048125" y1="0.5207" x2="4.05891875" y2="0.52908125" layer="94"/>
<rectangle x1="4.75488125" y1="0.5207" x2="4.97331875" y2="0.52908125" layer="94"/>
<rectangle x1="5.56768125" y1="0.5207" x2="5.78611875" y2="0.52908125" layer="94"/>
<rectangle x1="6.54811875" y1="0.5207" x2="6.7691" y2="0.52908125" layer="94"/>
<rectangle x1="7.10691875" y1="0.5207" x2="7.3279" y2="0.52908125" layer="94"/>
<rectangle x1="8.02131875" y1="0.5207" x2="8.2423" y2="0.52908125" layer="94"/>
<rectangle x1="8.3439" y1="0.5207" x2="8.56488125" y2="0.52908125" layer="94"/>
<rectangle x1="9.27608125" y1="0.5207" x2="9.4615" y2="0.52908125" layer="94"/>
<rectangle x1="0.0127" y1="0.52908125" x2="0.23368125" y2="0.53771875" layer="94"/>
<rectangle x1="0.9271" y1="0.52908125" x2="1.14808125" y2="0.53771875" layer="94"/>
<rectangle x1="1.2827" y1="0.52908125" x2="1.50368125" y2="0.53771875" layer="94"/>
<rectangle x1="2.1971" y1="0.52908125" x2="2.41808125" y2="0.53771875" layer="94"/>
<rectangle x1="2.56031875" y1="0.52908125" x2="2.7813" y2="0.53771875" layer="94"/>
<rectangle x1="3.47471875" y1="0.52908125" x2="3.6957" y2="0.53771875" layer="94"/>
<rectangle x1="3.84048125" y1="0.52908125" x2="4.05891875" y2="0.53771875" layer="94"/>
<rectangle x1="4.75488125" y1="0.52908125" x2="4.97331875" y2="0.53771875" layer="94"/>
<rectangle x1="5.56768125" y1="0.52908125" x2="5.78611875" y2="0.53771875" layer="94"/>
<rectangle x1="6.54811875" y1="0.52908125" x2="6.7691" y2="0.53771875" layer="94"/>
<rectangle x1="7.10691875" y1="0.52908125" x2="7.3279" y2="0.53771875" layer="94"/>
<rectangle x1="8.02131875" y1="0.52908125" x2="8.2423" y2="0.53771875" layer="94"/>
<rectangle x1="8.3439" y1="0.52908125" x2="8.56488125" y2="0.53771875" layer="94"/>
<rectangle x1="9.26591875" y1="0.52908125" x2="9.46911875" y2="0.53771875" layer="94"/>
<rectangle x1="0.0127" y1="0.53771875" x2="0.23368125" y2="0.5461" layer="94"/>
<rectangle x1="0.9271" y1="0.53771875" x2="1.14808125" y2="0.5461" layer="94"/>
<rectangle x1="1.2827" y1="0.53771875" x2="1.50368125" y2="0.5461" layer="94"/>
<rectangle x1="2.1971" y1="0.53771875" x2="2.41808125" y2="0.5461" layer="94"/>
<rectangle x1="2.56031875" y1="0.53771875" x2="2.7813" y2="0.5461" layer="94"/>
<rectangle x1="3.47471875" y1="0.53771875" x2="3.6957" y2="0.5461" layer="94"/>
<rectangle x1="3.84048125" y1="0.53771875" x2="4.05891875" y2="0.5461" layer="94"/>
<rectangle x1="4.75488125" y1="0.53771875" x2="4.97331875" y2="0.5461" layer="94"/>
<rectangle x1="5.56768125" y1="0.53771875" x2="5.78611875" y2="0.5461" layer="94"/>
<rectangle x1="6.54811875" y1="0.53771875" x2="6.7691" y2="0.5461" layer="94"/>
<rectangle x1="7.10691875" y1="0.53771875" x2="7.3279" y2="0.5461" layer="94"/>
<rectangle x1="8.02131875" y1="0.53771875" x2="8.2423" y2="0.5461" layer="94"/>
<rectangle x1="8.3439" y1="0.53771875" x2="8.56488125" y2="0.5461" layer="94"/>
<rectangle x1="9.2583" y1="0.53771875" x2="9.47928125" y2="0.5461" layer="94"/>
<rectangle x1="0.0127" y1="0.5461" x2="0.23368125" y2="0.55448125" layer="94"/>
<rectangle x1="0.9271" y1="0.5461" x2="1.14808125" y2="0.55448125" layer="94"/>
<rectangle x1="1.2827" y1="0.5461" x2="1.50368125" y2="0.55448125" layer="94"/>
<rectangle x1="2.1971" y1="0.5461" x2="2.41808125" y2="0.55448125" layer="94"/>
<rectangle x1="2.56031875" y1="0.5461" x2="2.7813" y2="0.55448125" layer="94"/>
<rectangle x1="3.47471875" y1="0.5461" x2="3.6957" y2="0.55448125" layer="94"/>
<rectangle x1="3.84048125" y1="0.5461" x2="4.05891875" y2="0.55448125" layer="94"/>
<rectangle x1="4.75488125" y1="0.5461" x2="4.97331875" y2="0.55448125" layer="94"/>
<rectangle x1="5.56768125" y1="0.5461" x2="5.78611875" y2="0.55448125" layer="94"/>
<rectangle x1="6.54811875" y1="0.5461" x2="6.7691" y2="0.55448125" layer="94"/>
<rectangle x1="7.10691875" y1="0.5461" x2="7.3279" y2="0.55448125" layer="94"/>
<rectangle x1="8.02131875" y1="0.5461" x2="8.2423" y2="0.55448125" layer="94"/>
<rectangle x1="8.3439" y1="0.5461" x2="8.56488125" y2="0.55448125" layer="94"/>
<rectangle x1="9.2583" y1="0.5461" x2="9.47928125" y2="0.55448125" layer="94"/>
<rectangle x1="0.0127" y1="0.55448125" x2="0.23368125" y2="0.56311875" layer="94"/>
<rectangle x1="0.9271" y1="0.55448125" x2="1.14808125" y2="0.56311875" layer="94"/>
<rectangle x1="1.2827" y1="0.55448125" x2="1.50368125" y2="0.56311875" layer="94"/>
<rectangle x1="2.1971" y1="0.55448125" x2="2.41808125" y2="0.56311875" layer="94"/>
<rectangle x1="2.56031875" y1="0.55448125" x2="2.7813" y2="0.56311875" layer="94"/>
<rectangle x1="3.47471875" y1="0.55448125" x2="3.6957" y2="0.56311875" layer="94"/>
<rectangle x1="3.84048125" y1="0.55448125" x2="4.05891875" y2="0.56311875" layer="94"/>
<rectangle x1="4.75488125" y1="0.55448125" x2="4.97331875" y2="0.56311875" layer="94"/>
<rectangle x1="5.56768125" y1="0.55448125" x2="5.78611875" y2="0.56311875" layer="94"/>
<rectangle x1="6.54811875" y1="0.55448125" x2="6.7691" y2="0.56311875" layer="94"/>
<rectangle x1="7.10691875" y1="0.55448125" x2="7.3279" y2="0.56311875" layer="94"/>
<rectangle x1="8.02131875" y1="0.55448125" x2="8.2423" y2="0.56311875" layer="94"/>
<rectangle x1="8.3439" y1="0.55448125" x2="8.56488125" y2="0.56311875" layer="94"/>
<rectangle x1="9.2583" y1="0.55448125" x2="9.47928125" y2="0.56311875" layer="94"/>
<rectangle x1="0.0127" y1="0.56311875" x2="0.23368125" y2="0.5715" layer="94"/>
<rectangle x1="0.9271" y1="0.56311875" x2="1.14808125" y2="0.5715" layer="94"/>
<rectangle x1="1.2827" y1="0.56311875" x2="1.50368125" y2="0.5715" layer="94"/>
<rectangle x1="2.1971" y1="0.56311875" x2="2.41808125" y2="0.5715" layer="94"/>
<rectangle x1="2.56031875" y1="0.56311875" x2="2.7813" y2="0.5715" layer="94"/>
<rectangle x1="3.47471875" y1="0.56311875" x2="3.6957" y2="0.5715" layer="94"/>
<rectangle x1="3.84048125" y1="0.56311875" x2="4.05891875" y2="0.5715" layer="94"/>
<rectangle x1="4.75488125" y1="0.56311875" x2="4.97331875" y2="0.5715" layer="94"/>
<rectangle x1="5.56768125" y1="0.56311875" x2="5.78611875" y2="0.5715" layer="94"/>
<rectangle x1="6.54811875" y1="0.56311875" x2="6.7691" y2="0.5715" layer="94"/>
<rectangle x1="7.10691875" y1="0.56311875" x2="7.3279" y2="0.5715" layer="94"/>
<rectangle x1="8.02131875" y1="0.56311875" x2="8.2423" y2="0.5715" layer="94"/>
<rectangle x1="8.3439" y1="0.56311875" x2="8.56488125" y2="0.5715" layer="94"/>
<rectangle x1="9.25068125" y1="0.56311875" x2="9.47928125" y2="0.5715" layer="94"/>
<rectangle x1="0.0127" y1="0.5715" x2="0.23368125" y2="0.57988125" layer="94"/>
<rectangle x1="0.9271" y1="0.5715" x2="1.14808125" y2="0.57988125" layer="94"/>
<rectangle x1="1.2827" y1="0.5715" x2="1.50368125" y2="0.57988125" layer="94"/>
<rectangle x1="2.1971" y1="0.5715" x2="2.41808125" y2="0.57988125" layer="94"/>
<rectangle x1="2.56031875" y1="0.5715" x2="2.7813" y2="0.57988125" layer="94"/>
<rectangle x1="3.47471875" y1="0.5715" x2="3.6957" y2="0.57988125" layer="94"/>
<rectangle x1="3.84048125" y1="0.5715" x2="4.05891875" y2="0.57988125" layer="94"/>
<rectangle x1="4.74471875" y1="0.5715" x2="4.97331875" y2="0.57988125" layer="94"/>
<rectangle x1="5.56768125" y1="0.5715" x2="5.78611875" y2="0.57988125" layer="94"/>
<rectangle x1="6.54811875" y1="0.5715" x2="6.7691" y2="0.57988125" layer="94"/>
<rectangle x1="7.10691875" y1="0.5715" x2="7.3279" y2="0.57988125" layer="94"/>
<rectangle x1="8.02131875" y1="0.5715" x2="8.2423" y2="0.57988125" layer="94"/>
<rectangle x1="8.3439" y1="0.5715" x2="8.56488125" y2="0.57988125" layer="94"/>
<rectangle x1="9.25068125" y1="0.5715" x2="9.47928125" y2="0.57988125" layer="94"/>
<rectangle x1="0.0127" y1="0.57988125" x2="0.23368125" y2="0.58851875" layer="94"/>
<rectangle x1="0.91948125" y1="0.57988125" x2="1.14808125" y2="0.58851875" layer="94"/>
<rectangle x1="1.2827" y1="0.57988125" x2="1.5113" y2="0.58851875" layer="94"/>
<rectangle x1="2.18948125" y1="0.57988125" x2="2.41808125" y2="0.58851875" layer="94"/>
<rectangle x1="2.56031875" y1="0.57988125" x2="2.7813" y2="0.58851875" layer="94"/>
<rectangle x1="3.45948125" y1="0.57988125" x2="3.6957" y2="0.58851875" layer="94"/>
<rectangle x1="3.84048125" y1="0.57988125" x2="4.06908125" y2="0.58851875" layer="94"/>
<rectangle x1="4.7371" y1="0.57988125" x2="4.97331875" y2="0.58851875" layer="94"/>
<rectangle x1="5.56768125" y1="0.57988125" x2="5.78611875" y2="0.58851875" layer="94"/>
<rectangle x1="6.54811875" y1="0.57988125" x2="6.7691" y2="0.58851875" layer="94"/>
<rectangle x1="7.10691875" y1="0.57988125" x2="7.34568125" y2="0.58851875" layer="94"/>
<rectangle x1="8.0137" y1="0.57988125" x2="8.2423" y2="0.58851875" layer="94"/>
<rectangle x1="8.3439" y1="0.57988125" x2="8.5725" y2="0.58851875" layer="94"/>
<rectangle x1="9.24051875" y1="0.57988125" x2="9.47928125" y2="0.58851875" layer="94"/>
<rectangle x1="0.0127" y1="0.58851875" x2="0.23368125" y2="0.5969" layer="94"/>
<rectangle x1="0.89408125" y1="0.58851875" x2="1.14808125" y2="0.5969" layer="94"/>
<rectangle x1="1.2827" y1="0.58851875" x2="1.52908125" y2="0.5969" layer="94"/>
<rectangle x1="2.16408125" y1="0.58851875" x2="2.41808125" y2="0.5969" layer="94"/>
<rectangle x1="2.56031875" y1="0.58851875" x2="2.7813" y2="0.5969" layer="94"/>
<rectangle x1="3.4417" y1="0.58851875" x2="3.6957" y2="0.5969" layer="94"/>
<rectangle x1="3.84048125" y1="0.58851875" x2="4.08431875" y2="0.5969" layer="94"/>
<rectangle x1="4.71931875" y1="0.58851875" x2="4.9657" y2="0.5969" layer="94"/>
<rectangle x1="5.56768125" y1="0.58851875" x2="5.78611875" y2="0.5969" layer="94"/>
<rectangle x1="6.54811875" y1="0.58851875" x2="6.7691" y2="0.5969" layer="94"/>
<rectangle x1="7.10691875" y1="0.58851875" x2="7.36091875" y2="0.5969" layer="94"/>
<rectangle x1="7.99591875" y1="0.58851875" x2="8.2423" y2="0.5969" layer="94"/>
<rectangle x1="8.3439" y1="0.58851875" x2="8.59028125" y2="0.5969" layer="94"/>
<rectangle x1="9.22528125" y1="0.58851875" x2="9.47928125" y2="0.5969" layer="94"/>
<rectangle x1="0.0127" y1="0.5969" x2="0.23368125" y2="0.60528125" layer="94"/>
<rectangle x1="0.86868125" y1="0.5969" x2="1.13791875" y2="0.60528125" layer="94"/>
<rectangle x1="1.2827" y1="0.5969" x2="1.55448125" y2="0.60528125" layer="94"/>
<rectangle x1="2.13868125" y1="0.5969" x2="2.40791875" y2="0.60528125" layer="94"/>
<rectangle x1="2.56031875" y1="0.5969" x2="2.7813" y2="0.60528125" layer="94"/>
<rectangle x1="3.4163" y1="0.5969" x2="3.68808125" y2="0.60528125" layer="94"/>
<rectangle x1="3.84048125" y1="0.5969" x2="4.10971875" y2="0.60528125" layer="94"/>
<rectangle x1="4.69391875" y1="0.5969" x2="4.9657" y2="0.60528125" layer="94"/>
<rectangle x1="5.56768125" y1="0.5969" x2="5.78611875" y2="0.60528125" layer="94"/>
<rectangle x1="6.54811875" y1="0.5969" x2="6.7691" y2="0.60528125" layer="94"/>
<rectangle x1="7.11708125" y1="0.5969" x2="7.3787" y2="0.60528125" layer="94"/>
<rectangle x1="7.97051875" y1="0.5969" x2="8.2423" y2="0.60528125" layer="94"/>
<rectangle x1="8.3439" y1="0.5969" x2="8.61568125" y2="0.60528125" layer="94"/>
<rectangle x1="9.19988125" y1="0.5969" x2="9.46911875" y2="0.60528125" layer="94"/>
<rectangle x1="0.0127" y1="0.60528125" x2="1.13791875" y2="0.61391875" layer="94"/>
<rectangle x1="1.29031875" y1="0.60528125" x2="2.40791875" y2="0.61391875" layer="94"/>
<rectangle x1="2.56031875" y1="0.60528125" x2="3.68808125" y2="0.61391875" layer="94"/>
<rectangle x1="3.8481" y1="0.60528125" x2="4.95808125" y2="0.61391875" layer="94"/>
<rectangle x1="5.17651875" y1="0.60528125" x2="6.1849" y2="0.61391875" layer="94"/>
<rectangle x1="6.38048125" y1="0.60528125" x2="6.92911875" y2="0.61391875" layer="94"/>
<rectangle x1="7.1247" y1="0.60528125" x2="8.23468125" y2="0.61391875" layer="94"/>
<rectangle x1="8.35151875" y1="0.60528125" x2="9.4615" y2="0.61391875" layer="94"/>
<rectangle x1="0.0127" y1="0.61391875" x2="1.1303" y2="0.6223" layer="94"/>
<rectangle x1="1.30048125" y1="0.61391875" x2="2.4003" y2="0.6223" layer="94"/>
<rectangle x1="2.56031875" y1="0.61391875" x2="3.67791875" y2="0.6223" layer="94"/>
<rectangle x1="3.85571875" y1="0.61391875" x2="4.94791875" y2="0.6223" layer="94"/>
<rectangle x1="5.15111875" y1="0.61391875" x2="6.2103" y2="0.6223" layer="94"/>
<rectangle x1="6.35508125" y1="0.61391875" x2="6.95451875" y2="0.6223" layer="94"/>
<rectangle x1="7.13231875" y1="0.61391875" x2="8.22451875" y2="0.6223" layer="94"/>
<rectangle x1="8.36168125" y1="0.61391875" x2="9.45388125" y2="0.6223" layer="94"/>
<rectangle x1="0.0127" y1="0.6223" x2="1.12268125" y2="0.63068125" layer="94"/>
<rectangle x1="1.31571875" y1="0.6223" x2="2.38251875" y2="0.63068125" layer="94"/>
<rectangle x1="2.56031875" y1="0.6223" x2="3.66268125" y2="0.63068125" layer="94"/>
<rectangle x1="3.86588125" y1="0.6223" x2="4.9403" y2="0.63068125" layer="94"/>
<rectangle x1="5.13588125" y1="0.6223" x2="6.22808125" y2="0.63068125" layer="94"/>
<rectangle x1="6.34491875" y1="0.6223" x2="6.9723" y2="0.63068125" layer="94"/>
<rectangle x1="7.14248125" y1="0.6223" x2="8.2169" y2="0.63068125" layer="94"/>
<rectangle x1="8.3693" y1="0.6223" x2="9.44371875" y2="0.63068125" layer="94"/>
<rectangle x1="0.0127" y1="0.63068125" x2="1.1049" y2="0.63931875" layer="94"/>
<rectangle x1="1.32588125" y1="0.63068125" x2="2.3749" y2="0.63931875" layer="94"/>
<rectangle x1="2.56031875" y1="0.63068125" x2="3.65251875" y2="0.63931875" layer="94"/>
<rectangle x1="3.88111875" y1="0.63068125" x2="4.93268125" y2="0.63931875" layer="94"/>
<rectangle x1="5.12571875" y1="0.63068125" x2="6.2357" y2="0.63931875" layer="94"/>
<rectangle x1="6.32968125" y1="0.63068125" x2="6.97991875" y2="0.63931875" layer="94"/>
<rectangle x1="7.1501" y1="0.63068125" x2="8.20928125" y2="0.63931875" layer="94"/>
<rectangle x1="8.37691875" y1="0.63068125" x2="9.4361" y2="0.63931875" layer="94"/>
<rectangle x1="0.0127" y1="0.63931875" x2="1.08711875" y2="0.6477" layer="94"/>
<rectangle x1="1.34111875" y1="0.63931875" x2="2.35711875" y2="0.6477" layer="94"/>
<rectangle x1="2.56031875" y1="0.63931875" x2="3.63728125" y2="0.6477" layer="94"/>
<rectangle x1="3.89128125" y1="0.63931875" x2="4.9149" y2="0.6477" layer="94"/>
<rectangle x1="5.1181" y1="0.63931875" x2="6.24331875" y2="0.6477" layer="94"/>
<rectangle x1="6.31951875" y1="0.63931875" x2="6.99008125" y2="0.6477" layer="94"/>
<rectangle x1="7.16788125" y1="0.63931875" x2="8.1915" y2="0.6477" layer="94"/>
<rectangle x1="8.3947" y1="0.63931875" x2="9.41831875" y2="0.6477" layer="94"/>
<rectangle x1="0.0127" y1="0.6477" x2="1.07188125" y2="0.65608125" layer="94"/>
<rectangle x1="1.3589" y1="0.6477" x2="2.34188125" y2="0.65608125" layer="94"/>
<rectangle x1="2.56031875" y1="0.6477" x2="3.6195" y2="0.65608125" layer="94"/>
<rectangle x1="3.90651875" y1="0.6477" x2="4.89711875" y2="0.65608125" layer="94"/>
<rectangle x1="5.1181" y1="0.6477" x2="6.24331875" y2="0.65608125" layer="94"/>
<rectangle x1="6.31951875" y1="0.6477" x2="6.9977" y2="0.65608125" layer="94"/>
<rectangle x1="7.18311875" y1="0.6477" x2="8.17371875" y2="0.65608125" layer="94"/>
<rectangle x1="8.41248125" y1="0.6477" x2="9.40308125" y2="0.65608125" layer="94"/>
<rectangle x1="0.0127" y1="0.65608125" x2="1.0541" y2="0.66471875" layer="94"/>
<rectangle x1="1.37668125" y1="0.65608125" x2="2.3241" y2="0.66471875" layer="94"/>
<rectangle x1="2.56031875" y1="0.65608125" x2="3.60171875" y2="0.66471875" layer="94"/>
<rectangle x1="3.9243" y1="0.65608125" x2="4.88188125" y2="0.66471875" layer="94"/>
<rectangle x1="5.1181" y1="0.65608125" x2="6.24331875" y2="0.66471875" layer="94"/>
<rectangle x1="6.31951875" y1="0.65608125" x2="6.9977" y2="0.66471875" layer="94"/>
<rectangle x1="7.2009" y1="0.65608125" x2="8.15848125" y2="0.66471875" layer="94"/>
<rectangle x1="8.42771875" y1="0.65608125" x2="9.3853" y2="0.66471875" layer="94"/>
<rectangle x1="0.0127" y1="0.66471875" x2="1.03631875" y2="0.6731" layer="94"/>
<rectangle x1="1.39191875" y1="0.66471875" x2="2.30631875" y2="0.6731" layer="94"/>
<rectangle x1="2.56031875" y1="0.66471875" x2="3.58648125" y2="0.6731" layer="94"/>
<rectangle x1="3.9497" y1="0.66471875" x2="4.8641" y2="0.6731" layer="94"/>
<rectangle x1="5.1181" y1="0.66471875" x2="6.24331875" y2="0.6731" layer="94"/>
<rectangle x1="6.31951875" y1="0.66471875" x2="6.9977" y2="0.6731" layer="94"/>
<rectangle x1="7.21868125" y1="0.66471875" x2="8.13308125" y2="0.6731" layer="94"/>
<rectangle x1="8.45311875" y1="0.66471875" x2="9.36751875" y2="0.6731" layer="94"/>
<rectangle x1="0.02031875" y1="0.6731" x2="1.01091875" y2="0.68148125" layer="94"/>
<rectangle x1="1.41731875" y1="0.6731" x2="2.28091875" y2="0.68148125" layer="94"/>
<rectangle x1="2.56031875" y1="0.6731" x2="3.56108125" y2="0.68148125" layer="94"/>
<rectangle x1="3.9751" y1="0.6731" x2="4.8387" y2="0.68148125" layer="94"/>
<rectangle x1="5.12571875" y1="0.6731" x2="6.2357" y2="0.68148125" layer="94"/>
<rectangle x1="6.32968125" y1="0.6731" x2="6.99008125" y2="0.68148125" layer="94"/>
<rectangle x1="7.24408125" y1="0.6731" x2="8.10768125" y2="0.68148125" layer="94"/>
<rectangle x1="8.47851875" y1="0.6731" x2="9.34211875" y2="0.68148125" layer="94"/>
<rectangle x1="0.03048125" y1="0.68148125" x2="0.98551875" y2="0.69011875" layer="94"/>
<rectangle x1="1.44271875" y1="0.68148125" x2="2.25551875" y2="0.69011875" layer="94"/>
<rectangle x1="2.5781" y1="0.68148125" x2="3.53568125" y2="0.69011875" layer="94"/>
<rectangle x1="4.0005" y1="0.68148125" x2="4.8133" y2="0.69011875" layer="94"/>
<rectangle x1="5.13588125" y1="0.68148125" x2="6.22808125" y2="0.69011875" layer="94"/>
<rectangle x1="6.3373" y1="0.68148125" x2="6.9723" y2="0.69011875" layer="94"/>
<rectangle x1="7.26948125" y1="0.68148125" x2="8.08228125" y2="0.69011875" layer="94"/>
<rectangle x1="8.50391875" y1="0.68148125" x2="9.31671875" y2="0.69011875" layer="94"/>
<rectangle x1="0.04571875" y1="0.69011875" x2="0.9525" y2="0.6985" layer="94"/>
<rectangle x1="1.47828125" y1="0.69011875" x2="2.2225" y2="0.6985" layer="94"/>
<rectangle x1="2.59588125" y1="0.69011875" x2="3.50011875" y2="0.6985" layer="94"/>
<rectangle x1="4.0259" y1="0.69011875" x2="4.78028125" y2="0.6985" layer="94"/>
<rectangle x1="5.1435" y1="0.69011875" x2="6.21791875" y2="0.6985" layer="94"/>
<rectangle x1="6.34491875" y1="0.69011875" x2="6.95451875" y2="0.6985" layer="94"/>
<rectangle x1="7.3025" y1="0.69011875" x2="8.05688125" y2="0.6985" layer="94"/>
<rectangle x1="8.52931875" y1="0.69011875" x2="9.29131875" y2="0.6985" layer="94"/>
<rectangle x1="0.08128125" y1="0.6985" x2="0.91948125" y2="0.70688125" layer="94"/>
<rectangle x1="1.5113" y1="0.6985" x2="2.18948125" y2="0.70688125" layer="94"/>
<rectangle x1="2.62128125" y1="0.6985" x2="3.45948125" y2="0.70688125" layer="94"/>
<rectangle x1="4.06908125" y1="0.6985" x2="4.7371" y2="0.70688125" layer="94"/>
<rectangle x1="5.1689" y1="0.6985" x2="6.19251875" y2="0.70688125" layer="94"/>
<rectangle x1="6.37031875" y1="0.6985" x2="6.92911875" y2="0.70688125" layer="94"/>
<rectangle x1="7.34568125" y1="0.6985" x2="8.0137" y2="0.70688125" layer="94"/>
<rectangle x1="8.5725" y1="0.6985" x2="9.25068125" y2="0.70688125" layer="94"/>
<rectangle x1="0.10668125" y1="0.89331875" x2="1.7907" y2="0.9017" layer="94"/>
<rectangle x1="2.07771875" y1="0.89331875" x2="3.14451875" y2="0.9017" layer="94"/>
<rectangle x1="4.2291" y1="0.89331875" x2="4.37388125" y2="0.9017" layer="94"/>
<rectangle x1="5.7531" y1="0.89331875" x2="5.88771875" y2="0.9017" layer="94"/>
<rectangle x1="6.3627" y1="0.89331875" x2="7.29488125" y2="0.9017" layer="94"/>
<rectangle x1="7.7089" y1="0.89331875" x2="7.82828125" y2="0.9017" layer="94"/>
<rectangle x1="9.2583" y1="0.89331875" x2="9.37768125" y2="0.9017" layer="94"/>
<rectangle x1="0.08128125" y1="0.9017" x2="1.8161" y2="0.91008125" layer="94"/>
<rectangle x1="2.05231875" y1="0.9017" x2="3.19531875" y2="0.91008125" layer="94"/>
<rectangle x1="4.2037" y1="0.9017" x2="4.38911875" y2="0.91008125" layer="94"/>
<rectangle x1="5.7277" y1="0.9017" x2="5.92328125" y2="0.91008125" layer="94"/>
<rectangle x1="6.3373" y1="0.9017" x2="7.32028125" y2="0.91008125" layer="94"/>
<rectangle x1="7.66571875" y1="0.9017" x2="7.86891875" y2="0.91008125" layer="94"/>
<rectangle x1="9.21511875" y1="0.9017" x2="9.4107" y2="0.91008125" layer="94"/>
<rectangle x1="0.0635" y1="0.91008125" x2="1.83388125" y2="0.91871875" layer="94"/>
<rectangle x1="2.03708125" y1="0.91008125" x2="3.23088125" y2="0.91871875" layer="94"/>
<rectangle x1="4.1783" y1="0.91008125" x2="4.39928125" y2="0.91871875" layer="94"/>
<rectangle x1="5.72008125" y1="0.91008125" x2="5.93851875" y2="0.91871875" layer="94"/>
<rectangle x1="6.32968125" y1="0.91008125" x2="7.33551875" y2="0.91871875" layer="94"/>
<rectangle x1="7.65048125" y1="0.91008125" x2="7.89431875" y2="0.91871875" layer="94"/>
<rectangle x1="9.19988125" y1="0.91008125" x2="9.4361" y2="0.91871875" layer="94"/>
<rectangle x1="0.05588125" y1="0.91871875" x2="1.84911875" y2="0.9271" layer="94"/>
<rectangle x1="2.02691875" y1="0.91871875" x2="3.2639" y2="0.9271" layer="94"/>
<rectangle x1="4.16051875" y1="0.91871875" x2="4.41451875" y2="0.9271" layer="94"/>
<rectangle x1="5.7023" y1="0.91871875" x2="5.9563" y2="0.9271" layer="94"/>
<rectangle x1="6.3119" y1="0.91871875" x2="7.34568125" y2="0.9271" layer="94"/>
<rectangle x1="7.6327" y1="0.91871875" x2="7.9121" y2="0.9271" layer="94"/>
<rectangle x1="9.1821" y1="0.91871875" x2="9.44371875" y2="0.9271" layer="94"/>
<rectangle x1="0.0381" y1="0.9271" x2="1.8669" y2="0.93548125" layer="94"/>
<rectangle x1="2.01168125" y1="0.9271" x2="3.2893" y2="0.93548125" layer="94"/>
<rectangle x1="4.14528125" y1="0.9271" x2="4.42468125" y2="0.93548125" layer="94"/>
<rectangle x1="5.68451875" y1="0.9271" x2="5.97408125" y2="0.93548125" layer="94"/>
<rectangle x1="6.30428125" y1="0.9271" x2="7.3533" y2="0.93548125" layer="94"/>
<rectangle x1="7.62508125" y1="0.9271" x2="7.92988125" y2="0.93548125" layer="94"/>
<rectangle x1="9.16431875" y1="0.9271" x2="9.4615" y2="0.93548125" layer="94"/>
<rectangle x1="0.03048125" y1="0.93548125" x2="1.87451875" y2="0.94411875" layer="94"/>
<rectangle x1="2.00151875" y1="0.93548125" x2="3.3147" y2="0.94411875" layer="94"/>
<rectangle x1="4.1275" y1="0.93548125" x2="4.43991875" y2="0.94411875" layer="94"/>
<rectangle x1="5.66928125" y1="0.93548125" x2="5.98931875" y2="0.94411875" layer="94"/>
<rectangle x1="6.29411875" y1="0.93548125" x2="7.37108125" y2="0.94411875" layer="94"/>
<rectangle x1="7.61491875" y1="0.93548125" x2="7.9375" y2="0.94411875" layer="94"/>
<rectangle x1="9.1567" y1="0.93548125" x2="9.46911875" y2="0.94411875" layer="94"/>
<rectangle x1="0.0127" y1="0.94411875" x2="1.88468125" y2="0.9525" layer="94"/>
<rectangle x1="1.9939" y1="0.94411875" x2="3.3401" y2="0.9525" layer="94"/>
<rectangle x1="4.11988125" y1="0.94411875" x2="4.4577" y2="0.9525" layer="94"/>
<rectangle x1="5.65911875" y1="0.94411875" x2="5.99948125" y2="0.9525" layer="94"/>
<rectangle x1="6.2865" y1="0.94411875" x2="7.3787" y2="0.9525" layer="94"/>
<rectangle x1="7.6073" y1="0.94411875" x2="7.94511875" y2="0.9525" layer="94"/>
<rectangle x1="9.14908125" y1="0.94411875" x2="9.47928125" y2="0.9525" layer="94"/>
<rectangle x1="0.00508125" y1="0.9525" x2="1.88468125" y2="0.96088125" layer="94"/>
<rectangle x1="1.97611875" y1="0.9525" x2="3.3655" y2="0.96088125" layer="94"/>
<rectangle x1="4.10971875" y1="0.9525" x2="4.46531875" y2="0.96088125" layer="94"/>
<rectangle x1="5.64388125" y1="0.9525" x2="6.0071" y2="0.96088125" layer="94"/>
<rectangle x1="6.27888125" y1="0.9525" x2="7.3787" y2="0.96088125" layer="94"/>
<rectangle x1="7.59968125" y1="0.9525" x2="7.95528125" y2="0.96088125" layer="94"/>
<rectangle x1="9.13891875" y1="0.9525" x2="9.4869" y2="0.96088125" layer="94"/>
<rectangle x1="0.00508125" y1="0.96088125" x2="1.8923" y2="0.96951875" layer="94"/>
<rectangle x1="1.97611875" y1="0.96088125" x2="3.38328125" y2="0.96951875" layer="94"/>
<rectangle x1="4.10971875" y1="0.96088125" x2="4.4831" y2="0.96951875" layer="94"/>
<rectangle x1="5.6261" y1="0.96088125" x2="6.0071" y2="0.96951875" layer="94"/>
<rectangle x1="6.27888125" y1="0.96088125" x2="7.38631875" y2="0.96951875" layer="94"/>
<rectangle x1="7.59968125" y1="0.96088125" x2="7.95528125" y2="0.96951875" layer="94"/>
<rectangle x1="9.1313" y1="0.96088125" x2="9.4869" y2="0.96951875" layer="94"/>
<rectangle x1="-0.00508125" y1="0.96951875" x2="1.8923" y2="0.9779" layer="94"/>
<rectangle x1="1.9685" y1="0.96951875" x2="3.39851875" y2="0.9779" layer="94"/>
<rectangle x1="4.10971875" y1="0.96951875" x2="4.50088125" y2="0.9779" layer="94"/>
<rectangle x1="5.60831875" y1="0.96951875" x2="6.01471875" y2="0.9779" layer="94"/>
<rectangle x1="6.26871875" y1="0.96951875" x2="7.39648125" y2="0.9779" layer="94"/>
<rectangle x1="7.58951875" y1="0.96951875" x2="7.9629" y2="0.9779" layer="94"/>
<rectangle x1="9.12368125" y1="0.96951875" x2="9.49451875" y2="0.9779" layer="94"/>
<rectangle x1="-0.00508125" y1="0.9779" x2="1.89991875" y2="0.98628125" layer="94"/>
<rectangle x1="1.9685" y1="0.9779" x2="3.42391875" y2="0.98628125" layer="94"/>
<rectangle x1="4.10971875" y1="0.9779" x2="4.51611875" y2="0.98628125" layer="94"/>
<rectangle x1="5.6007" y1="0.9779" x2="6.01471875" y2="0.98628125" layer="94"/>
<rectangle x1="6.2611" y1="0.9779" x2="7.39648125" y2="0.98628125" layer="94"/>
<rectangle x1="7.58951875" y1="0.9779" x2="7.9629" y2="0.98628125" layer="94"/>
<rectangle x1="9.11351875" y1="0.9779" x2="9.49451875" y2="0.98628125" layer="94"/>
<rectangle x1="-0.00508125" y1="0.98628125" x2="1.89991875" y2="0.99491875" layer="94"/>
<rectangle x1="1.9685" y1="0.98628125" x2="3.4417" y2="0.99491875" layer="94"/>
<rectangle x1="4.10971875" y1="0.98628125" x2="4.52628125" y2="0.99491875" layer="94"/>
<rectangle x1="5.58291875" y1="0.98628125" x2="6.01471875" y2="0.99491875" layer="94"/>
<rectangle x1="6.2611" y1="0.98628125" x2="7.4041" y2="0.99491875" layer="94"/>
<rectangle x1="7.58951875" y1="0.98628125" x2="7.9629" y2="0.99491875" layer="94"/>
<rectangle x1="9.1059" y1="0.98628125" x2="9.49451875" y2="0.99491875" layer="94"/>
<rectangle x1="-0.00508125" y1="0.99491875" x2="1.89991875" y2="1.0033" layer="94"/>
<rectangle x1="1.9685" y1="0.99491875" x2="3.45948125" y2="1.0033" layer="94"/>
<rectangle x1="4.10971875" y1="0.99491875" x2="4.54151875" y2="1.0033" layer="94"/>
<rectangle x1="5.5753" y1="0.99491875" x2="6.01471875" y2="1.0033" layer="94"/>
<rectangle x1="6.2611" y1="0.99491875" x2="7.4041" y2="1.0033" layer="94"/>
<rectangle x1="7.58951875" y1="0.99491875" x2="7.9629" y2="1.0033" layer="94"/>
<rectangle x1="9.08811875" y1="0.99491875" x2="9.49451875" y2="1.0033" layer="94"/>
<rectangle x1="-0.00508125" y1="1.0033" x2="1.89991875" y2="1.01168125" layer="94"/>
<rectangle x1="1.9685" y1="1.0033" x2="3.47471875" y2="1.01168125" layer="94"/>
<rectangle x1="4.10971875" y1="1.0033" x2="4.5593" y2="1.01168125" layer="94"/>
<rectangle x1="5.55751875" y1="1.0033" x2="6.01471875" y2="1.01168125" layer="94"/>
<rectangle x1="6.2611" y1="1.0033" x2="7.4041" y2="1.01168125" layer="94"/>
<rectangle x1="7.58951875" y1="1.0033" x2="7.9629" y2="1.01168125" layer="94"/>
<rectangle x1="9.0805" y1="1.0033" x2="9.49451875" y2="1.01168125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.01168125" x2="1.89991875" y2="1.02031875" layer="94"/>
<rectangle x1="1.9685" y1="1.01168125" x2="3.4925" y2="1.02031875" layer="94"/>
<rectangle x1="4.10971875" y1="1.01168125" x2="4.56691875" y2="1.02031875" layer="94"/>
<rectangle x1="5.54228125" y1="1.01168125" x2="6.01471875" y2="1.02031875" layer="94"/>
<rectangle x1="6.2611" y1="1.01168125" x2="7.39648125" y2="1.02031875" layer="94"/>
<rectangle x1="7.58951875" y1="1.01168125" x2="7.9629" y2="1.02031875" layer="94"/>
<rectangle x1="9.07288125" y1="1.01168125" x2="9.49451875" y2="1.02031875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.02031875" x2="1.89991875" y2="1.0287" layer="94"/>
<rectangle x1="1.9685" y1="1.02031875" x2="3.50011875" y2="1.0287" layer="94"/>
<rectangle x1="4.10971875" y1="1.02031875" x2="4.5847" y2="1.0287" layer="94"/>
<rectangle x1="5.53211875" y1="1.02031875" x2="6.01471875" y2="1.0287" layer="94"/>
<rectangle x1="6.2611" y1="1.02031875" x2="7.39648125" y2="1.0287" layer="94"/>
<rectangle x1="7.58951875" y1="1.02031875" x2="7.9629" y2="1.0287" layer="94"/>
<rectangle x1="9.06271875" y1="1.02031875" x2="9.49451875" y2="1.0287" layer="94"/>
<rectangle x1="-0.00508125" y1="1.0287" x2="1.8923" y2="1.03708125" layer="94"/>
<rectangle x1="1.9685" y1="1.0287" x2="3.5179" y2="1.03708125" layer="94"/>
<rectangle x1="4.10971875" y1="1.0287" x2="4.60248125" y2="1.03708125" layer="94"/>
<rectangle x1="5.51688125" y1="1.0287" x2="6.01471875" y2="1.03708125" layer="94"/>
<rectangle x1="6.26871875" y1="1.0287" x2="7.39648125" y2="1.03708125" layer="94"/>
<rectangle x1="7.58951875" y1="1.0287" x2="7.9629" y2="1.03708125" layer="94"/>
<rectangle x1="9.0551" y1="1.0287" x2="9.49451875" y2="1.03708125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.03708125" x2="1.8923" y2="1.04571875" layer="94"/>
<rectangle x1="1.9685" y1="1.03708125" x2="3.53568125" y2="1.04571875" layer="94"/>
<rectangle x1="4.10971875" y1="1.03708125" x2="4.6101" y2="1.04571875" layer="94"/>
<rectangle x1="5.4991" y1="1.03708125" x2="6.01471875" y2="1.04571875" layer="94"/>
<rectangle x1="6.27888125" y1="1.03708125" x2="7.38631875" y2="1.04571875" layer="94"/>
<rectangle x1="7.58951875" y1="1.03708125" x2="7.9629" y2="1.04571875" layer="94"/>
<rectangle x1="9.04748125" y1="1.03708125" x2="9.49451875" y2="1.04571875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.04571875" x2="1.88468125" y2="1.0541" layer="94"/>
<rectangle x1="1.9685" y1="1.04571875" x2="3.55091875" y2="1.0541" layer="94"/>
<rectangle x1="4.10971875" y1="1.04571875" x2="4.62788125" y2="1.0541" layer="94"/>
<rectangle x1="5.49148125" y1="1.04571875" x2="6.01471875" y2="1.0541" layer="94"/>
<rectangle x1="6.27888125" y1="1.04571875" x2="7.38631875" y2="1.0541" layer="94"/>
<rectangle x1="7.58951875" y1="1.04571875" x2="7.9629" y2="1.0541" layer="94"/>
<rectangle x1="9.03731875" y1="1.04571875" x2="9.49451875" y2="1.0541" layer="94"/>
<rectangle x1="-0.00508125" y1="1.0541" x2="1.88468125" y2="1.06248125" layer="94"/>
<rectangle x1="1.9685" y1="1.0541" x2="3.56108125" y2="1.06248125" layer="94"/>
<rectangle x1="4.10971875" y1="1.0541" x2="4.6355" y2="1.06248125" layer="94"/>
<rectangle x1="5.4737" y1="1.0541" x2="6.01471875" y2="1.06248125" layer="94"/>
<rectangle x1="6.2865" y1="1.0541" x2="7.3787" y2="1.06248125" layer="94"/>
<rectangle x1="7.58951875" y1="1.0541" x2="7.9629" y2="1.06248125" layer="94"/>
<rectangle x1="9.0297" y1="1.0541" x2="9.49451875" y2="1.06248125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.06248125" x2="1.87451875" y2="1.07111875" layer="94"/>
<rectangle x1="1.9685" y1="1.06248125" x2="3.57631875" y2="1.07111875" layer="94"/>
<rectangle x1="4.10971875" y1="1.06248125" x2="4.65328125" y2="1.07111875" layer="94"/>
<rectangle x1="5.45591875" y1="1.06248125" x2="6.01471875" y2="1.07111875" layer="94"/>
<rectangle x1="6.29411875" y1="1.06248125" x2="7.37108125" y2="1.07111875" layer="94"/>
<rectangle x1="7.58951875" y1="1.06248125" x2="7.9629" y2="1.07111875" layer="94"/>
<rectangle x1="9.02208125" y1="1.06248125" x2="9.49451875" y2="1.07111875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.07111875" x2="1.8669" y2="1.0795" layer="94"/>
<rectangle x1="1.9685" y1="1.07111875" x2="3.5941" y2="1.0795" layer="94"/>
<rectangle x1="4.10971875" y1="1.07111875" x2="4.66851875" y2="1.0795" layer="94"/>
<rectangle x1="5.4483" y1="1.07111875" x2="6.01471875" y2="1.0795" layer="94"/>
<rectangle x1="6.30428125" y1="1.07111875" x2="7.36091875" y2="1.0795" layer="94"/>
<rectangle x1="7.58951875" y1="1.07111875" x2="7.9629" y2="1.0795" layer="94"/>
<rectangle x1="9.01191875" y1="1.07111875" x2="9.49451875" y2="1.0795" layer="94"/>
<rectangle x1="-0.00508125" y1="1.0795" x2="1.85928125" y2="1.08788125" layer="94"/>
<rectangle x1="1.9685" y1="1.0795" x2="3.60171875" y2="1.08788125" layer="94"/>
<rectangle x1="4.10971875" y1="1.0795" x2="4.6863" y2="1.08788125" layer="94"/>
<rectangle x1="5.43051875" y1="1.0795" x2="6.01471875" y2="1.08788125" layer="94"/>
<rectangle x1="6.3119" y1="1.0795" x2="7.3533" y2="1.08788125" layer="94"/>
<rectangle x1="7.58951875" y1="1.0795" x2="7.9629" y2="1.08788125" layer="94"/>
<rectangle x1="9.0043" y1="1.0795" x2="9.49451875" y2="1.08788125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.08788125" x2="1.8415" y2="1.09651875" layer="94"/>
<rectangle x1="1.9685" y1="1.08788125" x2="3.6195" y2="1.09651875" layer="94"/>
<rectangle x1="4.10971875" y1="1.08788125" x2="4.69391875" y2="1.09651875" layer="94"/>
<rectangle x1="5.4229" y1="1.08788125" x2="6.01471875" y2="1.09651875" layer="94"/>
<rectangle x1="6.31951875" y1="1.08788125" x2="7.33551875" y2="1.09651875" layer="94"/>
<rectangle x1="7.58951875" y1="1.08788125" x2="7.9629" y2="1.09651875" layer="94"/>
<rectangle x1="8.99668125" y1="1.08788125" x2="9.49451875" y2="1.09651875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.09651875" x2="1.82371875" y2="1.1049" layer="94"/>
<rectangle x1="1.9685" y1="1.09651875" x2="3.63728125" y2="1.1049" layer="94"/>
<rectangle x1="4.10971875" y1="1.09651875" x2="4.7117" y2="1.1049" layer="94"/>
<rectangle x1="5.40511875" y1="1.09651875" x2="6.01471875" y2="1.1049" layer="94"/>
<rectangle x1="6.3373" y1="1.09651875" x2="7.32028125" y2="1.1049" layer="94"/>
<rectangle x1="7.58951875" y1="1.09651875" x2="7.9629" y2="1.1049" layer="94"/>
<rectangle x1="8.98651875" y1="1.09651875" x2="9.49451875" y2="1.1049" layer="94"/>
<rectangle x1="-0.00508125" y1="1.1049" x2="1.79831875" y2="1.11328125" layer="94"/>
<rectangle x1="1.9685" y1="1.1049" x2="3.6449" y2="1.11328125" layer="94"/>
<rectangle x1="4.10971875" y1="1.1049" x2="4.72948125" y2="1.11328125" layer="94"/>
<rectangle x1="5.3975" y1="1.1049" x2="6.01471875" y2="1.11328125" layer="94"/>
<rectangle x1="6.35508125" y1="1.1049" x2="7.3025" y2="1.11328125" layer="94"/>
<rectangle x1="7.58951875" y1="1.1049" x2="7.9629" y2="1.11328125" layer="94"/>
<rectangle x1="8.9789" y1="1.1049" x2="9.49451875" y2="1.11328125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.11328125" x2="1.7653" y2="1.12191875" layer="94"/>
<rectangle x1="1.9685" y1="1.11328125" x2="3.65251875" y2="1.12191875" layer="94"/>
<rectangle x1="4.10971875" y1="1.11328125" x2="4.7371" y2="1.12191875" layer="94"/>
<rectangle x1="5.37971875" y1="1.11328125" x2="6.01471875" y2="1.12191875" layer="94"/>
<rectangle x1="6.3881" y1="1.11328125" x2="7.26948125" y2="1.12191875" layer="94"/>
<rectangle x1="7.58951875" y1="1.11328125" x2="7.9629" y2="1.12191875" layer="94"/>
<rectangle x1="8.96111875" y1="1.11328125" x2="9.49451875" y2="1.12191875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.12191875" x2="0.3683" y2="1.1303" layer="94"/>
<rectangle x1="1.9685" y1="1.12191875" x2="2.34188125" y2="1.1303" layer="94"/>
<rectangle x1="3.05308125" y1="1.12191875" x2="3.6703" y2="1.1303" layer="94"/>
<rectangle x1="4.10971875" y1="1.12191875" x2="4.75488125" y2="1.1303" layer="94"/>
<rectangle x1="5.3721" y1="1.12191875" x2="6.01471875" y2="1.1303" layer="94"/>
<rectangle x1="6.6421" y1="1.12191875" x2="7.01548125" y2="1.1303" layer="94"/>
<rectangle x1="7.58951875" y1="1.12191875" x2="7.9629" y2="1.1303" layer="94"/>
<rectangle x1="8.96111875" y1="1.12191875" x2="9.49451875" y2="1.1303" layer="94"/>
<rectangle x1="-0.00508125" y1="1.1303" x2="0.3683" y2="1.13868125" layer="94"/>
<rectangle x1="1.9685" y1="1.1303" x2="2.34188125" y2="1.13868125" layer="94"/>
<rectangle x1="3.10388125" y1="1.1303" x2="3.67791875" y2="1.13868125" layer="94"/>
<rectangle x1="4.10971875" y1="1.1303" x2="4.77011875" y2="1.13868125" layer="94"/>
<rectangle x1="5.35431875" y1="1.1303" x2="6.01471875" y2="1.13868125" layer="94"/>
<rectangle x1="6.6421" y1="1.1303" x2="7.01548125" y2="1.13868125" layer="94"/>
<rectangle x1="7.58951875" y1="1.1303" x2="7.9629" y2="1.13868125" layer="94"/>
<rectangle x1="8.94588125" y1="1.1303" x2="9.49451875" y2="1.13868125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.13868125" x2="0.3683" y2="1.14731875" layer="94"/>
<rectangle x1="1.9685" y1="1.13868125" x2="2.34188125" y2="1.14731875" layer="94"/>
<rectangle x1="3.12928125" y1="1.13868125" x2="3.68808125" y2="1.14731875" layer="94"/>
<rectangle x1="4.10971875" y1="1.13868125" x2="4.78028125" y2="1.14731875" layer="94"/>
<rectangle x1="5.33908125" y1="1.13868125" x2="6.01471875" y2="1.14731875" layer="94"/>
<rectangle x1="6.6421" y1="1.13868125" x2="7.01548125" y2="1.14731875" layer="94"/>
<rectangle x1="7.58951875" y1="1.13868125" x2="7.9629" y2="1.14731875" layer="94"/>
<rectangle x1="8.93571875" y1="1.13868125" x2="9.49451875" y2="1.14731875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.14731875" x2="0.3683" y2="1.1557" layer="94"/>
<rectangle x1="1.9685" y1="1.14731875" x2="2.34188125" y2="1.1557" layer="94"/>
<rectangle x1="3.15468125" y1="1.14731875" x2="3.70331875" y2="1.1557" layer="94"/>
<rectangle x1="4.10971875" y1="1.14731875" x2="4.79551875" y2="1.1557" layer="94"/>
<rectangle x1="5.32891875" y1="1.14731875" x2="6.01471875" y2="1.1557" layer="94"/>
<rectangle x1="6.6421" y1="1.14731875" x2="7.01548125" y2="1.1557" layer="94"/>
<rectangle x1="7.58951875" y1="1.14731875" x2="7.9629" y2="1.1557" layer="94"/>
<rectangle x1="8.9281" y1="1.14731875" x2="9.49451875" y2="1.1557" layer="94"/>
<rectangle x1="-0.00508125" y1="1.1557" x2="0.3683" y2="1.16408125" layer="94"/>
<rectangle x1="1.9685" y1="1.1557" x2="2.34188125" y2="1.16408125" layer="94"/>
<rectangle x1="3.18008125" y1="1.1557" x2="3.71348125" y2="1.16408125" layer="94"/>
<rectangle x1="4.10971875" y1="1.1557" x2="4.80568125" y2="1.16408125" layer="94"/>
<rectangle x1="5.31368125" y1="1.1557" x2="6.01471875" y2="1.16408125" layer="94"/>
<rectangle x1="6.6421" y1="1.1557" x2="7.01548125" y2="1.16408125" layer="94"/>
<rectangle x1="7.58951875" y1="1.1557" x2="7.9629" y2="1.16408125" layer="94"/>
<rectangle x1="8.92048125" y1="1.1557" x2="9.49451875" y2="1.16408125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.16408125" x2="0.3683" y2="1.17271875" layer="94"/>
<rectangle x1="1.9685" y1="1.16408125" x2="2.34188125" y2="1.17271875" layer="94"/>
<rectangle x1="3.20548125" y1="1.16408125" x2="3.7211" y2="1.17271875" layer="94"/>
<rectangle x1="4.10971875" y1="1.16408125" x2="4.82091875" y2="1.17271875" layer="94"/>
<rectangle x1="5.2959" y1="1.16408125" x2="6.01471875" y2="1.17271875" layer="94"/>
<rectangle x1="6.6421" y1="1.16408125" x2="7.01548125" y2="1.17271875" layer="94"/>
<rectangle x1="7.58951875" y1="1.16408125" x2="7.9629" y2="1.17271875" layer="94"/>
<rectangle x1="8.91031875" y1="1.16408125" x2="9.49451875" y2="1.17271875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.17271875" x2="0.3683" y2="1.1811" layer="94"/>
<rectangle x1="1.9685" y1="1.17271875" x2="2.34188125" y2="1.1811" layer="94"/>
<rectangle x1="3.22071875" y1="1.17271875" x2="3.72871875" y2="1.1811" layer="94"/>
<rectangle x1="4.10971875" y1="1.17271875" x2="4.8387" y2="1.1811" layer="94"/>
<rectangle x1="5.28828125" y1="1.17271875" x2="6.01471875" y2="1.1811" layer="94"/>
<rectangle x1="6.6421" y1="1.17271875" x2="7.01548125" y2="1.1811" layer="94"/>
<rectangle x1="7.58951875" y1="1.17271875" x2="7.9629" y2="1.1811" layer="94"/>
<rectangle x1="8.9027" y1="1.17271875" x2="9.49451875" y2="1.1811" layer="94"/>
<rectangle x1="-0.00508125" y1="1.1811" x2="0.3683" y2="1.18948125" layer="94"/>
<rectangle x1="1.9685" y1="1.1811" x2="2.34188125" y2="1.18948125" layer="94"/>
<rectangle x1="3.2385" y1="1.1811" x2="3.73888125" y2="1.18948125" layer="94"/>
<rectangle x1="4.10971875" y1="1.1811" x2="4.84631875" y2="1.18948125" layer="94"/>
<rectangle x1="5.2705" y1="1.1811" x2="6.01471875" y2="1.18948125" layer="94"/>
<rectangle x1="6.6421" y1="1.1811" x2="7.01548125" y2="1.18948125" layer="94"/>
<rectangle x1="7.58951875" y1="1.1811" x2="7.9629" y2="1.18948125" layer="94"/>
<rectangle x1="8.89508125" y1="1.1811" x2="9.49451875" y2="1.18948125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.18948125" x2="0.3683" y2="1.19811875" layer="94"/>
<rectangle x1="1.9685" y1="1.18948125" x2="2.34188125" y2="1.19811875" layer="94"/>
<rectangle x1="3.2639" y1="1.18948125" x2="3.7465" y2="1.19811875" layer="94"/>
<rectangle x1="4.10971875" y1="1.18948125" x2="4.8641" y2="1.19811875" layer="94"/>
<rectangle x1="5.26288125" y1="1.18948125" x2="6.01471875" y2="1.19811875" layer="94"/>
<rectangle x1="6.6421" y1="1.18948125" x2="7.01548125" y2="1.19811875" layer="94"/>
<rectangle x1="7.58951875" y1="1.18948125" x2="7.9629" y2="1.19811875" layer="94"/>
<rectangle x1="8.88491875" y1="1.18948125" x2="9.49451875" y2="1.19811875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.19811875" x2="0.3683" y2="1.2065" layer="94"/>
<rectangle x1="1.9685" y1="1.19811875" x2="2.34188125" y2="1.2065" layer="94"/>
<rectangle x1="3.28168125" y1="1.19811875" x2="3.75411875" y2="1.2065" layer="94"/>
<rectangle x1="4.10971875" y1="1.19811875" x2="4.88188125" y2="1.2065" layer="94"/>
<rectangle x1="5.2451" y1="1.19811875" x2="6.01471875" y2="1.2065" layer="94"/>
<rectangle x1="6.6421" y1="1.19811875" x2="7.01548125" y2="1.2065" layer="94"/>
<rectangle x1="7.58951875" y1="1.19811875" x2="7.9629" y2="1.2065" layer="94"/>
<rectangle x1="8.8773" y1="1.19811875" x2="9.49451875" y2="1.2065" layer="94"/>
<rectangle x1="-0.00508125" y1="1.2065" x2="0.3683" y2="1.21488125" layer="94"/>
<rectangle x1="1.9685" y1="1.2065" x2="2.34188125" y2="1.21488125" layer="94"/>
<rectangle x1="3.29691875" y1="1.2065" x2="3.76428125" y2="1.21488125" layer="94"/>
<rectangle x1="4.10971875" y1="1.2065" x2="4.8895" y2="1.21488125" layer="94"/>
<rectangle x1="5.23748125" y1="1.2065" x2="6.01471875" y2="1.21488125" layer="94"/>
<rectangle x1="6.6421" y1="1.2065" x2="7.01548125" y2="1.21488125" layer="94"/>
<rectangle x1="7.58951875" y1="1.2065" x2="7.9629" y2="1.21488125" layer="94"/>
<rectangle x1="8.86968125" y1="1.2065" x2="9.49451875" y2="1.21488125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.21488125" x2="0.3683" y2="1.22351875" layer="94"/>
<rectangle x1="1.9685" y1="1.21488125" x2="2.34188125" y2="1.22351875" layer="94"/>
<rectangle x1="3.30708125" y1="1.21488125" x2="3.7719" y2="1.22351875" layer="94"/>
<rectangle x1="4.10971875" y1="1.21488125" x2="4.90728125" y2="1.22351875" layer="94"/>
<rectangle x1="5.2197" y1="1.21488125" x2="6.01471875" y2="1.22351875" layer="94"/>
<rectangle x1="6.6421" y1="1.21488125" x2="7.01548125" y2="1.22351875" layer="94"/>
<rectangle x1="7.58951875" y1="1.21488125" x2="7.9629" y2="1.22351875" layer="94"/>
<rectangle x1="8.85951875" y1="1.21488125" x2="9.49451875" y2="1.22351875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.22351875" x2="0.3683" y2="1.2319" layer="94"/>
<rectangle x1="1.9685" y1="1.22351875" x2="2.34188125" y2="1.2319" layer="94"/>
<rectangle x1="3.32231875" y1="1.22351875" x2="3.77951875" y2="1.2319" layer="94"/>
<rectangle x1="4.10971875" y1="1.22351875" x2="4.92251875" y2="1.2319" layer="94"/>
<rectangle x1="5.21208125" y1="1.22351875" x2="6.01471875" y2="1.2319" layer="94"/>
<rectangle x1="6.6421" y1="1.22351875" x2="7.01548125" y2="1.2319" layer="94"/>
<rectangle x1="7.58951875" y1="1.22351875" x2="7.9629" y2="1.2319" layer="94"/>
<rectangle x1="8.8519" y1="1.22351875" x2="9.49451875" y2="1.2319" layer="94"/>
<rectangle x1="-0.00508125" y1="1.2319" x2="0.3683" y2="1.24028125" layer="94"/>
<rectangle x1="1.9685" y1="1.2319" x2="2.34188125" y2="1.24028125" layer="94"/>
<rectangle x1="3.3401" y1="1.2319" x2="3.78968125" y2="1.24028125" layer="94"/>
<rectangle x1="4.10971875" y1="1.2319" x2="4.93268125" y2="1.24028125" layer="94"/>
<rectangle x1="5.1943" y1="1.2319" x2="6.01471875" y2="1.24028125" layer="94"/>
<rectangle x1="6.6421" y1="1.2319" x2="7.01548125" y2="1.24028125" layer="94"/>
<rectangle x1="7.58951875" y1="1.2319" x2="7.9629" y2="1.24028125" layer="94"/>
<rectangle x1="8.83411875" y1="1.2319" x2="9.49451875" y2="1.24028125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.24028125" x2="0.3683" y2="1.24891875" layer="94"/>
<rectangle x1="1.9685" y1="1.24028125" x2="2.34188125" y2="1.24891875" layer="94"/>
<rectangle x1="3.35788125" y1="1.24028125" x2="3.7973" y2="1.24891875" layer="94"/>
<rectangle x1="4.10971875" y1="1.24028125" x2="4.94791875" y2="1.24891875" layer="94"/>
<rectangle x1="5.17651875" y1="1.24028125" x2="6.01471875" y2="1.24891875" layer="94"/>
<rectangle x1="6.6421" y1="1.24028125" x2="7.01548125" y2="1.24891875" layer="94"/>
<rectangle x1="7.58951875" y1="1.24028125" x2="7.9629" y2="1.24891875" layer="94"/>
<rectangle x1="8.8265" y1="1.24028125" x2="9.49451875" y2="1.24891875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.24891875" x2="0.3683" y2="1.2573" layer="94"/>
<rectangle x1="1.9685" y1="1.24891875" x2="2.34188125" y2="1.2573" layer="94"/>
<rectangle x1="3.3655" y1="1.24891875" x2="3.7973" y2="1.2573" layer="94"/>
<rectangle x1="4.10971875" y1="1.24891875" x2="4.9657" y2="1.2573" layer="94"/>
<rectangle x1="5.1689" y1="1.24891875" x2="6.01471875" y2="1.2573" layer="94"/>
<rectangle x1="6.6421" y1="1.24891875" x2="7.01548125" y2="1.2573" layer="94"/>
<rectangle x1="7.58951875" y1="1.24891875" x2="7.9629" y2="1.2573" layer="94"/>
<rectangle x1="8.81888125" y1="1.24891875" x2="9.49451875" y2="1.2573" layer="94"/>
<rectangle x1="-0.00508125" y1="1.2573" x2="0.3683" y2="1.26568125" layer="94"/>
<rectangle x1="1.9685" y1="1.2573" x2="2.34188125" y2="1.26568125" layer="94"/>
<rectangle x1="3.37311875" y1="1.2573" x2="3.80491875" y2="1.26568125" layer="94"/>
<rectangle x1="4.10971875" y1="1.2573" x2="4.97331875" y2="1.26568125" layer="94"/>
<rectangle x1="5.15111875" y1="1.2573" x2="6.01471875" y2="1.26568125" layer="94"/>
<rectangle x1="6.6421" y1="1.2573" x2="7.01548125" y2="1.26568125" layer="94"/>
<rectangle x1="7.58951875" y1="1.2573" x2="7.9629" y2="1.26568125" layer="94"/>
<rectangle x1="8.80871875" y1="1.2573" x2="9.49451875" y2="1.26568125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.26568125" x2="0.3683" y2="1.27431875" layer="94"/>
<rectangle x1="1.9685" y1="1.26568125" x2="2.34188125" y2="1.27431875" layer="94"/>
<rectangle x1="3.3909" y1="1.26568125" x2="3.81508125" y2="1.27431875" layer="94"/>
<rectangle x1="4.10971875" y1="1.26568125" x2="4.9911" y2="1.27431875" layer="94"/>
<rectangle x1="5.1435" y1="1.26568125" x2="6.01471875" y2="1.27431875" layer="94"/>
<rectangle x1="6.6421" y1="1.26568125" x2="7.01548125" y2="1.27431875" layer="94"/>
<rectangle x1="7.58951875" y1="1.26568125" x2="7.9629" y2="1.27431875" layer="94"/>
<rectangle x1="8.8011" y1="1.26568125" x2="9.49451875" y2="1.27431875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.27431875" x2="0.3683" y2="1.2827" layer="94"/>
<rectangle x1="1.9685" y1="1.27431875" x2="2.34188125" y2="1.2827" layer="94"/>
<rectangle x1="3.39851875" y1="1.27431875" x2="3.81508125" y2="1.2827" layer="94"/>
<rectangle x1="4.10971875" y1="1.27431875" x2="5.00888125" y2="1.2827" layer="94"/>
<rectangle x1="5.12571875" y1="1.27431875" x2="6.01471875" y2="1.2827" layer="94"/>
<rectangle x1="6.6421" y1="1.27431875" x2="7.01548125" y2="1.2827" layer="94"/>
<rectangle x1="7.58951875" y1="1.27431875" x2="7.9629" y2="1.2827" layer="94"/>
<rectangle x1="8.79348125" y1="1.27431875" x2="9.49451875" y2="1.2827" layer="94"/>
<rectangle x1="-0.00508125" y1="1.2827" x2="0.3683" y2="1.29108125" layer="94"/>
<rectangle x1="1.9685" y1="1.2827" x2="2.34188125" y2="1.29108125" layer="94"/>
<rectangle x1="3.4163" y1="1.2827" x2="3.8227" y2="1.29108125" layer="94"/>
<rectangle x1="4.10971875" y1="1.2827" x2="4.4831" y2="1.29108125" layer="94"/>
<rectangle x1="4.50088125" y1="1.2827" x2="5.0165" y2="1.29108125" layer="94"/>
<rectangle x1="5.11048125" y1="1.2827" x2="5.61848125" y2="1.29108125" layer="94"/>
<rectangle x1="5.63371875" y1="1.2827" x2="6.01471875" y2="1.29108125" layer="94"/>
<rectangle x1="6.6421" y1="1.2827" x2="7.01548125" y2="1.29108125" layer="94"/>
<rectangle x1="7.58951875" y1="1.2827" x2="7.9629" y2="1.29108125" layer="94"/>
<rectangle x1="8.78331875" y1="1.2827" x2="9.49451875" y2="1.29108125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.29108125" x2="0.3683" y2="1.29971875" layer="94"/>
<rectangle x1="1.9685" y1="1.29108125" x2="2.34188125" y2="1.29971875" layer="94"/>
<rectangle x1="3.42391875" y1="1.29108125" x2="3.8227" y2="1.29971875" layer="94"/>
<rectangle x1="4.10971875" y1="1.29108125" x2="4.4831" y2="1.29971875" layer="94"/>
<rectangle x1="4.5085" y1="1.29108125" x2="5.03428125" y2="1.29971875" layer="94"/>
<rectangle x1="5.10031875" y1="1.29108125" x2="5.60831875" y2="1.29971875" layer="94"/>
<rectangle x1="5.63371875" y1="1.29108125" x2="6.01471875" y2="1.29971875" layer="94"/>
<rectangle x1="6.6421" y1="1.29108125" x2="7.01548125" y2="1.29971875" layer="94"/>
<rectangle x1="7.58951875" y1="1.29108125" x2="7.9629" y2="1.29971875" layer="94"/>
<rectangle x1="8.7757" y1="1.29108125" x2="9.49451875" y2="1.29971875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.29971875" x2="0.3683" y2="1.3081" layer="94"/>
<rectangle x1="1.9685" y1="1.29971875" x2="2.34188125" y2="1.3081" layer="94"/>
<rectangle x1="3.43408125" y1="1.29971875" x2="3.83031875" y2="1.3081" layer="94"/>
<rectangle x1="4.10971875" y1="1.29971875" x2="4.4831" y2="1.3081" layer="94"/>
<rectangle x1="4.52628125" y1="1.29971875" x2="5.04951875" y2="1.3081" layer="94"/>
<rectangle x1="5.08508125" y1="1.29971875" x2="5.59308125" y2="1.3081" layer="94"/>
<rectangle x1="5.63371875" y1="1.29971875" x2="6.01471875" y2="1.3081" layer="94"/>
<rectangle x1="6.6421" y1="1.29971875" x2="7.01548125" y2="1.3081" layer="94"/>
<rectangle x1="7.58951875" y1="1.29971875" x2="7.9629" y2="1.3081" layer="94"/>
<rectangle x1="8.76808125" y1="1.29971875" x2="9.49451875" y2="1.3081" layer="94"/>
<rectangle x1="-0.00508125" y1="1.3081" x2="0.3683" y2="1.31648125" layer="94"/>
<rectangle x1="1.9685" y1="1.3081" x2="2.34188125" y2="1.31648125" layer="94"/>
<rectangle x1="3.4417" y1="1.3081" x2="3.83031875" y2="1.31648125" layer="94"/>
<rectangle x1="4.10971875" y1="1.3081" x2="4.4831" y2="1.31648125" layer="94"/>
<rectangle x1="4.5339" y1="1.3081" x2="5.05968125" y2="1.31648125" layer="94"/>
<rectangle x1="5.07491875" y1="1.3081" x2="5.58291875" y2="1.31648125" layer="94"/>
<rectangle x1="5.63371875" y1="1.3081" x2="6.01471875" y2="1.31648125" layer="94"/>
<rectangle x1="6.6421" y1="1.3081" x2="7.01548125" y2="1.31648125" layer="94"/>
<rectangle x1="7.58951875" y1="1.3081" x2="7.9629" y2="1.31648125" layer="94"/>
<rectangle x1="8.75791875" y1="1.3081" x2="9.49451875" y2="1.31648125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.31648125" x2="0.3683" y2="1.32511875" layer="94"/>
<rectangle x1="1.9685" y1="1.31648125" x2="2.34188125" y2="1.32511875" layer="94"/>
<rectangle x1="3.44931875" y1="1.31648125" x2="3.84048125" y2="1.32511875" layer="94"/>
<rectangle x1="4.10971875" y1="1.31648125" x2="4.4831" y2="1.32511875" layer="94"/>
<rectangle x1="4.55168125" y1="1.31648125" x2="5.56768125" y2="1.32511875" layer="94"/>
<rectangle x1="5.63371875" y1="1.31648125" x2="6.01471875" y2="1.32511875" layer="94"/>
<rectangle x1="6.6421" y1="1.31648125" x2="7.01548125" y2="1.32511875" layer="94"/>
<rectangle x1="7.58951875" y1="1.31648125" x2="7.9629" y2="1.32511875" layer="94"/>
<rectangle x1="8.7503" y1="1.31648125" x2="9.49451875" y2="1.32511875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.32511875" x2="0.3683" y2="1.3335" layer="94"/>
<rectangle x1="1.9685" y1="1.32511875" x2="2.34188125" y2="1.3335" layer="94"/>
<rectangle x1="3.45948125" y1="1.32511875" x2="3.84048125" y2="1.3335" layer="94"/>
<rectangle x1="4.10971875" y1="1.32511875" x2="4.4831" y2="1.3335" layer="94"/>
<rectangle x1="4.5593" y1="1.32511875" x2="5.5499" y2="1.3335" layer="94"/>
<rectangle x1="5.63371875" y1="1.32511875" x2="6.01471875" y2="1.3335" layer="94"/>
<rectangle x1="6.6421" y1="1.32511875" x2="7.01548125" y2="1.3335" layer="94"/>
<rectangle x1="7.58951875" y1="1.32511875" x2="7.9629" y2="1.3335" layer="94"/>
<rectangle x1="8.74268125" y1="1.32511875" x2="9.49451875" y2="1.3335" layer="94"/>
<rectangle x1="-0.00508125" y1="1.3335" x2="0.3683" y2="1.34188125" layer="94"/>
<rectangle x1="1.9685" y1="1.3335" x2="2.34188125" y2="1.34188125" layer="94"/>
<rectangle x1="3.4671" y1="1.3335" x2="3.8481" y2="1.34188125" layer="94"/>
<rectangle x1="4.10971875" y1="1.3335" x2="4.4831" y2="1.34188125" layer="94"/>
<rectangle x1="4.57708125" y1="1.3335" x2="5.54228125" y2="1.34188125" layer="94"/>
<rectangle x1="5.63371875" y1="1.3335" x2="6.01471875" y2="1.34188125" layer="94"/>
<rectangle x1="6.6421" y1="1.3335" x2="7.01548125" y2="1.34188125" layer="94"/>
<rectangle x1="7.58951875" y1="1.3335" x2="7.9629" y2="1.34188125" layer="94"/>
<rectangle x1="8.73251875" y1="1.3335" x2="9.49451875" y2="1.34188125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.34188125" x2="0.3683" y2="1.35051875" layer="94"/>
<rectangle x1="1.9685" y1="1.34188125" x2="2.34188125" y2="1.35051875" layer="94"/>
<rectangle x1="3.47471875" y1="1.34188125" x2="3.8481" y2="1.35051875" layer="94"/>
<rectangle x1="4.10971875" y1="1.34188125" x2="4.4831" y2="1.35051875" layer="94"/>
<rectangle x1="4.5847" y1="1.34188125" x2="5.5245" y2="1.35051875" layer="94"/>
<rectangle x1="5.63371875" y1="1.34188125" x2="6.01471875" y2="1.35051875" layer="94"/>
<rectangle x1="6.6421" y1="1.34188125" x2="7.01548125" y2="1.35051875" layer="94"/>
<rectangle x1="7.58951875" y1="1.34188125" x2="7.9629" y2="1.35051875" layer="94"/>
<rectangle x1="8.7249" y1="1.34188125" x2="9.49451875" y2="1.35051875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.35051875" x2="0.3683" y2="1.3589" layer="94"/>
<rectangle x1="1.9685" y1="1.35051875" x2="2.34188125" y2="1.3589" layer="94"/>
<rectangle x1="3.47471875" y1="1.35051875" x2="3.85571875" y2="1.3589" layer="94"/>
<rectangle x1="4.10971875" y1="1.35051875" x2="4.4831" y2="1.3589" layer="94"/>
<rectangle x1="4.60248125" y1="1.35051875" x2="5.51688125" y2="1.3589" layer="94"/>
<rectangle x1="5.63371875" y1="1.35051875" x2="6.01471875" y2="1.3589" layer="94"/>
<rectangle x1="6.6421" y1="1.35051875" x2="7.01548125" y2="1.3589" layer="94"/>
<rectangle x1="7.58951875" y1="1.35051875" x2="7.9629" y2="1.3589" layer="94"/>
<rectangle x1="8.70711875" y1="1.35051875" x2="9.49451875" y2="1.3589" layer="94"/>
<rectangle x1="-0.00508125" y1="1.3589" x2="0.3683" y2="1.36728125" layer="94"/>
<rectangle x1="1.9685" y1="1.3589" x2="2.34188125" y2="1.36728125" layer="94"/>
<rectangle x1="3.48488125" y1="1.3589" x2="3.86588125" y2="1.36728125" layer="94"/>
<rectangle x1="4.10971875" y1="1.3589" x2="4.4831" y2="1.36728125" layer="94"/>
<rectangle x1="4.6101" y1="1.3589" x2="5.4991" y2="1.36728125" layer="94"/>
<rectangle x1="5.63371875" y1="1.3589" x2="6.01471875" y2="1.36728125" layer="94"/>
<rectangle x1="6.6421" y1="1.3589" x2="7.01548125" y2="1.36728125" layer="94"/>
<rectangle x1="7.58951875" y1="1.3589" x2="7.9629" y2="1.36728125" layer="94"/>
<rectangle x1="8.6995" y1="1.3589" x2="9.49451875" y2="1.36728125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.36728125" x2="0.3683" y2="1.37591875" layer="94"/>
<rectangle x1="1.9685" y1="1.36728125" x2="2.34188125" y2="1.37591875" layer="94"/>
<rectangle x1="3.4925" y1="1.36728125" x2="3.86588125" y2="1.37591875" layer="94"/>
<rectangle x1="4.10971875" y1="1.36728125" x2="4.4831" y2="1.37591875" layer="94"/>
<rectangle x1="4.62788125" y1="1.36728125" x2="5.48131875" y2="1.37591875" layer="94"/>
<rectangle x1="5.63371875" y1="1.36728125" x2="6.01471875" y2="1.37591875" layer="94"/>
<rectangle x1="6.6421" y1="1.36728125" x2="7.01548125" y2="1.37591875" layer="94"/>
<rectangle x1="7.58951875" y1="1.36728125" x2="7.9629" y2="1.37591875" layer="94"/>
<rectangle x1="8.69188125" y1="1.36728125" x2="9.49451875" y2="1.37591875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.37591875" x2="0.3683" y2="1.3843" layer="94"/>
<rectangle x1="1.9685" y1="1.37591875" x2="2.34188125" y2="1.3843" layer="94"/>
<rectangle x1="3.4925" y1="1.37591875" x2="3.8735" y2="1.3843" layer="94"/>
<rectangle x1="4.10971875" y1="1.37591875" x2="4.4831" y2="1.3843" layer="94"/>
<rectangle x1="4.6355" y1="1.37591875" x2="5.4737" y2="1.3843" layer="94"/>
<rectangle x1="5.63371875" y1="1.37591875" x2="6.01471875" y2="1.3843" layer="94"/>
<rectangle x1="6.6421" y1="1.37591875" x2="7.01548125" y2="1.3843" layer="94"/>
<rectangle x1="7.58951875" y1="1.37591875" x2="7.9629" y2="1.3843" layer="94"/>
<rectangle x1="8.68171875" y1="1.37591875" x2="9.49451875" y2="1.3843" layer="94"/>
<rectangle x1="-0.00508125" y1="1.3843" x2="0.3683" y2="1.39268125" layer="94"/>
<rectangle x1="1.9685" y1="1.3843" x2="2.34188125" y2="1.39268125" layer="94"/>
<rectangle x1="3.4925" y1="1.3843" x2="3.8735" y2="1.39268125" layer="94"/>
<rectangle x1="4.10971875" y1="1.3843" x2="4.4831" y2="1.39268125" layer="94"/>
<rectangle x1="4.65328125" y1="1.3843" x2="5.45591875" y2="1.39268125" layer="94"/>
<rectangle x1="5.63371875" y1="1.3843" x2="6.01471875" y2="1.39268125" layer="94"/>
<rectangle x1="6.6421" y1="1.3843" x2="7.01548125" y2="1.39268125" layer="94"/>
<rectangle x1="7.58951875" y1="1.3843" x2="7.9629" y2="1.39268125" layer="94"/>
<rectangle x1="8.6741" y1="1.3843" x2="9.49451875" y2="1.39268125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.39268125" x2="0.3683" y2="1.40131875" layer="94"/>
<rectangle x1="1.9685" y1="1.39268125" x2="2.34188125" y2="1.40131875" layer="94"/>
<rectangle x1="3.50011875" y1="1.39268125" x2="3.8735" y2="1.40131875" layer="94"/>
<rectangle x1="4.10971875" y1="1.39268125" x2="4.4831" y2="1.40131875" layer="94"/>
<rectangle x1="4.6609" y1="1.39268125" x2="5.4483" y2="1.40131875" layer="94"/>
<rectangle x1="5.63371875" y1="1.39268125" x2="6.01471875" y2="1.40131875" layer="94"/>
<rectangle x1="6.6421" y1="1.39268125" x2="7.01548125" y2="1.40131875" layer="94"/>
<rectangle x1="7.58951875" y1="1.39268125" x2="7.9629" y2="1.40131875" layer="94"/>
<rectangle x1="8.66648125" y1="1.39268125" x2="9.1059" y2="1.40131875" layer="94"/>
<rectangle x1="9.11351875" y1="1.39268125" x2="9.49451875" y2="1.40131875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.40131875" x2="0.3683" y2="1.4097" layer="94"/>
<rectangle x1="1.9685" y1="1.40131875" x2="2.34188125" y2="1.4097" layer="94"/>
<rectangle x1="3.50011875" y1="1.40131875" x2="3.8735" y2="1.4097" layer="94"/>
<rectangle x1="4.10971875" y1="1.40131875" x2="4.4831" y2="1.4097" layer="94"/>
<rectangle x1="4.67868125" y1="1.40131875" x2="5.43051875" y2="1.4097" layer="94"/>
<rectangle x1="5.63371875" y1="1.40131875" x2="6.01471875" y2="1.4097" layer="94"/>
<rectangle x1="6.6421" y1="1.40131875" x2="7.01548125" y2="1.4097" layer="94"/>
<rectangle x1="7.58951875" y1="1.40131875" x2="7.9629" y2="1.4097" layer="94"/>
<rectangle x1="8.65631875" y1="1.40131875" x2="9.09828125" y2="1.4097" layer="94"/>
<rectangle x1="9.11351875" y1="1.40131875" x2="9.49451875" y2="1.4097" layer="94"/>
<rectangle x1="-0.00508125" y1="1.4097" x2="0.3683" y2="1.41808125" layer="94"/>
<rectangle x1="1.9685" y1="1.4097" x2="2.34188125" y2="1.41808125" layer="94"/>
<rectangle x1="3.50011875" y1="1.4097" x2="3.8735" y2="1.41808125" layer="94"/>
<rectangle x1="4.10971875" y1="1.4097" x2="4.4831" y2="1.41808125" layer="94"/>
<rectangle x1="4.6863" y1="1.4097" x2="5.4229" y2="1.41808125" layer="94"/>
<rectangle x1="5.63371875" y1="1.4097" x2="6.01471875" y2="1.41808125" layer="94"/>
<rectangle x1="6.6421" y1="1.4097" x2="7.01548125" y2="1.41808125" layer="94"/>
<rectangle x1="7.58951875" y1="1.4097" x2="7.9629" y2="1.41808125" layer="94"/>
<rectangle x1="8.6487" y1="1.4097" x2="9.08811875" y2="1.41808125" layer="94"/>
<rectangle x1="9.11351875" y1="1.4097" x2="9.49451875" y2="1.41808125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.41808125" x2="0.3683" y2="1.42671875" layer="94"/>
<rectangle x1="1.9685" y1="1.41808125" x2="2.34188125" y2="1.42671875" layer="94"/>
<rectangle x1="3.50011875" y1="1.41808125" x2="3.8735" y2="1.42671875" layer="94"/>
<rectangle x1="4.10971875" y1="1.41808125" x2="4.4831" y2="1.42671875" layer="94"/>
<rectangle x1="4.70408125" y1="1.41808125" x2="5.40511875" y2="1.42671875" layer="94"/>
<rectangle x1="5.63371875" y1="1.41808125" x2="6.01471875" y2="1.42671875" layer="94"/>
<rectangle x1="6.6421" y1="1.41808125" x2="7.01548125" y2="1.42671875" layer="94"/>
<rectangle x1="7.58951875" y1="1.41808125" x2="7.9629" y2="1.42671875" layer="94"/>
<rectangle x1="8.64108125" y1="1.41808125" x2="9.0805" y2="1.42671875" layer="94"/>
<rectangle x1="9.11351875" y1="1.41808125" x2="9.49451875" y2="1.42671875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.42671875" x2="0.3683" y2="1.4351" layer="94"/>
<rectangle x1="1.9685" y1="1.42671875" x2="2.34188125" y2="1.4351" layer="94"/>
<rectangle x1="3.50011875" y1="1.42671875" x2="3.8735" y2="1.4351" layer="94"/>
<rectangle x1="4.10971875" y1="1.42671875" x2="4.4831" y2="1.4351" layer="94"/>
<rectangle x1="4.71931875" y1="1.42671875" x2="5.38988125" y2="1.4351" layer="94"/>
<rectangle x1="5.63371875" y1="1.42671875" x2="6.01471875" y2="1.4351" layer="94"/>
<rectangle x1="6.6421" y1="1.42671875" x2="7.01548125" y2="1.4351" layer="94"/>
<rectangle x1="7.58951875" y1="1.42671875" x2="7.9629" y2="1.4351" layer="94"/>
<rectangle x1="8.63091875" y1="1.42671875" x2="9.07288125" y2="1.4351" layer="94"/>
<rectangle x1="9.11351875" y1="1.42671875" x2="9.49451875" y2="1.4351" layer="94"/>
<rectangle x1="-0.00508125" y1="1.4351" x2="0.3683" y2="1.44348125" layer="94"/>
<rectangle x1="1.9685" y1="1.4351" x2="2.34188125" y2="1.44348125" layer="94"/>
<rectangle x1="3.50011875" y1="1.4351" x2="3.8735" y2="1.44348125" layer="94"/>
<rectangle x1="4.10971875" y1="1.4351" x2="4.4831" y2="1.44348125" layer="94"/>
<rectangle x1="4.72948125" y1="1.4351" x2="5.37971875" y2="1.44348125" layer="94"/>
<rectangle x1="5.63371875" y1="1.4351" x2="6.01471875" y2="1.44348125" layer="94"/>
<rectangle x1="6.6421" y1="1.4351" x2="7.01548125" y2="1.44348125" layer="94"/>
<rectangle x1="7.58951875" y1="1.4351" x2="7.9629" y2="1.44348125" layer="94"/>
<rectangle x1="8.6233" y1="1.4351" x2="9.06271875" y2="1.44348125" layer="94"/>
<rectangle x1="9.11351875" y1="1.4351" x2="9.49451875" y2="1.44348125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.44348125" x2="0.3683" y2="1.45211875" layer="94"/>
<rectangle x1="1.9685" y1="1.44348125" x2="2.34188125" y2="1.45211875" layer="94"/>
<rectangle x1="3.50011875" y1="1.44348125" x2="3.8735" y2="1.45211875" layer="94"/>
<rectangle x1="4.10971875" y1="1.44348125" x2="4.4831" y2="1.45211875" layer="94"/>
<rectangle x1="4.7371" y1="1.44348125" x2="5.36448125" y2="1.45211875" layer="94"/>
<rectangle x1="5.63371875" y1="1.44348125" x2="6.01471875" y2="1.45211875" layer="94"/>
<rectangle x1="6.6421" y1="1.44348125" x2="7.01548125" y2="1.45211875" layer="94"/>
<rectangle x1="7.58951875" y1="1.44348125" x2="7.9629" y2="1.45211875" layer="94"/>
<rectangle x1="8.61568125" y1="1.44348125" x2="9.0551" y2="1.45211875" layer="94"/>
<rectangle x1="9.11351875" y1="1.44348125" x2="9.49451875" y2="1.45211875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.45211875" x2="0.3683" y2="1.4605" layer="94"/>
<rectangle x1="1.9685" y1="1.45211875" x2="2.34188125" y2="1.4605" layer="94"/>
<rectangle x1="3.50011875" y1="1.45211875" x2="3.8735" y2="1.4605" layer="94"/>
<rectangle x1="4.10971875" y1="1.45211875" x2="4.4831" y2="1.4605" layer="94"/>
<rectangle x1="4.75488125" y1="1.45211875" x2="5.35431875" y2="1.4605" layer="94"/>
<rectangle x1="5.63371875" y1="1.45211875" x2="6.01471875" y2="1.4605" layer="94"/>
<rectangle x1="6.6421" y1="1.45211875" x2="7.01548125" y2="1.4605" layer="94"/>
<rectangle x1="7.58951875" y1="1.45211875" x2="7.9629" y2="1.4605" layer="94"/>
<rectangle x1="8.60551875" y1="1.45211875" x2="9.03731875" y2="1.4605" layer="94"/>
<rectangle x1="9.11351875" y1="1.45211875" x2="9.49451875" y2="1.4605" layer="94"/>
<rectangle x1="-0.00508125" y1="1.4605" x2="0.3683" y2="1.46888125" layer="94"/>
<rectangle x1="1.9685" y1="1.4605" x2="2.34188125" y2="1.46888125" layer="94"/>
<rectangle x1="3.50011875" y1="1.4605" x2="3.8735" y2="1.46888125" layer="94"/>
<rectangle x1="4.10971875" y1="1.4605" x2="4.4831" y2="1.46888125" layer="94"/>
<rectangle x1="4.7625" y1="1.4605" x2="5.33908125" y2="1.46888125" layer="94"/>
<rectangle x1="5.63371875" y1="1.4605" x2="6.01471875" y2="1.46888125" layer="94"/>
<rectangle x1="6.6421" y1="1.4605" x2="7.01548125" y2="1.46888125" layer="94"/>
<rectangle x1="7.58951875" y1="1.4605" x2="7.9629" y2="1.46888125" layer="94"/>
<rectangle x1="8.59028125" y1="1.4605" x2="9.0297" y2="1.46888125" layer="94"/>
<rectangle x1="9.11351875" y1="1.4605" x2="9.49451875" y2="1.46888125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.46888125" x2="0.3683" y2="1.47751875" layer="94"/>
<rectangle x1="1.9685" y1="1.46888125" x2="2.34188125" y2="1.47751875" layer="94"/>
<rectangle x1="3.50011875" y1="1.46888125" x2="3.8735" y2="1.47751875" layer="94"/>
<rectangle x1="4.10971875" y1="1.46888125" x2="4.4831" y2="1.47751875" layer="94"/>
<rectangle x1="4.78028125" y1="1.46888125" x2="5.3213" y2="1.47751875" layer="94"/>
<rectangle x1="5.63371875" y1="1.46888125" x2="6.01471875" y2="1.47751875" layer="94"/>
<rectangle x1="6.6421" y1="1.46888125" x2="7.01548125" y2="1.47751875" layer="94"/>
<rectangle x1="7.58951875" y1="1.46888125" x2="7.9629" y2="1.47751875" layer="94"/>
<rectangle x1="8.58011875" y1="1.46888125" x2="9.02208125" y2="1.47751875" layer="94"/>
<rectangle x1="9.11351875" y1="1.46888125" x2="9.49451875" y2="1.47751875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.47751875" x2="0.3683" y2="1.4859" layer="94"/>
<rectangle x1="1.9685" y1="1.47751875" x2="2.34188125" y2="1.4859" layer="94"/>
<rectangle x1="3.50011875" y1="1.47751875" x2="3.8735" y2="1.4859" layer="94"/>
<rectangle x1="4.10971875" y1="1.47751875" x2="4.4831" y2="1.4859" layer="94"/>
<rectangle x1="4.79551875" y1="1.47751875" x2="5.31368125" y2="1.4859" layer="94"/>
<rectangle x1="5.63371875" y1="1.47751875" x2="6.01471875" y2="1.4859" layer="94"/>
<rectangle x1="6.6421" y1="1.47751875" x2="7.01548125" y2="1.4859" layer="94"/>
<rectangle x1="7.58951875" y1="1.47751875" x2="7.9629" y2="1.4859" layer="94"/>
<rectangle x1="8.5725" y1="1.47751875" x2="9.01191875" y2="1.4859" layer="94"/>
<rectangle x1="9.11351875" y1="1.47751875" x2="9.49451875" y2="1.4859" layer="94"/>
<rectangle x1="-0.00508125" y1="1.4859" x2="0.3683" y2="1.49428125" layer="94"/>
<rectangle x1="1.9685" y1="1.4859" x2="2.34188125" y2="1.49428125" layer="94"/>
<rectangle x1="3.50011875" y1="1.4859" x2="3.8735" y2="1.49428125" layer="94"/>
<rectangle x1="4.10971875" y1="1.4859" x2="4.4831" y2="1.49428125" layer="94"/>
<rectangle x1="4.80568125" y1="1.4859" x2="5.2959" y2="1.49428125" layer="94"/>
<rectangle x1="5.63371875" y1="1.4859" x2="6.01471875" y2="1.49428125" layer="94"/>
<rectangle x1="6.6421" y1="1.4859" x2="7.01548125" y2="1.49428125" layer="94"/>
<rectangle x1="7.58951875" y1="1.4859" x2="7.9629" y2="1.49428125" layer="94"/>
<rectangle x1="8.56488125" y1="1.4859" x2="9.0043" y2="1.49428125" layer="94"/>
<rectangle x1="9.11351875" y1="1.4859" x2="9.49451875" y2="1.49428125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.49428125" x2="0.3683" y2="1.50291875" layer="94"/>
<rectangle x1="1.9685" y1="1.49428125" x2="2.34188125" y2="1.50291875" layer="94"/>
<rectangle x1="3.50011875" y1="1.49428125" x2="3.8735" y2="1.50291875" layer="94"/>
<rectangle x1="4.10971875" y1="1.49428125" x2="4.4831" y2="1.50291875" layer="94"/>
<rectangle x1="4.82091875" y1="1.49428125" x2="5.27811875" y2="1.50291875" layer="94"/>
<rectangle x1="5.63371875" y1="1.49428125" x2="6.01471875" y2="1.50291875" layer="94"/>
<rectangle x1="6.6421" y1="1.49428125" x2="7.01548125" y2="1.50291875" layer="94"/>
<rectangle x1="7.58951875" y1="1.49428125" x2="7.9629" y2="1.50291875" layer="94"/>
<rectangle x1="8.55471875" y1="1.49428125" x2="8.99668125" y2="1.50291875" layer="94"/>
<rectangle x1="9.11351875" y1="1.49428125" x2="9.49451875" y2="1.50291875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.50291875" x2="0.3683" y2="1.5113" layer="94"/>
<rectangle x1="1.9685" y1="1.50291875" x2="2.34188125" y2="1.5113" layer="94"/>
<rectangle x1="3.50011875" y1="1.50291875" x2="3.8735" y2="1.5113" layer="94"/>
<rectangle x1="4.10971875" y1="1.50291875" x2="4.4831" y2="1.5113" layer="94"/>
<rectangle x1="4.83108125" y1="1.50291875" x2="5.2705" y2="1.5113" layer="94"/>
<rectangle x1="5.63371875" y1="1.50291875" x2="6.01471875" y2="1.5113" layer="94"/>
<rectangle x1="6.6421" y1="1.50291875" x2="7.01548125" y2="1.5113" layer="94"/>
<rectangle x1="7.58951875" y1="1.50291875" x2="7.9629" y2="1.5113" layer="94"/>
<rectangle x1="8.5471" y1="1.50291875" x2="8.98651875" y2="1.5113" layer="94"/>
<rectangle x1="9.11351875" y1="1.50291875" x2="9.49451875" y2="1.5113" layer="94"/>
<rectangle x1="-0.00508125" y1="1.5113" x2="0.3683" y2="1.51968125" layer="94"/>
<rectangle x1="1.9685" y1="1.5113" x2="2.34188125" y2="1.51968125" layer="94"/>
<rectangle x1="3.50011875" y1="1.5113" x2="3.8735" y2="1.51968125" layer="94"/>
<rectangle x1="4.10971875" y1="1.5113" x2="4.4831" y2="1.51968125" layer="94"/>
<rectangle x1="4.84631875" y1="1.5113" x2="5.25271875" y2="1.51968125" layer="94"/>
<rectangle x1="5.63371875" y1="1.5113" x2="6.01471875" y2="1.51968125" layer="94"/>
<rectangle x1="6.6421" y1="1.5113" x2="7.01548125" y2="1.51968125" layer="94"/>
<rectangle x1="7.58951875" y1="1.5113" x2="7.9629" y2="1.51968125" layer="94"/>
<rectangle x1="8.53948125" y1="1.5113" x2="8.9789" y2="1.51968125" layer="94"/>
<rectangle x1="9.12368125" y1="1.5113" x2="9.49451875" y2="1.51968125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.51968125" x2="0.3683" y2="1.52831875" layer="94"/>
<rectangle x1="1.9685" y1="1.51968125" x2="2.34188125" y2="1.52831875" layer="94"/>
<rectangle x1="3.50011875" y1="1.51968125" x2="3.8735" y2="1.52831875" layer="94"/>
<rectangle x1="4.10971875" y1="1.51968125" x2="4.4831" y2="1.52831875" layer="94"/>
<rectangle x1="4.8641" y1="1.51968125" x2="5.23748125" y2="1.52831875" layer="94"/>
<rectangle x1="5.63371875" y1="1.51968125" x2="6.01471875" y2="1.52831875" layer="94"/>
<rectangle x1="6.6421" y1="1.51968125" x2="7.01548125" y2="1.52831875" layer="94"/>
<rectangle x1="7.58951875" y1="1.51968125" x2="7.9629" y2="1.52831875" layer="94"/>
<rectangle x1="8.52931875" y1="1.51968125" x2="8.97128125" y2="1.52831875" layer="94"/>
<rectangle x1="9.12368125" y1="1.51968125" x2="9.49451875" y2="1.52831875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.52831875" x2="0.3683" y2="1.5367" layer="94"/>
<rectangle x1="1.9685" y1="1.52831875" x2="2.34188125" y2="1.5367" layer="94"/>
<rectangle x1="3.50011875" y1="1.52831875" x2="3.8735" y2="1.5367" layer="94"/>
<rectangle x1="4.10971875" y1="1.52831875" x2="4.4831" y2="1.5367" layer="94"/>
<rectangle x1="4.88188125" y1="1.52831875" x2="5.2197" y2="1.5367" layer="94"/>
<rectangle x1="5.63371875" y1="1.52831875" x2="6.01471875" y2="1.5367" layer="94"/>
<rectangle x1="6.6421" y1="1.52831875" x2="7.01548125" y2="1.5367" layer="94"/>
<rectangle x1="7.58951875" y1="1.52831875" x2="7.9629" y2="1.5367" layer="94"/>
<rectangle x1="8.5217" y1="1.52831875" x2="8.96111875" y2="1.5367" layer="94"/>
<rectangle x1="9.12368125" y1="1.52831875" x2="9.49451875" y2="1.5367" layer="94"/>
<rectangle x1="-0.00508125" y1="1.5367" x2="0.3683" y2="1.54508125" layer="94"/>
<rectangle x1="1.9685" y1="1.5367" x2="2.34188125" y2="1.54508125" layer="94"/>
<rectangle x1="3.50011875" y1="1.5367" x2="3.8735" y2="1.54508125" layer="94"/>
<rectangle x1="4.10971875" y1="1.5367" x2="4.4831" y2="1.54508125" layer="94"/>
<rectangle x1="4.8895" y1="1.5367" x2="5.20191875" y2="1.54508125" layer="94"/>
<rectangle x1="5.63371875" y1="1.5367" x2="6.01471875" y2="1.54508125" layer="94"/>
<rectangle x1="6.6421" y1="1.5367" x2="7.01548125" y2="1.54508125" layer="94"/>
<rectangle x1="7.58951875" y1="1.5367" x2="7.9629" y2="1.54508125" layer="94"/>
<rectangle x1="8.51408125" y1="1.5367" x2="8.9535" y2="1.54508125" layer="94"/>
<rectangle x1="9.12368125" y1="1.5367" x2="9.49451875" y2="1.54508125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.54508125" x2="0.3683" y2="1.55371875" layer="94"/>
<rectangle x1="1.9685" y1="1.54508125" x2="2.34188125" y2="1.55371875" layer="94"/>
<rectangle x1="3.50011875" y1="1.54508125" x2="3.8735" y2="1.55371875" layer="94"/>
<rectangle x1="4.10971875" y1="1.54508125" x2="4.4831" y2="1.55371875" layer="94"/>
<rectangle x1="4.90728125" y1="1.54508125" x2="5.18668125" y2="1.55371875" layer="94"/>
<rectangle x1="5.63371875" y1="1.54508125" x2="6.01471875" y2="1.55371875" layer="94"/>
<rectangle x1="6.6421" y1="1.54508125" x2="7.01548125" y2="1.55371875" layer="94"/>
<rectangle x1="7.58951875" y1="1.54508125" x2="7.9629" y2="1.55371875" layer="94"/>
<rectangle x1="8.50391875" y1="1.54508125" x2="8.94588125" y2="1.55371875" layer="94"/>
<rectangle x1="9.12368125" y1="1.54508125" x2="9.49451875" y2="1.55371875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.55371875" x2="0.3683" y2="1.5621" layer="94"/>
<rectangle x1="1.9685" y1="1.55371875" x2="2.34188125" y2="1.5621" layer="94"/>
<rectangle x1="3.50011875" y1="1.55371875" x2="3.8735" y2="1.5621" layer="94"/>
<rectangle x1="4.10971875" y1="1.55371875" x2="4.4831" y2="1.5621" layer="94"/>
<rectangle x1="4.92251875" y1="1.55371875" x2="5.1689" y2="1.5621" layer="94"/>
<rectangle x1="5.63371875" y1="1.55371875" x2="6.01471875" y2="1.5621" layer="94"/>
<rectangle x1="6.6421" y1="1.55371875" x2="7.01548125" y2="1.5621" layer="94"/>
<rectangle x1="7.58951875" y1="1.55371875" x2="7.9629" y2="1.5621" layer="94"/>
<rectangle x1="8.4963" y1="1.55371875" x2="8.9281" y2="1.5621" layer="94"/>
<rectangle x1="9.12368125" y1="1.55371875" x2="9.49451875" y2="1.5621" layer="94"/>
<rectangle x1="-0.00508125" y1="1.5621" x2="0.3683" y2="1.57048125" layer="94"/>
<rectangle x1="1.9685" y1="1.5621" x2="2.34188125" y2="1.57048125" layer="94"/>
<rectangle x1="3.50011875" y1="1.5621" x2="3.8735" y2="1.57048125" layer="94"/>
<rectangle x1="4.10971875" y1="1.5621" x2="4.4831" y2="1.57048125" layer="94"/>
<rectangle x1="4.94791875" y1="1.5621" x2="5.1435" y2="1.57048125" layer="94"/>
<rectangle x1="5.63371875" y1="1.5621" x2="6.01471875" y2="1.57048125" layer="94"/>
<rectangle x1="6.6421" y1="1.5621" x2="7.01548125" y2="1.57048125" layer="94"/>
<rectangle x1="7.58951875" y1="1.5621" x2="7.9629" y2="1.57048125" layer="94"/>
<rectangle x1="8.47851875" y1="1.5621" x2="8.92048125" y2="1.57048125" layer="94"/>
<rectangle x1="9.12368125" y1="1.5621" x2="9.49451875" y2="1.57048125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.57048125" x2="0.3683" y2="1.57911875" layer="94"/>
<rectangle x1="1.9685" y1="1.57048125" x2="2.34188125" y2="1.57911875" layer="94"/>
<rectangle x1="3.50011875" y1="1.57048125" x2="3.8735" y2="1.57911875" layer="94"/>
<rectangle x1="4.10971875" y1="1.57048125" x2="4.4831" y2="1.57911875" layer="94"/>
<rectangle x1="4.97331875" y1="1.57048125" x2="5.1181" y2="1.57911875" layer="94"/>
<rectangle x1="5.63371875" y1="1.57048125" x2="6.01471875" y2="1.57911875" layer="94"/>
<rectangle x1="6.6421" y1="1.57048125" x2="7.01548125" y2="1.57911875" layer="94"/>
<rectangle x1="7.58951875" y1="1.57048125" x2="7.9629" y2="1.57911875" layer="94"/>
<rectangle x1="8.4709" y1="1.57048125" x2="8.91031875" y2="1.57911875" layer="94"/>
<rectangle x1="9.12368125" y1="1.57048125" x2="9.49451875" y2="1.57911875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.57911875" x2="0.3683" y2="1.5875" layer="94"/>
<rectangle x1="1.9685" y1="1.57911875" x2="2.34188125" y2="1.5875" layer="94"/>
<rectangle x1="3.50011875" y1="1.57911875" x2="3.8735" y2="1.5875" layer="94"/>
<rectangle x1="4.10971875" y1="1.57911875" x2="4.4831" y2="1.5875" layer="94"/>
<rectangle x1="4.99871875" y1="1.57911875" x2="5.0927" y2="1.5875" layer="94"/>
<rectangle x1="5.63371875" y1="1.57911875" x2="6.01471875" y2="1.5875" layer="94"/>
<rectangle x1="6.6421" y1="1.57911875" x2="7.01548125" y2="1.5875" layer="94"/>
<rectangle x1="7.58951875" y1="1.57911875" x2="7.9629" y2="1.5875" layer="94"/>
<rectangle x1="8.46328125" y1="1.57911875" x2="8.9027" y2="1.5875" layer="94"/>
<rectangle x1="9.12368125" y1="1.57911875" x2="9.49451875" y2="1.5875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.5875" x2="0.3683" y2="1.59588125" layer="94"/>
<rectangle x1="1.9685" y1="1.5875" x2="2.34188125" y2="1.59588125" layer="94"/>
<rectangle x1="3.50011875" y1="1.5875" x2="3.8735" y2="1.59588125" layer="94"/>
<rectangle x1="4.10971875" y1="1.5875" x2="4.4831" y2="1.59588125" layer="94"/>
<rectangle x1="5.63371875" y1="1.5875" x2="6.01471875" y2="1.59588125" layer="94"/>
<rectangle x1="6.6421" y1="1.5875" x2="7.01548125" y2="1.59588125" layer="94"/>
<rectangle x1="7.58951875" y1="1.5875" x2="7.9629" y2="1.59588125" layer="94"/>
<rectangle x1="8.45311875" y1="1.5875" x2="8.89508125" y2="1.59588125" layer="94"/>
<rectangle x1="9.12368125" y1="1.5875" x2="9.49451875" y2="1.59588125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.59588125" x2="1.3843" y2="1.60451875" layer="94"/>
<rectangle x1="1.9685" y1="1.59588125" x2="2.34188125" y2="1.60451875" layer="94"/>
<rectangle x1="3.50011875" y1="1.59588125" x2="3.8735" y2="1.60451875" layer="94"/>
<rectangle x1="4.10971875" y1="1.59588125" x2="4.4831" y2="1.60451875" layer="94"/>
<rectangle x1="5.63371875" y1="1.59588125" x2="6.01471875" y2="1.60451875" layer="94"/>
<rectangle x1="6.6421" y1="1.59588125" x2="7.01548125" y2="1.60451875" layer="94"/>
<rectangle x1="7.58951875" y1="1.59588125" x2="7.9629" y2="1.60451875" layer="94"/>
<rectangle x1="8.4455" y1="1.59588125" x2="8.88491875" y2="1.60451875" layer="94"/>
<rectangle x1="9.12368125" y1="1.59588125" x2="9.49451875" y2="1.60451875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.60451875" x2="1.41731875" y2="1.6129" layer="94"/>
<rectangle x1="1.9685" y1="1.60451875" x2="2.34188125" y2="1.6129" layer="94"/>
<rectangle x1="3.50011875" y1="1.60451875" x2="3.8735" y2="1.6129" layer="94"/>
<rectangle x1="4.10971875" y1="1.60451875" x2="4.4831" y2="1.6129" layer="94"/>
<rectangle x1="5.63371875" y1="1.60451875" x2="6.01471875" y2="1.6129" layer="94"/>
<rectangle x1="6.6421" y1="1.60451875" x2="7.01548125" y2="1.6129" layer="94"/>
<rectangle x1="7.58951875" y1="1.60451875" x2="7.9629" y2="1.6129" layer="94"/>
<rectangle x1="8.43788125" y1="1.60451875" x2="8.8773" y2="1.6129" layer="94"/>
<rectangle x1="9.12368125" y1="1.60451875" x2="9.49451875" y2="1.6129" layer="94"/>
<rectangle x1="-0.00508125" y1="1.6129" x2="1.4351" y2="1.62128125" layer="94"/>
<rectangle x1="1.9685" y1="1.6129" x2="2.34188125" y2="1.62128125" layer="94"/>
<rectangle x1="3.50011875" y1="1.6129" x2="3.8735" y2="1.62128125" layer="94"/>
<rectangle x1="4.10971875" y1="1.6129" x2="4.4831" y2="1.62128125" layer="94"/>
<rectangle x1="5.63371875" y1="1.6129" x2="6.01471875" y2="1.62128125" layer="94"/>
<rectangle x1="6.6421" y1="1.6129" x2="7.01548125" y2="1.62128125" layer="94"/>
<rectangle x1="7.58951875" y1="1.6129" x2="7.9629" y2="1.62128125" layer="94"/>
<rectangle x1="8.42771875" y1="1.6129" x2="8.86968125" y2="1.62128125" layer="94"/>
<rectangle x1="9.12368125" y1="1.6129" x2="9.49451875" y2="1.62128125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.62128125" x2="1.4605" y2="1.62991875" layer="94"/>
<rectangle x1="1.9685" y1="1.62128125" x2="2.34188125" y2="1.62991875" layer="94"/>
<rectangle x1="3.50011875" y1="1.62128125" x2="3.8735" y2="1.62991875" layer="94"/>
<rectangle x1="4.10971875" y1="1.62128125" x2="4.4831" y2="1.62991875" layer="94"/>
<rectangle x1="5.63371875" y1="1.62128125" x2="6.01471875" y2="1.62991875" layer="94"/>
<rectangle x1="6.6421" y1="1.62128125" x2="7.01548125" y2="1.62991875" layer="94"/>
<rectangle x1="7.58951875" y1="1.62128125" x2="7.9629" y2="1.62991875" layer="94"/>
<rectangle x1="8.4201" y1="1.62128125" x2="8.85951875" y2="1.62991875" layer="94"/>
<rectangle x1="9.12368125" y1="1.62128125" x2="9.49451875" y2="1.62991875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.62991875" x2="1.46811875" y2="1.6383" layer="94"/>
<rectangle x1="1.9685" y1="1.62991875" x2="2.34188125" y2="1.6383" layer="94"/>
<rectangle x1="3.50011875" y1="1.62991875" x2="3.8735" y2="1.6383" layer="94"/>
<rectangle x1="4.10971875" y1="1.62991875" x2="4.4831" y2="1.6383" layer="94"/>
<rectangle x1="5.63371875" y1="1.62991875" x2="6.01471875" y2="1.6383" layer="94"/>
<rectangle x1="6.6421" y1="1.62991875" x2="7.01548125" y2="1.6383" layer="94"/>
<rectangle x1="7.58951875" y1="1.62991875" x2="7.9629" y2="1.6383" layer="94"/>
<rectangle x1="8.41248125" y1="1.62991875" x2="8.8519" y2="1.6383" layer="94"/>
<rectangle x1="9.12368125" y1="1.62991875" x2="9.49451875" y2="1.6383" layer="94"/>
<rectangle x1="-0.00508125" y1="1.6383" x2="1.47828125" y2="1.64668125" layer="94"/>
<rectangle x1="1.9685" y1="1.6383" x2="2.34188125" y2="1.64668125" layer="94"/>
<rectangle x1="3.50011875" y1="1.6383" x2="3.8735" y2="1.64668125" layer="94"/>
<rectangle x1="4.10971875" y1="1.6383" x2="4.4831" y2="1.64668125" layer="94"/>
<rectangle x1="5.63371875" y1="1.6383" x2="6.01471875" y2="1.64668125" layer="94"/>
<rectangle x1="6.6421" y1="1.6383" x2="7.01548125" y2="1.64668125" layer="94"/>
<rectangle x1="7.58951875" y1="1.6383" x2="7.97051875" y2="1.64668125" layer="94"/>
<rectangle x1="8.40231875" y1="1.6383" x2="8.84428125" y2="1.64668125" layer="94"/>
<rectangle x1="9.12368125" y1="1.6383" x2="9.49451875" y2="1.64668125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.64668125" x2="1.4859" y2="1.65531875" layer="94"/>
<rectangle x1="1.9685" y1="1.64668125" x2="2.34188125" y2="1.65531875" layer="94"/>
<rectangle x1="3.50011875" y1="1.64668125" x2="3.8735" y2="1.65531875" layer="94"/>
<rectangle x1="4.10971875" y1="1.64668125" x2="4.4831" y2="1.65531875" layer="94"/>
<rectangle x1="5.63371875" y1="1.64668125" x2="6.01471875" y2="1.65531875" layer="94"/>
<rectangle x1="6.6421" y1="1.64668125" x2="7.01548125" y2="1.65531875" layer="94"/>
<rectangle x1="7.58951875" y1="1.64668125" x2="7.97051875" y2="1.65531875" layer="94"/>
<rectangle x1="8.3947" y1="1.64668125" x2="8.83411875" y2="1.65531875" layer="94"/>
<rectangle x1="9.12368125" y1="1.64668125" x2="9.49451875" y2="1.65531875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.65531875" x2="1.49351875" y2="1.6637" layer="94"/>
<rectangle x1="1.9685" y1="1.65531875" x2="2.34188125" y2="1.6637" layer="94"/>
<rectangle x1="3.50011875" y1="1.65531875" x2="3.8735" y2="1.6637" layer="94"/>
<rectangle x1="4.10971875" y1="1.65531875" x2="4.4831" y2="1.6637" layer="94"/>
<rectangle x1="5.63371875" y1="1.65531875" x2="6.01471875" y2="1.6637" layer="94"/>
<rectangle x1="6.6421" y1="1.65531875" x2="7.01548125" y2="1.6637" layer="94"/>
<rectangle x1="7.58951875" y1="1.65531875" x2="7.97051875" y2="1.6637" layer="94"/>
<rectangle x1="8.38708125" y1="1.65531875" x2="8.81888125" y2="1.6637" layer="94"/>
<rectangle x1="9.12368125" y1="1.65531875" x2="9.49451875" y2="1.6637" layer="94"/>
<rectangle x1="-0.00508125" y1="1.6637" x2="1.50368125" y2="1.67208125" layer="94"/>
<rectangle x1="1.9685" y1="1.6637" x2="2.34188125" y2="1.67208125" layer="94"/>
<rectangle x1="3.50011875" y1="1.6637" x2="3.8735" y2="1.67208125" layer="94"/>
<rectangle x1="4.10971875" y1="1.6637" x2="4.4831" y2="1.67208125" layer="94"/>
<rectangle x1="5.63371875" y1="1.6637" x2="6.01471875" y2="1.67208125" layer="94"/>
<rectangle x1="6.6421" y1="1.6637" x2="7.01548125" y2="1.67208125" layer="94"/>
<rectangle x1="7.58951875" y1="1.6637" x2="7.97051875" y2="1.67208125" layer="94"/>
<rectangle x1="8.3693" y1="1.6637" x2="8.80871875" y2="1.67208125" layer="94"/>
<rectangle x1="9.12368125" y1="1.6637" x2="9.49451875" y2="1.67208125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.67208125" x2="1.50368125" y2="1.68071875" layer="94"/>
<rectangle x1="1.9685" y1="1.67208125" x2="2.34188125" y2="1.68071875" layer="94"/>
<rectangle x1="3.50011875" y1="1.67208125" x2="3.8735" y2="1.68071875" layer="94"/>
<rectangle x1="4.10971875" y1="1.67208125" x2="4.4831" y2="1.68071875" layer="94"/>
<rectangle x1="5.63371875" y1="1.67208125" x2="6.01471875" y2="1.68071875" layer="94"/>
<rectangle x1="6.6421" y1="1.67208125" x2="7.01548125" y2="1.68071875" layer="94"/>
<rectangle x1="7.58951875" y1="1.67208125" x2="7.97051875" y2="1.68071875" layer="94"/>
<rectangle x1="8.3693" y1="1.67208125" x2="8.8011" y2="1.68071875" layer="94"/>
<rectangle x1="9.12368125" y1="1.67208125" x2="9.49451875" y2="1.68071875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.68071875" x2="1.5113" y2="1.6891" layer="94"/>
<rectangle x1="1.9685" y1="1.68071875" x2="2.34188125" y2="1.6891" layer="94"/>
<rectangle x1="3.50011875" y1="1.68071875" x2="3.8735" y2="1.6891" layer="94"/>
<rectangle x1="4.10971875" y1="1.68071875" x2="4.4831" y2="1.6891" layer="94"/>
<rectangle x1="5.63371875" y1="1.68071875" x2="6.01471875" y2="1.6891" layer="94"/>
<rectangle x1="6.6421" y1="1.68071875" x2="7.01548125" y2="1.6891" layer="94"/>
<rectangle x1="7.58951875" y1="1.68071875" x2="7.97051875" y2="1.6891" layer="94"/>
<rectangle x1="8.35151875" y1="1.68071875" x2="8.79348125" y2="1.6891" layer="94"/>
<rectangle x1="9.12368125" y1="1.68071875" x2="9.49451875" y2="1.6891" layer="94"/>
<rectangle x1="-0.00508125" y1="1.6891" x2="1.5113" y2="1.69748125" layer="94"/>
<rectangle x1="1.9685" y1="1.6891" x2="2.34188125" y2="1.69748125" layer="94"/>
<rectangle x1="3.50011875" y1="1.6891" x2="3.8735" y2="1.69748125" layer="94"/>
<rectangle x1="4.10971875" y1="1.6891" x2="4.4831" y2="1.69748125" layer="94"/>
<rectangle x1="5.63371875" y1="1.6891" x2="6.01471875" y2="1.69748125" layer="94"/>
<rectangle x1="6.6421" y1="1.6891" x2="7.01548125" y2="1.69748125" layer="94"/>
<rectangle x1="7.58951875" y1="1.6891" x2="7.97051875" y2="1.69748125" layer="94"/>
<rectangle x1="8.3439" y1="1.6891" x2="8.78331875" y2="1.69748125" layer="94"/>
<rectangle x1="9.12368125" y1="1.6891" x2="9.49451875" y2="1.69748125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.69748125" x2="1.5113" y2="1.70611875" layer="94"/>
<rectangle x1="1.9685" y1="1.69748125" x2="2.34188125" y2="1.70611875" layer="94"/>
<rectangle x1="3.50011875" y1="1.69748125" x2="3.8735" y2="1.70611875" layer="94"/>
<rectangle x1="4.10971875" y1="1.69748125" x2="4.4831" y2="1.70611875" layer="94"/>
<rectangle x1="5.63371875" y1="1.69748125" x2="6.01471875" y2="1.70611875" layer="94"/>
<rectangle x1="6.6421" y1="1.69748125" x2="7.01548125" y2="1.70611875" layer="94"/>
<rectangle x1="7.58951875" y1="1.69748125" x2="7.97051875" y2="1.70611875" layer="94"/>
<rectangle x1="8.33628125" y1="1.69748125" x2="8.7757" y2="1.70611875" layer="94"/>
<rectangle x1="9.12368125" y1="1.69748125" x2="9.49451875" y2="1.70611875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.70611875" x2="1.5113" y2="1.7145" layer="94"/>
<rectangle x1="1.9685" y1="1.70611875" x2="2.34188125" y2="1.7145" layer="94"/>
<rectangle x1="3.50011875" y1="1.70611875" x2="3.8735" y2="1.7145" layer="94"/>
<rectangle x1="4.10971875" y1="1.70611875" x2="4.4831" y2="1.7145" layer="94"/>
<rectangle x1="5.63371875" y1="1.70611875" x2="6.01471875" y2="1.7145" layer="94"/>
<rectangle x1="6.6421" y1="1.70611875" x2="7.01548125" y2="1.7145" layer="94"/>
<rectangle x1="7.58951875" y1="1.70611875" x2="7.97051875" y2="1.7145" layer="94"/>
<rectangle x1="8.32611875" y1="1.70611875" x2="8.76808125" y2="1.7145" layer="94"/>
<rectangle x1="9.12368125" y1="1.70611875" x2="9.49451875" y2="1.7145" layer="94"/>
<rectangle x1="-0.00508125" y1="1.7145" x2="1.5113" y2="1.72288125" layer="94"/>
<rectangle x1="1.9685" y1="1.7145" x2="2.34188125" y2="1.72288125" layer="94"/>
<rectangle x1="3.50011875" y1="1.7145" x2="3.8735" y2="1.72288125" layer="94"/>
<rectangle x1="4.10971875" y1="1.7145" x2="4.4831" y2="1.72288125" layer="94"/>
<rectangle x1="5.63371875" y1="1.7145" x2="6.01471875" y2="1.72288125" layer="94"/>
<rectangle x1="6.6421" y1="1.7145" x2="7.01548125" y2="1.72288125" layer="94"/>
<rectangle x1="7.58951875" y1="1.7145" x2="7.97051875" y2="1.72288125" layer="94"/>
<rectangle x1="8.3185" y1="1.7145" x2="8.75791875" y2="1.72288125" layer="94"/>
<rectangle x1="9.12368125" y1="1.7145" x2="9.49451875" y2="1.72288125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.72288125" x2="1.5113" y2="1.73151875" layer="94"/>
<rectangle x1="1.9685" y1="1.72288125" x2="2.34188125" y2="1.73151875" layer="94"/>
<rectangle x1="3.50011875" y1="1.72288125" x2="3.8735" y2="1.73151875" layer="94"/>
<rectangle x1="4.10971875" y1="1.72288125" x2="4.4831" y2="1.73151875" layer="94"/>
<rectangle x1="5.63371875" y1="1.72288125" x2="6.01471875" y2="1.73151875" layer="94"/>
<rectangle x1="6.6421" y1="1.72288125" x2="7.01548125" y2="1.73151875" layer="94"/>
<rectangle x1="7.58951875" y1="1.72288125" x2="7.97051875" y2="1.73151875" layer="94"/>
<rectangle x1="8.31088125" y1="1.72288125" x2="8.7503" y2="1.73151875" layer="94"/>
<rectangle x1="9.12368125" y1="1.72288125" x2="9.49451875" y2="1.73151875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.73151875" x2="1.5113" y2="1.7399" layer="94"/>
<rectangle x1="1.9685" y1="1.73151875" x2="2.34188125" y2="1.7399" layer="94"/>
<rectangle x1="3.50011875" y1="1.73151875" x2="3.8735" y2="1.7399" layer="94"/>
<rectangle x1="4.10971875" y1="1.73151875" x2="4.4831" y2="1.7399" layer="94"/>
<rectangle x1="5.63371875" y1="1.73151875" x2="6.01471875" y2="1.7399" layer="94"/>
<rectangle x1="6.6421" y1="1.73151875" x2="7.01548125" y2="1.7399" layer="94"/>
<rectangle x1="7.58951875" y1="1.73151875" x2="7.97051875" y2="1.7399" layer="94"/>
<rectangle x1="8.30071875" y1="1.73151875" x2="8.74268125" y2="1.7399" layer="94"/>
<rectangle x1="9.12368125" y1="1.73151875" x2="9.49451875" y2="1.7399" layer="94"/>
<rectangle x1="-0.00508125" y1="1.7399" x2="1.5113" y2="1.74828125" layer="94"/>
<rectangle x1="1.9685" y1="1.7399" x2="2.34188125" y2="1.74828125" layer="94"/>
<rectangle x1="3.50011875" y1="1.7399" x2="3.8735" y2="1.74828125" layer="94"/>
<rectangle x1="4.10971875" y1="1.7399" x2="4.4831" y2="1.74828125" layer="94"/>
<rectangle x1="5.63371875" y1="1.7399" x2="6.01471875" y2="1.74828125" layer="94"/>
<rectangle x1="6.6421" y1="1.7399" x2="7.01548125" y2="1.74828125" layer="94"/>
<rectangle x1="7.58951875" y1="1.7399" x2="7.97051875" y2="1.74828125" layer="94"/>
<rectangle x1="8.2931" y1="1.7399" x2="8.73251875" y2="1.74828125" layer="94"/>
<rectangle x1="9.12368125" y1="1.7399" x2="9.49451875" y2="1.74828125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.74828125" x2="1.50368125" y2="1.75691875" layer="94"/>
<rectangle x1="1.9685" y1="1.74828125" x2="2.34188125" y2="1.75691875" layer="94"/>
<rectangle x1="3.50011875" y1="1.74828125" x2="3.8735" y2="1.75691875" layer="94"/>
<rectangle x1="4.10971875" y1="1.74828125" x2="4.4831" y2="1.75691875" layer="94"/>
<rectangle x1="5.63371875" y1="1.74828125" x2="6.01471875" y2="1.75691875" layer="94"/>
<rectangle x1="6.6421" y1="1.74828125" x2="7.01548125" y2="1.75691875" layer="94"/>
<rectangle x1="7.58951875" y1="1.74828125" x2="7.97051875" y2="1.75691875" layer="94"/>
<rectangle x1="8.28548125" y1="1.74828125" x2="8.7249" y2="1.75691875" layer="94"/>
<rectangle x1="9.12368125" y1="1.74828125" x2="9.49451875" y2="1.75691875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.75691875" x2="1.50368125" y2="1.7653" layer="94"/>
<rectangle x1="1.9685" y1="1.75691875" x2="2.34188125" y2="1.7653" layer="94"/>
<rectangle x1="3.50011875" y1="1.75691875" x2="3.8735" y2="1.7653" layer="94"/>
<rectangle x1="4.10971875" y1="1.75691875" x2="4.4831" y2="1.7653" layer="94"/>
<rectangle x1="5.63371875" y1="1.75691875" x2="6.01471875" y2="1.7653" layer="94"/>
<rectangle x1="6.6421" y1="1.75691875" x2="7.01548125" y2="1.7653" layer="94"/>
<rectangle x1="7.58951875" y1="1.75691875" x2="7.97051875" y2="1.7653" layer="94"/>
<rectangle x1="8.27531875" y1="1.75691875" x2="8.70711875" y2="1.7653" layer="94"/>
<rectangle x1="9.12368125" y1="1.75691875" x2="9.49451875" y2="1.7653" layer="94"/>
<rectangle x1="-0.00508125" y1="1.7653" x2="1.49351875" y2="1.77368125" layer="94"/>
<rectangle x1="1.9685" y1="1.7653" x2="2.34188125" y2="1.77368125" layer="94"/>
<rectangle x1="3.50011875" y1="1.7653" x2="3.8735" y2="1.77368125" layer="94"/>
<rectangle x1="4.10971875" y1="1.7653" x2="4.4831" y2="1.77368125" layer="94"/>
<rectangle x1="5.63371875" y1="1.7653" x2="6.01471875" y2="1.77368125" layer="94"/>
<rectangle x1="6.6421" y1="1.7653" x2="7.01548125" y2="1.77368125" layer="94"/>
<rectangle x1="7.58951875" y1="1.7653" x2="7.97051875" y2="1.77368125" layer="94"/>
<rectangle x1="8.2677" y1="1.7653" x2="8.6995" y2="1.77368125" layer="94"/>
<rectangle x1="9.12368125" y1="1.7653" x2="9.49451875" y2="1.77368125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.77368125" x2="1.4859" y2="1.78231875" layer="94"/>
<rectangle x1="1.9685" y1="1.77368125" x2="2.34188125" y2="1.78231875" layer="94"/>
<rectangle x1="3.50011875" y1="1.77368125" x2="3.8735" y2="1.78231875" layer="94"/>
<rectangle x1="4.10971875" y1="1.77368125" x2="4.4831" y2="1.78231875" layer="94"/>
<rectangle x1="5.63371875" y1="1.77368125" x2="6.01471875" y2="1.78231875" layer="94"/>
<rectangle x1="6.6421" y1="1.77368125" x2="7.01548125" y2="1.78231875" layer="94"/>
<rectangle x1="7.58951875" y1="1.77368125" x2="7.97051875" y2="1.78231875" layer="94"/>
<rectangle x1="8.26008125" y1="1.77368125" x2="8.69188125" y2="1.78231875" layer="94"/>
<rectangle x1="9.12368125" y1="1.77368125" x2="9.49451875" y2="1.78231875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.78231875" x2="1.47828125" y2="1.7907" layer="94"/>
<rectangle x1="1.9685" y1="1.78231875" x2="2.34188125" y2="1.7907" layer="94"/>
<rectangle x1="3.50011875" y1="1.78231875" x2="3.8735" y2="1.7907" layer="94"/>
<rectangle x1="4.10971875" y1="1.78231875" x2="4.4831" y2="1.7907" layer="94"/>
<rectangle x1="5.63371875" y1="1.78231875" x2="6.01471875" y2="1.7907" layer="94"/>
<rectangle x1="6.6421" y1="1.78231875" x2="7.01548125" y2="1.7907" layer="94"/>
<rectangle x1="7.58951875" y1="1.78231875" x2="7.97051875" y2="1.7907" layer="94"/>
<rectangle x1="8.2423" y1="1.78231875" x2="8.68171875" y2="1.7907" layer="94"/>
<rectangle x1="9.12368125" y1="1.78231875" x2="9.49451875" y2="1.7907" layer="94"/>
<rectangle x1="-0.00508125" y1="1.7907" x2="1.46811875" y2="1.79908125" layer="94"/>
<rectangle x1="1.9685" y1="1.7907" x2="2.34188125" y2="1.79908125" layer="94"/>
<rectangle x1="3.50011875" y1="1.7907" x2="3.8735" y2="1.79908125" layer="94"/>
<rectangle x1="4.10971875" y1="1.7907" x2="4.4831" y2="1.79908125" layer="94"/>
<rectangle x1="5.63371875" y1="1.7907" x2="6.01471875" y2="1.79908125" layer="94"/>
<rectangle x1="6.6421" y1="1.7907" x2="7.01548125" y2="1.79908125" layer="94"/>
<rectangle x1="7.58951875" y1="1.7907" x2="7.97051875" y2="1.79908125" layer="94"/>
<rectangle x1="8.23468125" y1="1.7907" x2="8.6741" y2="1.79908125" layer="94"/>
<rectangle x1="9.12368125" y1="1.7907" x2="9.49451875" y2="1.79908125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.79908125" x2="1.45288125" y2="1.80771875" layer="94"/>
<rectangle x1="1.9685" y1="1.79908125" x2="2.34188125" y2="1.80771875" layer="94"/>
<rectangle x1="3.50011875" y1="1.79908125" x2="3.8735" y2="1.80771875" layer="94"/>
<rectangle x1="4.10971875" y1="1.79908125" x2="4.4831" y2="1.80771875" layer="94"/>
<rectangle x1="5.63371875" y1="1.79908125" x2="6.01471875" y2="1.80771875" layer="94"/>
<rectangle x1="6.6421" y1="1.79908125" x2="7.01548125" y2="1.80771875" layer="94"/>
<rectangle x1="7.58951875" y1="1.79908125" x2="7.97051875" y2="1.80771875" layer="94"/>
<rectangle x1="8.22451875" y1="1.79908125" x2="8.66648125" y2="1.80771875" layer="94"/>
<rectangle x1="9.12368125" y1="1.79908125" x2="9.49451875" y2="1.80771875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.80771875" x2="1.4351" y2="1.8161" layer="94"/>
<rectangle x1="1.9685" y1="1.80771875" x2="2.34188125" y2="1.8161" layer="94"/>
<rectangle x1="3.50011875" y1="1.80771875" x2="3.8735" y2="1.8161" layer="94"/>
<rectangle x1="4.10971875" y1="1.80771875" x2="4.4831" y2="1.8161" layer="94"/>
<rectangle x1="5.63371875" y1="1.80771875" x2="6.01471875" y2="1.8161" layer="94"/>
<rectangle x1="6.6421" y1="1.80771875" x2="7.01548125" y2="1.8161" layer="94"/>
<rectangle x1="7.58951875" y1="1.80771875" x2="7.97051875" y2="1.8161" layer="94"/>
<rectangle x1="8.2169" y1="1.80771875" x2="8.65631875" y2="1.8161" layer="94"/>
<rectangle x1="9.12368125" y1="1.80771875" x2="9.49451875" y2="1.8161" layer="94"/>
<rectangle x1="-0.00508125" y1="1.8161" x2="1.4097" y2="1.82448125" layer="94"/>
<rectangle x1="1.9685" y1="1.8161" x2="2.34188125" y2="1.82448125" layer="94"/>
<rectangle x1="3.50011875" y1="1.8161" x2="3.8735" y2="1.82448125" layer="94"/>
<rectangle x1="4.10971875" y1="1.8161" x2="4.4831" y2="1.82448125" layer="94"/>
<rectangle x1="5.63371875" y1="1.8161" x2="6.01471875" y2="1.82448125" layer="94"/>
<rectangle x1="6.6421" y1="1.8161" x2="7.01548125" y2="1.82448125" layer="94"/>
<rectangle x1="7.58951875" y1="1.8161" x2="7.97051875" y2="1.82448125" layer="94"/>
<rectangle x1="8.20928125" y1="1.8161" x2="8.6487" y2="1.82448125" layer="94"/>
<rectangle x1="9.12368125" y1="1.8161" x2="9.49451875" y2="1.82448125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.82448125" x2="0.3683" y2="1.83311875" layer="94"/>
<rectangle x1="1.9685" y1="1.82448125" x2="2.34188125" y2="1.83311875" layer="94"/>
<rectangle x1="3.50011875" y1="1.82448125" x2="3.8735" y2="1.83311875" layer="94"/>
<rectangle x1="4.10971875" y1="1.82448125" x2="4.4831" y2="1.83311875" layer="94"/>
<rectangle x1="5.63371875" y1="1.82448125" x2="6.01471875" y2="1.83311875" layer="94"/>
<rectangle x1="6.6421" y1="1.82448125" x2="7.01548125" y2="1.83311875" layer="94"/>
<rectangle x1="7.58951875" y1="1.82448125" x2="7.97051875" y2="1.83311875" layer="94"/>
<rectangle x1="8.19911875" y1="1.82448125" x2="8.64108125" y2="1.83311875" layer="94"/>
<rectangle x1="9.12368125" y1="1.82448125" x2="9.49451875" y2="1.83311875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.83311875" x2="0.3683" y2="1.8415" layer="94"/>
<rectangle x1="1.9685" y1="1.83311875" x2="2.34188125" y2="1.8415" layer="94"/>
<rectangle x1="3.50011875" y1="1.83311875" x2="3.8735" y2="1.8415" layer="94"/>
<rectangle x1="4.10971875" y1="1.83311875" x2="4.4831" y2="1.8415" layer="94"/>
<rectangle x1="5.63371875" y1="1.83311875" x2="6.01471875" y2="1.8415" layer="94"/>
<rectangle x1="6.6421" y1="1.83311875" x2="7.01548125" y2="1.8415" layer="94"/>
<rectangle x1="7.58951875" y1="1.83311875" x2="7.97051875" y2="1.8415" layer="94"/>
<rectangle x1="8.1915" y1="1.83311875" x2="8.63091875" y2="1.8415" layer="94"/>
<rectangle x1="9.12368125" y1="1.83311875" x2="9.49451875" y2="1.8415" layer="94"/>
<rectangle x1="-0.00508125" y1="1.8415" x2="0.3683" y2="1.84988125" layer="94"/>
<rectangle x1="1.9685" y1="1.8415" x2="2.34188125" y2="1.84988125" layer="94"/>
<rectangle x1="3.50011875" y1="1.8415" x2="3.8735" y2="1.84988125" layer="94"/>
<rectangle x1="4.10971875" y1="1.8415" x2="4.4831" y2="1.84988125" layer="94"/>
<rectangle x1="5.63371875" y1="1.8415" x2="6.01471875" y2="1.84988125" layer="94"/>
<rectangle x1="6.6421" y1="1.8415" x2="7.01548125" y2="1.84988125" layer="94"/>
<rectangle x1="7.58951875" y1="1.8415" x2="7.97051875" y2="1.84988125" layer="94"/>
<rectangle x1="8.18388125" y1="1.8415" x2="8.6233" y2="1.84988125" layer="94"/>
<rectangle x1="9.12368125" y1="1.8415" x2="9.49451875" y2="1.84988125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.84988125" x2="0.3683" y2="1.85851875" layer="94"/>
<rectangle x1="1.9685" y1="1.84988125" x2="2.34188125" y2="1.85851875" layer="94"/>
<rectangle x1="3.50011875" y1="1.84988125" x2="3.8735" y2="1.85851875" layer="94"/>
<rectangle x1="4.10971875" y1="1.84988125" x2="4.4831" y2="1.85851875" layer="94"/>
<rectangle x1="5.63371875" y1="1.84988125" x2="6.01471875" y2="1.85851875" layer="94"/>
<rectangle x1="6.6421" y1="1.84988125" x2="7.01548125" y2="1.85851875" layer="94"/>
<rectangle x1="7.58951875" y1="1.84988125" x2="7.97051875" y2="1.85851875" layer="94"/>
<rectangle x1="8.17371875" y1="1.84988125" x2="8.61568125" y2="1.85851875" layer="94"/>
<rectangle x1="9.12368125" y1="1.84988125" x2="9.49451875" y2="1.85851875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.85851875" x2="0.3683" y2="1.8669" layer="94"/>
<rectangle x1="1.9685" y1="1.85851875" x2="2.34188125" y2="1.8669" layer="94"/>
<rectangle x1="3.50011875" y1="1.85851875" x2="3.8735" y2="1.8669" layer="94"/>
<rectangle x1="4.10971875" y1="1.85851875" x2="4.4831" y2="1.8669" layer="94"/>
<rectangle x1="5.63371875" y1="1.85851875" x2="6.01471875" y2="1.8669" layer="94"/>
<rectangle x1="6.6421" y1="1.85851875" x2="7.01548125" y2="1.8669" layer="94"/>
<rectangle x1="7.58951875" y1="1.85851875" x2="7.97051875" y2="1.8669" layer="94"/>
<rectangle x1="8.1661" y1="1.85851875" x2="8.5979" y2="1.8669" layer="94"/>
<rectangle x1="9.12368125" y1="1.85851875" x2="9.49451875" y2="1.8669" layer="94"/>
<rectangle x1="-0.00508125" y1="1.8669" x2="0.3683" y2="1.87528125" layer="94"/>
<rectangle x1="1.9685" y1="1.8669" x2="2.34188125" y2="1.87528125" layer="94"/>
<rectangle x1="3.50011875" y1="1.8669" x2="3.8735" y2="1.87528125" layer="94"/>
<rectangle x1="4.10971875" y1="1.8669" x2="4.4831" y2="1.87528125" layer="94"/>
<rectangle x1="5.63371875" y1="1.8669" x2="6.01471875" y2="1.87528125" layer="94"/>
<rectangle x1="6.6421" y1="1.8669" x2="7.01548125" y2="1.87528125" layer="94"/>
<rectangle x1="7.58951875" y1="1.8669" x2="7.97051875" y2="1.87528125" layer="94"/>
<rectangle x1="8.15848125" y1="1.8669" x2="8.5979" y2="1.87528125" layer="94"/>
<rectangle x1="9.12368125" y1="1.8669" x2="9.49451875" y2="1.87528125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.87528125" x2="0.3683" y2="1.88391875" layer="94"/>
<rectangle x1="1.9685" y1="1.87528125" x2="2.34188125" y2="1.88391875" layer="94"/>
<rectangle x1="3.50011875" y1="1.87528125" x2="3.8735" y2="1.88391875" layer="94"/>
<rectangle x1="4.10971875" y1="1.87528125" x2="4.4831" y2="1.88391875" layer="94"/>
<rectangle x1="5.63371875" y1="1.87528125" x2="6.01471875" y2="1.88391875" layer="94"/>
<rectangle x1="6.6421" y1="1.87528125" x2="7.01548125" y2="1.88391875" layer="94"/>
<rectangle x1="7.58951875" y1="1.87528125" x2="7.97051875" y2="1.88391875" layer="94"/>
<rectangle x1="8.14831875" y1="1.87528125" x2="8.58011875" y2="1.88391875" layer="94"/>
<rectangle x1="9.12368125" y1="1.87528125" x2="9.49451875" y2="1.88391875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.88391875" x2="0.3683" y2="1.8923" layer="94"/>
<rectangle x1="1.9685" y1="1.88391875" x2="2.34188125" y2="1.8923" layer="94"/>
<rectangle x1="3.50011875" y1="1.88391875" x2="3.8735" y2="1.8923" layer="94"/>
<rectangle x1="4.10971875" y1="1.88391875" x2="4.4831" y2="1.8923" layer="94"/>
<rectangle x1="5.63371875" y1="1.88391875" x2="6.01471875" y2="1.8923" layer="94"/>
<rectangle x1="6.6421" y1="1.88391875" x2="7.01548125" y2="1.8923" layer="94"/>
<rectangle x1="7.58951875" y1="1.88391875" x2="7.97051875" y2="1.8923" layer="94"/>
<rectangle x1="8.1407" y1="1.88391875" x2="8.5725" y2="1.8923" layer="94"/>
<rectangle x1="9.12368125" y1="1.88391875" x2="9.49451875" y2="1.8923" layer="94"/>
<rectangle x1="-0.00508125" y1="1.8923" x2="0.3683" y2="1.90068125" layer="94"/>
<rectangle x1="1.9685" y1="1.8923" x2="2.34188125" y2="1.90068125" layer="94"/>
<rectangle x1="3.50011875" y1="1.8923" x2="3.8735" y2="1.90068125" layer="94"/>
<rectangle x1="4.10971875" y1="1.8923" x2="4.4831" y2="1.90068125" layer="94"/>
<rectangle x1="5.63371875" y1="1.8923" x2="6.01471875" y2="1.90068125" layer="94"/>
<rectangle x1="6.6421" y1="1.8923" x2="7.01548125" y2="1.90068125" layer="94"/>
<rectangle x1="7.58951875" y1="1.8923" x2="7.97051875" y2="1.90068125" layer="94"/>
<rectangle x1="8.12291875" y1="1.8923" x2="8.56488125" y2="1.90068125" layer="94"/>
<rectangle x1="9.12368125" y1="1.8923" x2="9.49451875" y2="1.90068125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.90068125" x2="0.3683" y2="1.90931875" layer="94"/>
<rectangle x1="1.9685" y1="1.90068125" x2="2.34188125" y2="1.90931875" layer="94"/>
<rectangle x1="3.50011875" y1="1.90068125" x2="3.8735" y2="1.90931875" layer="94"/>
<rectangle x1="4.10971875" y1="1.90068125" x2="4.4831" y2="1.90931875" layer="94"/>
<rectangle x1="5.63371875" y1="1.90068125" x2="6.01471875" y2="1.90931875" layer="94"/>
<rectangle x1="6.6421" y1="1.90068125" x2="7.01548125" y2="1.90931875" layer="94"/>
<rectangle x1="7.58951875" y1="1.90068125" x2="7.97051875" y2="1.90931875" layer="94"/>
<rectangle x1="8.1153" y1="1.90068125" x2="8.55471875" y2="1.90931875" layer="94"/>
<rectangle x1="9.12368125" y1="1.90068125" x2="9.49451875" y2="1.90931875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.90931875" x2="0.3683" y2="1.9177" layer="94"/>
<rectangle x1="1.9685" y1="1.90931875" x2="2.34188125" y2="1.9177" layer="94"/>
<rectangle x1="3.50011875" y1="1.90931875" x2="3.8735" y2="1.9177" layer="94"/>
<rectangle x1="4.10971875" y1="1.90931875" x2="4.4831" y2="1.9177" layer="94"/>
<rectangle x1="5.63371875" y1="1.90931875" x2="6.01471875" y2="1.9177" layer="94"/>
<rectangle x1="6.6421" y1="1.90931875" x2="7.01548125" y2="1.9177" layer="94"/>
<rectangle x1="7.58951875" y1="1.90931875" x2="7.97051875" y2="1.9177" layer="94"/>
<rectangle x1="8.10768125" y1="1.90931875" x2="8.5471" y2="1.9177" layer="94"/>
<rectangle x1="9.12368125" y1="1.90931875" x2="9.49451875" y2="1.9177" layer="94"/>
<rectangle x1="-0.00508125" y1="1.9177" x2="0.3683" y2="1.92608125" layer="94"/>
<rectangle x1="1.9685" y1="1.9177" x2="2.34188125" y2="1.92608125" layer="94"/>
<rectangle x1="3.50011875" y1="1.9177" x2="3.8735" y2="1.92608125" layer="94"/>
<rectangle x1="4.10971875" y1="1.9177" x2="4.4831" y2="1.92608125" layer="94"/>
<rectangle x1="5.63371875" y1="1.9177" x2="6.01471875" y2="1.92608125" layer="94"/>
<rectangle x1="6.6421" y1="1.9177" x2="7.01548125" y2="1.92608125" layer="94"/>
<rectangle x1="7.58951875" y1="1.9177" x2="7.97051875" y2="1.92608125" layer="94"/>
<rectangle x1="8.09751875" y1="1.9177" x2="8.53948125" y2="1.92608125" layer="94"/>
<rectangle x1="9.12368125" y1="1.9177" x2="9.49451875" y2="1.92608125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.92608125" x2="0.3683" y2="1.93471875" layer="94"/>
<rectangle x1="1.9685" y1="1.92608125" x2="2.34188125" y2="1.93471875" layer="94"/>
<rectangle x1="3.50011875" y1="1.92608125" x2="3.8735" y2="1.93471875" layer="94"/>
<rectangle x1="4.10971875" y1="1.92608125" x2="4.4831" y2="1.93471875" layer="94"/>
<rectangle x1="5.63371875" y1="1.92608125" x2="6.01471875" y2="1.93471875" layer="94"/>
<rectangle x1="6.6421" y1="1.92608125" x2="7.01548125" y2="1.93471875" layer="94"/>
<rectangle x1="7.58951875" y1="1.92608125" x2="7.97051875" y2="1.93471875" layer="94"/>
<rectangle x1="8.0899" y1="1.92608125" x2="8.52931875" y2="1.93471875" layer="94"/>
<rectangle x1="9.12368125" y1="1.92608125" x2="9.49451875" y2="1.93471875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.93471875" x2="0.3683" y2="1.9431" layer="94"/>
<rectangle x1="1.9685" y1="1.93471875" x2="2.34188125" y2="1.9431" layer="94"/>
<rectangle x1="3.50011875" y1="1.93471875" x2="3.8735" y2="1.9431" layer="94"/>
<rectangle x1="4.10971875" y1="1.93471875" x2="4.4831" y2="1.9431" layer="94"/>
<rectangle x1="5.63371875" y1="1.93471875" x2="6.01471875" y2="1.9431" layer="94"/>
<rectangle x1="6.6421" y1="1.93471875" x2="7.01548125" y2="1.9431" layer="94"/>
<rectangle x1="7.58951875" y1="1.93471875" x2="7.97051875" y2="1.9431" layer="94"/>
<rectangle x1="8.08228125" y1="1.93471875" x2="8.5217" y2="1.9431" layer="94"/>
<rectangle x1="9.12368125" y1="1.93471875" x2="9.49451875" y2="1.9431" layer="94"/>
<rectangle x1="-0.00508125" y1="1.9431" x2="0.3683" y2="1.95148125" layer="94"/>
<rectangle x1="1.9685" y1="1.9431" x2="2.34188125" y2="1.95148125" layer="94"/>
<rectangle x1="3.50011875" y1="1.9431" x2="3.8735" y2="1.95148125" layer="94"/>
<rectangle x1="4.10971875" y1="1.9431" x2="4.4831" y2="1.95148125" layer="94"/>
<rectangle x1="5.63371875" y1="1.9431" x2="6.01471875" y2="1.95148125" layer="94"/>
<rectangle x1="6.6421" y1="1.9431" x2="7.01548125" y2="1.95148125" layer="94"/>
<rectangle x1="7.58951875" y1="1.9431" x2="7.97051875" y2="1.95148125" layer="94"/>
<rectangle x1="8.07211875" y1="1.9431" x2="8.51408125" y2="1.95148125" layer="94"/>
<rectangle x1="9.12368125" y1="1.9431" x2="9.49451875" y2="1.95148125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.95148125" x2="0.3683" y2="1.96011875" layer="94"/>
<rectangle x1="1.9685" y1="1.95148125" x2="2.34188125" y2="1.96011875" layer="94"/>
<rectangle x1="3.50011875" y1="1.95148125" x2="3.8735" y2="1.96011875" layer="94"/>
<rectangle x1="4.10971875" y1="1.95148125" x2="4.4831" y2="1.96011875" layer="94"/>
<rectangle x1="5.63371875" y1="1.95148125" x2="6.01471875" y2="1.96011875" layer="94"/>
<rectangle x1="6.6421" y1="1.95148125" x2="7.01548125" y2="1.96011875" layer="94"/>
<rectangle x1="7.58951875" y1="1.95148125" x2="7.97051875" y2="1.96011875" layer="94"/>
<rectangle x1="8.0645" y1="1.95148125" x2="8.50391875" y2="1.96011875" layer="94"/>
<rectangle x1="9.12368125" y1="1.95148125" x2="9.49451875" y2="1.96011875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.96011875" x2="0.3683" y2="1.9685" layer="94"/>
<rectangle x1="1.9685" y1="1.96011875" x2="2.34188125" y2="1.9685" layer="94"/>
<rectangle x1="3.50011875" y1="1.96011875" x2="3.8735" y2="1.9685" layer="94"/>
<rectangle x1="4.10971875" y1="1.96011875" x2="4.4831" y2="1.9685" layer="94"/>
<rectangle x1="5.63371875" y1="1.96011875" x2="6.01471875" y2="1.9685" layer="94"/>
<rectangle x1="6.6421" y1="1.96011875" x2="7.01548125" y2="1.9685" layer="94"/>
<rectangle x1="7.58951875" y1="1.96011875" x2="7.97051875" y2="1.9685" layer="94"/>
<rectangle x1="8.05688125" y1="1.96011875" x2="8.4963" y2="1.9685" layer="94"/>
<rectangle x1="9.12368125" y1="1.96011875" x2="9.49451875" y2="1.9685" layer="94"/>
<rectangle x1="-0.00508125" y1="1.9685" x2="0.3683" y2="1.97688125" layer="94"/>
<rectangle x1="1.9685" y1="1.9685" x2="2.34188125" y2="1.97688125" layer="94"/>
<rectangle x1="3.50011875" y1="1.9685" x2="3.8735" y2="1.97688125" layer="94"/>
<rectangle x1="4.10971875" y1="1.9685" x2="4.4831" y2="1.97688125" layer="94"/>
<rectangle x1="5.63371875" y1="1.9685" x2="6.01471875" y2="1.97688125" layer="94"/>
<rectangle x1="6.6421" y1="1.9685" x2="7.01548125" y2="1.97688125" layer="94"/>
<rectangle x1="7.58951875" y1="1.9685" x2="7.97051875" y2="1.97688125" layer="94"/>
<rectangle x1="8.04671875" y1="1.9685" x2="8.48868125" y2="1.97688125" layer="94"/>
<rectangle x1="9.12368125" y1="1.9685" x2="9.49451875" y2="1.97688125" layer="94"/>
<rectangle x1="-0.00508125" y1="1.97688125" x2="0.3683" y2="1.98551875" layer="94"/>
<rectangle x1="1.9685" y1="1.97688125" x2="2.34188125" y2="1.98551875" layer="94"/>
<rectangle x1="3.50011875" y1="1.97688125" x2="3.8735" y2="1.98551875" layer="94"/>
<rectangle x1="4.10971875" y1="1.97688125" x2="4.4831" y2="1.98551875" layer="94"/>
<rectangle x1="5.63371875" y1="1.97688125" x2="6.01471875" y2="1.98551875" layer="94"/>
<rectangle x1="6.6421" y1="1.97688125" x2="7.01548125" y2="1.98551875" layer="94"/>
<rectangle x1="7.58951875" y1="1.97688125" x2="7.97051875" y2="1.98551875" layer="94"/>
<rectangle x1="8.0391" y1="1.97688125" x2="8.4709" y2="1.98551875" layer="94"/>
<rectangle x1="9.12368125" y1="1.97688125" x2="9.49451875" y2="1.98551875" layer="94"/>
<rectangle x1="-0.00508125" y1="1.98551875" x2="0.3683" y2="1.9939" layer="94"/>
<rectangle x1="1.9685" y1="1.98551875" x2="2.34188125" y2="1.9939" layer="94"/>
<rectangle x1="3.50011875" y1="1.98551875" x2="3.8735" y2="1.9939" layer="94"/>
<rectangle x1="4.10971875" y1="1.98551875" x2="4.4831" y2="1.9939" layer="94"/>
<rectangle x1="5.63371875" y1="1.98551875" x2="6.01471875" y2="1.9939" layer="94"/>
<rectangle x1="6.6421" y1="1.98551875" x2="7.01548125" y2="1.9939" layer="94"/>
<rectangle x1="7.58951875" y1="1.98551875" x2="7.97051875" y2="1.9939" layer="94"/>
<rectangle x1="8.02131875" y1="1.98551875" x2="8.46328125" y2="1.9939" layer="94"/>
<rectangle x1="9.12368125" y1="1.98551875" x2="9.49451875" y2="1.9939" layer="94"/>
<rectangle x1="-0.00508125" y1="1.9939" x2="0.3683" y2="2.00228125" layer="94"/>
<rectangle x1="1.9685" y1="1.9939" x2="2.34188125" y2="2.00228125" layer="94"/>
<rectangle x1="3.50011875" y1="1.9939" x2="3.8735" y2="2.00228125" layer="94"/>
<rectangle x1="4.10971875" y1="1.9939" x2="4.4831" y2="2.00228125" layer="94"/>
<rectangle x1="5.63371875" y1="1.9939" x2="6.01471875" y2="2.00228125" layer="94"/>
<rectangle x1="6.6421" y1="1.9939" x2="7.01548125" y2="2.00228125" layer="94"/>
<rectangle x1="7.58951875" y1="1.9939" x2="7.97051875" y2="2.00228125" layer="94"/>
<rectangle x1="8.0137" y1="1.9939" x2="8.45311875" y2="2.00228125" layer="94"/>
<rectangle x1="9.12368125" y1="1.9939" x2="9.49451875" y2="2.00228125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.00228125" x2="0.3683" y2="2.01091875" layer="94"/>
<rectangle x1="1.9685" y1="2.00228125" x2="2.34188125" y2="2.01091875" layer="94"/>
<rectangle x1="3.50011875" y1="2.00228125" x2="3.8735" y2="2.01091875" layer="94"/>
<rectangle x1="4.10971875" y1="2.00228125" x2="4.4831" y2="2.01091875" layer="94"/>
<rectangle x1="5.64388125" y1="2.00228125" x2="6.01471875" y2="2.01091875" layer="94"/>
<rectangle x1="6.6421" y1="2.00228125" x2="7.01548125" y2="2.01091875" layer="94"/>
<rectangle x1="7.58951875" y1="2.00228125" x2="7.97051875" y2="2.01091875" layer="94"/>
<rectangle x1="8.00608125" y1="2.00228125" x2="8.4455" y2="2.01091875" layer="94"/>
<rectangle x1="9.12368125" y1="2.00228125" x2="9.49451875" y2="2.01091875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.01091875" x2="0.3683" y2="2.0193" layer="94"/>
<rectangle x1="1.9685" y1="2.01091875" x2="2.34188125" y2="2.0193" layer="94"/>
<rectangle x1="3.50011875" y1="2.01091875" x2="3.8735" y2="2.0193" layer="94"/>
<rectangle x1="4.10971875" y1="2.01091875" x2="4.4831" y2="2.0193" layer="94"/>
<rectangle x1="5.64388125" y1="2.01091875" x2="6.01471875" y2="2.0193" layer="94"/>
<rectangle x1="6.6421" y1="2.01091875" x2="7.01548125" y2="2.0193" layer="94"/>
<rectangle x1="7.58951875" y1="2.01091875" x2="7.97051875" y2="2.0193" layer="94"/>
<rectangle x1="7.99591875" y1="2.01091875" x2="8.43788125" y2="2.0193" layer="94"/>
<rectangle x1="9.12368125" y1="2.01091875" x2="9.49451875" y2="2.0193" layer="94"/>
<rectangle x1="-0.00508125" y1="2.0193" x2="0.3683" y2="2.02768125" layer="94"/>
<rectangle x1="1.9685" y1="2.0193" x2="2.34188125" y2="2.02768125" layer="94"/>
<rectangle x1="3.50011875" y1="2.0193" x2="3.8735" y2="2.02768125" layer="94"/>
<rectangle x1="4.10971875" y1="2.0193" x2="4.4831" y2="2.02768125" layer="94"/>
<rectangle x1="5.64388125" y1="2.0193" x2="6.01471875" y2="2.02768125" layer="94"/>
<rectangle x1="6.6421" y1="2.0193" x2="7.01548125" y2="2.02768125" layer="94"/>
<rectangle x1="7.58951875" y1="2.0193" x2="7.97051875" y2="2.02768125" layer="94"/>
<rectangle x1="7.9883" y1="2.0193" x2="8.42771875" y2="2.02768125" layer="94"/>
<rectangle x1="9.12368125" y1="2.0193" x2="9.49451875" y2="2.02768125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.02768125" x2="0.3683" y2="2.03631875" layer="94"/>
<rectangle x1="1.9685" y1="2.02768125" x2="2.34188125" y2="2.03631875" layer="94"/>
<rectangle x1="3.50011875" y1="2.02768125" x2="3.8735" y2="2.03631875" layer="94"/>
<rectangle x1="4.10971875" y1="2.02768125" x2="4.4831" y2="2.03631875" layer="94"/>
<rectangle x1="5.64388125" y1="2.02768125" x2="6.01471875" y2="2.03631875" layer="94"/>
<rectangle x1="6.6421" y1="2.02768125" x2="7.01548125" y2="2.03631875" layer="94"/>
<rectangle x1="7.58951875" y1="2.02768125" x2="7.97051875" y2="2.03631875" layer="94"/>
<rectangle x1="7.98068125" y1="2.02768125" x2="8.4201" y2="2.03631875" layer="94"/>
<rectangle x1="9.12368125" y1="2.02768125" x2="9.49451875" y2="2.03631875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.03631875" x2="0.3683" y2="2.0447" layer="94"/>
<rectangle x1="1.9685" y1="2.03631875" x2="2.34188125" y2="2.0447" layer="94"/>
<rectangle x1="3.4925" y1="2.03631875" x2="3.86588125" y2="2.0447" layer="94"/>
<rectangle x1="4.10971875" y1="2.03631875" x2="4.4831" y2="2.0447" layer="94"/>
<rectangle x1="5.64388125" y1="2.03631875" x2="6.01471875" y2="2.0447" layer="94"/>
<rectangle x1="6.6421" y1="2.03631875" x2="7.01548125" y2="2.0447" layer="94"/>
<rectangle x1="7.58951875" y1="2.03631875" x2="8.41248125" y2="2.0447" layer="94"/>
<rectangle x1="9.12368125" y1="2.03631875" x2="9.49451875" y2="2.0447" layer="94"/>
<rectangle x1="-0.00508125" y1="2.0447" x2="0.3683" y2="2.05308125" layer="94"/>
<rectangle x1="1.9685" y1="2.0447" x2="2.34188125" y2="2.05308125" layer="94"/>
<rectangle x1="3.4925" y1="2.0447" x2="3.86588125" y2="2.05308125" layer="94"/>
<rectangle x1="4.10971875" y1="2.0447" x2="4.4831" y2="2.05308125" layer="94"/>
<rectangle x1="5.64388125" y1="2.0447" x2="6.01471875" y2="2.05308125" layer="94"/>
<rectangle x1="6.6421" y1="2.0447" x2="7.01548125" y2="2.05308125" layer="94"/>
<rectangle x1="7.58951875" y1="2.0447" x2="8.40231875" y2="2.05308125" layer="94"/>
<rectangle x1="9.12368125" y1="2.0447" x2="9.49451875" y2="2.05308125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.05308125" x2="0.3683" y2="2.06171875" layer="94"/>
<rectangle x1="1.9685" y1="2.05308125" x2="2.34188125" y2="2.06171875" layer="94"/>
<rectangle x1="3.48488125" y1="2.05308125" x2="3.85571875" y2="2.06171875" layer="94"/>
<rectangle x1="4.10971875" y1="2.05308125" x2="4.4831" y2="2.06171875" layer="94"/>
<rectangle x1="5.64388125" y1="2.05308125" x2="6.01471875" y2="2.06171875" layer="94"/>
<rectangle x1="6.6421" y1="2.05308125" x2="7.01548125" y2="2.06171875" layer="94"/>
<rectangle x1="7.58951875" y1="2.05308125" x2="8.3947" y2="2.06171875" layer="94"/>
<rectangle x1="9.12368125" y1="2.05308125" x2="9.49451875" y2="2.06171875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.06171875" x2="0.3683" y2="2.0701" layer="94"/>
<rectangle x1="1.9685" y1="2.06171875" x2="2.34188125" y2="2.0701" layer="94"/>
<rectangle x1="3.48488125" y1="2.06171875" x2="3.85571875" y2="2.0701" layer="94"/>
<rectangle x1="4.10971875" y1="2.06171875" x2="4.4831" y2="2.0701" layer="94"/>
<rectangle x1="5.64388125" y1="2.06171875" x2="6.01471875" y2="2.0701" layer="94"/>
<rectangle x1="6.6421" y1="2.06171875" x2="7.01548125" y2="2.0701" layer="94"/>
<rectangle x1="7.58951875" y1="2.06171875" x2="8.38708125" y2="2.0701" layer="94"/>
<rectangle x1="9.12368125" y1="2.06171875" x2="9.49451875" y2="2.0701" layer="94"/>
<rectangle x1="-0.00508125" y1="2.0701" x2="0.3683" y2="2.07848125" layer="94"/>
<rectangle x1="1.9685" y1="2.0701" x2="2.34188125" y2="2.07848125" layer="94"/>
<rectangle x1="3.47471875" y1="2.0701" x2="3.8481" y2="2.07848125" layer="94"/>
<rectangle x1="4.10971875" y1="2.0701" x2="4.4831" y2="2.07848125" layer="94"/>
<rectangle x1="5.64388125" y1="2.0701" x2="6.01471875" y2="2.07848125" layer="94"/>
<rectangle x1="6.6421" y1="2.0701" x2="7.01548125" y2="2.07848125" layer="94"/>
<rectangle x1="7.58951875" y1="2.0701" x2="8.37691875" y2="2.07848125" layer="94"/>
<rectangle x1="9.12368125" y1="2.0701" x2="9.49451875" y2="2.07848125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.07848125" x2="0.3683" y2="2.08711875" layer="94"/>
<rectangle x1="1.9685" y1="2.07848125" x2="2.34188125" y2="2.08711875" layer="94"/>
<rectangle x1="3.4671" y1="2.07848125" x2="3.8481" y2="2.08711875" layer="94"/>
<rectangle x1="4.10971875" y1="2.07848125" x2="4.4831" y2="2.08711875" layer="94"/>
<rectangle x1="5.64388125" y1="2.07848125" x2="6.01471875" y2="2.08711875" layer="94"/>
<rectangle x1="6.6421" y1="2.07848125" x2="7.01548125" y2="2.08711875" layer="94"/>
<rectangle x1="7.58951875" y1="2.07848125" x2="8.3693" y2="2.08711875" layer="94"/>
<rectangle x1="9.12368125" y1="2.07848125" x2="9.49451875" y2="2.08711875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.08711875" x2="0.3683" y2="2.0955" layer="94"/>
<rectangle x1="1.9685" y1="2.08711875" x2="2.34188125" y2="2.0955" layer="94"/>
<rectangle x1="3.45948125" y1="2.08711875" x2="3.84048125" y2="2.0955" layer="94"/>
<rectangle x1="4.10971875" y1="2.08711875" x2="4.4831" y2="2.0955" layer="94"/>
<rectangle x1="5.64388125" y1="2.08711875" x2="6.01471875" y2="2.0955" layer="94"/>
<rectangle x1="6.6421" y1="2.08711875" x2="7.01548125" y2="2.0955" layer="94"/>
<rectangle x1="7.58951875" y1="2.08711875" x2="8.35151875" y2="2.0955" layer="94"/>
<rectangle x1="9.12368125" y1="2.08711875" x2="9.49451875" y2="2.0955" layer="94"/>
<rectangle x1="-0.00508125" y1="2.0955" x2="0.3683" y2="2.10388125" layer="94"/>
<rectangle x1="1.9685" y1="2.0955" x2="2.34188125" y2="2.10388125" layer="94"/>
<rectangle x1="3.45948125" y1="2.0955" x2="3.84048125" y2="2.10388125" layer="94"/>
<rectangle x1="4.10971875" y1="2.0955" x2="4.4831" y2="2.10388125" layer="94"/>
<rectangle x1="5.64388125" y1="2.0955" x2="6.01471875" y2="2.10388125" layer="94"/>
<rectangle x1="6.6421" y1="2.0955" x2="7.01548125" y2="2.10388125" layer="94"/>
<rectangle x1="7.58951875" y1="2.0955" x2="8.3439" y2="2.10388125" layer="94"/>
<rectangle x1="9.12368125" y1="2.0955" x2="9.49451875" y2="2.10388125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.10388125" x2="0.3683" y2="2.11251875" layer="94"/>
<rectangle x1="1.9685" y1="2.10388125" x2="2.34188125" y2="2.11251875" layer="94"/>
<rectangle x1="3.44931875" y1="2.10388125" x2="3.84048125" y2="2.11251875" layer="94"/>
<rectangle x1="4.10971875" y1="2.10388125" x2="4.4831" y2="2.11251875" layer="94"/>
<rectangle x1="5.64388125" y1="2.10388125" x2="6.01471875" y2="2.11251875" layer="94"/>
<rectangle x1="6.6421" y1="2.10388125" x2="7.01548125" y2="2.11251875" layer="94"/>
<rectangle x1="7.58951875" y1="2.10388125" x2="8.33628125" y2="2.11251875" layer="94"/>
<rectangle x1="9.12368125" y1="2.10388125" x2="9.49451875" y2="2.11251875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.11251875" x2="0.3683" y2="2.1209" layer="94"/>
<rectangle x1="1.9685" y1="2.11251875" x2="2.34188125" y2="2.1209" layer="94"/>
<rectangle x1="3.4417" y1="2.11251875" x2="3.83031875" y2="2.1209" layer="94"/>
<rectangle x1="4.10971875" y1="2.11251875" x2="4.4831" y2="2.1209" layer="94"/>
<rectangle x1="5.64388125" y1="2.11251875" x2="6.01471875" y2="2.1209" layer="94"/>
<rectangle x1="6.6421" y1="2.11251875" x2="7.01548125" y2="2.1209" layer="94"/>
<rectangle x1="7.58951875" y1="2.11251875" x2="8.32611875" y2="2.1209" layer="94"/>
<rectangle x1="9.12368125" y1="2.11251875" x2="9.49451875" y2="2.1209" layer="94"/>
<rectangle x1="-0.00508125" y1="2.1209" x2="0.3683" y2="2.12928125" layer="94"/>
<rectangle x1="1.9685" y1="2.1209" x2="2.34188125" y2="2.12928125" layer="94"/>
<rectangle x1="3.42391875" y1="2.1209" x2="3.83031875" y2="2.12928125" layer="94"/>
<rectangle x1="4.10971875" y1="2.1209" x2="4.4831" y2="2.12928125" layer="94"/>
<rectangle x1="5.64388125" y1="2.1209" x2="6.01471875" y2="2.12928125" layer="94"/>
<rectangle x1="6.6421" y1="2.1209" x2="7.01548125" y2="2.12928125" layer="94"/>
<rectangle x1="7.58951875" y1="2.1209" x2="8.3185" y2="2.12928125" layer="94"/>
<rectangle x1="9.12368125" y1="2.1209" x2="9.49451875" y2="2.12928125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.12928125" x2="0.3683" y2="2.13791875" layer="94"/>
<rectangle x1="1.9685" y1="2.12928125" x2="2.34188125" y2="2.13791875" layer="94"/>
<rectangle x1="3.4163" y1="2.12928125" x2="3.8227" y2="2.13791875" layer="94"/>
<rectangle x1="4.10971875" y1="2.12928125" x2="4.4831" y2="2.13791875" layer="94"/>
<rectangle x1="5.64388125" y1="2.12928125" x2="6.01471875" y2="2.13791875" layer="94"/>
<rectangle x1="6.6421" y1="2.12928125" x2="7.01548125" y2="2.13791875" layer="94"/>
<rectangle x1="7.58951875" y1="2.12928125" x2="8.31088125" y2="2.13791875" layer="94"/>
<rectangle x1="9.12368125" y1="2.12928125" x2="9.49451875" y2="2.13791875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.13791875" x2="0.3683" y2="2.1463" layer="94"/>
<rectangle x1="1.9685" y1="2.13791875" x2="2.34188125" y2="2.1463" layer="94"/>
<rectangle x1="3.40868125" y1="2.13791875" x2="3.8227" y2="2.1463" layer="94"/>
<rectangle x1="4.10971875" y1="2.13791875" x2="4.4831" y2="2.1463" layer="94"/>
<rectangle x1="5.64388125" y1="2.13791875" x2="6.01471875" y2="2.1463" layer="94"/>
<rectangle x1="6.6421" y1="2.13791875" x2="7.01548125" y2="2.1463" layer="94"/>
<rectangle x1="7.58951875" y1="2.13791875" x2="8.30071875" y2="2.1463" layer="94"/>
<rectangle x1="9.12368125" y1="2.13791875" x2="9.49451875" y2="2.1463" layer="94"/>
<rectangle x1="-0.00508125" y1="2.1463" x2="0.3683" y2="2.15468125" layer="94"/>
<rectangle x1="1.9685" y1="2.1463" x2="2.34188125" y2="2.15468125" layer="94"/>
<rectangle x1="3.39851875" y1="2.1463" x2="3.81508125" y2="2.15468125" layer="94"/>
<rectangle x1="4.10971875" y1="2.1463" x2="4.4831" y2="2.15468125" layer="94"/>
<rectangle x1="5.64388125" y1="2.1463" x2="6.01471875" y2="2.15468125" layer="94"/>
<rectangle x1="6.6421" y1="2.1463" x2="7.01548125" y2="2.15468125" layer="94"/>
<rectangle x1="7.58951875" y1="2.1463" x2="8.2931" y2="2.15468125" layer="94"/>
<rectangle x1="9.12368125" y1="2.1463" x2="9.49451875" y2="2.15468125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.15468125" x2="0.3683" y2="2.16331875" layer="94"/>
<rectangle x1="1.9685" y1="2.15468125" x2="2.34188125" y2="2.16331875" layer="94"/>
<rectangle x1="3.38328125" y1="2.15468125" x2="3.80491875" y2="2.16331875" layer="94"/>
<rectangle x1="4.10971875" y1="2.15468125" x2="4.47548125" y2="2.16331875" layer="94"/>
<rectangle x1="5.64388125" y1="2.15468125" x2="6.01471875" y2="2.16331875" layer="94"/>
<rectangle x1="6.6421" y1="2.15468125" x2="7.01548125" y2="2.16331875" layer="94"/>
<rectangle x1="7.58951875" y1="2.15468125" x2="8.28548125" y2="2.16331875" layer="94"/>
<rectangle x1="9.12368125" y1="2.15468125" x2="9.49451875" y2="2.16331875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.16331875" x2="0.3683" y2="2.1717" layer="94"/>
<rectangle x1="1.9685" y1="2.16331875" x2="2.34188125" y2="2.1717" layer="94"/>
<rectangle x1="3.37311875" y1="2.16331875" x2="3.80491875" y2="2.1717" layer="94"/>
<rectangle x1="4.10971875" y1="2.16331875" x2="4.47548125" y2="2.1717" layer="94"/>
<rectangle x1="5.64388125" y1="2.16331875" x2="6.01471875" y2="2.1717" layer="94"/>
<rectangle x1="6.6421" y1="2.16331875" x2="7.01548125" y2="2.1717" layer="94"/>
<rectangle x1="7.58951875" y1="2.16331875" x2="8.27531875" y2="2.1717" layer="94"/>
<rectangle x1="9.12368125" y1="2.16331875" x2="9.49451875" y2="2.1717" layer="94"/>
<rectangle x1="-0.00508125" y1="2.1717" x2="0.3683" y2="2.18008125" layer="94"/>
<rectangle x1="1.9685" y1="2.1717" x2="2.34188125" y2="2.18008125" layer="94"/>
<rectangle x1="3.35788125" y1="2.1717" x2="3.7973" y2="2.18008125" layer="94"/>
<rectangle x1="4.10971875" y1="2.1717" x2="4.47548125" y2="2.18008125" layer="94"/>
<rectangle x1="5.64388125" y1="2.1717" x2="6.01471875" y2="2.18008125" layer="94"/>
<rectangle x1="6.6421" y1="2.1717" x2="7.01548125" y2="2.18008125" layer="94"/>
<rectangle x1="7.58951875" y1="2.1717" x2="8.2677" y2="2.18008125" layer="94"/>
<rectangle x1="9.12368125" y1="2.1717" x2="9.49451875" y2="2.18008125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.18008125" x2="0.3683" y2="2.18871875" layer="94"/>
<rectangle x1="1.9685" y1="2.18008125" x2="2.34188125" y2="2.18871875" layer="94"/>
<rectangle x1="3.34771875" y1="2.18008125" x2="3.78968125" y2="2.18871875" layer="94"/>
<rectangle x1="4.10971875" y1="2.18008125" x2="4.47548125" y2="2.18871875" layer="94"/>
<rectangle x1="5.64388125" y1="2.18008125" x2="6.01471875" y2="2.18871875" layer="94"/>
<rectangle x1="6.6421" y1="2.18008125" x2="7.01548125" y2="2.18871875" layer="94"/>
<rectangle x1="7.58951875" y1="2.18008125" x2="8.26008125" y2="2.18871875" layer="94"/>
<rectangle x1="9.12368125" y1="2.18008125" x2="9.49451875" y2="2.18871875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.18871875" x2="0.3683" y2="2.1971" layer="94"/>
<rectangle x1="1.9685" y1="2.18871875" x2="2.34188125" y2="2.1971" layer="94"/>
<rectangle x1="3.33248125" y1="2.18871875" x2="3.78968125" y2="2.1971" layer="94"/>
<rectangle x1="4.10971875" y1="2.18871875" x2="4.47548125" y2="2.1971" layer="94"/>
<rectangle x1="5.64388125" y1="2.18871875" x2="6.01471875" y2="2.1971" layer="94"/>
<rectangle x1="6.6421" y1="2.18871875" x2="7.01548125" y2="2.1971" layer="94"/>
<rectangle x1="7.58951875" y1="2.18871875" x2="8.24991875" y2="2.1971" layer="94"/>
<rectangle x1="9.12368125" y1="2.18871875" x2="9.49451875" y2="2.1971" layer="94"/>
<rectangle x1="-0.00508125" y1="2.1971" x2="0.3683" y2="2.20548125" layer="94"/>
<rectangle x1="1.9685" y1="2.1971" x2="2.34188125" y2="2.20548125" layer="94"/>
<rectangle x1="3.3147" y1="2.1971" x2="3.77951875" y2="2.20548125" layer="94"/>
<rectangle x1="4.10971875" y1="2.1971" x2="4.47548125" y2="2.20548125" layer="94"/>
<rectangle x1="5.64388125" y1="2.1971" x2="6.01471875" y2="2.20548125" layer="94"/>
<rectangle x1="6.6421" y1="2.1971" x2="7.01548125" y2="2.20548125" layer="94"/>
<rectangle x1="7.58951875" y1="2.1971" x2="8.2423" y2="2.20548125" layer="94"/>
<rectangle x1="9.12368125" y1="2.1971" x2="9.49451875" y2="2.20548125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.20548125" x2="0.3683" y2="2.21411875" layer="94"/>
<rectangle x1="1.9685" y1="2.20548125" x2="2.34188125" y2="2.21411875" layer="94"/>
<rectangle x1="3.30708125" y1="2.20548125" x2="3.7719" y2="2.21411875" layer="94"/>
<rectangle x1="4.10971875" y1="2.20548125" x2="4.47548125" y2="2.21411875" layer="94"/>
<rectangle x1="5.64388125" y1="2.20548125" x2="6.01471875" y2="2.21411875" layer="94"/>
<rectangle x1="6.6421" y1="2.20548125" x2="7.01548125" y2="2.21411875" layer="94"/>
<rectangle x1="7.58951875" y1="2.20548125" x2="8.23468125" y2="2.21411875" layer="94"/>
<rectangle x1="9.12368125" y1="2.20548125" x2="9.49451875" y2="2.21411875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.21411875" x2="0.3683" y2="2.2225" layer="94"/>
<rectangle x1="1.9685" y1="2.21411875" x2="2.34188125" y2="2.2225" layer="94"/>
<rectangle x1="3.2893" y1="2.21411875" x2="3.76428125" y2="2.2225" layer="94"/>
<rectangle x1="4.10971875" y1="2.21411875" x2="4.47548125" y2="2.2225" layer="94"/>
<rectangle x1="5.64388125" y1="2.21411875" x2="6.01471875" y2="2.2225" layer="94"/>
<rectangle x1="6.6421" y1="2.21411875" x2="7.01548125" y2="2.2225" layer="94"/>
<rectangle x1="7.58951875" y1="2.21411875" x2="8.2169" y2="2.2225" layer="94"/>
<rectangle x1="9.12368125" y1="2.21411875" x2="9.49451875" y2="2.2225" layer="94"/>
<rectangle x1="-0.00508125" y1="2.2225" x2="0.3683" y2="2.23088125" layer="94"/>
<rectangle x1="1.9685" y1="2.2225" x2="2.34188125" y2="2.23088125" layer="94"/>
<rectangle x1="3.27151875" y1="2.2225" x2="3.75411875" y2="2.23088125" layer="94"/>
<rectangle x1="4.10971875" y1="2.2225" x2="4.47548125" y2="2.23088125" layer="94"/>
<rectangle x1="5.64388125" y1="2.2225" x2="6.01471875" y2="2.23088125" layer="94"/>
<rectangle x1="6.6421" y1="2.2225" x2="7.01548125" y2="2.23088125" layer="94"/>
<rectangle x1="7.58951875" y1="2.2225" x2="8.20928125" y2="2.23088125" layer="94"/>
<rectangle x1="9.12368125" y1="2.2225" x2="9.49451875" y2="2.23088125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.23088125" x2="0.3683" y2="2.23951875" layer="94"/>
<rectangle x1="1.9685" y1="2.23088125" x2="2.34188125" y2="2.23951875" layer="94"/>
<rectangle x1="3.25628125" y1="2.23088125" x2="3.7465" y2="2.23951875" layer="94"/>
<rectangle x1="4.10971875" y1="2.23088125" x2="4.47548125" y2="2.23951875" layer="94"/>
<rectangle x1="5.64388125" y1="2.23088125" x2="6.01471875" y2="2.23951875" layer="94"/>
<rectangle x1="6.6421" y1="2.23088125" x2="7.01548125" y2="2.23951875" layer="94"/>
<rectangle x1="7.58951875" y1="2.23088125" x2="8.19911875" y2="2.23951875" layer="94"/>
<rectangle x1="9.12368125" y1="2.23088125" x2="9.49451875" y2="2.23951875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.23951875" x2="0.3683" y2="2.2479" layer="94"/>
<rectangle x1="1.9685" y1="2.23951875" x2="2.34188125" y2="2.2479" layer="94"/>
<rectangle x1="3.2385" y1="2.23951875" x2="3.73888125" y2="2.2479" layer="94"/>
<rectangle x1="4.10971875" y1="2.23951875" x2="4.47548125" y2="2.2479" layer="94"/>
<rectangle x1="5.64388125" y1="2.23951875" x2="6.01471875" y2="2.2479" layer="94"/>
<rectangle x1="6.6421" y1="2.23951875" x2="7.01548125" y2="2.2479" layer="94"/>
<rectangle x1="7.58951875" y1="2.23951875" x2="8.1915" y2="2.2479" layer="94"/>
<rectangle x1="9.12368125" y1="2.23951875" x2="9.49451875" y2="2.2479" layer="94"/>
<rectangle x1="-0.00508125" y1="2.2479" x2="0.3683" y2="2.25628125" layer="94"/>
<rectangle x1="1.9685" y1="2.2479" x2="2.34188125" y2="2.25628125" layer="94"/>
<rectangle x1="3.2131" y1="2.2479" x2="3.72871875" y2="2.25628125" layer="94"/>
<rectangle x1="4.10971875" y1="2.2479" x2="4.47548125" y2="2.25628125" layer="94"/>
<rectangle x1="5.64388125" y1="2.2479" x2="6.01471875" y2="2.25628125" layer="94"/>
<rectangle x1="6.6421" y1="2.2479" x2="7.01548125" y2="2.25628125" layer="94"/>
<rectangle x1="7.58951875" y1="2.2479" x2="8.18388125" y2="2.25628125" layer="94"/>
<rectangle x1="9.12368125" y1="2.2479" x2="9.49451875" y2="2.25628125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.25628125" x2="0.3683" y2="2.26491875" layer="94"/>
<rectangle x1="1.9685" y1="2.25628125" x2="2.34188125" y2="2.26491875" layer="94"/>
<rectangle x1="3.19531875" y1="2.25628125" x2="3.7211" y2="2.26491875" layer="94"/>
<rectangle x1="4.10971875" y1="2.25628125" x2="4.47548125" y2="2.26491875" layer="94"/>
<rectangle x1="5.64388125" y1="2.25628125" x2="6.01471875" y2="2.26491875" layer="94"/>
<rectangle x1="6.6421" y1="2.25628125" x2="7.01548125" y2="2.26491875" layer="94"/>
<rectangle x1="7.58951875" y1="2.25628125" x2="8.17371875" y2="2.26491875" layer="94"/>
<rectangle x1="9.12368125" y1="2.25628125" x2="9.49451875" y2="2.26491875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.26491875" x2="0.3683" y2="2.2733" layer="94"/>
<rectangle x1="1.9685" y1="2.26491875" x2="2.34188125" y2="2.2733" layer="94"/>
<rectangle x1="3.16991875" y1="2.26491875" x2="3.71348125" y2="2.2733" layer="94"/>
<rectangle x1="4.10971875" y1="2.26491875" x2="4.47548125" y2="2.2733" layer="94"/>
<rectangle x1="5.64388125" y1="2.26491875" x2="6.01471875" y2="2.2733" layer="94"/>
<rectangle x1="6.6421" y1="2.26491875" x2="7.01548125" y2="2.2733" layer="94"/>
<rectangle x1="7.58951875" y1="2.26491875" x2="8.1661" y2="2.2733" layer="94"/>
<rectangle x1="9.12368125" y1="2.26491875" x2="9.49451875" y2="2.2733" layer="94"/>
<rectangle x1="-0.00508125" y1="2.2733" x2="0.3683" y2="2.28168125" layer="94"/>
<rectangle x1="1.9685" y1="2.2733" x2="2.34188125" y2="2.28168125" layer="94"/>
<rectangle x1="3.14451875" y1="2.2733" x2="3.70331875" y2="2.28168125" layer="94"/>
<rectangle x1="4.10971875" y1="2.2733" x2="4.47548125" y2="2.28168125" layer="94"/>
<rectangle x1="5.64388125" y1="2.2733" x2="6.01471875" y2="2.28168125" layer="94"/>
<rectangle x1="6.6421" y1="2.2733" x2="7.01548125" y2="2.28168125" layer="94"/>
<rectangle x1="7.58951875" y1="2.2733" x2="8.15848125" y2="2.28168125" layer="94"/>
<rectangle x1="9.12368125" y1="2.2733" x2="9.49451875" y2="2.28168125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.28168125" x2="0.3683" y2="2.29031875" layer="94"/>
<rectangle x1="1.9685" y1="2.28168125" x2="2.34188125" y2="2.29031875" layer="94"/>
<rectangle x1="3.11911875" y1="2.28168125" x2="3.68808125" y2="2.29031875" layer="94"/>
<rectangle x1="4.10971875" y1="2.28168125" x2="4.47548125" y2="2.29031875" layer="94"/>
<rectangle x1="5.64388125" y1="2.28168125" x2="6.01471875" y2="2.29031875" layer="94"/>
<rectangle x1="6.6421" y1="2.28168125" x2="7.01548125" y2="2.29031875" layer="94"/>
<rectangle x1="7.58951875" y1="2.28168125" x2="8.14831875" y2="2.29031875" layer="94"/>
<rectangle x1="9.12368125" y1="2.28168125" x2="9.49451875" y2="2.29031875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.29031875" x2="0.3683" y2="2.2987" layer="94"/>
<rectangle x1="1.9685" y1="2.29031875" x2="2.34188125" y2="2.2987" layer="94"/>
<rectangle x1="3.0861" y1="2.29031875" x2="3.67791875" y2="2.2987" layer="94"/>
<rectangle x1="4.10971875" y1="2.29031875" x2="4.47548125" y2="2.2987" layer="94"/>
<rectangle x1="5.64388125" y1="2.29031875" x2="6.01471875" y2="2.2987" layer="94"/>
<rectangle x1="6.6421" y1="2.29031875" x2="7.01548125" y2="2.2987" layer="94"/>
<rectangle x1="7.58951875" y1="2.29031875" x2="8.1407" y2="2.2987" layer="94"/>
<rectangle x1="9.12368125" y1="2.29031875" x2="9.49451875" y2="2.2987" layer="94"/>
<rectangle x1="-0.00508125" y1="2.2987" x2="0.3683" y2="2.30708125" layer="94"/>
<rectangle x1="1.9685" y1="2.2987" x2="2.34188125" y2="2.30708125" layer="94"/>
<rectangle x1="3.0353" y1="2.2987" x2="3.6703" y2="2.30708125" layer="94"/>
<rectangle x1="4.10971875" y1="2.2987" x2="4.47548125" y2="2.30708125" layer="94"/>
<rectangle x1="5.64388125" y1="2.2987" x2="6.01471875" y2="2.30708125" layer="94"/>
<rectangle x1="6.6421" y1="2.2987" x2="7.01548125" y2="2.30708125" layer="94"/>
<rectangle x1="7.58951875" y1="2.2987" x2="8.13308125" y2="2.30708125" layer="94"/>
<rectangle x1="9.12368125" y1="2.2987" x2="9.49451875" y2="2.30708125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.30708125" x2="1.7653" y2="2.31571875" layer="94"/>
<rectangle x1="1.9685" y1="2.30708125" x2="3.66268125" y2="2.31571875" layer="94"/>
<rectangle x1="4.10971875" y1="2.30708125" x2="4.47548125" y2="2.31571875" layer="94"/>
<rectangle x1="5.64388125" y1="2.30708125" x2="6.01471875" y2="2.31571875" layer="94"/>
<rectangle x1="6.38048125" y1="2.30708125" x2="7.2771" y2="2.31571875" layer="94"/>
<rectangle x1="7.58951875" y1="2.30708125" x2="8.12291875" y2="2.31571875" layer="94"/>
<rectangle x1="9.12368125" y1="2.30708125" x2="9.49451875" y2="2.31571875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.31571875" x2="1.80848125" y2="2.3241" layer="94"/>
<rectangle x1="1.9685" y1="2.31571875" x2="3.6449" y2="2.3241" layer="94"/>
<rectangle x1="4.10971875" y1="2.31571875" x2="4.47548125" y2="2.3241" layer="94"/>
<rectangle x1="5.64388125" y1="2.31571875" x2="6.01471875" y2="2.3241" layer="94"/>
<rectangle x1="6.35508125" y1="2.31571875" x2="7.31011875" y2="2.3241" layer="94"/>
<rectangle x1="7.58951875" y1="2.31571875" x2="8.1153" y2="2.3241" layer="94"/>
<rectangle x1="9.12368125" y1="2.31571875" x2="9.49451875" y2="2.3241" layer="94"/>
<rectangle x1="-0.00508125" y1="2.3241" x2="1.83388125" y2="2.33248125" layer="94"/>
<rectangle x1="1.9685" y1="2.3241" x2="3.63728125" y2="2.33248125" layer="94"/>
<rectangle x1="4.10971875" y1="2.3241" x2="4.47548125" y2="2.33248125" layer="94"/>
<rectangle x1="5.64388125" y1="2.3241" x2="6.01471875" y2="2.33248125" layer="94"/>
<rectangle x1="6.3373" y1="2.3241" x2="7.32028125" y2="2.33248125" layer="94"/>
<rectangle x1="7.58951875" y1="2.3241" x2="8.09751875" y2="2.33248125" layer="94"/>
<rectangle x1="9.12368125" y1="2.3241" x2="9.49451875" y2="2.33248125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.33248125" x2="1.84911875" y2="2.34111875" layer="94"/>
<rectangle x1="1.9685" y1="2.33248125" x2="3.6195" y2="2.34111875" layer="94"/>
<rectangle x1="4.10971875" y1="2.33248125" x2="4.47548125" y2="2.34111875" layer="94"/>
<rectangle x1="5.64388125" y1="2.33248125" x2="6.01471875" y2="2.34111875" layer="94"/>
<rectangle x1="6.31951875" y1="2.33248125" x2="7.33551875" y2="2.34111875" layer="94"/>
<rectangle x1="7.58951875" y1="2.33248125" x2="8.0899" y2="2.34111875" layer="94"/>
<rectangle x1="9.12368125" y1="2.33248125" x2="9.49451875" y2="2.34111875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.34111875" x2="1.85928125" y2="2.3495" layer="94"/>
<rectangle x1="1.9685" y1="2.34111875" x2="3.61188125" y2="2.3495" layer="94"/>
<rectangle x1="4.10971875" y1="2.34111875" x2="4.47548125" y2="2.3495" layer="94"/>
<rectangle x1="5.64388125" y1="2.34111875" x2="6.01471875" y2="2.3495" layer="94"/>
<rectangle x1="6.3119" y1="2.34111875" x2="7.34568125" y2="2.3495" layer="94"/>
<rectangle x1="7.58951875" y1="2.34111875" x2="8.08228125" y2="2.3495" layer="94"/>
<rectangle x1="9.12368125" y1="2.34111875" x2="9.49451875" y2="2.3495" layer="94"/>
<rectangle x1="-0.00508125" y1="2.3495" x2="1.8669" y2="2.35788125" layer="94"/>
<rectangle x1="1.9685" y1="2.3495" x2="3.5941" y2="2.35788125" layer="94"/>
<rectangle x1="4.10971875" y1="2.3495" x2="4.47548125" y2="2.35788125" layer="94"/>
<rectangle x1="5.64388125" y1="2.3495" x2="6.01471875" y2="2.35788125" layer="94"/>
<rectangle x1="6.30428125" y1="2.3495" x2="7.3533" y2="2.35788125" layer="94"/>
<rectangle x1="7.58951875" y1="2.3495" x2="8.07211875" y2="2.35788125" layer="94"/>
<rectangle x1="9.12368125" y1="2.3495" x2="9.49451875" y2="2.35788125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.35788125" x2="1.87451875" y2="2.36651875" layer="94"/>
<rectangle x1="1.9685" y1="2.35788125" x2="3.57631875" y2="2.36651875" layer="94"/>
<rectangle x1="4.10971875" y1="2.35788125" x2="4.47548125" y2="2.36651875" layer="94"/>
<rectangle x1="5.64388125" y1="2.35788125" x2="6.01471875" y2="2.36651875" layer="94"/>
<rectangle x1="6.29411875" y1="2.35788125" x2="7.36091875" y2="2.36651875" layer="94"/>
<rectangle x1="7.58951875" y1="2.35788125" x2="8.0645" y2="2.36651875" layer="94"/>
<rectangle x1="9.12368125" y1="2.35788125" x2="9.49451875" y2="2.36651875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.36651875" x2="1.88468125" y2="2.3749" layer="94"/>
<rectangle x1="1.9685" y1="2.36651875" x2="3.5687" y2="2.3749" layer="94"/>
<rectangle x1="4.10971875" y1="2.36651875" x2="4.47548125" y2="2.3749" layer="94"/>
<rectangle x1="5.64388125" y1="2.36651875" x2="6.01471875" y2="2.3749" layer="94"/>
<rectangle x1="6.2865" y1="2.36651875" x2="7.37108125" y2="2.3749" layer="94"/>
<rectangle x1="7.58951875" y1="2.36651875" x2="8.05688125" y2="2.3749" layer="94"/>
<rectangle x1="9.12368125" y1="2.36651875" x2="9.49451875" y2="2.3749" layer="94"/>
<rectangle x1="-0.00508125" y1="2.3749" x2="1.88468125" y2="2.38328125" layer="94"/>
<rectangle x1="1.9685" y1="2.3749" x2="3.55091875" y2="2.38328125" layer="94"/>
<rectangle x1="4.10971875" y1="2.3749" x2="4.47548125" y2="2.38328125" layer="94"/>
<rectangle x1="5.64388125" y1="2.3749" x2="6.01471875" y2="2.38328125" layer="94"/>
<rectangle x1="6.27888125" y1="2.3749" x2="7.3787" y2="2.38328125" layer="94"/>
<rectangle x1="7.58951875" y1="2.3749" x2="8.04671875" y2="2.38328125" layer="94"/>
<rectangle x1="9.12368125" y1="2.3749" x2="9.49451875" y2="2.38328125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.38328125" x2="1.8923" y2="2.39191875" layer="94"/>
<rectangle x1="1.9685" y1="2.38328125" x2="3.53568125" y2="2.39191875" layer="94"/>
<rectangle x1="4.10971875" y1="2.38328125" x2="4.47548125" y2="2.39191875" layer="94"/>
<rectangle x1="5.64388125" y1="2.38328125" x2="6.01471875" y2="2.39191875" layer="94"/>
<rectangle x1="6.26871875" y1="2.38328125" x2="7.38631875" y2="2.39191875" layer="94"/>
<rectangle x1="7.58951875" y1="2.38328125" x2="8.0391" y2="2.39191875" layer="94"/>
<rectangle x1="9.12368125" y1="2.38328125" x2="9.49451875" y2="2.39191875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.39191875" x2="1.8923" y2="2.4003" layer="94"/>
<rectangle x1="1.9685" y1="2.39191875" x2="3.5179" y2="2.4003" layer="94"/>
<rectangle x1="4.10971875" y1="2.39191875" x2="4.47548125" y2="2.4003" layer="94"/>
<rectangle x1="5.64388125" y1="2.39191875" x2="6.01471875" y2="2.4003" layer="94"/>
<rectangle x1="6.2611" y1="2.39191875" x2="7.39648125" y2="2.4003" layer="94"/>
<rectangle x1="7.58951875" y1="2.39191875" x2="8.03148125" y2="2.4003" layer="94"/>
<rectangle x1="9.12368125" y1="2.39191875" x2="9.49451875" y2="2.4003" layer="94"/>
<rectangle x1="-0.00508125" y1="2.4003" x2="1.8923" y2="2.40868125" layer="94"/>
<rectangle x1="1.9685" y1="2.4003" x2="3.51028125" y2="2.40868125" layer="94"/>
<rectangle x1="4.10971875" y1="2.4003" x2="4.47548125" y2="2.40868125" layer="94"/>
<rectangle x1="5.64388125" y1="2.4003" x2="6.01471875" y2="2.40868125" layer="94"/>
<rectangle x1="6.2611" y1="2.4003" x2="7.39648125" y2="2.40868125" layer="94"/>
<rectangle x1="7.58951875" y1="2.4003" x2="8.02131875" y2="2.40868125" layer="94"/>
<rectangle x1="9.12368125" y1="2.4003" x2="9.49451875" y2="2.40868125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.40868125" x2="1.8923" y2="2.41731875" layer="94"/>
<rectangle x1="1.9685" y1="2.40868125" x2="3.4925" y2="2.41731875" layer="94"/>
<rectangle x1="4.10971875" y1="2.40868125" x2="4.47548125" y2="2.41731875" layer="94"/>
<rectangle x1="5.64388125" y1="2.40868125" x2="6.01471875" y2="2.41731875" layer="94"/>
<rectangle x1="6.2611" y1="2.40868125" x2="7.4041" y2="2.41731875" layer="94"/>
<rectangle x1="7.58951875" y1="2.40868125" x2="8.0137" y2="2.41731875" layer="94"/>
<rectangle x1="9.12368125" y1="2.40868125" x2="9.49451875" y2="2.41731875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.41731875" x2="1.8923" y2="2.4257" layer="94"/>
<rectangle x1="1.9685" y1="2.41731875" x2="3.47471875" y2="2.4257" layer="94"/>
<rectangle x1="4.10971875" y1="2.41731875" x2="4.47548125" y2="2.4257" layer="94"/>
<rectangle x1="5.64388125" y1="2.41731875" x2="6.01471875" y2="2.4257" layer="94"/>
<rectangle x1="6.2611" y1="2.41731875" x2="7.4041" y2="2.4257" layer="94"/>
<rectangle x1="7.58951875" y1="2.41731875" x2="8.00608125" y2="2.4257" layer="94"/>
<rectangle x1="9.12368125" y1="2.41731875" x2="9.49451875" y2="2.4257" layer="94"/>
<rectangle x1="-0.00508125" y1="2.4257" x2="1.8923" y2="2.43408125" layer="94"/>
<rectangle x1="1.9685" y1="2.4257" x2="3.45948125" y2="2.43408125" layer="94"/>
<rectangle x1="4.10971875" y1="2.4257" x2="4.47548125" y2="2.43408125" layer="94"/>
<rectangle x1="5.64388125" y1="2.4257" x2="6.01471875" y2="2.43408125" layer="94"/>
<rectangle x1="6.2611" y1="2.4257" x2="7.39648125" y2="2.43408125" layer="94"/>
<rectangle x1="7.58951875" y1="2.4257" x2="7.99591875" y2="2.43408125" layer="94"/>
<rectangle x1="9.12368125" y1="2.4257" x2="9.49451875" y2="2.43408125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.43408125" x2="1.8923" y2="2.44271875" layer="94"/>
<rectangle x1="1.9685" y1="2.43408125" x2="3.4417" y2="2.44271875" layer="94"/>
<rectangle x1="4.10971875" y1="2.43408125" x2="4.47548125" y2="2.44271875" layer="94"/>
<rectangle x1="5.64388125" y1="2.43408125" x2="6.01471875" y2="2.44271875" layer="94"/>
<rectangle x1="6.2611" y1="2.43408125" x2="7.39648125" y2="2.44271875" layer="94"/>
<rectangle x1="7.58951875" y1="2.43408125" x2="7.9883" y2="2.44271875" layer="94"/>
<rectangle x1="9.12368125" y1="2.43408125" x2="9.49451875" y2="2.44271875" layer="94"/>
<rectangle x1="-0.00508125" y1="2.44271875" x2="1.8923" y2="2.4511" layer="94"/>
<rectangle x1="1.9685" y1="2.44271875" x2="3.4163" y2="2.4511" layer="94"/>
<rectangle x1="4.10971875" y1="2.44271875" x2="4.4831" y2="2.4511" layer="94"/>
<rectangle x1="5.64388125" y1="2.44271875" x2="6.01471875" y2="2.4511" layer="94"/>
<rectangle x1="6.2611" y1="2.44271875" x2="7.39648125" y2="2.4511" layer="94"/>
<rectangle x1="7.58951875" y1="2.44271875" x2="7.98068125" y2="2.4511" layer="94"/>
<rectangle x1="9.12368125" y1="2.44271875" x2="9.49451875" y2="2.4511" layer="94"/>
<rectangle x1="-0.00508125" y1="2.4511" x2="1.8923" y2="2.45948125" layer="94"/>
<rectangle x1="1.9685" y1="2.4511" x2="3.39851875" y2="2.45948125" layer="94"/>
<rectangle x1="4.10971875" y1="2.4511" x2="4.47548125" y2="2.45948125" layer="94"/>
<rectangle x1="5.64388125" y1="2.4511" x2="6.01471875" y2="2.45948125" layer="94"/>
<rectangle x1="6.26871875" y1="2.4511" x2="7.39648125" y2="2.45948125" layer="94"/>
<rectangle x1="7.58951875" y1="2.4511" x2="7.9629" y2="2.45948125" layer="94"/>
<rectangle x1="9.1313" y1="2.4511" x2="9.49451875" y2="2.45948125" layer="94"/>
<rectangle x1="-0.00508125" y1="2.45948125" x2="1.88468125" y2="2.46811875" layer="94"/>
<rectangle x1="1.9685" y1="2.45948125" x2="3.38328125" y2="2.46811875" layer="94"/>
<rectangle x1="4.10971875" y1="2.45948125" x2="4.47548125" y2="2.46811875" layer="94"/>
<rectangle x1="5.6515" y1="2.45948125" x2="6.0071" y2="2.46811875" layer="94"/>
<rectangle x1="6.26871875" y1="2.45948125" x2="7.39648125" y2="2.46811875" layer="94"/>
<rectangle x1="7.59968125" y1="2.45948125" x2="7.95528125" y2="2.46811875" layer="94"/>
<rectangle x1="9.1313" y1="2.45948125" x2="9.49451875" y2="2.46811875" layer="94"/>
<rectangle x1="0.00508125" y1="2.46811875" x2="1.88468125" y2="2.4765" layer="94"/>
<rectangle x1="1.97611875" y1="2.46811875" x2="3.35788125" y2="2.4765" layer="94"/>
<rectangle x1="4.11988125" y1="2.46811875" x2="4.46531875" y2="2.4765" layer="94"/>
<rectangle x1="5.6515" y1="2.46811875" x2="6.0071" y2="2.4765" layer="94"/>
<rectangle x1="6.27888125" y1="2.46811875" x2="7.38631875" y2="2.4765" layer="94"/>
<rectangle x1="7.59968125" y1="2.46811875" x2="7.94511875" y2="2.4765" layer="94"/>
<rectangle x1="9.13891875" y1="2.46811875" x2="9.4869" y2="2.4765" layer="94"/>
<rectangle x1="0.0127" y1="2.4765" x2="1.87451875" y2="2.48488125" layer="94"/>
<rectangle x1="1.98628125" y1="2.4765" x2="3.33248125" y2="2.48488125" layer="94"/>
<rectangle x1="4.1275" y1="2.4765" x2="4.4577" y2="2.48488125" layer="94"/>
<rectangle x1="5.65911875" y1="2.4765" x2="5.99948125" y2="2.48488125" layer="94"/>
<rectangle x1="6.27888125" y1="2.4765" x2="7.3787" y2="2.48488125" layer="94"/>
<rectangle x1="7.6073" y1="2.4765" x2="7.9375" y2="2.48488125" layer="94"/>
<rectangle x1="9.14908125" y1="2.4765" x2="9.47928125" y2="2.48488125" layer="94"/>
<rectangle x1="0.02031875" y1="2.48488125" x2="1.8669" y2="2.49351875" layer="94"/>
<rectangle x1="1.9939" y1="2.48488125" x2="3.30708125" y2="2.49351875" layer="94"/>
<rectangle x1="4.13511875" y1="2.48488125" x2="4.45008125" y2="2.49351875" layer="94"/>
<rectangle x1="5.66928125" y1="2.48488125" x2="5.98931875" y2="2.49351875" layer="94"/>
<rectangle x1="6.2865" y1="2.48488125" x2="7.36091875" y2="2.49351875" layer="94"/>
<rectangle x1="7.61491875" y1="2.48488125" x2="7.92988125" y2="2.49351875" layer="94"/>
<rectangle x1="9.1567" y1="2.48488125" x2="9.46911875" y2="2.49351875" layer="94"/>
<rectangle x1="0.0381" y1="2.49351875" x2="1.85928125" y2="2.5019" layer="94"/>
<rectangle x1="2.01168125" y1="2.49351875" x2="3.28168125" y2="2.5019" layer="94"/>
<rectangle x1="4.14528125" y1="2.49351875" x2="4.4323" y2="2.5019" layer="94"/>
<rectangle x1="5.6769" y1="2.49351875" x2="5.97408125" y2="2.5019" layer="94"/>
<rectangle x1="6.29411875" y1="2.49351875" x2="7.3533" y2="2.5019" layer="94"/>
<rectangle x1="7.6327" y1="2.49351875" x2="7.91971875" y2="2.5019" layer="94"/>
<rectangle x1="9.16431875" y1="2.49351875" x2="9.4615" y2="2.5019" layer="94"/>
<rectangle x1="0.05588125" y1="2.5019" x2="1.84911875" y2="2.51028125" layer="94"/>
<rectangle x1="2.02691875" y1="2.5019" x2="3.25628125" y2="2.51028125" layer="94"/>
<rectangle x1="4.16051875" y1="2.5019" x2="4.42468125" y2="2.51028125" layer="94"/>
<rectangle x1="5.69468125" y1="2.5019" x2="5.96391875" y2="2.51028125" layer="94"/>
<rectangle x1="6.30428125" y1="2.5019" x2="7.33551875" y2="2.51028125" layer="94"/>
<rectangle x1="7.64031875" y1="2.5019" x2="7.90448125" y2="2.51028125" layer="94"/>
<rectangle x1="9.1821" y1="2.5019" x2="9.45388125" y2="2.51028125" layer="94"/>
<rectangle x1="0.07111875" y1="2.51028125" x2="1.83388125" y2="2.51891875" layer="94"/>
<rectangle x1="2.0447" y1="2.51028125" x2="3.22071875" y2="2.51891875" layer="94"/>
<rectangle x1="4.18591875" y1="2.51028125" x2="4.4069" y2="2.51891875" layer="94"/>
<rectangle x1="5.70991875" y1="2.51028125" x2="5.93851875" y2="2.51891875" layer="94"/>
<rectangle x1="6.31951875" y1="2.51028125" x2="7.32028125" y2="2.51891875" layer="94"/>
<rectangle x1="7.6581" y1="2.51028125" x2="7.8867" y2="2.51891875" layer="94"/>
<rectangle x1="9.19988125" y1="2.51028125" x2="9.42848125" y2="2.51891875" layer="94"/>
<rectangle x1="0.0889" y1="2.51891875" x2="1.8161" y2="2.5273" layer="94"/>
<rectangle x1="2.06248125" y1="2.51891875" x2="3.18008125" y2="2.5273" layer="94"/>
<rectangle x1="4.2037" y1="2.51891875" x2="4.3815" y2="2.5273" layer="94"/>
<rectangle x1="5.7277" y1="2.51891875" x2="5.91311875" y2="2.5273" layer="94"/>
<rectangle x1="6.3373" y1="2.51891875" x2="7.3025" y2="2.5273" layer="94"/>
<rectangle x1="7.6835" y1="2.51891875" x2="7.8613" y2="2.5273" layer="94"/>
<rectangle x1="9.22528125" y1="2.51891875" x2="9.4107" y2="2.5273" layer="94"/>
<rectangle x1="0.1143" y1="2.5273" x2="1.7907" y2="2.53568125" layer="94"/>
<rectangle x1="2.08788125" y1="2.5273" x2="3.12928125" y2="2.53568125" layer="94"/>
<rectangle x1="4.2291" y1="2.5273" x2="4.3561" y2="2.53568125" layer="94"/>
<rectangle x1="5.76071875" y1="2.5273" x2="5.88771875" y2="2.53568125" layer="94"/>
<rectangle x1="6.3627" y1="2.5273" x2="7.2771" y2="2.53568125" layer="94"/>
<rectangle x1="7.7343" y1="2.5273" x2="7.8105" y2="2.53568125" layer="94"/>
<rectangle x1="9.27608125" y1="2.5273" x2="9.3599" y2="2.53568125" layer="94"/>
<rectangle x1="5.81151875" y1="2.53568125" x2="5.83691875" y2="2.54431875" layer="94"/>
<rectangle x1="0.02031875" y1="2.71348125" x2="0.45211875" y2="2.72211875" layer="94"/>
<rectangle x1="0.51308125" y1="2.71348125" x2="0.5461" y2="2.72211875" layer="94"/>
<rectangle x1="0.61468125" y1="2.71348125" x2="0.6477" y2="2.72211875" layer="94"/>
<rectangle x1="0.7239" y1="2.71348125" x2="0.75691875" y2="2.72211875" layer="94"/>
<rectangle x1="0.83311875" y1="2.71348125" x2="0.86868125" y2="2.72211875" layer="94"/>
<rectangle x1="0.96011875" y1="2.71348125" x2="0.99568125" y2="2.72211875" layer="94"/>
<rectangle x1="1.0795" y1="2.71348125" x2="1.11251875" y2="2.72211875" layer="94"/>
<rectangle x1="1.1811" y1="2.71348125" x2="1.83388125" y2="2.72211875" layer="94"/>
<rectangle x1="1.9685" y1="2.71348125" x2="2.78891875" y2="2.72211875" layer="94"/>
<rectangle x1="2.83971875" y1="2.71348125" x2="2.87528125" y2="2.72211875" layer="94"/>
<rectangle x1="2.92608125" y1="2.71348125" x2="3.68808125" y2="2.72211875" layer="94"/>
<rectangle x1="3.7719" y1="2.71348125" x2="4.4323" y2="2.72211875" layer="94"/>
<rectangle x1="5.10031875" y1="2.71348125" x2="5.72008125" y2="2.72211875" layer="94"/>
<rectangle x1="5.81151875" y1="2.71348125" x2="6.57351875" y2="2.72211875" layer="94"/>
<rectangle x1="6.6167" y1="2.71348125" x2="6.64971875" y2="2.72211875" layer="94"/>
<rectangle x1="6.70051875" y1="2.71348125" x2="7.52348125" y2="2.72211875" layer="94"/>
<rectangle x1="7.6581" y1="2.71348125" x2="8.30071875" y2="2.72211875" layer="94"/>
<rectangle x1="8.37691875" y1="2.71348125" x2="8.41248125" y2="2.72211875" layer="94"/>
<rectangle x1="8.4963" y1="2.71348125" x2="8.52931875" y2="2.72211875" layer="94"/>
<rectangle x1="8.61568125" y1="2.71348125" x2="8.6487" y2="2.72211875" layer="94"/>
<rectangle x1="8.73251875" y1="2.71348125" x2="8.76808125" y2="2.72211875" layer="94"/>
<rectangle x1="8.83411875" y1="2.71348125" x2="8.86968125" y2="2.72211875" layer="94"/>
<rectangle x1="8.93571875" y1="2.71348125" x2="8.97128125" y2="2.72211875" layer="94"/>
<rectangle x1="9.03731875" y1="2.71348125" x2="9.49451875" y2="2.72211875" layer="94"/>
<rectangle x1="0.02031875" y1="2.72211875" x2="0.45211875" y2="2.7305" layer="94"/>
<rectangle x1="0.51308125" y1="2.72211875" x2="0.5461" y2="2.7305" layer="94"/>
<rectangle x1="0.61468125" y1="2.72211875" x2="0.6477" y2="2.7305" layer="94"/>
<rectangle x1="0.7239" y1="2.72211875" x2="0.75691875" y2="2.7305" layer="94"/>
<rectangle x1="0.83311875" y1="2.72211875" x2="0.86868125" y2="2.7305" layer="94"/>
<rectangle x1="0.96011875" y1="2.72211875" x2="0.99568125" y2="2.7305" layer="94"/>
<rectangle x1="1.0795" y1="2.72211875" x2="1.11251875" y2="2.7305" layer="94"/>
<rectangle x1="1.1811" y1="2.72211875" x2="1.83388125" y2="2.7305" layer="94"/>
<rectangle x1="1.9685" y1="2.72211875" x2="2.78891875" y2="2.7305" layer="94"/>
<rectangle x1="2.83971875" y1="2.72211875" x2="2.87528125" y2="2.7305" layer="94"/>
<rectangle x1="2.92608125" y1="2.72211875" x2="3.68808125" y2="2.7305" layer="94"/>
<rectangle x1="3.7719" y1="2.72211875" x2="4.4323" y2="2.7305" layer="94"/>
<rectangle x1="5.10031875" y1="2.72211875" x2="5.72008125" y2="2.7305" layer="94"/>
<rectangle x1="5.81151875" y1="2.72211875" x2="6.57351875" y2="2.7305" layer="94"/>
<rectangle x1="6.6167" y1="2.72211875" x2="6.64971875" y2="2.7305" layer="94"/>
<rectangle x1="6.70051875" y1="2.72211875" x2="7.52348125" y2="2.7305" layer="94"/>
<rectangle x1="7.6581" y1="2.72211875" x2="8.30071875" y2="2.7305" layer="94"/>
<rectangle x1="8.37691875" y1="2.72211875" x2="8.41248125" y2="2.7305" layer="94"/>
<rectangle x1="8.4963" y1="2.72211875" x2="8.52931875" y2="2.7305" layer="94"/>
<rectangle x1="8.61568125" y1="2.72211875" x2="8.6487" y2="2.7305" layer="94"/>
<rectangle x1="8.73251875" y1="2.72211875" x2="8.76808125" y2="2.7305" layer="94"/>
<rectangle x1="8.83411875" y1="2.72211875" x2="8.86968125" y2="2.7305" layer="94"/>
<rectangle x1="8.93571875" y1="2.72211875" x2="8.97128125" y2="2.7305" layer="94"/>
<rectangle x1="9.03731875" y1="2.72211875" x2="9.49451875" y2="2.7305" layer="94"/>
<rectangle x1="0.02031875" y1="2.7305" x2="0.45211875" y2="2.73888125" layer="94"/>
<rectangle x1="0.51308125" y1="2.7305" x2="0.5461" y2="2.73888125" layer="94"/>
<rectangle x1="0.61468125" y1="2.7305" x2="0.6477" y2="2.73888125" layer="94"/>
<rectangle x1="0.7239" y1="2.7305" x2="0.75691875" y2="2.73888125" layer="94"/>
<rectangle x1="0.83311875" y1="2.7305" x2="0.86868125" y2="2.73888125" layer="94"/>
<rectangle x1="0.96011875" y1="2.7305" x2="0.99568125" y2="2.73888125" layer="94"/>
<rectangle x1="1.0795" y1="2.7305" x2="1.11251875" y2="2.73888125" layer="94"/>
<rectangle x1="1.1811" y1="2.7305" x2="1.83388125" y2="2.73888125" layer="94"/>
<rectangle x1="1.9685" y1="2.7305" x2="2.78891875" y2="2.73888125" layer="94"/>
<rectangle x1="2.83971875" y1="2.7305" x2="2.87528125" y2="2.73888125" layer="94"/>
<rectangle x1="2.92608125" y1="2.7305" x2="3.68808125" y2="2.73888125" layer="94"/>
<rectangle x1="3.7719" y1="2.7305" x2="4.4323" y2="2.73888125" layer="94"/>
<rectangle x1="5.10031875" y1="2.7305" x2="5.72008125" y2="2.73888125" layer="94"/>
<rectangle x1="5.81151875" y1="2.7305" x2="6.57351875" y2="2.73888125" layer="94"/>
<rectangle x1="6.6167" y1="2.7305" x2="6.64971875" y2="2.73888125" layer="94"/>
<rectangle x1="6.70051875" y1="2.7305" x2="7.52348125" y2="2.73888125" layer="94"/>
<rectangle x1="7.6581" y1="2.7305" x2="8.30071875" y2="2.73888125" layer="94"/>
<rectangle x1="8.37691875" y1="2.7305" x2="8.41248125" y2="2.73888125" layer="94"/>
<rectangle x1="8.4963" y1="2.7305" x2="8.52931875" y2="2.73888125" layer="94"/>
<rectangle x1="8.61568125" y1="2.7305" x2="8.6487" y2="2.73888125" layer="94"/>
<rectangle x1="8.73251875" y1="2.7305" x2="8.76808125" y2="2.73888125" layer="94"/>
<rectangle x1="8.83411875" y1="2.7305" x2="8.86968125" y2="2.73888125" layer="94"/>
<rectangle x1="8.93571875" y1="2.7305" x2="8.97128125" y2="2.73888125" layer="94"/>
<rectangle x1="9.03731875" y1="2.7305" x2="9.49451875" y2="2.73888125" layer="94"/>
<rectangle x1="0.02031875" y1="2.73888125" x2="0.45211875" y2="2.74751875" layer="94"/>
<rectangle x1="0.51308125" y1="2.73888125" x2="0.5461" y2="2.74751875" layer="94"/>
<rectangle x1="0.61468125" y1="2.73888125" x2="0.6477" y2="2.74751875" layer="94"/>
<rectangle x1="0.7239" y1="2.73888125" x2="0.75691875" y2="2.74751875" layer="94"/>
<rectangle x1="0.83311875" y1="2.73888125" x2="0.86868125" y2="2.74751875" layer="94"/>
<rectangle x1="0.96011875" y1="2.73888125" x2="0.99568125" y2="2.74751875" layer="94"/>
<rectangle x1="1.0795" y1="2.73888125" x2="1.11251875" y2="2.74751875" layer="94"/>
<rectangle x1="1.1811" y1="2.73888125" x2="1.83388125" y2="2.74751875" layer="94"/>
<rectangle x1="1.9685" y1="2.73888125" x2="2.78891875" y2="2.74751875" layer="94"/>
<rectangle x1="2.83971875" y1="2.73888125" x2="2.87528125" y2="2.74751875" layer="94"/>
<rectangle x1="2.92608125" y1="2.73888125" x2="3.68808125" y2="2.74751875" layer="94"/>
<rectangle x1="3.7719" y1="2.73888125" x2="4.4323" y2="2.74751875" layer="94"/>
<rectangle x1="5.10031875" y1="2.73888125" x2="5.72008125" y2="2.74751875" layer="94"/>
<rectangle x1="5.81151875" y1="2.73888125" x2="6.57351875" y2="2.74751875" layer="94"/>
<rectangle x1="6.6167" y1="2.73888125" x2="6.64971875" y2="2.74751875" layer="94"/>
<rectangle x1="6.70051875" y1="2.73888125" x2="7.52348125" y2="2.74751875" layer="94"/>
<rectangle x1="7.6581" y1="2.73888125" x2="8.30071875" y2="2.74751875" layer="94"/>
<rectangle x1="8.37691875" y1="2.73888125" x2="8.41248125" y2="2.74751875" layer="94"/>
<rectangle x1="8.4963" y1="2.73888125" x2="8.52931875" y2="2.74751875" layer="94"/>
<rectangle x1="8.61568125" y1="2.73888125" x2="8.6487" y2="2.74751875" layer="94"/>
<rectangle x1="8.73251875" y1="2.73888125" x2="8.76808125" y2="2.74751875" layer="94"/>
<rectangle x1="8.83411875" y1="2.73888125" x2="8.86968125" y2="2.74751875" layer="94"/>
<rectangle x1="8.93571875" y1="2.73888125" x2="8.97128125" y2="2.74751875" layer="94"/>
<rectangle x1="9.03731875" y1="2.73888125" x2="9.49451875" y2="2.74751875" layer="94"/>
<rectangle x1="0.02031875" y1="2.74751875" x2="0.45211875" y2="2.7559" layer="94"/>
<rectangle x1="0.51308125" y1="2.74751875" x2="0.5461" y2="2.7559" layer="94"/>
<rectangle x1="0.61468125" y1="2.74751875" x2="0.6477" y2="2.7559" layer="94"/>
<rectangle x1="0.7239" y1="2.74751875" x2="0.75691875" y2="2.7559" layer="94"/>
<rectangle x1="0.83311875" y1="2.74751875" x2="0.86868125" y2="2.7559" layer="94"/>
<rectangle x1="0.96011875" y1="2.74751875" x2="0.99568125" y2="2.7559" layer="94"/>
<rectangle x1="1.0795" y1="2.74751875" x2="1.11251875" y2="2.7559" layer="94"/>
<rectangle x1="1.1811" y1="2.74751875" x2="1.83388125" y2="2.7559" layer="94"/>
<rectangle x1="1.9685" y1="2.74751875" x2="2.78891875" y2="2.7559" layer="94"/>
<rectangle x1="2.83971875" y1="2.74751875" x2="2.87528125" y2="2.7559" layer="94"/>
<rectangle x1="2.92608125" y1="2.74751875" x2="3.68808125" y2="2.7559" layer="94"/>
<rectangle x1="3.7719" y1="2.74751875" x2="4.4323" y2="2.7559" layer="94"/>
<rectangle x1="5.10031875" y1="2.74751875" x2="5.72008125" y2="2.7559" layer="94"/>
<rectangle x1="5.81151875" y1="2.74751875" x2="6.57351875" y2="2.7559" layer="94"/>
<rectangle x1="6.6167" y1="2.74751875" x2="6.64971875" y2="2.7559" layer="94"/>
<rectangle x1="6.70051875" y1="2.74751875" x2="7.52348125" y2="2.7559" layer="94"/>
<rectangle x1="7.6581" y1="2.74751875" x2="8.30071875" y2="2.7559" layer="94"/>
<rectangle x1="8.37691875" y1="2.74751875" x2="8.41248125" y2="2.7559" layer="94"/>
<rectangle x1="8.4963" y1="2.74751875" x2="8.52931875" y2="2.7559" layer="94"/>
<rectangle x1="8.61568125" y1="2.74751875" x2="8.6487" y2="2.7559" layer="94"/>
<rectangle x1="8.73251875" y1="2.74751875" x2="8.76808125" y2="2.7559" layer="94"/>
<rectangle x1="8.83411875" y1="2.74751875" x2="8.86968125" y2="2.7559" layer="94"/>
<rectangle x1="8.93571875" y1="2.74751875" x2="8.97128125" y2="2.7559" layer="94"/>
<rectangle x1="9.03731875" y1="2.74751875" x2="9.49451875" y2="2.7559" layer="94"/>
<rectangle x1="0.02031875" y1="2.7559" x2="0.45211875" y2="2.76428125" layer="94"/>
<rectangle x1="0.51308125" y1="2.7559" x2="0.5461" y2="2.76428125" layer="94"/>
<rectangle x1="0.61468125" y1="2.7559" x2="0.6477" y2="2.76428125" layer="94"/>
<rectangle x1="0.7239" y1="2.7559" x2="0.75691875" y2="2.76428125" layer="94"/>
<rectangle x1="0.83311875" y1="2.7559" x2="0.86868125" y2="2.76428125" layer="94"/>
<rectangle x1="0.96011875" y1="2.7559" x2="0.99568125" y2="2.76428125" layer="94"/>
<rectangle x1="1.0795" y1="2.7559" x2="1.11251875" y2="2.76428125" layer="94"/>
<rectangle x1="1.1811" y1="2.7559" x2="1.83388125" y2="2.76428125" layer="94"/>
<rectangle x1="1.9685" y1="2.7559" x2="2.78891875" y2="2.76428125" layer="94"/>
<rectangle x1="2.83971875" y1="2.7559" x2="2.87528125" y2="2.76428125" layer="94"/>
<rectangle x1="2.92608125" y1="2.7559" x2="3.68808125" y2="2.76428125" layer="94"/>
<rectangle x1="3.7719" y1="2.7559" x2="4.4323" y2="2.76428125" layer="94"/>
<rectangle x1="5.10031875" y1="2.7559" x2="5.72008125" y2="2.76428125" layer="94"/>
<rectangle x1="5.81151875" y1="2.7559" x2="6.57351875" y2="2.76428125" layer="94"/>
<rectangle x1="6.6167" y1="2.7559" x2="6.64971875" y2="2.76428125" layer="94"/>
<rectangle x1="6.70051875" y1="2.7559" x2="7.52348125" y2="2.76428125" layer="94"/>
<rectangle x1="7.6581" y1="2.7559" x2="8.30071875" y2="2.76428125" layer="94"/>
<rectangle x1="8.37691875" y1="2.7559" x2="8.41248125" y2="2.76428125" layer="94"/>
<rectangle x1="8.4963" y1="2.7559" x2="8.52931875" y2="2.76428125" layer="94"/>
<rectangle x1="8.61568125" y1="2.7559" x2="8.6487" y2="2.76428125" layer="94"/>
<rectangle x1="8.73251875" y1="2.7559" x2="8.76808125" y2="2.76428125" layer="94"/>
<rectangle x1="8.83411875" y1="2.7559" x2="8.86968125" y2="2.76428125" layer="94"/>
<rectangle x1="8.93571875" y1="2.7559" x2="8.97128125" y2="2.76428125" layer="94"/>
<rectangle x1="9.03731875" y1="2.7559" x2="9.49451875" y2="2.76428125" layer="94"/>
<rectangle x1="0.02031875" y1="2.76428125" x2="0.45211875" y2="2.77291875" layer="94"/>
<rectangle x1="0.51308125" y1="2.76428125" x2="0.5461" y2="2.77291875" layer="94"/>
<rectangle x1="0.61468125" y1="2.76428125" x2="0.6477" y2="2.77291875" layer="94"/>
<rectangle x1="0.7239" y1="2.76428125" x2="0.75691875" y2="2.77291875" layer="94"/>
<rectangle x1="0.83311875" y1="2.76428125" x2="0.86868125" y2="2.77291875" layer="94"/>
<rectangle x1="0.96011875" y1="2.76428125" x2="0.99568125" y2="2.77291875" layer="94"/>
<rectangle x1="1.0795" y1="2.76428125" x2="1.11251875" y2="2.77291875" layer="94"/>
<rectangle x1="1.1811" y1="2.76428125" x2="1.83388125" y2="2.77291875" layer="94"/>
<rectangle x1="1.9685" y1="2.76428125" x2="2.78891875" y2="2.77291875" layer="94"/>
<rectangle x1="2.83971875" y1="2.76428125" x2="2.87528125" y2="2.77291875" layer="94"/>
<rectangle x1="2.92608125" y1="2.76428125" x2="3.68808125" y2="2.77291875" layer="94"/>
<rectangle x1="3.7719" y1="2.76428125" x2="4.4323" y2="2.77291875" layer="94"/>
<rectangle x1="5.10031875" y1="2.76428125" x2="5.72008125" y2="2.77291875" layer="94"/>
<rectangle x1="5.81151875" y1="2.76428125" x2="6.57351875" y2="2.77291875" layer="94"/>
<rectangle x1="6.6167" y1="2.76428125" x2="6.64971875" y2="2.77291875" layer="94"/>
<rectangle x1="6.70051875" y1="2.76428125" x2="7.52348125" y2="2.77291875" layer="94"/>
<rectangle x1="7.6581" y1="2.76428125" x2="8.30071875" y2="2.77291875" layer="94"/>
<rectangle x1="8.37691875" y1="2.76428125" x2="8.41248125" y2="2.77291875" layer="94"/>
<rectangle x1="8.4963" y1="2.76428125" x2="8.52931875" y2="2.77291875" layer="94"/>
<rectangle x1="8.61568125" y1="2.76428125" x2="8.6487" y2="2.77291875" layer="94"/>
<rectangle x1="8.73251875" y1="2.76428125" x2="8.76808125" y2="2.77291875" layer="94"/>
<rectangle x1="8.83411875" y1="2.76428125" x2="8.86968125" y2="2.77291875" layer="94"/>
<rectangle x1="8.93571875" y1="2.76428125" x2="8.97128125" y2="2.77291875" layer="94"/>
<rectangle x1="9.03731875" y1="2.76428125" x2="9.49451875" y2="2.77291875" layer="94"/>
<rectangle x1="0.02031875" y1="2.77291875" x2="0.45211875" y2="2.7813" layer="94"/>
<rectangle x1="0.51308125" y1="2.77291875" x2="0.5461" y2="2.7813" layer="94"/>
<rectangle x1="0.61468125" y1="2.77291875" x2="0.6477" y2="2.7813" layer="94"/>
<rectangle x1="0.7239" y1="2.77291875" x2="0.75691875" y2="2.7813" layer="94"/>
<rectangle x1="0.83311875" y1="2.77291875" x2="0.86868125" y2="2.7813" layer="94"/>
<rectangle x1="0.96011875" y1="2.77291875" x2="0.99568125" y2="2.7813" layer="94"/>
<rectangle x1="1.0795" y1="2.77291875" x2="1.11251875" y2="2.7813" layer="94"/>
<rectangle x1="1.1811" y1="2.77291875" x2="1.83388125" y2="2.7813" layer="94"/>
<rectangle x1="1.9685" y1="2.77291875" x2="2.78891875" y2="2.7813" layer="94"/>
<rectangle x1="2.83971875" y1="2.77291875" x2="2.87528125" y2="2.7813" layer="94"/>
<rectangle x1="2.92608125" y1="2.77291875" x2="3.68808125" y2="2.7813" layer="94"/>
<rectangle x1="3.7719" y1="2.77291875" x2="4.4323" y2="2.7813" layer="94"/>
<rectangle x1="5.10031875" y1="2.77291875" x2="5.72008125" y2="2.7813" layer="94"/>
<rectangle x1="5.81151875" y1="2.77291875" x2="6.57351875" y2="2.7813" layer="94"/>
<rectangle x1="6.6167" y1="2.77291875" x2="6.64971875" y2="2.7813" layer="94"/>
<rectangle x1="6.70051875" y1="2.77291875" x2="7.52348125" y2="2.7813" layer="94"/>
<rectangle x1="7.6581" y1="2.77291875" x2="8.30071875" y2="2.7813" layer="94"/>
<rectangle x1="8.37691875" y1="2.77291875" x2="8.41248125" y2="2.7813" layer="94"/>
<rectangle x1="8.4963" y1="2.77291875" x2="8.52931875" y2="2.7813" layer="94"/>
<rectangle x1="8.61568125" y1="2.77291875" x2="8.6487" y2="2.7813" layer="94"/>
<rectangle x1="8.73251875" y1="2.77291875" x2="8.76808125" y2="2.7813" layer="94"/>
<rectangle x1="8.83411875" y1="2.77291875" x2="8.86968125" y2="2.7813" layer="94"/>
<rectangle x1="8.93571875" y1="2.77291875" x2="8.97128125" y2="2.7813" layer="94"/>
<rectangle x1="9.03731875" y1="2.77291875" x2="9.49451875" y2="2.7813" layer="94"/>
<rectangle x1="0.02031875" y1="2.7813" x2="0.45211875" y2="2.78968125" layer="94"/>
<rectangle x1="0.51308125" y1="2.7813" x2="0.5461" y2="2.78968125" layer="94"/>
<rectangle x1="0.61468125" y1="2.7813" x2="0.6477" y2="2.78968125" layer="94"/>
<rectangle x1="0.7239" y1="2.7813" x2="0.75691875" y2="2.78968125" layer="94"/>
<rectangle x1="0.83311875" y1="2.7813" x2="0.86868125" y2="2.78968125" layer="94"/>
<rectangle x1="0.96011875" y1="2.7813" x2="0.99568125" y2="2.78968125" layer="94"/>
<rectangle x1="1.0795" y1="2.7813" x2="1.11251875" y2="2.78968125" layer="94"/>
<rectangle x1="1.1811" y1="2.7813" x2="1.83388125" y2="2.78968125" layer="94"/>
<rectangle x1="1.9685" y1="2.7813" x2="2.78891875" y2="2.78968125" layer="94"/>
<rectangle x1="2.83971875" y1="2.7813" x2="2.87528125" y2="2.78968125" layer="94"/>
<rectangle x1="2.92608125" y1="2.7813" x2="3.68808125" y2="2.78968125" layer="94"/>
<rectangle x1="3.7719" y1="2.7813" x2="4.4323" y2="2.78968125" layer="94"/>
<rectangle x1="5.10031875" y1="2.7813" x2="5.72008125" y2="2.78968125" layer="94"/>
<rectangle x1="5.81151875" y1="2.7813" x2="6.57351875" y2="2.78968125" layer="94"/>
<rectangle x1="6.6167" y1="2.7813" x2="6.64971875" y2="2.78968125" layer="94"/>
<rectangle x1="6.70051875" y1="2.7813" x2="7.52348125" y2="2.78968125" layer="94"/>
<rectangle x1="7.6581" y1="2.7813" x2="8.30071875" y2="2.78968125" layer="94"/>
<rectangle x1="8.37691875" y1="2.7813" x2="8.41248125" y2="2.78968125" layer="94"/>
<rectangle x1="8.4963" y1="2.7813" x2="8.52931875" y2="2.78968125" layer="94"/>
<rectangle x1="8.61568125" y1="2.7813" x2="8.6487" y2="2.78968125" layer="94"/>
<rectangle x1="8.73251875" y1="2.7813" x2="8.76808125" y2="2.78968125" layer="94"/>
<rectangle x1="8.83411875" y1="2.7813" x2="8.86968125" y2="2.78968125" layer="94"/>
<rectangle x1="8.93571875" y1="2.7813" x2="8.97128125" y2="2.78968125" layer="94"/>
<rectangle x1="9.03731875" y1="2.7813" x2="9.49451875" y2="2.78968125" layer="94"/>
<rectangle x1="0.02031875" y1="2.78968125" x2="0.45211875" y2="2.79831875" layer="94"/>
<rectangle x1="0.51308125" y1="2.78968125" x2="0.5461" y2="2.79831875" layer="94"/>
<rectangle x1="0.61468125" y1="2.78968125" x2="0.6477" y2="2.79831875" layer="94"/>
<rectangle x1="0.7239" y1="2.78968125" x2="0.75691875" y2="2.79831875" layer="94"/>
<rectangle x1="0.83311875" y1="2.78968125" x2="0.86868125" y2="2.79831875" layer="94"/>
<rectangle x1="0.96011875" y1="2.78968125" x2="0.99568125" y2="2.79831875" layer="94"/>
<rectangle x1="1.0795" y1="2.78968125" x2="1.11251875" y2="2.79831875" layer="94"/>
<rectangle x1="1.1811" y1="2.78968125" x2="1.83388125" y2="2.79831875" layer="94"/>
<rectangle x1="1.9685" y1="2.78968125" x2="2.78891875" y2="2.79831875" layer="94"/>
<rectangle x1="2.83971875" y1="2.78968125" x2="2.87528125" y2="2.79831875" layer="94"/>
<rectangle x1="2.92608125" y1="2.78968125" x2="3.68808125" y2="2.79831875" layer="94"/>
<rectangle x1="3.7719" y1="2.78968125" x2="4.4323" y2="2.79831875" layer="94"/>
<rectangle x1="5.10031875" y1="2.78968125" x2="5.72008125" y2="2.79831875" layer="94"/>
<rectangle x1="5.81151875" y1="2.78968125" x2="6.57351875" y2="2.79831875" layer="94"/>
<rectangle x1="6.6167" y1="2.78968125" x2="6.64971875" y2="2.79831875" layer="94"/>
<rectangle x1="6.70051875" y1="2.78968125" x2="7.52348125" y2="2.79831875" layer="94"/>
<rectangle x1="7.6581" y1="2.78968125" x2="8.30071875" y2="2.79831875" layer="94"/>
<rectangle x1="8.37691875" y1="2.78968125" x2="8.41248125" y2="2.79831875" layer="94"/>
<rectangle x1="8.4963" y1="2.78968125" x2="8.52931875" y2="2.79831875" layer="94"/>
<rectangle x1="8.61568125" y1="2.78968125" x2="8.6487" y2="2.79831875" layer="94"/>
<rectangle x1="8.73251875" y1="2.78968125" x2="8.76808125" y2="2.79831875" layer="94"/>
<rectangle x1="8.83411875" y1="2.78968125" x2="8.86968125" y2="2.79831875" layer="94"/>
<rectangle x1="8.93571875" y1="2.78968125" x2="8.97128125" y2="2.79831875" layer="94"/>
<rectangle x1="9.03731875" y1="2.78968125" x2="9.49451875" y2="2.79831875" layer="94"/>
<rectangle x1="0.02031875" y1="2.79831875" x2="0.45211875" y2="2.8067" layer="94"/>
<rectangle x1="0.51308125" y1="2.79831875" x2="0.5461" y2="2.8067" layer="94"/>
<rectangle x1="0.61468125" y1="2.79831875" x2="0.6477" y2="2.8067" layer="94"/>
<rectangle x1="0.7239" y1="2.79831875" x2="0.75691875" y2="2.8067" layer="94"/>
<rectangle x1="0.83311875" y1="2.79831875" x2="0.86868125" y2="2.8067" layer="94"/>
<rectangle x1="0.96011875" y1="2.79831875" x2="0.99568125" y2="2.8067" layer="94"/>
<rectangle x1="1.0795" y1="2.79831875" x2="1.11251875" y2="2.8067" layer="94"/>
<rectangle x1="1.1811" y1="2.79831875" x2="1.83388125" y2="2.8067" layer="94"/>
<rectangle x1="1.9685" y1="2.79831875" x2="2.78891875" y2="2.8067" layer="94"/>
<rectangle x1="2.83971875" y1="2.79831875" x2="2.87528125" y2="2.8067" layer="94"/>
<rectangle x1="2.92608125" y1="2.79831875" x2="3.68808125" y2="2.8067" layer="94"/>
<rectangle x1="3.7719" y1="2.79831875" x2="4.4323" y2="2.8067" layer="94"/>
<rectangle x1="5.10031875" y1="2.79831875" x2="5.72008125" y2="2.8067" layer="94"/>
<rectangle x1="5.81151875" y1="2.79831875" x2="6.57351875" y2="2.8067" layer="94"/>
<rectangle x1="6.6167" y1="2.79831875" x2="6.64971875" y2="2.8067" layer="94"/>
<rectangle x1="6.70051875" y1="2.79831875" x2="7.52348125" y2="2.8067" layer="94"/>
<rectangle x1="7.6581" y1="2.79831875" x2="8.30071875" y2="2.8067" layer="94"/>
<rectangle x1="8.37691875" y1="2.79831875" x2="8.41248125" y2="2.8067" layer="94"/>
<rectangle x1="8.4963" y1="2.79831875" x2="8.52931875" y2="2.8067" layer="94"/>
<rectangle x1="8.61568125" y1="2.79831875" x2="8.6487" y2="2.8067" layer="94"/>
<rectangle x1="8.73251875" y1="2.79831875" x2="8.76808125" y2="2.8067" layer="94"/>
<rectangle x1="8.83411875" y1="2.79831875" x2="8.86968125" y2="2.8067" layer="94"/>
<rectangle x1="8.93571875" y1="2.79831875" x2="8.97128125" y2="2.8067" layer="94"/>
<rectangle x1="9.03731875" y1="2.79831875" x2="9.49451875" y2="2.8067" layer="94"/>
<rectangle x1="0.41148125" y1="2.8067" x2="0.45211875" y2="2.81508125" layer="94"/>
<rectangle x1="0.51308125" y1="2.8067" x2="0.5461" y2="2.81508125" layer="94"/>
<rectangle x1="0.61468125" y1="2.8067" x2="0.6477" y2="2.81508125" layer="94"/>
<rectangle x1="0.7239" y1="2.8067" x2="0.75691875" y2="2.81508125" layer="94"/>
<rectangle x1="0.83311875" y1="2.8067" x2="0.86868125" y2="2.81508125" layer="94"/>
<rectangle x1="0.96011875" y1="2.8067" x2="0.99568125" y2="2.81508125" layer="94"/>
<rectangle x1="1.0795" y1="2.8067" x2="1.11251875" y2="2.81508125" layer="94"/>
<rectangle x1="1.1811" y1="2.8067" x2="1.21411875" y2="2.81508125" layer="94"/>
<rectangle x1="1.79831875" y1="2.8067" x2="1.83388125" y2="2.81508125" layer="94"/>
<rectangle x1="1.9685" y1="2.8067" x2="2.00151875" y2="2.81508125" layer="94"/>
<rectangle x1="2.7559" y1="2.8067" x2="2.78891875" y2="2.81508125" layer="94"/>
<rectangle x1="2.83971875" y1="2.8067" x2="2.87528125" y2="2.81508125" layer="94"/>
<rectangle x1="2.92608125" y1="2.8067" x2="2.9591" y2="2.81508125" layer="94"/>
<rectangle x1="3.65251875" y1="2.8067" x2="3.68808125" y2="2.81508125" layer="94"/>
<rectangle x1="3.7719" y1="2.8067" x2="3.80491875" y2="2.81508125" layer="94"/>
<rectangle x1="4.4069" y1="2.8067" x2="4.4323" y2="2.81508125" layer="94"/>
<rectangle x1="5.10031875" y1="2.8067" x2="5.13588125" y2="2.81508125" layer="94"/>
<rectangle x1="5.68451875" y1="2.8067" x2="5.72008125" y2="2.81508125" layer="94"/>
<rectangle x1="5.81151875" y1="2.8067" x2="5.84708125" y2="2.81508125" layer="94"/>
<rectangle x1="6.5405" y1="2.8067" x2="6.57351875" y2="2.81508125" layer="94"/>
<rectangle x1="6.6167" y1="2.8067" x2="6.64971875" y2="2.81508125" layer="94"/>
<rectangle x1="6.70051875" y1="2.8067" x2="6.73608125" y2="2.81508125" layer="94"/>
<rectangle x1="7.48791875" y1="2.8067" x2="7.52348125" y2="2.81508125" layer="94"/>
<rectangle x1="7.6581" y1="2.8067" x2="7.69111875" y2="2.81508125" layer="94"/>
<rectangle x1="8.2677" y1="2.8067" x2="8.30071875" y2="2.81508125" layer="94"/>
<rectangle x1="8.37691875" y1="2.8067" x2="8.41248125" y2="2.81508125" layer="94"/>
<rectangle x1="8.4963" y1="2.8067" x2="8.52931875" y2="2.81508125" layer="94"/>
<rectangle x1="8.61568125" y1="2.8067" x2="8.6487" y2="2.81508125" layer="94"/>
<rectangle x1="8.73251875" y1="2.8067" x2="8.76808125" y2="2.81508125" layer="94"/>
<rectangle x1="8.83411875" y1="2.8067" x2="8.86968125" y2="2.81508125" layer="94"/>
<rectangle x1="8.93571875" y1="2.8067" x2="8.97128125" y2="2.81508125" layer="94"/>
<rectangle x1="9.03731875" y1="2.8067" x2="9.07288125" y2="2.81508125" layer="94"/>
<rectangle x1="0.4191" y1="2.81508125" x2="0.45211875" y2="2.82371875" layer="94"/>
<rectangle x1="0.51308125" y1="2.81508125" x2="0.5461" y2="2.82371875" layer="94"/>
<rectangle x1="0.61468125" y1="2.81508125" x2="0.6477" y2="2.82371875" layer="94"/>
<rectangle x1="0.7239" y1="2.81508125" x2="0.75691875" y2="2.82371875" layer="94"/>
<rectangle x1="0.83311875" y1="2.81508125" x2="0.86868125" y2="2.82371875" layer="94"/>
<rectangle x1="0.96011875" y1="2.81508125" x2="0.99568125" y2="2.82371875" layer="94"/>
<rectangle x1="1.0795" y1="2.81508125" x2="1.11251875" y2="2.82371875" layer="94"/>
<rectangle x1="1.1811" y1="2.81508125" x2="1.21411875" y2="2.82371875" layer="94"/>
<rectangle x1="1.79831875" y1="2.81508125" x2="1.83388125" y2="2.82371875" layer="94"/>
<rectangle x1="1.9685" y1="2.81508125" x2="2.00151875" y2="2.82371875" layer="94"/>
<rectangle x1="2.7559" y1="2.81508125" x2="2.78891875" y2="2.82371875" layer="94"/>
<rectangle x1="2.83971875" y1="2.81508125" x2="2.87528125" y2="2.82371875" layer="94"/>
<rectangle x1="2.92608125" y1="2.81508125" x2="2.9591" y2="2.82371875" layer="94"/>
<rectangle x1="3.65251875" y1="2.81508125" x2="3.68808125" y2="2.82371875" layer="94"/>
<rectangle x1="3.7719" y1="2.81508125" x2="3.80491875" y2="2.82371875" layer="94"/>
<rectangle x1="4.4069" y1="2.81508125" x2="4.4323" y2="2.82371875" layer="94"/>
<rectangle x1="5.10031875" y1="2.81508125" x2="5.13588125" y2="2.82371875" layer="94"/>
<rectangle x1="5.68451875" y1="2.81508125" x2="5.72008125" y2="2.82371875" layer="94"/>
<rectangle x1="5.81151875" y1="2.81508125" x2="5.84708125" y2="2.82371875" layer="94"/>
<rectangle x1="6.5405" y1="2.81508125" x2="6.57351875" y2="2.82371875" layer="94"/>
<rectangle x1="6.6167" y1="2.81508125" x2="6.64971875" y2="2.82371875" layer="94"/>
<rectangle x1="6.70051875" y1="2.81508125" x2="6.73608125" y2="2.82371875" layer="94"/>
<rectangle x1="7.48791875" y1="2.81508125" x2="7.52348125" y2="2.82371875" layer="94"/>
<rectangle x1="7.6581" y1="2.81508125" x2="7.69111875" y2="2.82371875" layer="94"/>
<rectangle x1="8.2677" y1="2.81508125" x2="8.30071875" y2="2.82371875" layer="94"/>
<rectangle x1="8.37691875" y1="2.81508125" x2="8.41248125" y2="2.82371875" layer="94"/>
<rectangle x1="8.4963" y1="2.81508125" x2="8.52931875" y2="2.82371875" layer="94"/>
<rectangle x1="8.61568125" y1="2.81508125" x2="8.6487" y2="2.82371875" layer="94"/>
<rectangle x1="8.73251875" y1="2.81508125" x2="8.76808125" y2="2.82371875" layer="94"/>
<rectangle x1="8.83411875" y1="2.81508125" x2="8.86968125" y2="2.82371875" layer="94"/>
<rectangle x1="8.93571875" y1="2.81508125" x2="8.97128125" y2="2.82371875" layer="94"/>
<rectangle x1="9.03731875" y1="2.81508125" x2="9.07288125" y2="2.82371875" layer="94"/>
<rectangle x1="0.4191" y1="2.82371875" x2="0.45211875" y2="2.8321" layer="94"/>
<rectangle x1="0.51308125" y1="2.82371875" x2="0.5461" y2="2.8321" layer="94"/>
<rectangle x1="0.61468125" y1="2.82371875" x2="0.6477" y2="2.8321" layer="94"/>
<rectangle x1="0.7239" y1="2.82371875" x2="0.75691875" y2="2.8321" layer="94"/>
<rectangle x1="0.83311875" y1="2.82371875" x2="0.86868125" y2="2.8321" layer="94"/>
<rectangle x1="0.96011875" y1="2.82371875" x2="0.99568125" y2="2.8321" layer="94"/>
<rectangle x1="1.0795" y1="2.82371875" x2="1.11251875" y2="2.8321" layer="94"/>
<rectangle x1="1.1811" y1="2.82371875" x2="1.21411875" y2="2.8321" layer="94"/>
<rectangle x1="1.79831875" y1="2.82371875" x2="1.83388125" y2="2.8321" layer="94"/>
<rectangle x1="1.9685" y1="2.82371875" x2="2.00151875" y2="2.8321" layer="94"/>
<rectangle x1="2.7559" y1="2.82371875" x2="2.78891875" y2="2.8321" layer="94"/>
<rectangle x1="2.83971875" y1="2.82371875" x2="2.87528125" y2="2.8321" layer="94"/>
<rectangle x1="2.92608125" y1="2.82371875" x2="2.9591" y2="2.8321" layer="94"/>
<rectangle x1="3.65251875" y1="2.82371875" x2="3.68808125" y2="2.8321" layer="94"/>
<rectangle x1="3.7719" y1="2.82371875" x2="3.80491875" y2="2.8321" layer="94"/>
<rectangle x1="4.4069" y1="2.82371875" x2="4.4323" y2="2.8321" layer="94"/>
<rectangle x1="5.10031875" y1="2.82371875" x2="5.13588125" y2="2.8321" layer="94"/>
<rectangle x1="5.68451875" y1="2.82371875" x2="5.72008125" y2="2.8321" layer="94"/>
<rectangle x1="5.81151875" y1="2.82371875" x2="5.84708125" y2="2.8321" layer="94"/>
<rectangle x1="6.5405" y1="2.82371875" x2="6.57351875" y2="2.8321" layer="94"/>
<rectangle x1="6.6167" y1="2.82371875" x2="6.64971875" y2="2.8321" layer="94"/>
<rectangle x1="6.70051875" y1="2.82371875" x2="6.73608125" y2="2.8321" layer="94"/>
<rectangle x1="7.48791875" y1="2.82371875" x2="7.52348125" y2="2.8321" layer="94"/>
<rectangle x1="7.6581" y1="2.82371875" x2="7.69111875" y2="2.8321" layer="94"/>
<rectangle x1="8.2677" y1="2.82371875" x2="8.30071875" y2="2.8321" layer="94"/>
<rectangle x1="8.37691875" y1="2.82371875" x2="8.41248125" y2="2.8321" layer="94"/>
<rectangle x1="8.4963" y1="2.82371875" x2="8.52931875" y2="2.8321" layer="94"/>
<rectangle x1="8.61568125" y1="2.82371875" x2="8.6487" y2="2.8321" layer="94"/>
<rectangle x1="8.73251875" y1="2.82371875" x2="8.76808125" y2="2.8321" layer="94"/>
<rectangle x1="8.83411875" y1="2.82371875" x2="8.86968125" y2="2.8321" layer="94"/>
<rectangle x1="8.93571875" y1="2.82371875" x2="8.97128125" y2="2.8321" layer="94"/>
<rectangle x1="9.03731875" y1="2.82371875" x2="9.07288125" y2="2.8321" layer="94"/>
<rectangle x1="0.4191" y1="2.8321" x2="0.45211875" y2="2.84048125" layer="94"/>
<rectangle x1="0.51308125" y1="2.8321" x2="0.5461" y2="2.84048125" layer="94"/>
<rectangle x1="0.61468125" y1="2.8321" x2="0.6477" y2="2.84048125" layer="94"/>
<rectangle x1="0.7239" y1="2.8321" x2="0.75691875" y2="2.84048125" layer="94"/>
<rectangle x1="0.83311875" y1="2.8321" x2="0.86868125" y2="2.84048125" layer="94"/>
<rectangle x1="0.96011875" y1="2.8321" x2="0.99568125" y2="2.84048125" layer="94"/>
<rectangle x1="1.0795" y1="2.8321" x2="1.11251875" y2="2.84048125" layer="94"/>
<rectangle x1="1.1811" y1="2.8321" x2="1.21411875" y2="2.84048125" layer="94"/>
<rectangle x1="1.79831875" y1="2.8321" x2="1.83388125" y2="2.84048125" layer="94"/>
<rectangle x1="1.9685" y1="2.8321" x2="2.00151875" y2="2.84048125" layer="94"/>
<rectangle x1="2.7559" y1="2.8321" x2="2.78891875" y2="2.84048125" layer="94"/>
<rectangle x1="2.83971875" y1="2.8321" x2="2.87528125" y2="2.84048125" layer="94"/>
<rectangle x1="2.92608125" y1="2.8321" x2="2.9591" y2="2.84048125" layer="94"/>
<rectangle x1="3.65251875" y1="2.8321" x2="3.68808125" y2="2.84048125" layer="94"/>
<rectangle x1="3.7719" y1="2.8321" x2="3.80491875" y2="2.84048125" layer="94"/>
<rectangle x1="4.4069" y1="2.8321" x2="4.4323" y2="2.84048125" layer="94"/>
<rectangle x1="5.10031875" y1="2.8321" x2="5.13588125" y2="2.84048125" layer="94"/>
<rectangle x1="5.68451875" y1="2.8321" x2="5.72008125" y2="2.84048125" layer="94"/>
<rectangle x1="5.81151875" y1="2.8321" x2="5.84708125" y2="2.84048125" layer="94"/>
<rectangle x1="6.5405" y1="2.8321" x2="6.57351875" y2="2.84048125" layer="94"/>
<rectangle x1="6.6167" y1="2.8321" x2="6.64971875" y2="2.84048125" layer="94"/>
<rectangle x1="6.70051875" y1="2.8321" x2="6.73608125" y2="2.84048125" layer="94"/>
<rectangle x1="7.48791875" y1="2.8321" x2="7.52348125" y2="2.84048125" layer="94"/>
<rectangle x1="7.6581" y1="2.8321" x2="7.69111875" y2="2.84048125" layer="94"/>
<rectangle x1="8.2677" y1="2.8321" x2="8.30071875" y2="2.84048125" layer="94"/>
<rectangle x1="8.37691875" y1="2.8321" x2="8.41248125" y2="2.84048125" layer="94"/>
<rectangle x1="8.4963" y1="2.8321" x2="8.52931875" y2="2.84048125" layer="94"/>
<rectangle x1="8.61568125" y1="2.8321" x2="8.6487" y2="2.84048125" layer="94"/>
<rectangle x1="8.73251875" y1="2.8321" x2="8.76808125" y2="2.84048125" layer="94"/>
<rectangle x1="8.83411875" y1="2.8321" x2="8.86968125" y2="2.84048125" layer="94"/>
<rectangle x1="8.93571875" y1="2.8321" x2="8.97128125" y2="2.84048125" layer="94"/>
<rectangle x1="9.03731875" y1="2.8321" x2="9.07288125" y2="2.84048125" layer="94"/>
<rectangle x1="0.4191" y1="2.84048125" x2="0.45211875" y2="2.84911875" layer="94"/>
<rectangle x1="0.51308125" y1="2.84048125" x2="0.5461" y2="2.84911875" layer="94"/>
<rectangle x1="0.61468125" y1="2.84048125" x2="0.6477" y2="2.84911875" layer="94"/>
<rectangle x1="0.7239" y1="2.84048125" x2="0.75691875" y2="2.84911875" layer="94"/>
<rectangle x1="0.83311875" y1="2.84048125" x2="0.86868125" y2="2.84911875" layer="94"/>
<rectangle x1="0.96011875" y1="2.84048125" x2="0.99568125" y2="2.84911875" layer="94"/>
<rectangle x1="1.0795" y1="2.84048125" x2="1.11251875" y2="2.84911875" layer="94"/>
<rectangle x1="1.1811" y1="2.84048125" x2="1.21411875" y2="2.84911875" layer="94"/>
<rectangle x1="1.79831875" y1="2.84048125" x2="1.83388125" y2="2.84911875" layer="94"/>
<rectangle x1="1.9685" y1="2.84048125" x2="2.00151875" y2="2.84911875" layer="94"/>
<rectangle x1="2.7559" y1="2.84048125" x2="2.78891875" y2="2.84911875" layer="94"/>
<rectangle x1="2.83971875" y1="2.84048125" x2="2.87528125" y2="2.84911875" layer="94"/>
<rectangle x1="2.92608125" y1="2.84048125" x2="2.9591" y2="2.84911875" layer="94"/>
<rectangle x1="3.65251875" y1="2.84048125" x2="3.68808125" y2="2.84911875" layer="94"/>
<rectangle x1="3.7719" y1="2.84048125" x2="3.80491875" y2="2.84911875" layer="94"/>
<rectangle x1="4.4069" y1="2.84048125" x2="4.4323" y2="2.84911875" layer="94"/>
<rectangle x1="5.10031875" y1="2.84048125" x2="5.13588125" y2="2.84911875" layer="94"/>
<rectangle x1="5.68451875" y1="2.84048125" x2="5.72008125" y2="2.84911875" layer="94"/>
<rectangle x1="5.81151875" y1="2.84048125" x2="5.84708125" y2="2.84911875" layer="94"/>
<rectangle x1="6.5405" y1="2.84048125" x2="6.57351875" y2="2.84911875" layer="94"/>
<rectangle x1="6.6167" y1="2.84048125" x2="6.64971875" y2="2.84911875" layer="94"/>
<rectangle x1="6.70051875" y1="2.84048125" x2="6.73608125" y2="2.84911875" layer="94"/>
<rectangle x1="7.48791875" y1="2.84048125" x2="7.52348125" y2="2.84911875" layer="94"/>
<rectangle x1="7.6581" y1="2.84048125" x2="7.69111875" y2="2.84911875" layer="94"/>
<rectangle x1="8.2677" y1="2.84048125" x2="8.30071875" y2="2.84911875" layer="94"/>
<rectangle x1="8.37691875" y1="2.84048125" x2="8.41248125" y2="2.84911875" layer="94"/>
<rectangle x1="8.4963" y1="2.84048125" x2="8.52931875" y2="2.84911875" layer="94"/>
<rectangle x1="8.61568125" y1="2.84048125" x2="8.6487" y2="2.84911875" layer="94"/>
<rectangle x1="8.73251875" y1="2.84048125" x2="8.76808125" y2="2.84911875" layer="94"/>
<rectangle x1="8.83411875" y1="2.84048125" x2="8.86968125" y2="2.84911875" layer="94"/>
<rectangle x1="8.93571875" y1="2.84048125" x2="8.97128125" y2="2.84911875" layer="94"/>
<rectangle x1="9.03731875" y1="2.84048125" x2="9.07288125" y2="2.84911875" layer="94"/>
<rectangle x1="0.4191" y1="2.84911875" x2="0.45211875" y2="2.8575" layer="94"/>
<rectangle x1="0.51308125" y1="2.84911875" x2="0.5461" y2="2.8575" layer="94"/>
<rectangle x1="0.61468125" y1="2.84911875" x2="0.6477" y2="2.8575" layer="94"/>
<rectangle x1="0.7239" y1="2.84911875" x2="0.75691875" y2="2.8575" layer="94"/>
<rectangle x1="0.83311875" y1="2.84911875" x2="0.86868125" y2="2.8575" layer="94"/>
<rectangle x1="0.96011875" y1="2.84911875" x2="0.99568125" y2="2.8575" layer="94"/>
<rectangle x1="1.0795" y1="2.84911875" x2="1.11251875" y2="2.8575" layer="94"/>
<rectangle x1="1.1811" y1="2.84911875" x2="1.21411875" y2="2.8575" layer="94"/>
<rectangle x1="1.79831875" y1="2.84911875" x2="1.83388125" y2="2.8575" layer="94"/>
<rectangle x1="1.9685" y1="2.84911875" x2="2.00151875" y2="2.8575" layer="94"/>
<rectangle x1="2.7559" y1="2.84911875" x2="2.78891875" y2="2.8575" layer="94"/>
<rectangle x1="2.83971875" y1="2.84911875" x2="2.87528125" y2="2.8575" layer="94"/>
<rectangle x1="2.92608125" y1="2.84911875" x2="2.9591" y2="2.8575" layer="94"/>
<rectangle x1="3.65251875" y1="2.84911875" x2="3.68808125" y2="2.8575" layer="94"/>
<rectangle x1="3.7719" y1="2.84911875" x2="3.80491875" y2="2.8575" layer="94"/>
<rectangle x1="4.4069" y1="2.84911875" x2="4.4323" y2="2.8575" layer="94"/>
<rectangle x1="5.10031875" y1="2.84911875" x2="5.13588125" y2="2.8575" layer="94"/>
<rectangle x1="5.68451875" y1="2.84911875" x2="5.72008125" y2="2.8575" layer="94"/>
<rectangle x1="5.81151875" y1="2.84911875" x2="5.84708125" y2="2.8575" layer="94"/>
<rectangle x1="6.5405" y1="2.84911875" x2="6.57351875" y2="2.8575" layer="94"/>
<rectangle x1="6.6167" y1="2.84911875" x2="6.64971875" y2="2.8575" layer="94"/>
<rectangle x1="6.70051875" y1="2.84911875" x2="6.73608125" y2="2.8575" layer="94"/>
<rectangle x1="7.48791875" y1="2.84911875" x2="7.52348125" y2="2.8575" layer="94"/>
<rectangle x1="7.6581" y1="2.84911875" x2="7.69111875" y2="2.8575" layer="94"/>
<rectangle x1="8.2677" y1="2.84911875" x2="8.30071875" y2="2.8575" layer="94"/>
<rectangle x1="8.37691875" y1="2.84911875" x2="8.41248125" y2="2.8575" layer="94"/>
<rectangle x1="8.4963" y1="2.84911875" x2="8.52931875" y2="2.8575" layer="94"/>
<rectangle x1="8.61568125" y1="2.84911875" x2="8.6487" y2="2.8575" layer="94"/>
<rectangle x1="8.73251875" y1="2.84911875" x2="8.76808125" y2="2.8575" layer="94"/>
<rectangle x1="8.83411875" y1="2.84911875" x2="8.86968125" y2="2.8575" layer="94"/>
<rectangle x1="8.93571875" y1="2.84911875" x2="8.97128125" y2="2.8575" layer="94"/>
<rectangle x1="9.03731875" y1="2.84911875" x2="9.07288125" y2="2.8575" layer="94"/>
<rectangle x1="0.4191" y1="2.8575" x2="0.45211875" y2="2.86588125" layer="94"/>
<rectangle x1="0.51308125" y1="2.8575" x2="0.5461" y2="2.86588125" layer="94"/>
<rectangle x1="0.61468125" y1="2.8575" x2="0.6477" y2="2.86588125" layer="94"/>
<rectangle x1="0.7239" y1="2.8575" x2="0.75691875" y2="2.86588125" layer="94"/>
<rectangle x1="0.83311875" y1="2.8575" x2="0.86868125" y2="2.86588125" layer="94"/>
<rectangle x1="0.96011875" y1="2.8575" x2="0.99568125" y2="2.86588125" layer="94"/>
<rectangle x1="1.0795" y1="2.8575" x2="1.11251875" y2="2.86588125" layer="94"/>
<rectangle x1="1.1811" y1="2.8575" x2="1.21411875" y2="2.86588125" layer="94"/>
<rectangle x1="1.79831875" y1="2.8575" x2="1.83388125" y2="2.86588125" layer="94"/>
<rectangle x1="1.9685" y1="2.8575" x2="2.00151875" y2="2.86588125" layer="94"/>
<rectangle x1="2.7559" y1="2.8575" x2="2.78891875" y2="2.86588125" layer="94"/>
<rectangle x1="2.83971875" y1="2.8575" x2="2.87528125" y2="2.86588125" layer="94"/>
<rectangle x1="2.92608125" y1="2.8575" x2="2.9591" y2="2.86588125" layer="94"/>
<rectangle x1="3.65251875" y1="2.8575" x2="3.68808125" y2="2.86588125" layer="94"/>
<rectangle x1="3.7719" y1="2.8575" x2="3.80491875" y2="2.86588125" layer="94"/>
<rectangle x1="4.4069" y1="2.8575" x2="4.4323" y2="2.86588125" layer="94"/>
<rectangle x1="5.10031875" y1="2.8575" x2="5.13588125" y2="2.86588125" layer="94"/>
<rectangle x1="5.68451875" y1="2.8575" x2="5.72008125" y2="2.86588125" layer="94"/>
<rectangle x1="5.81151875" y1="2.8575" x2="5.84708125" y2="2.86588125" layer="94"/>
<rectangle x1="6.5405" y1="2.8575" x2="6.57351875" y2="2.86588125" layer="94"/>
<rectangle x1="6.6167" y1="2.8575" x2="6.64971875" y2="2.86588125" layer="94"/>
<rectangle x1="6.70051875" y1="2.8575" x2="6.73608125" y2="2.86588125" layer="94"/>
<rectangle x1="7.48791875" y1="2.8575" x2="7.52348125" y2="2.86588125" layer="94"/>
<rectangle x1="7.6581" y1="2.8575" x2="7.69111875" y2="2.86588125" layer="94"/>
<rectangle x1="8.2677" y1="2.8575" x2="8.30071875" y2="2.86588125" layer="94"/>
<rectangle x1="8.37691875" y1="2.8575" x2="8.41248125" y2="2.86588125" layer="94"/>
<rectangle x1="8.4963" y1="2.8575" x2="8.52931875" y2="2.86588125" layer="94"/>
<rectangle x1="8.61568125" y1="2.8575" x2="8.6487" y2="2.86588125" layer="94"/>
<rectangle x1="8.73251875" y1="2.8575" x2="8.76808125" y2="2.86588125" layer="94"/>
<rectangle x1="8.83411875" y1="2.8575" x2="8.86968125" y2="2.86588125" layer="94"/>
<rectangle x1="8.93571875" y1="2.8575" x2="8.97128125" y2="2.86588125" layer="94"/>
<rectangle x1="9.03731875" y1="2.8575" x2="9.07288125" y2="2.86588125" layer="94"/>
<rectangle x1="0.4191" y1="2.86588125" x2="0.45211875" y2="2.87451875" layer="94"/>
<rectangle x1="0.51308125" y1="2.86588125" x2="0.5461" y2="2.87451875" layer="94"/>
<rectangle x1="0.61468125" y1="2.86588125" x2="0.6477" y2="2.87451875" layer="94"/>
<rectangle x1="0.7239" y1="2.86588125" x2="0.75691875" y2="2.87451875" layer="94"/>
<rectangle x1="0.83311875" y1="2.86588125" x2="0.86868125" y2="2.87451875" layer="94"/>
<rectangle x1="0.96011875" y1="2.86588125" x2="0.99568125" y2="2.87451875" layer="94"/>
<rectangle x1="1.0795" y1="2.86588125" x2="1.11251875" y2="2.87451875" layer="94"/>
<rectangle x1="1.1811" y1="2.86588125" x2="1.21411875" y2="2.87451875" layer="94"/>
<rectangle x1="1.79831875" y1="2.86588125" x2="1.83388125" y2="2.87451875" layer="94"/>
<rectangle x1="1.9685" y1="2.86588125" x2="2.00151875" y2="2.87451875" layer="94"/>
<rectangle x1="2.7559" y1="2.86588125" x2="2.78891875" y2="2.87451875" layer="94"/>
<rectangle x1="2.83971875" y1="2.86588125" x2="2.87528125" y2="2.87451875" layer="94"/>
<rectangle x1="2.92608125" y1="2.86588125" x2="2.9591" y2="2.87451875" layer="94"/>
<rectangle x1="3.65251875" y1="2.86588125" x2="3.68808125" y2="2.87451875" layer="94"/>
<rectangle x1="3.7719" y1="2.86588125" x2="3.80491875" y2="2.87451875" layer="94"/>
<rectangle x1="4.4069" y1="2.86588125" x2="4.4323" y2="2.87451875" layer="94"/>
<rectangle x1="5.10031875" y1="2.86588125" x2="5.13588125" y2="2.87451875" layer="94"/>
<rectangle x1="5.68451875" y1="2.86588125" x2="5.72008125" y2="2.87451875" layer="94"/>
<rectangle x1="5.81151875" y1="2.86588125" x2="5.84708125" y2="2.87451875" layer="94"/>
<rectangle x1="6.5405" y1="2.86588125" x2="6.57351875" y2="2.87451875" layer="94"/>
<rectangle x1="6.6167" y1="2.86588125" x2="6.64971875" y2="2.87451875" layer="94"/>
<rectangle x1="6.70051875" y1="2.86588125" x2="6.73608125" y2="2.87451875" layer="94"/>
<rectangle x1="7.48791875" y1="2.86588125" x2="7.52348125" y2="2.87451875" layer="94"/>
<rectangle x1="7.6581" y1="2.86588125" x2="7.69111875" y2="2.87451875" layer="94"/>
<rectangle x1="8.2677" y1="2.86588125" x2="8.30071875" y2="2.87451875" layer="94"/>
<rectangle x1="8.37691875" y1="2.86588125" x2="8.41248125" y2="2.87451875" layer="94"/>
<rectangle x1="8.4963" y1="2.86588125" x2="8.52931875" y2="2.87451875" layer="94"/>
<rectangle x1="8.61568125" y1="2.86588125" x2="8.6487" y2="2.87451875" layer="94"/>
<rectangle x1="8.73251875" y1="2.86588125" x2="8.76808125" y2="2.87451875" layer="94"/>
<rectangle x1="8.83411875" y1="2.86588125" x2="8.86968125" y2="2.87451875" layer="94"/>
<rectangle x1="8.93571875" y1="2.86588125" x2="8.97128125" y2="2.87451875" layer="94"/>
<rectangle x1="9.03731875" y1="2.86588125" x2="9.07288125" y2="2.87451875" layer="94"/>
<rectangle x1="0.4191" y1="2.87451875" x2="0.45211875" y2="2.8829" layer="94"/>
<rectangle x1="0.51308125" y1="2.87451875" x2="0.5461" y2="2.8829" layer="94"/>
<rectangle x1="0.61468125" y1="2.87451875" x2="0.6477" y2="2.8829" layer="94"/>
<rectangle x1="0.7239" y1="2.87451875" x2="0.75691875" y2="2.8829" layer="94"/>
<rectangle x1="0.83311875" y1="2.87451875" x2="0.86868125" y2="2.8829" layer="94"/>
<rectangle x1="0.96011875" y1="2.87451875" x2="0.99568125" y2="2.8829" layer="94"/>
<rectangle x1="1.0795" y1="2.87451875" x2="1.11251875" y2="2.8829" layer="94"/>
<rectangle x1="1.1811" y1="2.87451875" x2="1.21411875" y2="2.8829" layer="94"/>
<rectangle x1="1.79831875" y1="2.87451875" x2="1.83388125" y2="2.8829" layer="94"/>
<rectangle x1="1.9685" y1="2.87451875" x2="2.00151875" y2="2.8829" layer="94"/>
<rectangle x1="2.7559" y1="2.87451875" x2="2.78891875" y2="2.8829" layer="94"/>
<rectangle x1="2.83971875" y1="2.87451875" x2="2.87528125" y2="2.8829" layer="94"/>
<rectangle x1="2.92608125" y1="2.87451875" x2="2.9591" y2="2.8829" layer="94"/>
<rectangle x1="3.65251875" y1="2.87451875" x2="3.68808125" y2="2.8829" layer="94"/>
<rectangle x1="3.7719" y1="2.87451875" x2="3.80491875" y2="2.8829" layer="94"/>
<rectangle x1="4.4069" y1="2.87451875" x2="4.4323" y2="2.8829" layer="94"/>
<rectangle x1="5.10031875" y1="2.87451875" x2="5.13588125" y2="2.8829" layer="94"/>
<rectangle x1="5.68451875" y1="2.87451875" x2="5.72008125" y2="2.8829" layer="94"/>
<rectangle x1="5.81151875" y1="2.87451875" x2="5.84708125" y2="2.8829" layer="94"/>
<rectangle x1="6.5405" y1="2.87451875" x2="6.57351875" y2="2.8829" layer="94"/>
<rectangle x1="6.6167" y1="2.87451875" x2="6.64971875" y2="2.8829" layer="94"/>
<rectangle x1="6.70051875" y1="2.87451875" x2="6.73608125" y2="2.8829" layer="94"/>
<rectangle x1="7.48791875" y1="2.87451875" x2="7.52348125" y2="2.8829" layer="94"/>
<rectangle x1="7.6581" y1="2.87451875" x2="7.69111875" y2="2.8829" layer="94"/>
<rectangle x1="8.2677" y1="2.87451875" x2="8.30071875" y2="2.8829" layer="94"/>
<rectangle x1="8.37691875" y1="2.87451875" x2="8.41248125" y2="2.8829" layer="94"/>
<rectangle x1="8.4963" y1="2.87451875" x2="8.52931875" y2="2.8829" layer="94"/>
<rectangle x1="8.61568125" y1="2.87451875" x2="8.6487" y2="2.8829" layer="94"/>
<rectangle x1="8.73251875" y1="2.87451875" x2="8.76808125" y2="2.8829" layer="94"/>
<rectangle x1="8.83411875" y1="2.87451875" x2="8.86968125" y2="2.8829" layer="94"/>
<rectangle x1="8.93571875" y1="2.87451875" x2="8.97128125" y2="2.8829" layer="94"/>
<rectangle x1="9.03731875" y1="2.87451875" x2="9.07288125" y2="2.8829" layer="94"/>
<rectangle x1="0.4191" y1="2.8829" x2="0.45211875" y2="2.89128125" layer="94"/>
<rectangle x1="0.51308125" y1="2.8829" x2="0.5461" y2="2.89128125" layer="94"/>
<rectangle x1="0.61468125" y1="2.8829" x2="0.6477" y2="2.89128125" layer="94"/>
<rectangle x1="0.7239" y1="2.8829" x2="0.75691875" y2="2.89128125" layer="94"/>
<rectangle x1="0.83311875" y1="2.8829" x2="0.86868125" y2="2.89128125" layer="94"/>
<rectangle x1="0.96011875" y1="2.8829" x2="0.99568125" y2="2.89128125" layer="94"/>
<rectangle x1="1.0795" y1="2.8829" x2="1.11251875" y2="2.89128125" layer="94"/>
<rectangle x1="1.1811" y1="2.8829" x2="1.21411875" y2="2.89128125" layer="94"/>
<rectangle x1="1.79831875" y1="2.8829" x2="1.83388125" y2="2.89128125" layer="94"/>
<rectangle x1="1.9685" y1="2.8829" x2="2.00151875" y2="2.89128125" layer="94"/>
<rectangle x1="2.7559" y1="2.8829" x2="2.78891875" y2="2.89128125" layer="94"/>
<rectangle x1="2.83971875" y1="2.8829" x2="2.87528125" y2="2.89128125" layer="94"/>
<rectangle x1="2.92608125" y1="2.8829" x2="2.9591" y2="2.89128125" layer="94"/>
<rectangle x1="3.65251875" y1="2.8829" x2="3.68808125" y2="2.89128125" layer="94"/>
<rectangle x1="3.7719" y1="2.8829" x2="3.80491875" y2="2.89128125" layer="94"/>
<rectangle x1="4.4069" y1="2.8829" x2="4.4323" y2="2.89128125" layer="94"/>
<rectangle x1="5.10031875" y1="2.8829" x2="5.13588125" y2="2.89128125" layer="94"/>
<rectangle x1="5.68451875" y1="2.8829" x2="5.72008125" y2="2.89128125" layer="94"/>
<rectangle x1="5.81151875" y1="2.8829" x2="5.84708125" y2="2.89128125" layer="94"/>
<rectangle x1="6.5405" y1="2.8829" x2="6.57351875" y2="2.89128125" layer="94"/>
<rectangle x1="6.6167" y1="2.8829" x2="6.64971875" y2="2.89128125" layer="94"/>
<rectangle x1="6.70051875" y1="2.8829" x2="6.73608125" y2="2.89128125" layer="94"/>
<rectangle x1="7.48791875" y1="2.8829" x2="7.52348125" y2="2.89128125" layer="94"/>
<rectangle x1="7.6581" y1="2.8829" x2="7.69111875" y2="2.89128125" layer="94"/>
<rectangle x1="8.2677" y1="2.8829" x2="8.30071875" y2="2.89128125" layer="94"/>
<rectangle x1="8.37691875" y1="2.8829" x2="8.41248125" y2="2.89128125" layer="94"/>
<rectangle x1="8.4963" y1="2.8829" x2="8.52931875" y2="2.89128125" layer="94"/>
<rectangle x1="8.61568125" y1="2.8829" x2="8.6487" y2="2.89128125" layer="94"/>
<rectangle x1="8.73251875" y1="2.8829" x2="8.76808125" y2="2.89128125" layer="94"/>
<rectangle x1="8.83411875" y1="2.8829" x2="8.86968125" y2="2.89128125" layer="94"/>
<rectangle x1="8.93571875" y1="2.8829" x2="8.97128125" y2="2.89128125" layer="94"/>
<rectangle x1="9.03731875" y1="2.8829" x2="9.07288125" y2="2.89128125" layer="94"/>
<rectangle x1="0.4191" y1="2.89128125" x2="0.45211875" y2="2.89991875" layer="94"/>
<rectangle x1="0.51308125" y1="2.89128125" x2="0.5461" y2="2.89991875" layer="94"/>
<rectangle x1="0.61468125" y1="2.89128125" x2="0.6477" y2="2.89991875" layer="94"/>
<rectangle x1="0.7239" y1="2.89128125" x2="0.75691875" y2="2.89991875" layer="94"/>
<rectangle x1="0.83311875" y1="2.89128125" x2="0.86868125" y2="2.89991875" layer="94"/>
<rectangle x1="0.96011875" y1="2.89128125" x2="0.99568125" y2="2.89991875" layer="94"/>
<rectangle x1="1.0795" y1="2.89128125" x2="1.11251875" y2="2.89991875" layer="94"/>
<rectangle x1="1.1811" y1="2.89128125" x2="1.21411875" y2="2.89991875" layer="94"/>
<rectangle x1="1.79831875" y1="2.89128125" x2="1.83388125" y2="2.89991875" layer="94"/>
<rectangle x1="1.9685" y1="2.89128125" x2="2.00151875" y2="2.89991875" layer="94"/>
<rectangle x1="2.7559" y1="2.89128125" x2="2.78891875" y2="2.89991875" layer="94"/>
<rectangle x1="2.83971875" y1="2.89128125" x2="2.87528125" y2="2.89991875" layer="94"/>
<rectangle x1="2.92608125" y1="2.89128125" x2="2.9591" y2="2.89991875" layer="94"/>
<rectangle x1="3.65251875" y1="2.89128125" x2="3.68808125" y2="2.89991875" layer="94"/>
<rectangle x1="3.7719" y1="2.89128125" x2="3.80491875" y2="2.89991875" layer="94"/>
<rectangle x1="4.4069" y1="2.89128125" x2="4.4323" y2="2.89991875" layer="94"/>
<rectangle x1="5.10031875" y1="2.89128125" x2="5.13588125" y2="2.89991875" layer="94"/>
<rectangle x1="5.68451875" y1="2.89128125" x2="5.72008125" y2="2.89991875" layer="94"/>
<rectangle x1="5.81151875" y1="2.89128125" x2="5.84708125" y2="2.89991875" layer="94"/>
<rectangle x1="6.5405" y1="2.89128125" x2="6.57351875" y2="2.89991875" layer="94"/>
<rectangle x1="6.6167" y1="2.89128125" x2="6.64971875" y2="2.89991875" layer="94"/>
<rectangle x1="6.70051875" y1="2.89128125" x2="6.73608125" y2="2.89991875" layer="94"/>
<rectangle x1="7.48791875" y1="2.89128125" x2="7.52348125" y2="2.89991875" layer="94"/>
<rectangle x1="7.6581" y1="2.89128125" x2="7.69111875" y2="2.89991875" layer="94"/>
<rectangle x1="8.2677" y1="2.89128125" x2="8.30071875" y2="2.89991875" layer="94"/>
<rectangle x1="8.37691875" y1="2.89128125" x2="8.41248125" y2="2.89991875" layer="94"/>
<rectangle x1="8.4963" y1="2.89128125" x2="8.52931875" y2="2.89991875" layer="94"/>
<rectangle x1="8.61568125" y1="2.89128125" x2="8.6487" y2="2.89991875" layer="94"/>
<rectangle x1="8.73251875" y1="2.89128125" x2="8.76808125" y2="2.89991875" layer="94"/>
<rectangle x1="8.83411875" y1="2.89128125" x2="8.86968125" y2="2.89991875" layer="94"/>
<rectangle x1="8.93571875" y1="2.89128125" x2="8.97128125" y2="2.89991875" layer="94"/>
<rectangle x1="9.03731875" y1="2.89128125" x2="9.07288125" y2="2.89991875" layer="94"/>
<rectangle x1="0.4191" y1="2.89991875" x2="0.45211875" y2="2.9083" layer="94"/>
<rectangle x1="0.51308125" y1="2.89991875" x2="0.5461" y2="2.9083" layer="94"/>
<rectangle x1="0.61468125" y1="2.89991875" x2="0.6477" y2="2.9083" layer="94"/>
<rectangle x1="0.7239" y1="2.89991875" x2="0.75691875" y2="2.9083" layer="94"/>
<rectangle x1="0.83311875" y1="2.89991875" x2="0.86868125" y2="2.9083" layer="94"/>
<rectangle x1="0.96011875" y1="2.89991875" x2="0.99568125" y2="2.9083" layer="94"/>
<rectangle x1="1.0795" y1="2.89991875" x2="1.11251875" y2="2.9083" layer="94"/>
<rectangle x1="1.1811" y1="2.89991875" x2="1.21411875" y2="2.9083" layer="94"/>
<rectangle x1="1.79831875" y1="2.89991875" x2="1.83388125" y2="2.9083" layer="94"/>
<rectangle x1="1.9685" y1="2.89991875" x2="2.00151875" y2="2.9083" layer="94"/>
<rectangle x1="2.7559" y1="2.89991875" x2="2.78891875" y2="2.9083" layer="94"/>
<rectangle x1="2.83971875" y1="2.89991875" x2="2.87528125" y2="2.9083" layer="94"/>
<rectangle x1="2.92608125" y1="2.89991875" x2="2.9591" y2="2.9083" layer="94"/>
<rectangle x1="3.65251875" y1="2.89991875" x2="3.68808125" y2="2.9083" layer="94"/>
<rectangle x1="3.7719" y1="2.89991875" x2="3.80491875" y2="2.9083" layer="94"/>
<rectangle x1="4.4069" y1="2.89991875" x2="4.4323" y2="2.9083" layer="94"/>
<rectangle x1="5.10031875" y1="2.89991875" x2="5.13588125" y2="2.9083" layer="94"/>
<rectangle x1="5.68451875" y1="2.89991875" x2="5.72008125" y2="2.9083" layer="94"/>
<rectangle x1="5.81151875" y1="2.89991875" x2="5.84708125" y2="2.9083" layer="94"/>
<rectangle x1="6.5405" y1="2.89991875" x2="6.57351875" y2="2.9083" layer="94"/>
<rectangle x1="6.6167" y1="2.89991875" x2="6.64971875" y2="2.9083" layer="94"/>
<rectangle x1="6.70051875" y1="2.89991875" x2="6.73608125" y2="2.9083" layer="94"/>
<rectangle x1="7.48791875" y1="2.89991875" x2="7.52348125" y2="2.9083" layer="94"/>
<rectangle x1="7.6581" y1="2.89991875" x2="7.69111875" y2="2.9083" layer="94"/>
<rectangle x1="8.2677" y1="2.89991875" x2="8.30071875" y2="2.9083" layer="94"/>
<rectangle x1="8.37691875" y1="2.89991875" x2="8.41248125" y2="2.9083" layer="94"/>
<rectangle x1="8.4963" y1="2.89991875" x2="8.52931875" y2="2.9083" layer="94"/>
<rectangle x1="8.61568125" y1="2.89991875" x2="8.6487" y2="2.9083" layer="94"/>
<rectangle x1="8.73251875" y1="2.89991875" x2="8.76808125" y2="2.9083" layer="94"/>
<rectangle x1="8.83411875" y1="2.89991875" x2="8.86968125" y2="2.9083" layer="94"/>
<rectangle x1="8.93571875" y1="2.89991875" x2="8.97128125" y2="2.9083" layer="94"/>
<rectangle x1="9.03731875" y1="2.89991875" x2="9.07288125" y2="2.9083" layer="94"/>
<rectangle x1="0.4191" y1="2.9083" x2="0.45211875" y2="2.91668125" layer="94"/>
<rectangle x1="0.51308125" y1="2.9083" x2="0.5461" y2="2.91668125" layer="94"/>
<rectangle x1="0.61468125" y1="2.9083" x2="0.6477" y2="2.91668125" layer="94"/>
<rectangle x1="0.7239" y1="2.9083" x2="0.75691875" y2="2.91668125" layer="94"/>
<rectangle x1="0.83311875" y1="2.9083" x2="0.86868125" y2="2.91668125" layer="94"/>
<rectangle x1="0.96011875" y1="2.9083" x2="0.99568125" y2="2.91668125" layer="94"/>
<rectangle x1="1.0795" y1="2.9083" x2="1.11251875" y2="2.91668125" layer="94"/>
<rectangle x1="1.1811" y1="2.9083" x2="1.21411875" y2="2.91668125" layer="94"/>
<rectangle x1="1.79831875" y1="2.9083" x2="1.83388125" y2="2.91668125" layer="94"/>
<rectangle x1="1.9685" y1="2.9083" x2="2.00151875" y2="2.91668125" layer="94"/>
<rectangle x1="2.7559" y1="2.9083" x2="2.78891875" y2="2.91668125" layer="94"/>
<rectangle x1="2.83971875" y1="2.9083" x2="2.87528125" y2="2.91668125" layer="94"/>
<rectangle x1="2.92608125" y1="2.9083" x2="2.9591" y2="2.91668125" layer="94"/>
<rectangle x1="3.65251875" y1="2.9083" x2="3.68808125" y2="2.91668125" layer="94"/>
<rectangle x1="3.7719" y1="2.9083" x2="3.80491875" y2="2.91668125" layer="94"/>
<rectangle x1="4.4069" y1="2.9083" x2="4.4323" y2="2.91668125" layer="94"/>
<rectangle x1="5.10031875" y1="2.9083" x2="5.13588125" y2="2.91668125" layer="94"/>
<rectangle x1="5.68451875" y1="2.9083" x2="5.72008125" y2="2.91668125" layer="94"/>
<rectangle x1="5.81151875" y1="2.9083" x2="5.84708125" y2="2.91668125" layer="94"/>
<rectangle x1="6.5405" y1="2.9083" x2="6.57351875" y2="2.91668125" layer="94"/>
<rectangle x1="6.6167" y1="2.9083" x2="6.64971875" y2="2.91668125" layer="94"/>
<rectangle x1="6.70051875" y1="2.9083" x2="6.73608125" y2="2.91668125" layer="94"/>
<rectangle x1="7.48791875" y1="2.9083" x2="7.52348125" y2="2.91668125" layer="94"/>
<rectangle x1="7.6581" y1="2.9083" x2="7.69111875" y2="2.91668125" layer="94"/>
<rectangle x1="8.2677" y1="2.9083" x2="8.30071875" y2="2.91668125" layer="94"/>
<rectangle x1="8.37691875" y1="2.9083" x2="8.41248125" y2="2.91668125" layer="94"/>
<rectangle x1="8.4963" y1="2.9083" x2="8.52931875" y2="2.91668125" layer="94"/>
<rectangle x1="8.61568125" y1="2.9083" x2="8.6487" y2="2.91668125" layer="94"/>
<rectangle x1="8.73251875" y1="2.9083" x2="8.76808125" y2="2.91668125" layer="94"/>
<rectangle x1="8.83411875" y1="2.9083" x2="8.86968125" y2="2.91668125" layer="94"/>
<rectangle x1="8.93571875" y1="2.9083" x2="8.97128125" y2="2.91668125" layer="94"/>
<rectangle x1="9.03731875" y1="2.9083" x2="9.07288125" y2="2.91668125" layer="94"/>
<rectangle x1="0.4191" y1="2.91668125" x2="0.45211875" y2="2.92531875" layer="94"/>
<rectangle x1="0.51308125" y1="2.91668125" x2="0.5461" y2="2.92531875" layer="94"/>
<rectangle x1="0.61468125" y1="2.91668125" x2="0.6477" y2="2.92531875" layer="94"/>
<rectangle x1="0.7239" y1="2.91668125" x2="0.75691875" y2="2.92531875" layer="94"/>
<rectangle x1="0.83311875" y1="2.91668125" x2="0.86868125" y2="2.92531875" layer="94"/>
<rectangle x1="0.96011875" y1="2.91668125" x2="0.99568125" y2="2.92531875" layer="94"/>
<rectangle x1="1.0795" y1="2.91668125" x2="1.11251875" y2="2.92531875" layer="94"/>
<rectangle x1="1.1811" y1="2.91668125" x2="1.21411875" y2="2.92531875" layer="94"/>
<rectangle x1="1.79831875" y1="2.91668125" x2="1.83388125" y2="2.92531875" layer="94"/>
<rectangle x1="1.9685" y1="2.91668125" x2="2.00151875" y2="2.92531875" layer="94"/>
<rectangle x1="2.7559" y1="2.91668125" x2="2.78891875" y2="2.92531875" layer="94"/>
<rectangle x1="2.83971875" y1="2.91668125" x2="2.87528125" y2="2.92531875" layer="94"/>
<rectangle x1="2.92608125" y1="2.91668125" x2="2.9591" y2="2.92531875" layer="94"/>
<rectangle x1="3.65251875" y1="2.91668125" x2="3.68808125" y2="2.92531875" layer="94"/>
<rectangle x1="3.7719" y1="2.91668125" x2="3.80491875" y2="2.92531875" layer="94"/>
<rectangle x1="4.4069" y1="2.91668125" x2="4.4323" y2="2.92531875" layer="94"/>
<rectangle x1="5.10031875" y1="2.91668125" x2="5.13588125" y2="2.92531875" layer="94"/>
<rectangle x1="5.68451875" y1="2.91668125" x2="5.72008125" y2="2.92531875" layer="94"/>
<rectangle x1="5.81151875" y1="2.91668125" x2="5.84708125" y2="2.92531875" layer="94"/>
<rectangle x1="6.5405" y1="2.91668125" x2="6.57351875" y2="2.92531875" layer="94"/>
<rectangle x1="6.6167" y1="2.91668125" x2="6.64971875" y2="2.92531875" layer="94"/>
<rectangle x1="6.70051875" y1="2.91668125" x2="6.73608125" y2="2.92531875" layer="94"/>
<rectangle x1="7.48791875" y1="2.91668125" x2="7.52348125" y2="2.92531875" layer="94"/>
<rectangle x1="7.6581" y1="2.91668125" x2="7.69111875" y2="2.92531875" layer="94"/>
<rectangle x1="8.2677" y1="2.91668125" x2="8.30071875" y2="2.92531875" layer="94"/>
<rectangle x1="8.37691875" y1="2.91668125" x2="8.41248125" y2="2.92531875" layer="94"/>
<rectangle x1="8.4963" y1="2.91668125" x2="8.52931875" y2="2.92531875" layer="94"/>
<rectangle x1="8.61568125" y1="2.91668125" x2="8.6487" y2="2.92531875" layer="94"/>
<rectangle x1="8.73251875" y1="2.91668125" x2="8.76808125" y2="2.92531875" layer="94"/>
<rectangle x1="8.83411875" y1="2.91668125" x2="8.86968125" y2="2.92531875" layer="94"/>
<rectangle x1="8.93571875" y1="2.91668125" x2="8.97128125" y2="2.92531875" layer="94"/>
<rectangle x1="9.03731875" y1="2.91668125" x2="9.07288125" y2="2.92531875" layer="94"/>
<rectangle x1="0.4191" y1="2.92531875" x2="0.45211875" y2="2.9337" layer="94"/>
<rectangle x1="0.51308125" y1="2.92531875" x2="0.5461" y2="2.9337" layer="94"/>
<rectangle x1="0.61468125" y1="2.92531875" x2="0.6477" y2="2.9337" layer="94"/>
<rectangle x1="0.7239" y1="2.92531875" x2="0.75691875" y2="2.9337" layer="94"/>
<rectangle x1="0.83311875" y1="2.92531875" x2="0.86868125" y2="2.9337" layer="94"/>
<rectangle x1="0.96011875" y1="2.92531875" x2="0.99568125" y2="2.9337" layer="94"/>
<rectangle x1="1.0795" y1="2.92531875" x2="1.11251875" y2="2.9337" layer="94"/>
<rectangle x1="1.1811" y1="2.92531875" x2="1.21411875" y2="2.9337" layer="94"/>
<rectangle x1="1.79831875" y1="2.92531875" x2="1.83388125" y2="2.9337" layer="94"/>
<rectangle x1="1.9685" y1="2.92531875" x2="2.00151875" y2="2.9337" layer="94"/>
<rectangle x1="2.7559" y1="2.92531875" x2="2.78891875" y2="2.9337" layer="94"/>
<rectangle x1="2.83971875" y1="2.92531875" x2="2.87528125" y2="2.9337" layer="94"/>
<rectangle x1="2.92608125" y1="2.92531875" x2="2.9591" y2="2.9337" layer="94"/>
<rectangle x1="3.65251875" y1="2.92531875" x2="3.68808125" y2="2.9337" layer="94"/>
<rectangle x1="3.7719" y1="2.92531875" x2="3.80491875" y2="2.9337" layer="94"/>
<rectangle x1="4.4069" y1="2.92531875" x2="4.4323" y2="2.9337" layer="94"/>
<rectangle x1="5.10031875" y1="2.92531875" x2="5.13588125" y2="2.9337" layer="94"/>
<rectangle x1="5.68451875" y1="2.92531875" x2="5.72008125" y2="2.9337" layer="94"/>
<rectangle x1="5.81151875" y1="2.92531875" x2="5.84708125" y2="2.9337" layer="94"/>
<rectangle x1="6.5405" y1="2.92531875" x2="6.57351875" y2="2.9337" layer="94"/>
<rectangle x1="6.6167" y1="2.92531875" x2="6.64971875" y2="2.9337" layer="94"/>
<rectangle x1="6.70051875" y1="2.92531875" x2="6.73608125" y2="2.9337" layer="94"/>
<rectangle x1="7.48791875" y1="2.92531875" x2="7.52348125" y2="2.9337" layer="94"/>
<rectangle x1="7.6581" y1="2.92531875" x2="7.69111875" y2="2.9337" layer="94"/>
<rectangle x1="8.2677" y1="2.92531875" x2="8.30071875" y2="2.9337" layer="94"/>
<rectangle x1="8.37691875" y1="2.92531875" x2="8.41248125" y2="2.9337" layer="94"/>
<rectangle x1="8.4963" y1="2.92531875" x2="8.52931875" y2="2.9337" layer="94"/>
<rectangle x1="8.61568125" y1="2.92531875" x2="8.6487" y2="2.9337" layer="94"/>
<rectangle x1="8.73251875" y1="2.92531875" x2="8.76808125" y2="2.9337" layer="94"/>
<rectangle x1="8.83411875" y1="2.92531875" x2="8.86968125" y2="2.9337" layer="94"/>
<rectangle x1="8.93571875" y1="2.92531875" x2="8.97128125" y2="2.9337" layer="94"/>
<rectangle x1="9.03731875" y1="2.92531875" x2="9.07288125" y2="2.9337" layer="94"/>
<rectangle x1="0.4191" y1="2.9337" x2="0.45211875" y2="2.94208125" layer="94"/>
<rectangle x1="0.51308125" y1="2.9337" x2="0.5461" y2="2.94208125" layer="94"/>
<rectangle x1="0.61468125" y1="2.9337" x2="0.6477" y2="2.94208125" layer="94"/>
<rectangle x1="0.7239" y1="2.9337" x2="0.75691875" y2="2.94208125" layer="94"/>
<rectangle x1="0.83311875" y1="2.9337" x2="0.86868125" y2="2.94208125" layer="94"/>
<rectangle x1="0.96011875" y1="2.9337" x2="0.99568125" y2="2.94208125" layer="94"/>
<rectangle x1="1.0795" y1="2.9337" x2="1.11251875" y2="2.94208125" layer="94"/>
<rectangle x1="1.1811" y1="2.9337" x2="1.21411875" y2="2.94208125" layer="94"/>
<rectangle x1="1.79831875" y1="2.9337" x2="1.83388125" y2="2.94208125" layer="94"/>
<rectangle x1="1.9685" y1="2.9337" x2="2.00151875" y2="2.94208125" layer="94"/>
<rectangle x1="2.7559" y1="2.9337" x2="2.78891875" y2="2.94208125" layer="94"/>
<rectangle x1="2.83971875" y1="2.9337" x2="2.87528125" y2="2.94208125" layer="94"/>
<rectangle x1="2.92608125" y1="2.9337" x2="2.9591" y2="2.94208125" layer="94"/>
<rectangle x1="3.65251875" y1="2.9337" x2="3.68808125" y2="2.94208125" layer="94"/>
<rectangle x1="3.7719" y1="2.9337" x2="3.80491875" y2="2.94208125" layer="94"/>
<rectangle x1="4.4069" y1="2.9337" x2="4.4323" y2="2.94208125" layer="94"/>
<rectangle x1="5.10031875" y1="2.9337" x2="5.13588125" y2="2.94208125" layer="94"/>
<rectangle x1="5.68451875" y1="2.9337" x2="5.72008125" y2="2.94208125" layer="94"/>
<rectangle x1="5.81151875" y1="2.9337" x2="5.84708125" y2="2.94208125" layer="94"/>
<rectangle x1="6.5405" y1="2.9337" x2="6.57351875" y2="2.94208125" layer="94"/>
<rectangle x1="6.6167" y1="2.9337" x2="6.64971875" y2="2.94208125" layer="94"/>
<rectangle x1="6.70051875" y1="2.9337" x2="6.73608125" y2="2.94208125" layer="94"/>
<rectangle x1="7.48791875" y1="2.9337" x2="7.52348125" y2="2.94208125" layer="94"/>
<rectangle x1="7.6581" y1="2.9337" x2="7.69111875" y2="2.94208125" layer="94"/>
<rectangle x1="8.2677" y1="2.9337" x2="8.30071875" y2="2.94208125" layer="94"/>
<rectangle x1="8.37691875" y1="2.9337" x2="8.41248125" y2="2.94208125" layer="94"/>
<rectangle x1="8.4963" y1="2.9337" x2="8.52931875" y2="2.94208125" layer="94"/>
<rectangle x1="8.61568125" y1="2.9337" x2="8.6487" y2="2.94208125" layer="94"/>
<rectangle x1="8.73251875" y1="2.9337" x2="8.76808125" y2="2.94208125" layer="94"/>
<rectangle x1="8.83411875" y1="2.9337" x2="8.86968125" y2="2.94208125" layer="94"/>
<rectangle x1="8.93571875" y1="2.9337" x2="8.97128125" y2="2.94208125" layer="94"/>
<rectangle x1="9.03731875" y1="2.9337" x2="9.07288125" y2="2.94208125" layer="94"/>
<rectangle x1="0.3937" y1="2.94208125" x2="1.24968125" y2="2.95071875" layer="94"/>
<rectangle x1="1.74751875" y1="2.94208125" x2="2.06248125" y2="2.95071875" layer="94"/>
<rectangle x1="2.73811875" y1="2.94208125" x2="2.97688125" y2="2.95071875" layer="94"/>
<rectangle x1="3.60171875" y1="2.94208125" x2="3.85571875" y2="2.95071875" layer="94"/>
<rectangle x1="4.4069" y1="2.94208125" x2="4.4323" y2="2.95071875" layer="94"/>
<rectangle x1="5.10031875" y1="2.94208125" x2="5.13588125" y2="2.95071875" layer="94"/>
<rectangle x1="5.64388125" y1="2.94208125" x2="5.89788125" y2="2.95071875" layer="94"/>
<rectangle x1="6.5151" y1="2.94208125" x2="6.76148125" y2="2.95071875" layer="94"/>
<rectangle x1="7.4295" y1="2.94208125" x2="7.74191875" y2="2.95071875" layer="94"/>
<rectangle x1="8.23468125" y1="2.94208125" x2="9.09828125" y2="2.95071875" layer="94"/>
<rectangle x1="0.3937" y1="2.95071875" x2="1.24968125" y2="2.9591" layer="94"/>
<rectangle x1="1.74751875" y1="2.95071875" x2="2.06248125" y2="2.9591" layer="94"/>
<rectangle x1="2.73811875" y1="2.95071875" x2="2.97688125" y2="2.9591" layer="94"/>
<rectangle x1="3.60171875" y1="2.95071875" x2="3.65251875" y2="2.9591" layer="94"/>
<rectangle x1="3.68808125" y1="2.95071875" x2="3.77951875" y2="2.9591" layer="94"/>
<rectangle x1="3.8227" y1="2.95071875" x2="3.85571875" y2="2.9591" layer="94"/>
<rectangle x1="4.4069" y1="2.95071875" x2="4.4323" y2="2.9591" layer="94"/>
<rectangle x1="5.10031875" y1="2.95071875" x2="5.13588125" y2="2.9591" layer="94"/>
<rectangle x1="5.64388125" y1="2.95071875" x2="5.69468125" y2="2.9591" layer="94"/>
<rectangle x1="5.72008125" y1="2.95071875" x2="5.81151875" y2="2.9591" layer="94"/>
<rectangle x1="5.86231875" y1="2.95071875" x2="5.89788125" y2="2.9591" layer="94"/>
<rectangle x1="6.5151" y1="2.95071875" x2="6.76148125" y2="2.9591" layer="94"/>
<rectangle x1="7.4295" y1="2.95071875" x2="7.74191875" y2="2.9591" layer="94"/>
<rectangle x1="8.23468125" y1="2.95071875" x2="9.09828125" y2="2.9591" layer="94"/>
<rectangle x1="0.3937" y1="2.9591" x2="1.24968125" y2="2.96748125" layer="94"/>
<rectangle x1="1.74751875" y1="2.9591" x2="2.06248125" y2="2.96748125" layer="94"/>
<rectangle x1="2.73811875" y1="2.9591" x2="2.97688125" y2="2.96748125" layer="94"/>
<rectangle x1="3.60171875" y1="2.9591" x2="3.65251875" y2="2.96748125" layer="94"/>
<rectangle x1="3.68808125" y1="2.9591" x2="3.7719" y2="2.96748125" layer="94"/>
<rectangle x1="3.8227" y1="2.9591" x2="3.85571875" y2="2.96748125" layer="94"/>
<rectangle x1="4.4069" y1="2.9591" x2="4.4323" y2="2.96748125" layer="94"/>
<rectangle x1="5.10031875" y1="2.9591" x2="5.13588125" y2="2.96748125" layer="94"/>
<rectangle x1="5.64388125" y1="2.9591" x2="5.69468125" y2="2.96748125" layer="94"/>
<rectangle x1="5.72008125" y1="2.9591" x2="5.81151875" y2="2.96748125" layer="94"/>
<rectangle x1="5.86231875" y1="2.9591" x2="5.89788125" y2="2.96748125" layer="94"/>
<rectangle x1="6.5151" y1="2.9591" x2="6.76148125" y2="2.96748125" layer="94"/>
<rectangle x1="7.4295" y1="2.9591" x2="7.74191875" y2="2.96748125" layer="94"/>
<rectangle x1="8.23468125" y1="2.9591" x2="9.09828125" y2="2.96748125" layer="94"/>
<rectangle x1="0.3937" y1="2.96748125" x2="1.24968125" y2="2.97611875" layer="94"/>
<rectangle x1="1.74751875" y1="2.96748125" x2="2.06248125" y2="2.97611875" layer="94"/>
<rectangle x1="2.73811875" y1="2.96748125" x2="2.97688125" y2="2.97611875" layer="94"/>
<rectangle x1="3.60171875" y1="2.96748125" x2="3.65251875" y2="2.97611875" layer="94"/>
<rectangle x1="3.68808125" y1="2.96748125" x2="3.76428125" y2="2.97611875" layer="94"/>
<rectangle x1="3.8227" y1="2.96748125" x2="3.85571875" y2="2.97611875" layer="94"/>
<rectangle x1="4.4069" y1="2.96748125" x2="4.4323" y2="2.97611875" layer="94"/>
<rectangle x1="5.10031875" y1="2.96748125" x2="5.13588125" y2="2.97611875" layer="94"/>
<rectangle x1="5.64388125" y1="2.96748125" x2="5.69468125" y2="2.97611875" layer="94"/>
<rectangle x1="5.72008125" y1="2.96748125" x2="5.8039" y2="2.97611875" layer="94"/>
<rectangle x1="5.86231875" y1="2.96748125" x2="5.89788125" y2="2.97611875" layer="94"/>
<rectangle x1="6.5151" y1="2.96748125" x2="6.76148125" y2="2.97611875" layer="94"/>
<rectangle x1="7.4295" y1="2.96748125" x2="7.74191875" y2="2.97611875" layer="94"/>
<rectangle x1="8.23468125" y1="2.96748125" x2="9.09828125" y2="2.97611875" layer="94"/>
<rectangle x1="0.3937" y1="2.97611875" x2="1.24968125" y2="2.9845" layer="94"/>
<rectangle x1="1.74751875" y1="2.97611875" x2="2.06248125" y2="2.9845" layer="94"/>
<rectangle x1="2.73811875" y1="2.97611875" x2="2.97688125" y2="2.9845" layer="94"/>
<rectangle x1="3.60171875" y1="2.97611875" x2="3.65251875" y2="2.9845" layer="94"/>
<rectangle x1="3.68808125" y1="2.97611875" x2="3.75411875" y2="2.9845" layer="94"/>
<rectangle x1="3.8227" y1="2.97611875" x2="3.85571875" y2="2.9845" layer="94"/>
<rectangle x1="4.4069" y1="2.97611875" x2="4.4323" y2="2.9845" layer="94"/>
<rectangle x1="4.99871875" y1="2.97611875" x2="5.00888125" y2="2.9845" layer="94"/>
<rectangle x1="5.10031875" y1="2.97611875" x2="5.13588125" y2="2.9845" layer="94"/>
<rectangle x1="5.64388125" y1="2.97611875" x2="5.69468125" y2="2.9845" layer="94"/>
<rectangle x1="5.72008125" y1="2.97611875" x2="5.78611875" y2="2.9845" layer="94"/>
<rectangle x1="5.86231875" y1="2.97611875" x2="5.89788125" y2="2.9845" layer="94"/>
<rectangle x1="6.5151" y1="2.97611875" x2="6.76148125" y2="2.9845" layer="94"/>
<rectangle x1="7.4295" y1="2.97611875" x2="7.74191875" y2="2.9845" layer="94"/>
<rectangle x1="8.23468125" y1="2.97611875" x2="9.09828125" y2="2.9845" layer="94"/>
<rectangle x1="0.3937" y1="2.9845" x2="1.24968125" y2="2.99288125" layer="94"/>
<rectangle x1="1.74751875" y1="2.9845" x2="2.06248125" y2="2.99288125" layer="94"/>
<rectangle x1="2.73811875" y1="2.9845" x2="2.97688125" y2="2.99288125" layer="94"/>
<rectangle x1="3.60171875" y1="2.9845" x2="3.65251875" y2="2.99288125" layer="94"/>
<rectangle x1="3.68808125" y1="2.9845" x2="3.7465" y2="2.99288125" layer="94"/>
<rectangle x1="3.8227" y1="2.9845" x2="3.85571875" y2="2.99288125" layer="94"/>
<rectangle x1="4.4069" y1="2.9845" x2="4.4323" y2="2.99288125" layer="94"/>
<rectangle x1="5.10031875" y1="2.9845" x2="5.13588125" y2="2.99288125" layer="94"/>
<rectangle x1="5.64388125" y1="2.9845" x2="5.69468125" y2="2.99288125" layer="94"/>
<rectangle x1="5.72008125" y1="2.9845" x2="5.78611875" y2="2.99288125" layer="94"/>
<rectangle x1="5.86231875" y1="2.9845" x2="5.89788125" y2="2.99288125" layer="94"/>
<rectangle x1="6.5151" y1="2.9845" x2="6.76148125" y2="2.99288125" layer="94"/>
<rectangle x1="7.4295" y1="2.9845" x2="7.74191875" y2="2.99288125" layer="94"/>
<rectangle x1="8.23468125" y1="2.9845" x2="9.09828125" y2="2.99288125" layer="94"/>
<rectangle x1="0.3937" y1="2.99288125" x2="1.24968125" y2="3.00151875" layer="94"/>
<rectangle x1="1.74751875" y1="2.99288125" x2="2.06248125" y2="3.00151875" layer="94"/>
<rectangle x1="2.73811875" y1="2.99288125" x2="2.97688125" y2="3.00151875" layer="94"/>
<rectangle x1="3.60171875" y1="2.99288125" x2="3.65251875" y2="3.00151875" layer="94"/>
<rectangle x1="3.68808125" y1="2.99288125" x2="3.7465" y2="3.00151875" layer="94"/>
<rectangle x1="3.8227" y1="2.99288125" x2="3.85571875" y2="3.00151875" layer="94"/>
<rectangle x1="4.4069" y1="2.99288125" x2="4.4323" y2="3.00151875" layer="94"/>
<rectangle x1="5.10031875" y1="2.99288125" x2="5.13588125" y2="3.00151875" layer="94"/>
<rectangle x1="5.64388125" y1="2.99288125" x2="5.69468125" y2="3.00151875" layer="94"/>
<rectangle x1="5.72008125" y1="2.99288125" x2="5.78611875" y2="3.00151875" layer="94"/>
<rectangle x1="5.86231875" y1="2.99288125" x2="5.89788125" y2="3.00151875" layer="94"/>
<rectangle x1="6.5151" y1="2.99288125" x2="6.76148125" y2="3.00151875" layer="94"/>
<rectangle x1="7.4295" y1="2.99288125" x2="7.74191875" y2="3.00151875" layer="94"/>
<rectangle x1="8.23468125" y1="2.99288125" x2="9.09828125" y2="3.00151875" layer="94"/>
<rectangle x1="0.3937" y1="3.00151875" x2="1.24968125" y2="3.0099" layer="94"/>
<rectangle x1="1.74751875" y1="3.00151875" x2="2.06248125" y2="3.0099" layer="94"/>
<rectangle x1="2.73811875" y1="3.00151875" x2="2.97688125" y2="3.0099" layer="94"/>
<rectangle x1="3.60171875" y1="3.00151875" x2="3.65251875" y2="3.0099" layer="94"/>
<rectangle x1="3.68808125" y1="3.00151875" x2="3.7465" y2="3.0099" layer="94"/>
<rectangle x1="3.8227" y1="3.00151875" x2="3.85571875" y2="3.0099" layer="94"/>
<rectangle x1="4.4069" y1="3.00151875" x2="4.4323" y2="3.0099" layer="94"/>
<rectangle x1="5.0165" y1="3.00151875" x2="5.02411875" y2="3.0099" layer="94"/>
<rectangle x1="5.10031875" y1="3.00151875" x2="5.13588125" y2="3.0099" layer="94"/>
<rectangle x1="5.64388125" y1="3.00151875" x2="5.69468125" y2="3.0099" layer="94"/>
<rectangle x1="5.72008125" y1="3.00151875" x2="5.78611875" y2="3.0099" layer="94"/>
<rectangle x1="5.86231875" y1="3.00151875" x2="5.89788125" y2="3.0099" layer="94"/>
<rectangle x1="6.5151" y1="3.00151875" x2="6.76148125" y2="3.0099" layer="94"/>
<rectangle x1="7.4295" y1="3.00151875" x2="7.74191875" y2="3.0099" layer="94"/>
<rectangle x1="8.23468125" y1="3.00151875" x2="9.09828125" y2="3.0099" layer="94"/>
<rectangle x1="0.3937" y1="3.0099" x2="1.24968125" y2="3.01828125" layer="94"/>
<rectangle x1="1.74751875" y1="3.0099" x2="2.06248125" y2="3.01828125" layer="94"/>
<rectangle x1="2.73811875" y1="3.0099" x2="2.97688125" y2="3.01828125" layer="94"/>
<rectangle x1="3.60171875" y1="3.0099" x2="3.65251875" y2="3.01828125" layer="94"/>
<rectangle x1="3.68808125" y1="3.0099" x2="3.7465" y2="3.01828125" layer="94"/>
<rectangle x1="3.8227" y1="3.0099" x2="3.85571875" y2="3.01828125" layer="94"/>
<rectangle x1="4.4069" y1="3.0099" x2="4.4323" y2="3.01828125" layer="94"/>
<rectangle x1="4.4831" y1="3.0099" x2="4.54151875" y2="3.01828125" layer="94"/>
<rectangle x1="4.5593" y1="3.0099" x2="4.6609" y2="3.01828125" layer="94"/>
<rectangle x1="4.67868125" y1="3.0099" x2="4.70408125" y2="3.01828125" layer="94"/>
<rectangle x1="4.72948125" y1="3.0099" x2="4.99871875" y2="3.01828125" layer="94"/>
<rectangle x1="5.0165" y1="3.0099" x2="5.05968125" y2="3.01828125" layer="94"/>
<rectangle x1="5.10031875" y1="3.0099" x2="5.13588125" y2="3.01828125" layer="94"/>
<rectangle x1="5.64388125" y1="3.0099" x2="5.69468125" y2="3.01828125" layer="94"/>
<rectangle x1="5.72008125" y1="3.0099" x2="5.78611875" y2="3.01828125" layer="94"/>
<rectangle x1="5.86231875" y1="3.0099" x2="5.89788125" y2="3.01828125" layer="94"/>
<rectangle x1="6.5151" y1="3.0099" x2="6.76148125" y2="3.01828125" layer="94"/>
<rectangle x1="7.4295" y1="3.0099" x2="7.74191875" y2="3.01828125" layer="94"/>
<rectangle x1="8.23468125" y1="3.0099" x2="9.09828125" y2="3.01828125" layer="94"/>
<rectangle x1="0.3937" y1="3.01828125" x2="1.24968125" y2="3.02691875" layer="94"/>
<rectangle x1="1.74751875" y1="3.01828125" x2="2.06248125" y2="3.02691875" layer="94"/>
<rectangle x1="2.73811875" y1="3.01828125" x2="2.97688125" y2="3.02691875" layer="94"/>
<rectangle x1="3.60171875" y1="3.01828125" x2="3.65251875" y2="3.02691875" layer="94"/>
<rectangle x1="3.68808125" y1="3.01828125" x2="3.7465" y2="3.02691875" layer="94"/>
<rectangle x1="3.8227" y1="3.01828125" x2="3.85571875" y2="3.02691875" layer="94"/>
<rectangle x1="4.4069" y1="3.01828125" x2="4.4323" y2="3.02691875" layer="94"/>
<rectangle x1="4.4831" y1="3.01828125" x2="4.54151875" y2="3.02691875" layer="94"/>
<rectangle x1="4.5593" y1="3.01828125" x2="4.6609" y2="3.02691875" layer="94"/>
<rectangle x1="4.67868125" y1="3.01828125" x2="4.70408125" y2="3.02691875" layer="94"/>
<rectangle x1="4.72948125" y1="3.01828125" x2="4.99871875" y2="3.02691875" layer="94"/>
<rectangle x1="5.0165" y1="3.01828125" x2="5.05968125" y2="3.02691875" layer="94"/>
<rectangle x1="5.10031875" y1="3.01828125" x2="5.13588125" y2="3.02691875" layer="94"/>
<rectangle x1="5.64388125" y1="3.01828125" x2="5.69468125" y2="3.02691875" layer="94"/>
<rectangle x1="5.72008125" y1="3.01828125" x2="5.78611875" y2="3.02691875" layer="94"/>
<rectangle x1="5.86231875" y1="3.01828125" x2="5.89788125" y2="3.02691875" layer="94"/>
<rectangle x1="6.5151" y1="3.01828125" x2="6.76148125" y2="3.02691875" layer="94"/>
<rectangle x1="7.4295" y1="3.01828125" x2="7.74191875" y2="3.02691875" layer="94"/>
<rectangle x1="8.23468125" y1="3.01828125" x2="9.09828125" y2="3.02691875" layer="94"/>
<rectangle x1="0.3937" y1="3.02691875" x2="1.24968125" y2="3.0353" layer="94"/>
<rectangle x1="1.74751875" y1="3.02691875" x2="2.06248125" y2="3.0353" layer="94"/>
<rectangle x1="2.73811875" y1="3.02691875" x2="2.97688125" y2="3.0353" layer="94"/>
<rectangle x1="3.60171875" y1="3.02691875" x2="3.65251875" y2="3.0353" layer="94"/>
<rectangle x1="3.68808125" y1="3.02691875" x2="3.7465" y2="3.0353" layer="94"/>
<rectangle x1="3.8227" y1="3.02691875" x2="3.85571875" y2="3.0353" layer="94"/>
<rectangle x1="4.4069" y1="3.02691875" x2="4.4323" y2="3.0353" layer="94"/>
<rectangle x1="4.4831" y1="3.02691875" x2="4.54151875" y2="3.0353" layer="94"/>
<rectangle x1="4.5593" y1="3.02691875" x2="4.6609" y2="3.0353" layer="94"/>
<rectangle x1="4.67868125" y1="3.02691875" x2="4.70408125" y2="3.0353" layer="94"/>
<rectangle x1="4.72948125" y1="3.02691875" x2="4.99871875" y2="3.0353" layer="94"/>
<rectangle x1="5.0165" y1="3.02691875" x2="5.05968125" y2="3.0353" layer="94"/>
<rectangle x1="5.10031875" y1="3.02691875" x2="5.13588125" y2="3.0353" layer="94"/>
<rectangle x1="5.64388125" y1="3.02691875" x2="5.69468125" y2="3.0353" layer="94"/>
<rectangle x1="5.72008125" y1="3.02691875" x2="5.78611875" y2="3.0353" layer="94"/>
<rectangle x1="5.86231875" y1="3.02691875" x2="5.89788125" y2="3.0353" layer="94"/>
<rectangle x1="6.5151" y1="3.02691875" x2="6.76148125" y2="3.0353" layer="94"/>
<rectangle x1="7.4295" y1="3.02691875" x2="7.74191875" y2="3.0353" layer="94"/>
<rectangle x1="8.23468125" y1="3.02691875" x2="9.09828125" y2="3.0353" layer="94"/>
<rectangle x1="0.3937" y1="3.0353" x2="1.24968125" y2="3.04368125" layer="94"/>
<rectangle x1="1.74751875" y1="3.0353" x2="2.06248125" y2="3.04368125" layer="94"/>
<rectangle x1="2.73811875" y1="3.0353" x2="2.97688125" y2="3.04368125" layer="94"/>
<rectangle x1="3.60171875" y1="3.0353" x2="3.65251875" y2="3.04368125" layer="94"/>
<rectangle x1="3.68808125" y1="3.0353" x2="3.7465" y2="3.04368125" layer="94"/>
<rectangle x1="3.8227" y1="3.0353" x2="3.85571875" y2="3.04368125" layer="94"/>
<rectangle x1="4.4069" y1="3.0353" x2="4.54151875" y2="3.04368125" layer="94"/>
<rectangle x1="4.5593" y1="3.0353" x2="4.6609" y2="3.04368125" layer="94"/>
<rectangle x1="4.67868125" y1="3.0353" x2="4.70408125" y2="3.04368125" layer="94"/>
<rectangle x1="4.72948125" y1="3.0353" x2="4.99871875" y2="3.04368125" layer="94"/>
<rectangle x1="5.0165" y1="3.0353" x2="5.13588125" y2="3.04368125" layer="94"/>
<rectangle x1="5.64388125" y1="3.0353" x2="5.69468125" y2="3.04368125" layer="94"/>
<rectangle x1="5.72008125" y1="3.0353" x2="5.78611875" y2="3.04368125" layer="94"/>
<rectangle x1="5.86231875" y1="3.0353" x2="5.89788125" y2="3.04368125" layer="94"/>
<rectangle x1="6.5151" y1="3.0353" x2="6.76148125" y2="3.04368125" layer="94"/>
<rectangle x1="7.4295" y1="3.0353" x2="7.74191875" y2="3.04368125" layer="94"/>
<rectangle x1="8.23468125" y1="3.0353" x2="9.09828125" y2="3.04368125" layer="94"/>
<rectangle x1="0.3937" y1="3.04368125" x2="1.24968125" y2="3.05231875" layer="94"/>
<rectangle x1="1.74751875" y1="3.04368125" x2="2.06248125" y2="3.05231875" layer="94"/>
<rectangle x1="2.73811875" y1="3.04368125" x2="2.97688125" y2="3.05231875" layer="94"/>
<rectangle x1="3.60171875" y1="3.04368125" x2="3.65251875" y2="3.05231875" layer="94"/>
<rectangle x1="3.68808125" y1="3.04368125" x2="3.7465" y2="3.05231875" layer="94"/>
<rectangle x1="3.8227" y1="3.04368125" x2="3.85571875" y2="3.05231875" layer="94"/>
<rectangle x1="4.4069" y1="3.04368125" x2="4.54151875" y2="3.05231875" layer="94"/>
<rectangle x1="4.5593" y1="3.04368125" x2="4.6609" y2="3.05231875" layer="94"/>
<rectangle x1="4.67868125" y1="3.04368125" x2="4.70408125" y2="3.05231875" layer="94"/>
<rectangle x1="4.72948125" y1="3.04368125" x2="4.99871875" y2="3.05231875" layer="94"/>
<rectangle x1="5.0165" y1="3.04368125" x2="5.13588125" y2="3.05231875" layer="94"/>
<rectangle x1="5.64388125" y1="3.04368125" x2="5.69468125" y2="3.05231875" layer="94"/>
<rectangle x1="5.72008125" y1="3.04368125" x2="5.78611875" y2="3.05231875" layer="94"/>
<rectangle x1="5.86231875" y1="3.04368125" x2="5.89788125" y2="3.05231875" layer="94"/>
<rectangle x1="6.5151" y1="3.04368125" x2="6.76148125" y2="3.05231875" layer="94"/>
<rectangle x1="7.4295" y1="3.04368125" x2="7.74191875" y2="3.05231875" layer="94"/>
<rectangle x1="8.23468125" y1="3.04368125" x2="9.09828125" y2="3.05231875" layer="94"/>
<rectangle x1="0.3937" y1="3.05231875" x2="1.24968125" y2="3.0607" layer="94"/>
<rectangle x1="1.74751875" y1="3.05231875" x2="2.06248125" y2="3.0607" layer="94"/>
<rectangle x1="2.73811875" y1="3.05231875" x2="2.97688125" y2="3.0607" layer="94"/>
<rectangle x1="3.60171875" y1="3.05231875" x2="3.65251875" y2="3.0607" layer="94"/>
<rectangle x1="3.68808125" y1="3.05231875" x2="3.7465" y2="3.0607" layer="94"/>
<rectangle x1="3.8227" y1="3.05231875" x2="3.85571875" y2="3.0607" layer="94"/>
<rectangle x1="4.4069" y1="3.05231875" x2="4.54151875" y2="3.0607" layer="94"/>
<rectangle x1="4.5593" y1="3.05231875" x2="4.6609" y2="3.0607" layer="94"/>
<rectangle x1="4.67868125" y1="3.05231875" x2="4.70408125" y2="3.0607" layer="94"/>
<rectangle x1="4.72948125" y1="3.05231875" x2="4.99871875" y2="3.0607" layer="94"/>
<rectangle x1="5.0165" y1="3.05231875" x2="5.13588125" y2="3.0607" layer="94"/>
<rectangle x1="5.64388125" y1="3.05231875" x2="5.69468125" y2="3.0607" layer="94"/>
<rectangle x1="5.72008125" y1="3.05231875" x2="5.78611875" y2="3.0607" layer="94"/>
<rectangle x1="5.86231875" y1="3.05231875" x2="5.89788125" y2="3.0607" layer="94"/>
<rectangle x1="6.5151" y1="3.05231875" x2="6.76148125" y2="3.0607" layer="94"/>
<rectangle x1="7.4295" y1="3.05231875" x2="7.74191875" y2="3.0607" layer="94"/>
<rectangle x1="8.23468125" y1="3.05231875" x2="9.09828125" y2="3.0607" layer="94"/>
<rectangle x1="0.3937" y1="3.0607" x2="1.24968125" y2="3.06908125" layer="94"/>
<rectangle x1="1.74751875" y1="3.0607" x2="2.06248125" y2="3.06908125" layer="94"/>
<rectangle x1="2.73811875" y1="3.0607" x2="2.97688125" y2="3.06908125" layer="94"/>
<rectangle x1="3.60171875" y1="3.0607" x2="3.65251875" y2="3.06908125" layer="94"/>
<rectangle x1="3.68808125" y1="3.0607" x2="3.7465" y2="3.06908125" layer="94"/>
<rectangle x1="3.8227" y1="3.0607" x2="3.85571875" y2="3.06908125" layer="94"/>
<rectangle x1="4.4069" y1="3.0607" x2="4.54151875" y2="3.06908125" layer="94"/>
<rectangle x1="4.5593" y1="3.0607" x2="4.6609" y2="3.06908125" layer="94"/>
<rectangle x1="4.67868125" y1="3.0607" x2="4.70408125" y2="3.06908125" layer="94"/>
<rectangle x1="4.72948125" y1="3.0607" x2="4.99871875" y2="3.06908125" layer="94"/>
<rectangle x1="5.0165" y1="3.0607" x2="5.13588125" y2="3.06908125" layer="94"/>
<rectangle x1="5.64388125" y1="3.0607" x2="5.69468125" y2="3.06908125" layer="94"/>
<rectangle x1="5.72008125" y1="3.0607" x2="5.78611875" y2="3.06908125" layer="94"/>
<rectangle x1="5.86231875" y1="3.0607" x2="5.89788125" y2="3.06908125" layer="94"/>
<rectangle x1="6.5151" y1="3.0607" x2="6.76148125" y2="3.06908125" layer="94"/>
<rectangle x1="7.4295" y1="3.0607" x2="7.74191875" y2="3.06908125" layer="94"/>
<rectangle x1="8.23468125" y1="3.0607" x2="9.09828125" y2="3.06908125" layer="94"/>
<rectangle x1="0.3937" y1="3.06908125" x2="1.24968125" y2="3.07771875" layer="94"/>
<rectangle x1="1.74751875" y1="3.06908125" x2="2.06248125" y2="3.07771875" layer="94"/>
<rectangle x1="2.73811875" y1="3.06908125" x2="2.97688125" y2="3.07771875" layer="94"/>
<rectangle x1="3.60171875" y1="3.06908125" x2="3.65251875" y2="3.07771875" layer="94"/>
<rectangle x1="3.68808125" y1="3.06908125" x2="3.75411875" y2="3.07771875" layer="94"/>
<rectangle x1="3.8227" y1="3.06908125" x2="3.85571875" y2="3.07771875" layer="94"/>
<rectangle x1="4.4069" y1="3.06908125" x2="4.54151875" y2="3.07771875" layer="94"/>
<rectangle x1="4.5593" y1="3.06908125" x2="4.6609" y2="3.07771875" layer="94"/>
<rectangle x1="4.67868125" y1="3.06908125" x2="4.70408125" y2="3.07771875" layer="94"/>
<rectangle x1="4.72948125" y1="3.06908125" x2="4.99871875" y2="3.07771875" layer="94"/>
<rectangle x1="5.0165" y1="3.06908125" x2="5.13588125" y2="3.07771875" layer="94"/>
<rectangle x1="5.64388125" y1="3.06908125" x2="5.69468125" y2="3.07771875" layer="94"/>
<rectangle x1="5.72008125" y1="3.06908125" x2="5.79628125" y2="3.07771875" layer="94"/>
<rectangle x1="5.86231875" y1="3.06908125" x2="5.89788125" y2="3.07771875" layer="94"/>
<rectangle x1="6.5151" y1="3.06908125" x2="6.76148125" y2="3.07771875" layer="94"/>
<rectangle x1="7.4295" y1="3.06908125" x2="7.74191875" y2="3.07771875" layer="94"/>
<rectangle x1="8.23468125" y1="3.06908125" x2="9.09828125" y2="3.07771875" layer="94"/>
<rectangle x1="1.74751875" y1="3.07771875" x2="2.06248125" y2="3.0861" layer="94"/>
<rectangle x1="2.73811875" y1="3.07771875" x2="2.97688125" y2="3.0861" layer="94"/>
<rectangle x1="3.60171875" y1="3.07771875" x2="3.65251875" y2="3.0861" layer="94"/>
<rectangle x1="3.71348125" y1="3.07771875" x2="3.76428125" y2="3.0861" layer="94"/>
<rectangle x1="3.8227" y1="3.07771875" x2="3.85571875" y2="3.0861" layer="94"/>
<rectangle x1="4.4831" y1="3.07771875" x2="4.54151875" y2="3.0861" layer="94"/>
<rectangle x1="4.5593" y1="3.07771875" x2="4.6609" y2="3.0861" layer="94"/>
<rectangle x1="4.67868125" y1="3.07771875" x2="4.70408125" y2="3.0861" layer="94"/>
<rectangle x1="4.72948125" y1="3.07771875" x2="4.99871875" y2="3.0861" layer="94"/>
<rectangle x1="5.0165" y1="3.07771875" x2="5.05968125" y2="3.0861" layer="94"/>
<rectangle x1="5.64388125" y1="3.07771875" x2="5.69468125" y2="3.0861" layer="94"/>
<rectangle x1="5.74548125" y1="3.07771875" x2="5.8039" y2="3.0861" layer="94"/>
<rectangle x1="5.86231875" y1="3.07771875" x2="5.89788125" y2="3.0861" layer="94"/>
<rectangle x1="6.5151" y1="3.07771875" x2="6.76148125" y2="3.0861" layer="94"/>
<rectangle x1="7.4295" y1="3.07771875" x2="7.74191875" y2="3.0861" layer="94"/>
<rectangle x1="1.74751875" y1="3.0861" x2="2.06248125" y2="3.09448125" layer="94"/>
<rectangle x1="2.73811875" y1="3.0861" x2="2.97688125" y2="3.09448125" layer="94"/>
<rectangle x1="3.60171875" y1="3.0861" x2="3.65251875" y2="3.09448125" layer="94"/>
<rectangle x1="3.71348125" y1="3.0861" x2="3.75411875" y2="3.09448125" layer="94"/>
<rectangle x1="3.8227" y1="3.0861" x2="3.85571875" y2="3.09448125" layer="94"/>
<rectangle x1="4.4831" y1="3.0861" x2="4.54151875" y2="3.09448125" layer="94"/>
<rectangle x1="4.5593" y1="3.0861" x2="4.6609" y2="3.09448125" layer="94"/>
<rectangle x1="4.67868125" y1="3.0861" x2="4.70408125" y2="3.09448125" layer="94"/>
<rectangle x1="4.72948125" y1="3.0861" x2="4.99871875" y2="3.09448125" layer="94"/>
<rectangle x1="5.0165" y1="3.0861" x2="5.05968125" y2="3.09448125" layer="94"/>
<rectangle x1="5.64388125" y1="3.0861" x2="5.69468125" y2="3.09448125" layer="94"/>
<rectangle x1="5.74548125" y1="3.0861" x2="5.79628125" y2="3.09448125" layer="94"/>
<rectangle x1="5.86231875" y1="3.0861" x2="5.89788125" y2="3.09448125" layer="94"/>
<rectangle x1="6.5151" y1="3.0861" x2="6.76148125" y2="3.09448125" layer="94"/>
<rectangle x1="7.4295" y1="3.0861" x2="7.74191875" y2="3.09448125" layer="94"/>
<rectangle x1="1.74751875" y1="3.09448125" x2="2.06248125" y2="3.10311875" layer="94"/>
<rectangle x1="2.73811875" y1="3.09448125" x2="2.97688125" y2="3.10311875" layer="94"/>
<rectangle x1="3.60171875" y1="3.09448125" x2="3.65251875" y2="3.10311875" layer="94"/>
<rectangle x1="3.70331875" y1="3.09448125" x2="3.7465" y2="3.10311875" layer="94"/>
<rectangle x1="3.8227" y1="3.09448125" x2="3.85571875" y2="3.10311875" layer="94"/>
<rectangle x1="4.4831" y1="3.09448125" x2="4.54151875" y2="3.10311875" layer="94"/>
<rectangle x1="4.5593" y1="3.09448125" x2="4.6609" y2="3.10311875" layer="94"/>
<rectangle x1="4.67868125" y1="3.09448125" x2="4.70408125" y2="3.10311875" layer="94"/>
<rectangle x1="4.72948125" y1="3.09448125" x2="4.99871875" y2="3.10311875" layer="94"/>
<rectangle x1="5.0165" y1="3.09448125" x2="5.05968125" y2="3.10311875" layer="94"/>
<rectangle x1="5.64388125" y1="3.09448125" x2="5.69468125" y2="3.10311875" layer="94"/>
<rectangle x1="5.74548125" y1="3.09448125" x2="5.7785" y2="3.10311875" layer="94"/>
<rectangle x1="5.86231875" y1="3.09448125" x2="5.89788125" y2="3.10311875" layer="94"/>
<rectangle x1="6.5151" y1="3.09448125" x2="6.76148125" y2="3.10311875" layer="94"/>
<rectangle x1="7.4295" y1="3.09448125" x2="7.74191875" y2="3.10311875" layer="94"/>
<rectangle x1="1.74751875" y1="3.10311875" x2="2.06248125" y2="3.1115" layer="94"/>
<rectangle x1="2.73811875" y1="3.10311875" x2="2.97688125" y2="3.1115" layer="94"/>
<rectangle x1="3.60171875" y1="3.10311875" x2="3.65251875" y2="3.1115" layer="94"/>
<rectangle x1="3.6957" y1="3.10311875" x2="3.72871875" y2="3.1115" layer="94"/>
<rectangle x1="3.8227" y1="3.10311875" x2="3.85571875" y2="3.1115" layer="94"/>
<rectangle x1="4.4831" y1="3.10311875" x2="4.54151875" y2="3.1115" layer="94"/>
<rectangle x1="4.5593" y1="3.10311875" x2="4.6609" y2="3.1115" layer="94"/>
<rectangle x1="4.67868125" y1="3.10311875" x2="4.70408125" y2="3.1115" layer="94"/>
<rectangle x1="4.72948125" y1="3.10311875" x2="4.99871875" y2="3.1115" layer="94"/>
<rectangle x1="5.0165" y1="3.10311875" x2="5.05968125" y2="3.1115" layer="94"/>
<rectangle x1="5.64388125" y1="3.10311875" x2="5.69468125" y2="3.1115" layer="94"/>
<rectangle x1="5.73531875" y1="3.10311875" x2="5.77088125" y2="3.1115" layer="94"/>
<rectangle x1="5.86231875" y1="3.10311875" x2="5.89788125" y2="3.1115" layer="94"/>
<rectangle x1="6.5151" y1="3.10311875" x2="6.76148125" y2="3.1115" layer="94"/>
<rectangle x1="7.4295" y1="3.10311875" x2="7.74191875" y2="3.1115" layer="94"/>
<rectangle x1="1.74751875" y1="3.1115" x2="2.06248125" y2="3.11988125" layer="94"/>
<rectangle x1="2.73811875" y1="3.1115" x2="2.97688125" y2="3.11988125" layer="94"/>
<rectangle x1="3.60171875" y1="3.1115" x2="3.65251875" y2="3.11988125" layer="94"/>
<rectangle x1="3.68808125" y1="3.1115" x2="3.7211" y2="3.11988125" layer="94"/>
<rectangle x1="3.8227" y1="3.1115" x2="3.85571875" y2="3.11988125" layer="94"/>
<rectangle x1="4.4831" y1="3.1115" x2="4.54151875" y2="3.11988125" layer="94"/>
<rectangle x1="4.5593" y1="3.1115" x2="4.6609" y2="3.11988125" layer="94"/>
<rectangle x1="4.67868125" y1="3.1115" x2="4.70408125" y2="3.11988125" layer="94"/>
<rectangle x1="4.72948125" y1="3.1115" x2="4.99871875" y2="3.11988125" layer="94"/>
<rectangle x1="5.0165" y1="3.1115" x2="5.05968125" y2="3.11988125" layer="94"/>
<rectangle x1="5.64388125" y1="3.1115" x2="5.69468125" y2="3.11988125" layer="94"/>
<rectangle x1="5.72008125" y1="3.1115" x2="5.7531" y2="3.11988125" layer="94"/>
<rectangle x1="5.86231875" y1="3.1115" x2="5.89788125" y2="3.11988125" layer="94"/>
<rectangle x1="6.5151" y1="3.1115" x2="6.76148125" y2="3.11988125" layer="94"/>
<rectangle x1="7.4295" y1="3.1115" x2="7.74191875" y2="3.11988125" layer="94"/>
<rectangle x1="1.74751875" y1="3.11988125" x2="2.06248125" y2="3.12851875" layer="94"/>
<rectangle x1="2.73811875" y1="3.11988125" x2="2.97688125" y2="3.12851875" layer="94"/>
<rectangle x1="3.60171875" y1="3.11988125" x2="3.85571875" y2="3.12851875" layer="94"/>
<rectangle x1="4.4831" y1="3.11988125" x2="4.54151875" y2="3.12851875" layer="94"/>
<rectangle x1="4.5593" y1="3.11988125" x2="4.6609" y2="3.12851875" layer="94"/>
<rectangle x1="4.67868125" y1="3.11988125" x2="4.70408125" y2="3.12851875" layer="94"/>
<rectangle x1="4.72948125" y1="3.11988125" x2="4.99871875" y2="3.12851875" layer="94"/>
<rectangle x1="5.0165" y1="3.11988125" x2="5.05968125" y2="3.12851875" layer="94"/>
<rectangle x1="5.64388125" y1="3.11988125" x2="5.89788125" y2="3.12851875" layer="94"/>
<rectangle x1="6.5151" y1="3.11988125" x2="6.76148125" y2="3.12851875" layer="94"/>
<rectangle x1="7.4295" y1="3.11988125" x2="7.74191875" y2="3.12851875" layer="94"/>
<rectangle x1="1.74751875" y1="3.12851875" x2="2.06248125" y2="3.1369" layer="94"/>
<rectangle x1="2.73811875" y1="3.12851875" x2="2.97688125" y2="3.1369" layer="94"/>
<rectangle x1="3.60171875" y1="3.12851875" x2="3.85571875" y2="3.1369" layer="94"/>
<rectangle x1="5.64388125" y1="3.12851875" x2="5.89788125" y2="3.1369" layer="94"/>
<rectangle x1="6.5151" y1="3.12851875" x2="6.76148125" y2="3.1369" layer="94"/>
<rectangle x1="7.4295" y1="3.12851875" x2="7.74191875" y2="3.1369" layer="94"/>
<rectangle x1="1.74751875" y1="3.1369" x2="2.06248125" y2="3.14528125" layer="94"/>
<rectangle x1="2.73811875" y1="3.1369" x2="2.97688125" y2="3.14528125" layer="94"/>
<rectangle x1="3.60171875" y1="3.1369" x2="3.85571875" y2="3.14528125" layer="94"/>
<rectangle x1="5.64388125" y1="3.1369" x2="5.89788125" y2="3.14528125" layer="94"/>
<rectangle x1="6.5151" y1="3.1369" x2="6.76148125" y2="3.14528125" layer="94"/>
<rectangle x1="7.4295" y1="3.1369" x2="7.74191875" y2="3.14528125" layer="94"/>
<rectangle x1="1.74751875" y1="3.14528125" x2="2.06248125" y2="3.15391875" layer="94"/>
<rectangle x1="2.73811875" y1="3.14528125" x2="2.97688125" y2="3.15391875" layer="94"/>
<rectangle x1="3.60171875" y1="3.14528125" x2="3.85571875" y2="3.15391875" layer="94"/>
<rectangle x1="5.64388125" y1="3.14528125" x2="5.89788125" y2="3.15391875" layer="94"/>
<rectangle x1="6.5151" y1="3.14528125" x2="6.76148125" y2="3.15391875" layer="94"/>
<rectangle x1="7.4295" y1="3.14528125" x2="7.74191875" y2="3.15391875" layer="94"/>
<rectangle x1="1.74751875" y1="3.15391875" x2="2.06248125" y2="3.1623" layer="94"/>
<rectangle x1="2.73811875" y1="3.15391875" x2="2.97688125" y2="3.1623" layer="94"/>
<rectangle x1="3.60171875" y1="3.15391875" x2="3.85571875" y2="3.1623" layer="94"/>
<rectangle x1="5.64388125" y1="3.15391875" x2="5.89788125" y2="3.1623" layer="94"/>
<rectangle x1="6.5151" y1="3.15391875" x2="6.76148125" y2="3.1623" layer="94"/>
<rectangle x1="7.4295" y1="3.15391875" x2="7.74191875" y2="3.1623" layer="94"/>
<rectangle x1="1.74751875" y1="3.1623" x2="2.06248125" y2="3.17068125" layer="94"/>
<rectangle x1="3.5941" y1="3.1623" x2="3.85571875" y2="3.17068125" layer="94"/>
<rectangle x1="5.64388125" y1="3.1623" x2="5.89788125" y2="3.17068125" layer="94"/>
<rectangle x1="7.4295" y1="3.1623" x2="7.74191875" y2="3.17068125" layer="94"/>
<rectangle x1="1.74751875" y1="3.17068125" x2="2.06248125" y2="3.17931875" layer="94"/>
<rectangle x1="3.5941" y1="3.17068125" x2="3.85571875" y2="3.17931875" layer="94"/>
<rectangle x1="5.64388125" y1="3.17068125" x2="5.89788125" y2="3.17931875" layer="94"/>
<rectangle x1="7.4295" y1="3.17068125" x2="7.74191875" y2="3.17931875" layer="94"/>
<rectangle x1="1.74751875" y1="3.17931875" x2="2.06248125" y2="3.1877" layer="94"/>
<rectangle x1="3.60171875" y1="3.17931875" x2="3.85571875" y2="3.1877" layer="94"/>
<rectangle x1="5.63371875" y1="3.17931875" x2="5.89788125" y2="3.1877" layer="94"/>
<rectangle x1="7.4295" y1="3.17931875" x2="7.74191875" y2="3.1877" layer="94"/>
<rectangle x1="1.74751875" y1="3.1877" x2="2.06248125" y2="3.19608125" layer="94"/>
<rectangle x1="3.60171875" y1="3.1877" x2="3.85571875" y2="3.19608125" layer="94"/>
<rectangle x1="5.64388125" y1="3.1877" x2="5.88771875" y2="3.19608125" layer="94"/>
<rectangle x1="7.4295" y1="3.1877" x2="7.74191875" y2="3.19608125" layer="94"/>
<rectangle x1="1.74751875" y1="3.19608125" x2="2.06248125" y2="3.20471875" layer="94"/>
<rectangle x1="3.6195" y1="3.19608125" x2="3.84048125" y2="3.20471875" layer="94"/>
<rectangle x1="5.6515" y1="3.19608125" x2="5.8801" y2="3.20471875" layer="94"/>
<rectangle x1="7.4295" y1="3.19608125" x2="7.74191875" y2="3.20471875" layer="94"/>
<rectangle x1="1.74751875" y1="3.20471875" x2="2.06248125" y2="3.2131" layer="94"/>
<rectangle x1="3.63728125" y1="3.20471875" x2="3.8227" y2="3.2131" layer="94"/>
<rectangle x1="5.66928125" y1="3.20471875" x2="5.86231875" y2="3.2131" layer="94"/>
<rectangle x1="7.4295" y1="3.20471875" x2="7.74191875" y2="3.2131" layer="94"/>
<rectangle x1="1.74751875" y1="3.2131" x2="2.06248125" y2="3.22148125" layer="94"/>
<rectangle x1="3.66268125" y1="3.2131" x2="3.7973" y2="3.22148125" layer="94"/>
<rectangle x1="5.69468125" y1="3.2131" x2="5.83691875" y2="3.22148125" layer="94"/>
<rectangle x1="7.4295" y1="3.2131" x2="7.74191875" y2="3.22148125" layer="94"/>
<rectangle x1="1.74751875" y1="3.22148125" x2="2.06248125" y2="3.23011875" layer="94"/>
<rectangle x1="7.4295" y1="3.22148125" x2="7.74191875" y2="3.23011875" layer="94"/>
<rectangle x1="1.74751875" y1="3.23011875" x2="2.06248125" y2="3.2385" layer="94"/>
<rectangle x1="7.4295" y1="3.23011875" x2="7.74191875" y2="3.2385" layer="94"/>
<rectangle x1="1.74751875" y1="3.2385" x2="2.06248125" y2="3.24688125" layer="94"/>
<rectangle x1="7.4295" y1="3.2385" x2="7.74191875" y2="3.24688125" layer="94"/>
<rectangle x1="1.74751875" y1="3.24688125" x2="2.06248125" y2="3.25551875" layer="94"/>
<rectangle x1="7.4295" y1="3.24688125" x2="7.74191875" y2="3.25551875" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LOGO-9.5MMX4.5MM">
<gates>
<gate name="G$1" symbol="LOGO-9.5MMX4.5MM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LOGO-9.5MMX4.5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-power">
<description>&lt;b&gt;Power Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TO220BH">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 2.54 mm</description>
<wire x1="-5.207" y1="-1.27" x2="5.207" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.207" y1="14.605" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-1.27" x2="5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="5.207" y1="11.176" x2="4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="4.318" y1="11.176" x2="4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="4.318" y1="12.7" x2="5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="5.207" y1="12.7" x2="5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="11.176" x2="-4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="11.176" x2="-4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="12.7" x2="-5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="12.7" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<circle x="0" y="11.176" radius="1.8034" width="0.1524" layer="21"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="42"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="43"/>
<pad name="G" x="-2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="D" x="0" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="S" x="2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<text x="-3.81" y="5.207" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.937" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.445" y="7.874" size="1.016" layer="21" ratio="10">A17,5mm</text>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-4.064" layer="21"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-4.064" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-4.064" layer="21"/>
<rectangle x1="-3.175" y1="-4.064" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-0.635" y1="-4.064" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-4.064" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="-2.921" y1="-6.604" x2="-2.159" y2="-4.699" layer="51"/>
<rectangle x1="-0.381" y1="-6.604" x2="0.381" y2="-4.699" layer="51"/>
<rectangle x1="2.159" y1="-6.604" x2="2.921" y2="-4.699" layer="51"/>
<hole x="0" y="11.176" drill="3.302"/>
</package>
<package name="TO220BV">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 2.54 mm</description>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="G" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="D" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="S" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MFP">
<wire x1="-1.016" y1="2.54" x2="-1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.5334" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.2352" y1="0" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.508" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="1.016" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.508" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0.254" x2="2.032" y2="0" width="0.3048" layer="94"/>
<wire x1="2.032" y1="0" x2="1.143" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.143" y1="-0.254" x2="1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="1.143" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.397" y="-3.175" size="0.8128" layer="93">D</text>
<text x="1.397" y="2.413" size="0.8128" layer="93">S</text>
<text x="-2.54" y="1.397" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PMOSFET_P" prefix="Q" uservalue="yes">
<description>&lt;b&gt;Power MOSFET P-Channel&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MFP" x="0" y="0"/>
</gates>
<devices>
<device name="TO220BH" package="TO220BH">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO220BV" package="TO220BV">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="Q1" library="adafruit" deviceset="MOSFET-N" device="GDS_TO220V"/>
<part name="X1" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="JP1" library="pinhead" deviceset="PINHD-1X3" device=""/>
<part name="KK1" library="heatsink" deviceset="SK129-PAD" device=""/>
<part name="KK5" library="heatsink" deviceset="D03PA-PAD" device=""/>
<part name="J1" library="SparkFun-Jumpers" deviceset="JUMPER-SMT_3_NO" device="_SILK"/>
<part name="GND1" library="ER" deviceset="GND" device=""/>
<part name="P+1" library="ER" deviceset="VCC" device=""/>
<part name="JP4" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="U$3" library="ER" deviceset="LOGO-9.5MMX4.5MM" device=""/>
<part name="KK2" library="heatsink" deviceset="SK129-PAD" device=""/>
<part name="KK6" library="heatsink" deviceset="D03PA-PAD" device=""/>
<part name="Q2" library="transistor-power" deviceset="PMOSFET_P" device="TO220BV"/>
<part name="X2" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="JP2" library="pinhead" deviceset="PINHD-1X2" device=""/>
<part name="KK3" library="heatsink" deviceset="FK222" device=""/>
<part name="KK4" library="heatsink" deviceset="FK222" device=""/>
<part name="R2" library="rcl" deviceset="R-US_" device="0207/2V" value="R1_U/D"/>
<part name="R4" library="rcl" deviceset="R-US_" device="0207/2V" value="R1_G"/>
<part name="JP3" library="pinhead" deviceset="PINHD-1X3" device=""/>
<part name="R1" library="rcl" deviceset="R-US_" device="0207/2V" value="R_G"/>
<part name="J2" library="SparkFun-Jumpers" deviceset="JUMPER-SMT_3_NO" device="_SILK"/>
<part name="GND2" library="ER" deviceset="GND" device=""/>
<part name="P+2" library="ER" deviceset="VCC" device=""/>
<part name="R3" library="rcl" deviceset="R-US_" device="0207/2V" value="R_U/D"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="0" y1="0" x2="195.58" y2="0" width="0.1524" layer="115"/>
<wire x1="195.58" y1="0" x2="195.58" y2="50.8" width="0.1524" layer="115"/>
<wire x1="195.58" y1="50.8" x2="0" y2="50.8" width="0.1524" layer="115"/>
<wire x1="0" y1="50.8" x2="0" y2="0" width="0.1524" layer="115"/>
</plain>
<instances>
<instance part="Q1" gate="G$1" x="15.24" y="17.78" smashed="yes">
<attribute name="NAME" x="20.32" y="18.415" size="1.27" layer="95"/>
<attribute name="VALUE" x="20.32" y="16.51" size="1.27" layer="96"/>
</instance>
<instance part="X1" gate="-1" x="63.246" y="29.21" smashed="yes">
<attribute name="NAME" x="63.246" y="30.099" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="X1" gate="-2" x="63.246" y="24.13" smashed="yes">
<attribute name="VALUE" x="60.706" y="20.447" size="1.778" layer="96"/>
<attribute name="NAME" x="63.246" y="25.019" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP1" gate="A" x="109.22" y="10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="115.57" y="4.445" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="115.57" y="17.78" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="KK1" gate="G$1" x="20.32" y="43.18" smashed="yes">
<attribute name="NAME" x="26.035" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="26.035" y="43.815" size="1.778" layer="96"/>
</instance>
<instance part="KK5" gate="G$1" x="76.2" y="43.18" smashed="yes">
<attribute name="NAME" x="81.915" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="81.915" y="43.815" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="144.78" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="142.748" y="31.369" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="144.78" y="22.86" smashed="yes">
<attribute name="VALUE" x="142.24" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="VCC" x="144.78" y="38.1" smashed="yes">
<attribute name="VALUE" x="143.51" y="38.862" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP4" gate="G$1" x="83.82" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="87.63" y="32.385" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="90.17" y="33.02" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U$3" gate="G$1" x="184.15" y="0.762" smashed="yes"/>
<instance part="KK2" gate="G$1" x="116.84" y="43.18" smashed="yes">
<attribute name="NAME" x="122.555" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="122.555" y="43.815" size="1.778" layer="96"/>
</instance>
<instance part="KK6" gate="G$1" x="172.72" y="43.18" smashed="yes">
<attribute name="NAME" x="178.435" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="178.435" y="43.815" size="1.778" layer="96"/>
</instance>
<instance part="Q2" gate="G$1" x="40.64" y="17.78" smashed="yes">
<attribute name="NAME" x="45.72" y="20.32" size="1.778" layer="95"/>
<attribute name="VALUE" x="45.72" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="X2" gate="-1" x="63.246" y="13.97" smashed="yes">
<attribute name="NAME" x="63.246" y="14.859" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="X2" gate="-2" x="63.246" y="8.89" smashed="yes">
<attribute name="VALUE" x="60.706" y="5.207" size="1.778" layer="96"/>
<attribute name="NAME" x="63.246" y="9.779" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP2" gate="G$1" x="83.82" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="87.63" y="17.145" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="90.17" y="17.78" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="KK3" gate="G$1" x="50.8" y="43.18" smashed="yes">
<attribute name="NAME" x="56.515" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="56.515" y="43.815" size="1.778" layer="96"/>
</instance>
<instance part="KK4" gate="G$1" x="149.86" y="43.18" smashed="yes">
<attribute name="NAME" x="155.575" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="155.575" y="43.815" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="154.94" y="30.48" smashed="yes">
<attribute name="NAME" x="153.416" y="32.4866" size="1.778" layer="95"/>
<attribute name="VALUE" x="150.114" y="34.798" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="165.1" y="30.48" smashed="yes">
<attribute name="NAME" x="163.322" y="32.2326" size="1.778" layer="95"/>
<attribute name="VALUE" x="161.798" y="34.798" size="1.778" layer="96"/>
</instance>
<instance part="JP3" gate="A" x="109.22" y="25.4" smashed="yes" rot="R180">
<attribute name="NAME" x="115.57" y="19.685" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="115.57" y="33.02" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R1" gate="G$1" x="165.1" y="10.16" smashed="yes">
<attribute name="NAME" x="164.084" y="11.9126" size="1.778" layer="95"/>
<attribute name="VALUE" x="163.322" y="14.478" size="1.778" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="144.78" y="10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="142.748" y="11.049" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND2" gate="1" x="144.78" y="2.54" smashed="yes">
<attribute name="VALUE" x="142.24" y="0" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="VCC" x="144.78" y="17.78" smashed="yes">
<attribute name="VALUE" x="143.764" y="18.034" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="154.94" y="10.16" smashed="yes">
<attribute name="NAME" x="153.67" y="11.4046" size="1.778" layer="95"/>
<attribute name="VALUE" x="151.638" y="14.224" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="S" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="10.16" width="0.1524" layer="91"/>
<label x="17.78" y="10.16" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="-1" pin="KL"/>
<wire x1="68.326" y1="13.97" x2="70.866" y2="13.97" width="0.1524" layer="91"/>
<label x="70.866" y="13.97" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<wire x1="86.36" y1="12.7" x2="93.98" y2="12.7" width="0.1524" layer="91"/>
<label x="93.98" y="12.7" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="D" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="17.78" y1="22.86" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<label x="17.78" y="27.94" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="-2" pin="KL"/>
<wire x1="68.326" y1="8.89" x2="70.866" y2="8.89" width="0.1524" layer="91"/>
<label x="70.866" y="8.89" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="86.36" y1="10.16" x2="93.98" y2="10.16" width="0.1524" layer="91"/>
<label x="93.98" y="10.16" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
</net>
<net name="GATE" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="12.7" y1="15.24" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<label x="10.16" y="15.24" size="1.27" layer="95" font="vector" ratio="10" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="170.18" y1="10.16" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<label x="172.72" y="10.16" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CONTROL" class="0">
<segment>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="111.76" y1="12.7" x2="116.84" y2="12.7" width="0.1524" layer="91"/>
<label x="116.84" y="12.7" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<label x="160.02" y="5.08" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="160.02" y1="10.16" x2="160.02" y2="5.08" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<junction x="160.02" y="10.16"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="111.76" y1="7.62" x2="116.84" y2="7.62" width="0.1524" layer="91"/>
<label x="116.84" y="7.62" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="1"/>
<wire x1="111.76" y1="22.86" x2="116.84" y2="22.86" width="0.1524" layer="91"/>
<label x="116.84" y="22.86" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="111.76" y1="10.16" x2="116.84" y2="10.16" width="0.1524" layer="91"/>
<label x="116.84" y="10.16" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="2"/>
<wire x1="111.76" y1="25.4" x2="116.84" y2="25.4" width="0.1524" layer="91"/>
<label x="116.84" y="25.4" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="43.18" y1="22.86" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<label x="43.18" y="25.4" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-1" pin="KL"/>
<wire x1="68.326" y1="29.21" x2="70.866" y2="29.21" width="0.1524" layer="91"/>
<label x="70.866" y="29.21" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<wire x1="86.36" y1="27.94" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<label x="93.98" y="27.94" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="S1" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="43.18" y1="12.7" x2="43.18" y2="10.16" width="0.1524" layer="91"/>
<label x="43.18" y="10.16" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-2" pin="KL"/>
<wire x1="68.326" y1="24.13" x2="70.866" y2="24.13" width="0.1524" layer="91"/>
<label x="70.866" y="24.13" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="86.36" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<label x="93.98" y="25.4" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CONTROL1" class="0">
<segment>
<label x="160.02" y="25.4" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="160.02" y1="30.48" x2="160.02" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<junction x="160.02" y="30.48"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="3"/>
<wire x1="111.76" y1="27.94" x2="116.84" y2="27.94" width="0.1524" layer="91"/>
<label x="116.84" y="27.94" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
</segment>
</net>
<net name="GATE1" class="0">
<segment>
<wire x1="170.18" y1="30.48" x2="172.72" y2="30.48" width="0.1524" layer="91"/>
<label x="172.72" y="30.48" size="1.27" layer="95" font="vector" ratio="10" xref="yes"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="35.56" y1="20.32" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
<label x="33.02" y="20.32" size="1.27" layer="95" font="vector" ratio="10" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
