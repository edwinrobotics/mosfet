# [MOSFET Breakout Board PCB](https://shop.edwinrobotics.com/breakout-boards/600-mosfet-breakout-board-pcb.html) #

![MOSFET-Breakout Board.JPG](https://bitbucket.org/repo/koeyXy/images/3042699558-MOSFET-Breakout%20Board.JPG)

This is a breakout board from Edwin Robotics, suitable for N Channel and P channel MOSFET, the board is designed to use two different types of MOSFET at the same time. The board comes with standard layout for MOSFET and you are able to connect high power circuits via screw terminals to this board, the board comes with 5mm Screw terminal slot and jumper headers to control Gate pin via microcontrollers.

The board is compatible to use with any voltage levels that your MOSFET supports thus giving you a perfect option to interface Arduino and Raspberry pi with inductive loads like motors, pump, etc. You can use any MOSFET on this board.The board is designed to use with T0-220 through hole Package MOSFET, which is easily available in the market.

**FEATURES:**

* Support N-Channel and P-Channel MOSFET
* Heat Sink Slot available
* Supports T0-220 Package

**SPECIFICATION:**

* Logic Interface: VCC, GND, Control
* Dimensions: 33 x 30.5mm

**DOCUMENTS:**

[Hookup Guide](http://learn.edwinrobotics.com/mosfet-breakout-board-hookup-guide/)